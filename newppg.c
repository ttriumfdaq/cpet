/*********************************************************************

  Name:         newppg.c
  Created by:   
        based on v292.c and trPPG.c Pierre-Andre Amaudruz

  Contents:     PPG Pulse Programmer
                
  $Id: tppg.c 3638 2007-03-02 01:35:35Z amaudruz $
*********************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>     /* gmtime(),strftime() */
#include <ctype.h>
#include "vmicvme.h"
#include "unistd.h" // for sleep
#include "common.h"
#include "newppg.h"

int ddd=0; //debug


FILE  *ppgld;
int  num_instructions=0; // number of instructions from last loaded ppg file
int  start_pc=0; // pc for start of last loaded ppg file
#define FALSE 0
#define TRUE 1

#define HALT 0
#define DELAY 1
#define LOOP 2
#define ENDLOOP 3


/*****************************************************************/

/*------------------------------------------------------------------*/

void ppg(void)
{
   printf("       TRIUMF PPG function support\n");
   printf("A Init               B Load default file\n");
   printf("C StopSequencer      E StartSequencer\n");
   printf("F EnableExtTrig      G DisableExtTrig\n");
   printf("                     I StatusRead\n");
   printf("J PolMaskRead        K PolMaskWrite\n");
   printf("L RegWrite           M RegRead\n");
   printf("N ToggleClkSource       O ReadIns\n");
   printf("R ToggleClkSource AND load divide-downs  \n");
   printf("S Reset pgm          T Load a file \n");
   printf("U EnableTestMode     V DisableTestMode\n");
   printf("W SetPgmStartAddr    Y ReadPgmStartAddr\n");
   printf("D debug (toggles)    P Print this list\n");
   printf("Z zero outputs       X exit \n");
   printf("\n");
}

/*------------------------------------------------------------------*/
/** TPPGRegRead
    Read TRIUMF PPG register.
    @memo Read PPG.
    @param base\_adr PPG VME base addroless
    @param reg\_offset PPG register
    @return register (32 bit)
*/
DWORD TPPGRegRead(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD reg_offset)
{
  DWORD value,myreg;
  int  cmode, localam;
  
    mvme_get_dmode(mvme, &cmode); // store present data mode
    mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32 

    mvme_get_am(mvme, &localam); // store present addressing mode
    mvme_set_am(mvme, MVME_AM_A32_ND); // set A32 

  myreg =  base_adr + reg_offset;
  if (ddd) printf("Reading from address 0x%x\n", myreg );
  value = mvme_read_value(mvme, myreg );
  if(ddd) printf("Read back 0x%x (%u) (%08x)\n",value,value,(int)value);

   mvme_set_dmode(mvme, cmode);// restore data mode
   mvme_set_am(mvme, localam);// restore addressing mode
  return  value;
}


/*------------------------------------------------------------------*/
/** TPPGRegWrite
    Write into TRIUMF PPG register.
    @memo Write TPPG.
    @param base\_adr TPPG VME base address
    @param reg\_offset TPPG register
    @param value (8bit)
    @return status register
*/
DWORD TPPGRegWrite(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD reg_offset, DWORD value)
{

  DWORD myval,myreg;
  int  cmode, localam;
  
  mvme_get_dmode(mvme, &cmode);  // remember present VME data mode
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32

  mvme_get_am(mvme, &localam); // remember present VME addressing mode
  mvme_set_am(mvme, MVME_AM_A32_ND); // set A32

  myreg =  base_adr + reg_offset;
  myval = (DWORD)value;
  mvme_write_value(mvme, myreg, value);
  if (ddd) printf("Writing reg 0x%x value 0x%x\n", myreg,value);
  myval = 0x1BAD1BAD; // set to a different value in case of failure
  myval = mvme_read_value(mvme, myreg);
  // testing ppgload - read always returns zero
  //   if(ddd)printf("mvme_read_value returns 0x%x (%u)\n",myval,myval);

  mvme_set_dmode(mvme, cmode); // restore VME data mode
  mvme_set_am(mvme, localam); // restore VME addressing mode

  return myval;
}



/*------------------------------------------------------------------*/
/** TPPGReadIns
    Read back a complete instruction
    @memo Read an instruction
    @param base\_adr PPG VME base address
    @param address  Address to read instruction
    @return register (32 bit)
*/
COMMAND TPPGReadIns(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD addr)
{
  DWORD data, tmp;
  DWORD setbits,clrbits;
  DWORD  ins,delay_count;
  int  cmode, localam;

  COMMAND read_info;

  if (addr < 0 || addr >128)
      printf ("TPPGReadIns: Warning - illegal address for instruction (%d)\n",(int)addr);


  mvme_get_dmode(mvme, &cmode); // store present data mode
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32 

  mvme_get_am(mvme, &localam); // store present addressing mode
  mvme_set_am(mvme, MVME_AM_A32_ND); // set A32 

  data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , addr); // write instruction address 
  if(ddd)printf("TPPGReadIns: after writing instruction addr %d,  data = %d\n",(int)addr,(int)data);

  setbits=0;
  setbits = TPPGRegRead(mvme, base_adr,  TPPG_DATA_LO ); // read "set" bits (32 bits)
  if(ddd)printf("TPPGReadIns: after reading data lo (set bits) = 0x%x\n",setbits);
  
  clrbits=0;
  clrbits = TPPGRegRead(mvme, base_adr,  TPPG_DATA_MED );// read "clear" bits (32 bits)
  if(ddd)printf("TPPGReadIns: after reading data med (clear bits) = 0x%x\n", clrbits);

  delay_count=0;
  delay_count = TPPGRegRead(mvme, base_adr,  TPPG_DATA_HI );// read delay count (32 bits)
  if(ddd)printf("TPPGReadIns: after reading data hi (delay count) = 0x%x\n", delay_count);
  
  data = 0;
  data = TPPGRegRead(mvme, base_adr,  TPPG_DATA_TOP  );// instruction and data 
  if(ddd)printf("TPPGReadIns: after reading data top = 0x%x\n",data);
  ins = data & 0xFFFFF;
  ins = ((ins >> 20) & 7);
  if (ins > 6)
      printf ("TPPGReadIns: Warning - illegal instruction (%d)\n",(int)ins);
  tmp = data & 0xFFFF; // mask away instruction
   
  printf ("TPPGReadIns: InstrAddr:%d SetBitpat:0x%x ClrBitpat:0x%x DelayCount:%u Instruction %s (%d) Instruction data %u\n",
	  (int)addr,setbits,clrbits,delay_count,instructions[ins],(int)ins,tmp );
	  


  mvme_set_dmode(mvme, cmode); // restore VME data mode
  mvme_set_am(mvme, localam); // restore VME addressing mode


  read_info.pc = addr;
  read_info.setpat = setbits ;
  read_info.clrpat = clrbits;
  read_info.delay = delay_count;
  read_info.ins_data = data;
  return read_info; // success
}




/*------------------------------------------------------------------*/
/** TPPGInit
    Initialize the TRIUMF PPG
    @memo Initialize PPG
    @param base\_adr PPG VME base address
    @return void
*/
void TPPGInit(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD bitpat)
{
  DWORD zero=0;
  TPPGReset(mvme,base_adr); // reset stops PPG even if in long delay
  TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , zero); // clear all bits in CSR0 (running,reset,ext clk, ext trig etc)
  TPPGPolmskWrite(mvme, base_adr, zero); // clear polarity
  TPPGZeroAll(mvme, base_adr ); // zero all outputs

  // Set instruction address back 0
  TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , zero ); // write start instruction address 
  TPPGStatusRead(mvme, base_adr);
  return;
}

/*------------------------------------------------------------------*/
/** TPPGStatusRead
    Read Status register.
    @memo Read status.
    @param base\_adr PPG VME base address
    @return status register
*/
DWORD TPPGStatusRead(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,temp;
  char str[256];

  data = TPPGRegRead(mvme, base_adr,  TPPG_CSR_REG );
  //if (ddd)
  printf("TPPGStatusRead: status = 0x%x\n\n",data);
  if (data & 1)    
    sprintf(str,"PPG running: Y ");
  else
    sprintf(str,"PPG running: N ");

  if (data & 0x10000)
    strcat(str,"External clock : Y ");
  else
    strcat(str,"External clock : N ");
  
  if (data  & 0x20000)
    strcat(str," (Good); ");
  else
    strcat(str," (Bad);  ");
   
  if (data & 4)
    strcat(str,"External trigger: Y ");
  else
    strcat(str,"External trigger: N ");

  if (data & 8)
    strcat(str,"Reset bit : Y ");
  else
    strcat(str,"Reset bit : N ");

  if (data & 0x10)
    strcat(str,"Test Mode: Y ");
  else
    strcat(str,"Test Mode: N \n");

  printf("%s\n",str);
 
  //temp = (data & TPPG_STATUS_READBACK_MASK) >> 5 ; // mask away all but status readback bits (5-31)
  // printf("PC,SP, Current Delay counter: 0x%x\n\n",(unsigned int)temp); 
  return data;
}


/*------------------------------------------------------------------*/
/** TPPGStartSequencer
    Start the PPG sequencer (internal trigger) at PC of last loaded program
    @memo start the PPG sequencer.
    @param base\_adr PPG VME base address
    @return void
*/
  void TPPGStartSequencer(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data;
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  data = (data & TPPG_RUN_MASK) | 1;  // mask all but clock,start bits
  if (ddd)printf("TPPGStartSequencer: writing 0x%4.4x to CSR 0x%2.2x \n",(unsigned int) data, TPPG_CSR_REG);
  TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  return;
}

/*------------------------------------------------------------------*/
/** TPPGStopSequencer
    Stop the TRIUMF PPG sequencer.
    @memo Stop the PPG sequencer.
    @param base\_adr PPG VME base address
    @return void
*/
void TPPGStopSequencer(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;

  data1 = TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  data = (data1 & TPPG_RUN_MASK) ;  // 0x1E  mask all control bits except run bit
  //if (ddd)
    printf("TPPGStopSequencer: writing 0x%4.4x to CSR 0 (was 0x%4.4x) \n",
		 (unsigned int) data,(unsigned int) data1 );
  TPPGRegWrite(mvme, base_adr,   TPPG_CSR_REG  , data);
  return;
}

/*------------------------------------------------------------------*/
/** TPPGSetStartPC
    Set the starting PC of the program in the PPG sequencer.
    @memo Set the pgm start PC.
    @param base\_adr PPG VME base address
    @param pc  Address to start program 
    @return void
*/
DWORD TPPGSetStartPC (MVME_INTERFACE *mvme,  const DWORD base_adr, DWORD pc)
{
  DWORD data;
  // Set instruction address back to start of program
  data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , pc ); // write start instruction address 
  return data;
}






void TPPGZeroAll(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  int  cmode, localam;
  char clear[]="000 0x0 0xffffffff  0x4 0x0";
  char halt[] ="000 0x0 0x0  0x0 0x0";
  COMMAND  command_info;
  DWORD data;
  unsigned int pc=0;

  mvme_get_dmode(mvme, &cmode); // store present data mode
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32 
  
  mvme_get_am(mvme, &localam); // store present addressing mode
  mvme_set_am(mvme, MVME_AM_A32_ND); // set A32 
  
  // seem to need two instructions for this i.e. clear then halt
  printf("TPPGZeroAll: Writing clear instruction at pc=%d\n",pc);
  
  command_info = lineRead(clear);
  
  //if(ddd)
    {
      printf("Clear instruction:\n");
      printf("pc:%3.3d ",pc);
      printf("set bitpat:%8.8x ",command_info.setpat);
      printf("clr bitpat:%8.8x ",command_info.clrpat);
      printf("delay:%8.8x ",command_info.delay);
      printf("ins/data:%8.8x\n",command_info.ins_data);
    }

 
  printf("Writing clear instruction to pc %d\n",pc);
  // load this instruction
  command_info.pc = pc;
  data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , command_info.pc ); // write instruction address 
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_LO  , command_info.setpat); // "set" bits (32 bits)
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_MED  ,command_info.clrpat );// "clear" bits (32 bits)
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_HI  , command_info.delay);// delay count
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_TOP  , command_info.ins_data);// instruction and any data
  
  pc++;
  printf("TPPGZeroAll: Writing halt instruction at pc=%d\n",pc);

  command_info = lineRead(halt);
  
  //if(ddd)
    {
      printf("Halt instruction:\n");
      printf("pc:%3.3d ",pc);
      printf("set bitpat:%8.8x ",command_info.setpat);
      printf("clr bitpat:%8.8x ",command_info.clrpat);
      printf("delay:%8.8x ",command_info.delay);
      printf("ins/data:%8.8x\n",command_info.ins_data);
    }

 
  printf("Writing halt instruction to pc %d\n",pc);
  // load this instruction
  command_info.pc = pc;
  data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , command_info.pc ); // write instruction address 
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_LO  , command_info.setpat); // "set" bits (32 bits)
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_MED  ,command_info.clrpat );// "clear" bits (32 bits)
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_HI  , command_info.delay);// delay count
  data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_TOP  , command_info.ins_data);// instruction and any data
  
  // Set instruction address back to start of program i.e. 0
  data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , 0 ); // write start instruction address 
  
  mvme_set_dmode(mvme, cmode); // restore VME data mode
  mvme_set_am(mvme, localam); // restore VME addressing mode
   

  printf("\nNow running program (Clear then HALT instructions) ...\n");
  TPPGStopSequencer(mvme, base_adr); // Start needs an edge
  sleep(0.5) ; // sleep 0.5s
  TPPGStartSequencer(mvme, base_adr);
  sleep(1) ; // sleep 1s
  TPPGStopSequencer(mvme, base_adr); // needs an edge
  TPPGStatusRead(mvme, base_adr);
  return;
}











int TPPGCheckIns (MVME_INTERFACE *mvme, const DWORD base_adr, COMMAND *command_info)
{ // Can only check the last instruction written
  COMMAND read_info;
  int ret_code;

  ret_code =0;

  if(ddd)
    {
      printf("TPPGCheckIns:  Wrote:\n");
      printf("pc:%3.3d ",command_info->pc);
      printf("set bitpat:%8.8x ",command_info->setpat);
      printf("clr bitpat:%8.8x ",command_info->clrpat);
      printf("delay:%8.8x ",command_info->delay);
      printf("ins/data:%8.8x\n",command_info->ins_data);
    }

  read_info = TPPGReadIns(mvme,base_adr,command_info->pc);

  if(ddd)
    {
      printf("TPPGCheckIns:  Read back:\n");
      printf("pc:%3.3d ",read_info.pc);
      printf("set bitpat:%8.8x ",read_info.setpat);
      printf("clr bitpat:%8.8x ",read_info.clrpat);
      printf("delay:%8.8x",read_info.delay);
      printf("ins/data:%8.8x\n",read_info.ins_data);
    }

  if( read_info.pc != command_info->pc)
    {
      printf("TPPGCheckIns : wrote addr as %3.3d, read back %3.3d\n",command_info->pc,read_info.pc);
      ret_code=1;
    }
  
  if( read_info.setpat != command_info->setpat)
    {  
      printf("TPPGCheckIns : wrote setpat as %8.8x, read back %8.8x\n",command_info->setpat,read_info.setpat);
      ret_code=1;
    }
  if( read_info.clrpat != command_info->clrpat)
    {
      printf("TPPGCheckIns : wrote clrpat as %8.8x, read back %8.8x\n",command_info->clrpat,read_info.clrpat);
      ret_code=1;
    }
  if( read_info.delay != command_info->delay)
    {
      printf("TPPGCheckIns : wrote delay as %8.8x, read back %8.8x\n",command_info->delay,read_info.delay);
      ret_code=1;
    }
  if( read_info.ins_data != command_info->ins_data)
    {
      printf("TPPGCheckIns : wrote ins/data as %8.8x, read back %8.8x\n",command_info->ins_data,read_info.ins_data);
      ret_code=1;
    }
  
  return ret_code;
}
/*------------------------------------------------------------------*/
/**  TPPGEnableExtTrig(ppg_base)
    Enable front panel trigger input so external inputs can start the sequence
    @memo Enable external trigger  input so external inputs can start the sequence
    @param base\_adr PPG VME base address
    @return data
*/
int TPPGEnableExtTrig(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  data = (data & TPPG_TRIG_MASK) | 4 ;  // 0x1B mask all but trigger bit (bit 2)
  data1 = TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  data = data1 & 4; // mask away all but trigger bit
  if( data != 4 )
    {
      printf("TPPGEnableExtTrig: Error enabling external trigger; trigger bit is not set (read back 0x%x)\n",data1);
      return FAILURE;
    }
  printf("TPPGEnableExtTrig: ExtTrig selected\n");    
  return SUCCESS;
}



/*------------------------------------------------------------------*/
/**  TPPGDisableExtTrig(ppg_base)
    Disable front panel trigger input so external inputs cannot start the sequence
    @memo Disable external trigger  input so external inputs cannot start the sequence
    @param base\_adr PPG VME base address
    @return data
*/
int TPPGDisableExtTrig(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data = TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  data = (data & TPPG_TRIG_MASK)  ;  // mask away all but clock,run bits
  data1 = TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  data = data1 & 4 ; // mask away all but trigger bit

  if( data != 0 )
    {
      printf("TPPGDisableExtTrig:  Error disabling external trigger; trigger bit is not clear (read back 0x%x)\n",data1);
      return FAILURE;
    }
  printf("TPPGDisableExtTrig: ExtTrig disabled\n");
  return SUCCESS;
}




/*------------------------------------------------------------------*/
/**  TPPGToggleExtClock(ppg_base)
    Enable/Disable front panel clock input 
    @memo Enable/Disable external clock input (needs an edge) 
    @param base\_adr PPG VME base address
    @return data
*/
int TPPGToggleExtClock(MVME_INTERFACE *mvme, const DWORD base_adr, int dd_flag)
{
  DWORD data,data1,freq_MHz;
  int clock_good, ext_clock, prev_ext_clock;
  int status;

  // If dd_flag = TRUE, set divide-down automatically.

  // needs an edge... set to 1 then 0
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );

  // Module will not work  if set to ext clock with no clock present
    if (data & 0x20000)
    clock_good=1;
  else
    clock_good=0;

  if (data & 0x10000)
    ext_clock=1;
  else
    ext_clock=0;

  if (ext_clock==0 && clock_good==0)
    {
      printf("Cannot set external clock. External Clock is bad\n");
      return FAILURE;
    }
  if (ext_clock)
    {
      printf("Setting internal clock\n");
      freq_MHz=10;
    }
  else
    {
      printf("Setting external clock\n");
      freq_MHz=100;
    }
  data = (data & TPPG_CLOCK_MASK);
  data1=TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  printf("Wrote 0x%x to CSR;  read back 0x%x\n",data,data1);
  data = (data & TPPG_CLOCK_MASK) | 2 ;  // 0x1D mask all but ext-clock bit
  data1=TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  printf("Wrote 0x%x to CSR;  read back 0x%x\n",data,data1);
  data = (data & TPPG_CLOCK_MASK);
  sleep (1);
  data1=TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);  
  printf("Wrote 0x%x to CSR;  read back 0x%x\n",data,data1);
  printf("Clock source has been toggled\n");

  prev_ext_clock = ext_clock; // remember previous state
  sleep (1);
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );  
  if (data & 0x10000)
    ext_clock=1;
  else
    ext_clock=0;

  if (ext_clock == prev_ext_clock)
    {
      printf("Error - clock did not toggle as expected\n");
      return FAILURE;
    }

  if(!dd_flag)
    {
      printf("\n*** Now you must load divide-downs using ");
      if(freq_MHz==100)
	printf("f100.sh ***\n");
      else
	printf("f10.sh ***\n");
      return SUCCESS ; // do not set divide-down automatically
    }

  printf("Now setting divide-down for frequency %d MHz \n",freq_MHz);
  status = TPPGSetDivClock(mvme, base_adr, freq_MHz);
  return status;
}


/*------------------------------------------------------------------*/
/**  TPPGSetDivClock100(ppg_base)
    Set CLOCK DIVIDE register so External Clock of 100MHz is acceptable
    @memo Enable/Disable external clock input (needs an edge) 
    @param base\_adr PPG VME base address
    @return data
*/
int TPPGSetDivClock(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD freq_MHz)
{
  DWORD data_100[]={0x0, 0x00000305, 0x0, 0x01000205, 0x0, 0x05000105, 0x0, 0x04000005, 0x0, 0x3, 0x0};
  DWORD data_10[]={0x04000105,0x0,0x3,0x0}; 
  int i,num_ins;

  if(freq_MHz == 100)
    {
      num_ins = sizeof(data_100)/sizeof(DWORD);
      printf("Programming clock divide down register for %d MHz clock speed\n",freq_MHz);
      for (i=0; i< num_ins; i++)
	{
	  TPPGRegWrite(mvme, base_adr, TPPG_CLOCK_DIVIDE, data_100[i] );
	  printf("Wrote 0x%x to Reg offset 0x%x \n", data_100[i], TPPG_CLOCK_DIVIDE);
	  //sleep(1);
	}
    }
  else if (freq_MHz == 10)
    {
      num_ins = sizeof(data_10)/sizeof(DWORD);
      printf("Programming clock divide down register for %d MHz clock speed\n",freq_MHz);
      for (i=0; i< num_ins; i++)
	{
	  TPPGRegWrite(mvme, base_adr, TPPG_CLOCK_DIVIDE, data_10[i] );
	  printf("Wrote 0x%x to Reg offset 0x%x \n", data_10[i], TPPG_CLOCK_DIVIDE);
	  //sleep(1);
	}
    }
  else
    {
      printf("TPPGSetDivClock: error - invalid frequency (%d). Only 10 or 100 MHz is supported\n",freq_MHz);
      return FAILURE;
    }
  return SUCCESS;
}



/*------------------------------------------------------------------*/
/**  TPPGReset(ppg_base)
    Reset PPG (stop PPG even during long delay)
    @memo Reset PPG
    @param base\_adr PPG VME base address
    @return data
*/
int TPPGReset(MVME_INTERFACE *mvme, const DWORD base_adr)
{  // Set then clear reset bit. Also clears bit 0 (running) in CSR0
   // Stops sequencer even if on a long delay
   // Does not affect clock frequency
  DWORD data,data1;
  
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  data = (data & TPPG_RESET_MASK) | 8 ;  // 0x16 mask 
  data1=TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data); // stop the PPG running


  // Now clear reset bit 
  data1 = data1  & TPPG_RESET_MASK;
  data=TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data1);  // clear the reset bit
  printf("Wrote 0x%x to CSR;  read back 0x%x\n",data1,data);
 
  printf("PPG is reset\n");  
  return SUCCESS;
}



/*------------------------------------------------------------------*/
/**  TPPGEnableTestMode(ppg_base)
    Enable test mode (LED/NIM follow state of address reg)
    @memo Enable test mode  (LED/NIM follow state of address reg)
    @param base\_adr PPG VME base address
    @return data
*/
int TPPGEnableTestMode(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  data = (data & TPPG_TEST_MODE_MASK) | 0x10 ;  // 0x0F mask - set test mode bit
  data1=TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  data = data  & TPPG_STATUS_MASK;
  data1 = data1 & TPPG_STATUS_MASK;

  if( data != data1 )
    {
      printf("TPPGEnableTestMode: Error - values written (0x%x) and read back (0x%x) should be identical\n",data,data1);
      return FAILURE;
    }
  printf("TPPGEnableTestMode: TestMode selected\n");    
  return SUCCESS;
}

/*------------------------------------------------------------------*/
/**  TPPGDisableTestMode(ppg_base)
    Disable test mode
    @memo Disable test mode
    @param base\_adr PPG VME base address
    @return data
*/
int TPPGDisableTestMode(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD data,data1;
  data =  TPPGRegRead(mvme, base_adr, TPPG_CSR_REG );
  data = (data & TPPG_TEST_MODE_MASK)  ;  // 0x0F mask - clear test mode bit

  data1 = TPPGRegWrite(mvme, base_adr,  TPPG_CSR_REG  , data);
  data = data  & TPPG_STATUS_MASK;
  data1 = data1 & TPPG_STATUS_MASK;

  if( data != data1 )
    {
      printf("TPPGDisableTestMode: Error - values written (0x%x) and read back (0x%x) should be identical\n",data,data1);
      return FAILURE;
    }
  printf("TPPGDisableTestMode: TestMode disabled\n");
  return SUCCESS;
}

/*------------------------------------------------------------------*/
/** TPPGPolmskRead 
    Read the Polarity mask.
    @memo Read polarity mask.
    @param base\_adr PPG VME base address
    @return polarity (24bit)
*/
  DWORD TPPGPolmskRead(MVME_INTERFACE *mvme, const DWORD base_adr)
{
  DWORD  data;
  data = TPPGRegRead(mvme, base_adr,  TPPG_POL_MASK );
  return data;
}

/*------------------------------------------------------------------*/
/** TPPGPolmskWrite
    Write the Polarity mask.
    @memo Write polarity mask.
    @param base\_adr PPG VME base address
    @return polarity (24bit)
*/
DWORD TPPGPolmskWrite(MVME_INTERFACE *mvme, const DWORD base_adr, const DWORD pol)
{
  DWORD  data;
  data = TPPGRegWrite(mvme, base_adr,  TPPG_POL_MASK, pol );
  return data;
}


/*------------------------------------------------------------------*/
/** ppgLoad
    Load PPG file into sequencer.
    @memo Load file PPG.
    @param base\_adr PPG VME base address
    @param pc\_offset Offset at which to load program
    @return 1=SUCCESS, -1=file not found
*/
int TPPGLoad(MVME_INTERFACE *mvme, const DWORD base_adr, DWORD pc_offset, char *file)
{
  /*  Local Variables  */

  COMMAND  command_info;
  char  line[128];
  int  cmode, localam;
  int linenum;
  char *p;
  int header_flag;
  int counter;
  DWORD data;


  //  New PPG
  //  SET bits:0-31    |  CLR bits 32-63  | Delay Count:64-95  | Delay Count: 96-115 | Instruction 116-119 |  120-127
  //        32 bits    |        32 bits   |    32 bits         |    32 bits          |     4 bits          |  ignored

  
  // printf("TPPGLoad: ppgld %p (expect NULL)\n",ppgld);
  printf("TPPGLoad: starting with base_addr=0x%x pc_offset=%d file=%s\n", base_adr,pc_offset,file);
  printf("TPPGLoad: stopping sequencer\n");
  TPPGStopSequencer (mvme, base_adr);
  
  start_pc = num_instructions = 0; // globals
  printf("Opening ppg load file: %s   ...  \n",file);
  
  if(ppgld != NULL)
    {
      printf("TPPGLoad: ppgld is not NULL; cannot open file %s\n",file);
      return FAILURE;
    }

  ppgld = fopen(file,"r");
  //sleep(1) ; // sleep 1s
  if(ppgld == NULL){
    printf("ppgLoad: ppg load file %s could not be opened. [%p]\n", file, ppgld);
    return FAILURE;
  }
  
  linenum=counter=0;
  header_flag=1;
  
  mvme_get_dmode(mvme, &cmode); // store present data mode
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32 

  mvme_get_am(mvme, &localam); // store present addressing mode
  mvme_set_am(mvme, MVME_AM_A32_ND); // set A32 

  // Find number of instructions
  while (getinsline(line,127,ppgld) > 0) // line[128]
    {        
      linenum++;


      //     printf("File line %d: %s\n",linenum,line);
      if(header_flag)
	{
	  if ( strncmp(line,"#",1) !=0 ) // skip comments at begin of file
	    {
	      if ( strncmp(line,"Num Instruction Lines =",23) ==0 ) // first line after comments
		{
		  p=line + 23;  // skip "Instruction Lines "
		  char *q;
		  q =strchr(p,'#');
		  if(q != 0)
		    *q='\0';
		  if(ddd)printf("Now line is %s\n",p);
		  sscanf(p,"%d",&num_instructions);
		  printf("instruction lines = %d\n",num_instructions);  
		  printf("Base Address = %x\n",base_adr);
		  printf("File line %d: %s\n",linenum,line);
		  header_flag=0; // we have processed header
		}
	    }
	  else  // print out the # comment line
	    printf("File line %d: %s",linenum,line);
	}
      else
	{ // instructions
	  command_info = lineRead(line);
	  counter++;
	  if (counter==1)
	    {
	      start_pc =  command_info.pc  + pc_offset; // remember where program starts
	      printf("TPPGLoad: offset =  %d; program will start at PC = %d\n",pc_offset,start_pc);
	      printf("              #PC set bitpat clr bitpat       delay  ins/data\n");
	    }
	  printf("File line %2.2d: %s",linenum,line);
	  if (ddd)
	    {
	      printf("Line %d (instr %d) : ",linenum,counter);
	      printf("pc:%3.3d ",command_info.pc);
	      printf("setpat:%8.8x ",command_info.setpat);
	      printf("clrpat:%8.8x ",command_info.clrpat);
	      printf("delay:%8.8x",command_info.delay);
	      printf("ins/data:%8.8x\n",command_info.ins_data);
	    }
	    command_info.pc += pc_offset;
	    if ( command_info.pc > 0x1000) 
	      {
		printf ("Error : instruction address (%d) exceedes program memory capacity\n",command_info.pc);
		return FAILURE;
	      }
	  // load this instruction
	   data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , command_info.pc ); // write instruction address 
	   data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_LO  , command_info.setpat); // "set" bits (32 bits)
	   data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_MED  ,command_info.clrpat );// "clear" bits (32 bits)
	   data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_HI  , command_info.delay);// delay count
	   data = TPPGRegWrite(mvme, base_adr,  TPPG_DATA_TOP  , command_info.ins_data);// instruction and any data
	}
    } // end of while


  if(header_flag > 0)
    {
      printf("TPPGLoad: incorrect format for ppg load file. Did not find number of instruction lines\n");
      return FAILURE;
    }

  if(counter != num_instructions)
    printf("TPPGLoad: Warning - stated number of instructions lines (%d) does not agree with actual number(%d)\n",
	   num_instructions,counter);
  
  

  // Set instruction address back to start of program
  data = TPPGRegWrite(mvme, base_adr,  TPPG_ADDR  , start_pc ); // write start instruction address 
  printf("TPPGLoad: set instruction address (reg 0x%x) back to start of program (pc = %d)\n",
	 TPPG_ADDR,data);

  mvme_set_dmode(mvme, cmode); // restore VME data mode
  mvme_set_am(mvme, localam); // restore VME addressing mode
    
  fclose(ppgld);
  ppgld=NULL;
  if(ddd) printf("Programming ended, PPG ready");
  return SUCCESS;
}
 
char *firmware_date(time_t val)
{
   static char fwtimebuf[FWTBUFSIZ];
   struct tm *tptr = gmtime(&val);
   strftime(fwtimebuf, FWTBUFSIZ, "%d%b%g_%H:%M", tptr);
   return(fwtimebuf);
}
/*****************************************************************/
/*-PAA- For test purpose only */
#ifdef MAIN_ENABLE
int main (int argc, char* argv[]) {


  DWORD PPG_BASE = 0x00100000;

  MVME_INTERFACE *mvme;

  int status;
 
  DWORD data,reg_offset;
  //DWORD setbits,clrbits,delay;
  char cmd[]="hallo";

  int s,v;
  DWORD ival;
  char filename[128];

  DWORD rpol,pol,adr;
  DWORD freq_MHz;

  char default_file[80];
  char expt[20];


  strncpy(expt, getenv("MIDAS_EXPT_NAME"), sizeof(expt));
  
  //printf("Current experiment is %s\n",expt);
  sprintf(default_file,"/home/%s/online/ppg/ppgload/ppgload.dat",expt);
  //printf("default file is %s\n",default_file);
 
  if (argc>1) {
    sscanf(argv[1],"%lx",&PPG_BASE);
  }
  if (argc>2) {
    sscanf(argv[2],"%d",&ddd);
  }

  printf("PPG base: %lx   debug=%d\n",PPG_BASE,ddd); 
    
  // Test under vmic   
  status = mvme_open(&mvme, 0);
  if(status != SUCCESS)
    {
      printf("failure after mvme_open, status = %d\n",status);
      return status;
    }

  mvme_set_am(mvme, MVME_AM_A32_ND); // set A32 
  mvme_set_dmode(mvme, MVME_DMODE_D32); // set D32 

 
  v=TPPGRegRead(mvme, PPG_BASE, TPPG_FIRMWARE_ID);
  printf("\nPPG FirmwareID: %s [0x%08x]\n\n", firmware_date(v), v );


  TPPGStatusRead(mvme,PPG_BASE);
    
  ppg();
  while ( isalpha(cmd[0]) )
    {
      printf("\nEnter command (A-Y) X to exit?  ");
      scanf("%s",cmd);
      //  printf("cmd=%s\n",cmd);
      cmd[0]=toupper(cmd[0]);
      s=cmd[0];
      
      switch(s)
	{
	case ('A'):
	  TPPGInit(mvme, PPG_BASE,0);
	  break;
	case ('B'):
          printf("Loading default filename %s\n",default_file);
	  strcpy(filename,default_file);
          ival = 0;
	  printf("Loading program at PC %d\n",ival);
	  TPPGLoad(mvme, PPG_BASE, ival, filename);
	  break;
	case ('C'):
	  TPPGStopSequencer(mvme, PPG_BASE);
	  break;
	case ('E'): // start sequencer
	  TPPGStopSequencer(mvme, PPG_BASE); // needs an edge
          sleep(0.5) ; // sleep 0.5s
	  TPPGStartSequencer(mvme, PPG_BASE);
	  break;
	case ('F'):
	  TPPGEnableExtTrig(mvme, PPG_BASE);
	  break;
	case ('G'):
	  TPPGDisableExtTrig(mvme, PPG_BASE);
	  break;
	  //	case ('H'):
	  
	  //break;
	case ('I'):
	    TPPGStatusRead(mvme, PPG_BASE);
	    break;
	case ('J'):
	  rpol = TPPGPolmskRead(mvme, PPG_BASE);
	  printf("Read back Pol Mask=0x%lx\n",rpol);
	  break;
	case ('K'):
	  printf("Enter Mask value to write :0x");
	  scanf("%lx",&pol);
	  printf("Writing Pol Mask=0x%lx\n",pol);
	  rpol = TPPGPolmskWrite(mvme, PPG_BASE, pol);
	  printf("Read back Pol Mask=0x%lx\n",rpol);
	  break;
	case ('L'):
	  printf("Enter PPG register offset : 0x");
	  scanf("%lx",&reg_offset);
	  printf("Enter data to write: 0x");
	  scanf("%lx",&ival);
	  printf("Writing 0x%lx (%ld) to register offset 0x%lx (%lu)\n",
		 ival,ival,reg_offset,reg_offset);
	  data=TPPGRegWrite(mvme, PPG_BASE, reg_offset, ival);
	  printf("Read back from offset 0x%lx (%lu)  data = 0x%lx (%ld)\n",
		 reg_offset,reg_offset,data,data);
	  break;
	case ('M'):
	  printf("Enter PPG register offset : 0x");
	  scanf("%lx",&reg_offset);
	  data=TPPGRegRead(mvme, PPG_BASE, reg_offset);
	  printf("Read back from offset 0x%lx (%lu)    data = 0x%lx (%ld)\n ",
		 reg_offset,reg_offset,data,data);
	  break;
	case ('N'):
	  TPPGToggleExtClock(mvme, PPG_BASE, FALSE);
	  // do not set clock divide down
	  break;
	case ('O'):
	  printf("Enter address of instruction to read :0x");
	  scanf("%lx",&adr);
	  TPPGReadIns(mvme, PPG_BASE,adr);
	  break;
	case ('R'):
	  TPPGToggleExtClock(mvme, PPG_BASE, TRUE);
	  // sets clock divide down
	  break;
	case ('S'):
	  TPPGReset(mvme, PPG_BASE);
	  break;
	case ('T'):
	  printf("Enter PPG filename :");
	  scanf("%s",filename);
	  printf("Enter program start address (usually 0) (dec) : ");
	  scanf("%ld", (long int *) &ival);
	  printf("Loading program at PC %d\n",ival);
	  TPPGLoad(mvme, PPG_BASE, ival, filename);
	  break;

	  break;
	case ('U'):
	  TPPGEnableTestMode(mvme, PPG_BASE);
	  break;
	case ('V'):
	  TPPGDisableTestMode(mvme, PPG_BASE);
	  break;
	case ('W'):
	  printf("Enter program start address (decimal) : ");
	  scanf("%ld",&ival);
	  TPPGSetStartPC(mvme, PPG_BASE, ival);
	  printf("Program will start at address %d\n",ival);
	  break;
	case ('Y'):
	  data=TPPGRegRead(mvme, PPG_BASE, TPPG_ADDR);
	  printf("Program will start at address %d\n",data);
	  break;
	case ('Z'):
	  TPPGZeroAll(mvme, PPG_BASE );
	  break;
	case ('D'):
	  if (ddd)
	    ddd=0;
	  else
	    ddd=1;	  break;
	case ('P'):
	  ppg();
	  break;
	case ('X'):
	  return SUCCESS;
	default:
	  break;
	}
    }



  status = mvme_close(mvme);
  return SUCCESS;
}	
#endif
