/********************************************************************\

  Name:         experim.h
  Created by:   ODBedit program

  Contents:     This file contains C structures for the "Experiment"
                tree in the ODB and the "/Analyzer/Parameters" tree.

                Additionally, it contains the "Settings" subtree for
                all items listed under "/Equipment" as well as their
                event definition.

                It can be used by the frontend and analyzer to work
                with these information.

                All C structures are accompanied with a string represen-
                tation which can be used in the db_create_record function
                to setup an ODB structure which matches the C structure.

  Created on:   Wed Aug 17 17:34:28 2016

\********************************************************************/

#define EXP_PARAM_DEFINED

typedef struct {
  char      run_description[256];
} EXP_PARAM;

#define EXP_PARAM_STR(_name) const char *_name[] = {\
"[.]",\
"Run Description = STRING : [256] Sequencer Test Run ",\
"",\
NULL }

#define EXP_EDIT_DEFINED

typedef struct {
  BOOL      write_data;
} EXP_EDIT;

#define EXP_EDIT_STR(_name) const char *_name[] = {\
"[.]",\
"Write Data = LINK : [19] /Logger/Write data",\
"number of scans = LINK : [53] /Equipment/TITAN_ACQ/ppg cycle/begin_scan/loop count",\
"Run Description = LINK : [43] /Experiment/Run Parameters/Run Description",\
"",\
NULL }

#ifndef EXCL_POLL_PPG

#define POLL_PPG_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
} POLL_PPG_COMMON;

#define POLL_PPG_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 1",\
"Period = INT : 500",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxcpet.triumf.ca",\
"Frontend name = STRING : [32] fecpet",\
"Frontend file name = STRING : [256] fecpet.c",\
"Status = STRING : [256] fecpet@lxcpet.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"",\
NULL }

#endif

#ifndef EXCL_SCALER

#define SCALER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
} SCALER_COMMON;

#define SCALER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 2",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxcpet.triumf.ca",\
"Frontend name = STRING : [32] fecpet",\
"Frontend file name = STRING : [256] fecpet.c",\
"Status = STRING : [256] fecpet@lxcpet.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"",\
NULL }

#endif

#ifndef EXCL_TITAN_ACQ

#define TITAN_ACQ_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
  char      status[256];
  char      status_color[32];
} TITAN_ACQ_COMMON;

#define TITAN_ACQ_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 10",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 1",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : n",\
"Read on = INT : 377",\
"Period = INT : 10000",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxcpet.triumf.ca",\
"Frontend name = STRING : [32] fecpet",\
"Frontend file name = STRING : [256] fecpet.c",\
"Status = STRING : [256] fecpet@lxcpet.triumf.ca",\
"Status color = STRING : [32] #00FF00",\
"",\
NULL }

#define TITAN_ACQ_SETTINGS_DEFINED

typedef struct {
  struct {
    struct {
      char      experiment_name[32];
      char      ppgload_path[50];
      char      ppg_path[50];
      char      ppg_perl_path[50];
      float     daq_service_time__ms_;
      float     standard_pulse_width__ms_;
      float     awg_clock_width__ms_;
      float     tdcblock_width__ms_;
      float     ppg_clock__mhz_;
      DWORD     polarity;
      BOOL      external_trigger;
      BOOL      external_clock;
    } input;
    struct {
      char      compiled_file_time[32];
      DWORD     compiled_file_time__binary_;
      float     time_slice__ms_;
      float     ppg_nominal_frequency__mhz_;
      float     minimal_delay__ms_;
      float     ppg_freq_conversion_factor;
      BOOL      ppg_running;
      double    last_transition__ms_;
      BOOL      external_clock;
      BOOL      good_external_clock;
      struct {
        INT       dummy;
        INT       num_loops;
        double    start_loop[10];
        double    one_loop_duration[10];
        double    all_loops_end[10];
        char      loop_names[10][20];
      } loops;
      struct {
        BOOL      external_clock_alarm;
        BOOL      ppg_status_alarm;
      } alarms;
    } output;
  } ppg;
} TITAN_ACQ_SETTINGS;

#define TITAN_ACQ_SETTINGS_STR(_name) const char *_name[] = {\
"[ppg/input]",\
"Experiment name = STRING : [32] cpet",\
"PPGLOAD path = STRING : [50] /home/cpet/online/ppg/ppgload",\
"PPG path = STRING : [50] /home/cpet/online/ppg",\
"PPG perl path = STRING : [50] /home/cpet/online/ppg/perl",\
"DAQ service time (ms) = FLOAT : 0",\
"standard pulse width (ms) = FLOAT : 0.001",\
"AWG clock width (ms) = FLOAT : 0.001",\
"TDCBLOCK width (ms) = FLOAT : 0.001",\
"PPG clock (MHz) = FLOAT : 100",\
"Polarity = DWORD : 0",\
"external trigger = BOOL : y",\
"external clock = BOOL : n",\
"",\
"[ppg/output]",\
"compiled file time = STRING : [32] Wed Aug 17 16:57:11 2016",\
"compiled file time (binary) = DWORD : 1471478231",\
"Time Slice (ms) = FLOAT : 1e-05",\
"PPG nominal frequency (MHz) = FLOAT : 10",\
"Minimal Delay (ms) = FLOAT : 3e-05",\
"PPG freq conversion factor = FLOAT : 10",\
"PPG running = BOOL : n",\
"last transition (ms) = DOUBLE : 4013",\
"external clock = BOOL : n",\
"good external clock = BOOL : y",\
"",\
"[ppg/output/loops]",\
"dummy = INT : 0",\
"num_loops = INT : 0",\
"start_loop = DOUBLE[10] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"one_loop_duration = DOUBLE[10] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"all_loops_end = DOUBLE[10] :",\
"[0] 0",\
"[1] 0",\
"[2] 0",\
"[3] 0",\
"[4] 0",\
"[5] 0",\
"[6] 0",\
"[7] 0",\
"[8] 0",\
"[9] 0",\
"loop_names = STRING[10] :",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] ",\
"[20] 2012",\
"",\
"[ppg/output/alarms]",\
"external clock alarm = BOOL : n",\
"ppg status alarm = BOOL : n",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_32_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse17;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_ramp;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[16];
  } trans14a;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse25;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_ramp;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[16];
  } trans14b;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse32;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse15;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
  } image;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    DWORD     bit_pattern;
  } pattern_test;
} TITAN_ACQ_PPG_CYCLE_32;

#define TITAN_ACQ_PPG_CYCLE_32_STR(_name) const char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 100000",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] ",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH1",\
"",\
"[pulse17]",\
"time offset (ms) = DOUBLE : 15",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH17",\
"",\
"[begin_ramp]",\
"time offset (ms) = DOUBLE : 20",\
"loop count = INT : 20",\
"",\
"[trans14A]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGRAMP",\
"ppg signal name = STRING : [16] CH14",\
"",\
"[pulse25]",\
"time offset (ms) = DOUBLE : 0.003",\
"time reference = STRING : [20] _TBEGRAMP",\
"pulse width (ms) = DOUBLE : 0.003",\
"ppg signal name = STRING : [16] CH25",\
"",\
"[end_ramp]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] ",\
"",\
"[trans14B]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TENDRAMP",\
"ppg signal name = STRING : [16] CH14",\
"",\
"[pulse32]",\
"time offset (ms) = DOUBLE : 15",\
"time reference = STRING : [20] _TENDRAMP",\
"pulse width (ms) = DOUBLE : 15",\
"ppg signal name = STRING : [16] CH32",\
"",\
"[pulse15]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDRAMP",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH15",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TEND_PULSE32",\
"",\
"[skip]",\
"dummy = DOUBLE : 0",\
"",\
"[image]",\
"name = STRING : [32] none",\
"mode = STRING : [8] 1e",\
"",\
"[pattern_test]",\
"time offset (ms) = DOUBLE : 120",\
"time reference = STRING : [20] T0",\
"bit pattern = DWORD : 61680",\
"",\
NULL }

#define TITAN_ACQ_MODE_NAME_DEFINED

typedef struct {
} TITAN_ACQ_MODE_NAME;

#define TITAN_ACQ_MODE_NAME_STR(_name) const char *_name[] = {\
"mode_name = STRING : [8] 1e",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1A_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    dummy;
  } skip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    undummy;
  } unskip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_ep11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
    INT       num_channels_to_display;
  } image;
  struct {
    char      names[13][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1A;

#define TITAN_ACQ_PPG_CYCLE_MODE_1A_STR(_name) const char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0.0001",\
"loop count = INT : 100000",\
"",\
"[trans1]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[begin_extr]",\
"time offset (ms) = DOUBLE : 1",\
"loop count = INT : 10",\
"time reference = STRING : [20] _TTRANS1",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TEND_PULSE2",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 1e-06",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[skip1]",\
"dummy = DOUBLE : 1",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[unskip1]",\
"undummy = DOUBLE : 2",\
"",\
"[end_extr]",\
"time offset (ms) = DOUBLE : 0.0005",\
"time reference = STRING : [20] _TEND_PULSE4",\
"",\
"[trans2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[trans3]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[begin_sclr]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 20",\
"time reference = STRING : [20] ",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[end_sclr]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] _TBEGSCLR",\
"",\
"[trans4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] _TSTART_PULSE10",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[time_ep11]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] _TTIME_EP11",\
"",\
"[skip]",\
"dummy = DOUBLE : 1",\
"",\
"[image]",\
"name = STRING : [32] cpet_pc_skip_trans.gif",\
"mode = STRING : [8] 1a",\
"num channels to display = INT : 16",\
"",\
"[names]",\
"names = STRING[13] :",\
"[64] 1 ISACBeamGate",\
"[64] 2 TitanBmGate1",\
"[64] 3 TitanBmGate2",\
"[64] 4 RFQ Extractn",\
"[64] 5    Capture 1",\
"[64] 6   Transfer 1",\
"[64] 7   Transfer 2",\
"[64] 8     Scaler 1",\
"[64] 9     Scaler 2",\
"[64] 10       TIG10",\
"[64] 11        Dump",\
"[64] 12     Kicker1",\
"[64] 13     Kicker2",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1B_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_extr;
  struct {
    double    dummy;
  } skip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    undummy;
  } unskip1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_ep11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
    INT       num_channels_to_display;
  } image;
  struct {
    char      names[15][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1B;

#define TITAN_ACQ_PPG_CYCLE_MODE_1B_STR(_name) const char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 6e-05",\
"loop count = INT : 1000000",\
"",\
"[trans1]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[begin_extr]",\
"time offset (ms) = DOUBLE : 1",\
"loop count = INT : 2",\
"time reference = STRING : [20] _TTRANS1",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 4",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 4",\
"time reference = STRING : [20] _TEND_PULSE2",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 3",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[end_extr]",\
"time offset (ms) = DOUBLE : 0.0005",\
"time reference = STRING : [20] _TEND_PULSE6",\
"",\
"[skip1]",\
"dummy = DOUBLE : 1",\
"",\
"[trans2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[trans3]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[begin_sclr]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 20",\
"time reference = STRING : [20] ",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[end_sclr]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] _TBEGSCLR",\
"",\
"[trans4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TSTART_PULSE10",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[unskip1]",\
"undummy = DOUBLE : 2",\
"",\
"[time_ep11]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[skip]",\
"dummy = DOUBLE : 0",\
"",\
"[image]",\
"name = STRING : [32] cpet_pc_end_after_extr.gif",\
"mode = STRING : [8] 1b",\
"num channels to display = INT : 15",\
"",\
"[names]",\
"names = STRING[15] :",\
"[64] 1 ISACBeamGate",\
"[64] 2 TitanBmGate1",\
"[64] 3 TitanBmGate2",\
"[64] 4 RFQ Extractn",\
"[64] 5    Capture 1",\
"[64] 6   Transfer 1",\
"[64] 7   Transfer 2",\
"[64] 8     Scaler 1",\
"[64] 9     Scaler 2",\
"[64] 10       TIG10",\
"[64] 11        Dump",\
"[64] 12     Kicker1",\
"[64] 13     Kicker2",\
"[64] 14     E-Beam1",\
"[64] 15     E-Beam2",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1C_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_ep11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
    INT       num_channels_to_display;
  } image;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_last;
  struct {
    char      names[15][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1C;

#define TITAN_ACQ_PPG_CYCLE_MODE_1C_STR(_name) const char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0.01",\
"loop count = INT : 10000000",\
"",\
"[trans1]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[begin_extr]",\
"time offset (ms) = DOUBLE : 0.01",\
"loop count = INT : 1",\
"time reference = STRING : [20] _TTRANS1",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 20",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 15",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[end_extr]",\
"time offset (ms) = DOUBLE : 0.0005",\
"time reference = STRING : [20] _TEND_PULSE5",\
"",\
"[trans2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[trans3]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] _TENDEXTR",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[begin_sclr]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 1",\
"time reference = STRING : [20] _TTRANS3",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 15",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 20",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[end_sclr]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TBEGSCLR",\
"",\
"[trans4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TENDSCLR",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 15",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] _TENDSCLR",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] _TSTART_PULSE10",\
"pulse width (ms) = DOUBLE : 2000",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[time_ep11]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[trans5]",\
"time offset (ms) = DOUBLE : 0.5",\
"time reference = STRING : [20] _TENDEXTR",\
"ppg signal name = STRING : [15] CH14",\
"",\
"[trans6]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TENDSCLR",\
"ppg signal name = STRING : [15] CH14",\
"",\
"[trans7]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDEXTR",\
"ppg signal name = STRING : [15] CH15",\
"",\
"[trans8]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TENDSCLR",\
"ppg signal name = STRING : [15] CH15",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 27",\
"time reference = STRING : [20] _TTIME_EP11",\
"",\
"[skip]",\
"dummy = DOUBLE : 0",\
"",\
"[image]",\
"name = STRING : [32] cpet_pc_all.gif",\
"mode = STRING : [8] 1c",\
"num channels to display = INT : 16",\
"",\
"[time_last]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[names]",\
"names = STRING[15] :",\
"[64] 1   ISACBeamGate",\
"[64] 2 TitanBeamGate1",\
"[64] 3 TitanBeamGate2",\
"[64] 4 RFQ Extraction",\
"[64] 5      Capture 1",\
"[64] 6     Transfer 1",\
"[64] 7     Transfer 2",\
"[64] 8       Scaler 1",\
"[64] 9       Scaler 2",\
"[64] 10         TIG10",\
"[64] 11          Dump",\
"[64] 12       Kicker1",\
"[64] 13       Kicker2",\
"[64] 14       EBeam-1",\
"[64] 15       EBeam-2",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1D_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    char      time_reference[20];
  } pulse9;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    char      time_reference[20];
  } pulse10;
  struct {
    double    time_offset__ms_;
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
    char      time_reference[20];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans1;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_extr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans3;
  struct {
    double    time_offset__ms_;
    INT       loop_count;
    char      time_reference[20];
  } begin_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_sclr;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } time_ep11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    char      ppg_signal_name[15];
  } trans8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } delay_eoc;
  struct {
    double    dummy;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
    INT       num_channels_to_display;
  } image;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } delay1;
  struct {
    char      names[15][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1D;

#define TITAN_ACQ_PPG_CYCLE_MODE_1D_STR(_name) const char *_name[] = {\
"[pulse9]",\
"time offset (ms) = DOUBLE : 0",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH11",\
"time reference = STRING : [20] T0",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 0.001",\
"pulse width (ms) = DOUBLE : 0.5",\
"ppg signal name = STRING : [16] CH12",\
"time reference = STRING : [20] T0",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0.001",\
"pulse width (ms) = DOUBLE : 0.5",\
"ppg signal name = STRING : [16] CH13",\
"time reference = STRING : [20] T0",\
"",\
"[trans1]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] T0",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[begin_extr]",\
"time offset (ms) = DOUBLE : 0.01",\
"loop count = INT : 1",\
"time reference = STRING : [20] _TTRANS1",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0.0003",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 1e-05",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0.0004",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 1e-05",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 0.002",\
"time reference = STRING : [20] _TBEGEXTR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TEND_PULSE3",\
"pulse width (ms) = DOUBLE : 0.0805",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 0.0001",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 0.015",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 0.1",\
"time reference = STRING : [20] _TSTART_PULSE4",\
"pulse width (ms) = DOUBLE : 0.015",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[end_extr]",\
"time offset (ms) = DOUBLE : 0.0005",\
"time reference = STRING : [20] _TEND_PULSE4",\
"",\
"[trans2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH1",\
"",\
"[trans3]",\
"time offset (ms) = DOUBLE : 0.01",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[begin_sclr]",\
"time offset (ms) = DOUBLE : 0",\
"loop count = INT : 1",\
"time reference = STRING : [20] ",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 0.001",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCLR",\
"pulse width (ms) = DOUBLE : 0.001",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[end_sclr]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] _TBEGSCLR",\
"",\
"[trans4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] ",\
"ppg signal name = STRING : [15] CH10",\
"",\
"[time_ep11]",\
"time offset (ms) = DOUBLE : 5e-05",\
"time reference = STRING : [20] ",\
"",\
"[trans5]",\
"time offset (ms) = DOUBLE : 0.5",\
"time reference = STRING : [20] _TENDEXTR",\
"ppg signal name = STRING : [15] CH14",\
"",\
"[trans6]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDSCLR",\
"ppg signal name = STRING : [15] CH14",\
"",\
"[trans7]",\
"time offset (ms) = DOUBLE : 1",\
"time reference = STRING : [20] _TENDEXTR",\
"ppg signal name = STRING : [15] CH15",\
"",\
"[trans8]",\
"time offset (ms) = DOUBLE : 0.5",\
"time reference = STRING : [20] _TENDSCLR",\
"ppg signal name = STRING : [15] CH15",\
"",\
"[delay_eoc]",\
"time offset (ms) = DOUBLE : 1.5",\
"time reference = STRING : [20] _TENDSCLR",\
"",\
"[skip]",\
"dummy = DOUBLE : 0",\
"",\
"[image]",\
"name = STRING : [32] cpet_pc_ext_trig.gif",\
"mode = STRING : [8] 1d",\
"num channels to display = INT : 16",\
"",\
"[delay1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TTIME_EP11",\
"",\
"[names]",\
"names = STRING[15] :",\
"[64] 1 ISACBeamGate",\
"[64] 2 TitanBmGate1",\
"[64] 3 TitanBmGate2",\
"[64] 4 RFQ Extractn",\
"[64] 5    Capture 1",\
"[64] 6   Transfer 1",\
"[64] 7   Transfer 2",\
"[64] 8     Scaler 1",\
"[64] 9     Scaler 2",\
"[64] 10       TIG10",\
"[64] 11       Dump",\
"[64] 12    Kicker1",\
"[64] 13    Kicker2",\
"[64] 14    E-Beam1",\
"[64] 15    E-Beam2",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1E_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse12;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse13;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse14;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse15;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse16;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse17;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse18;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse19;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse20;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse21;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse22;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse23;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse24;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse25;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse26;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse27;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse28;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse29;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse30;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse31;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse32;
  struct {
    INT       channel;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
    INT       num_channels_to_display;
    INT       max_active_channel;
  } image;
  struct {
    char      names[32][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1E;

#define TITAN_ACQ_PPG_CYCLE_MODE_1E_STR(_name) const char *_name[] = {\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH1",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 3300",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 3500",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 4000",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 13",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 301",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0.01",\
"ppg signal name = STRING : [16] CH10",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse12]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse13]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[pulse14]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH14",\
"",\
"[pulse15]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH15",\
"",\
"[pulse16]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH16",\
"",\
"[pulse17]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH17",\
"",\
"[pulse18]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH18",\
"",\
"[pulse19]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH19",\
"",\
"[pulse20]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH20",\
"",\
"[pulse21]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH21",\
"",\
"[pulse22]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH22",\
"",\
"[pulse23]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH23",\
"",\
"[pulse24]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH24",\
"",\
"[pulse25]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH25",\
"",\
"[pulse26]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH26",\
"",\
"[pulse27]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH27",\
"",\
"[pulse28]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH28",\
"",\
"[pulse29]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH29",\
"",\
"[pulse30]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH30",\
"",\
"[pulse31]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH31",\
"",\
"[pulse32]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH32",\
"",\
"[skip]",\
"channel = INT : 11",\
"",\
"[image]",\
"name = STRING : [32] none",\
"mode = STRING : [8] 1e",\
"num channels to display = INT : 16",\
"max active channel = INT : 32",\
"",\
"[names]",\
"names = STRING[32] :",\
"[64] anode (e- gate)",\
"[64] G2 (inj)",\
"[64] G1 (ext)",\
"[64] channel 4",\
"[64] FC",\
"[64] Signal gate",\
"[64] Function Generator",\
"[64] Camera",\
"[64] Just Delay",\
"[64] test Behlke 10",\
"[64] channel 11",\
"[64] channel 12",\
"[64] channel 13",\
"[64] channel 14",\
"[64] channel 15",\
"[64] scope trig",\
"[64] PPG total cycle",\
"[64] channel 18",\
"[64] channel 19",\
"[64] channel 20",\
"[64] channel 21",\
"[64] channel 22",\
"[64] channel 23",\
"[64] channel 24",\
"[64] channel 25",\
"[64] channel 26",\
"[64] channel 27",\
"[64] channel 28",\
"[64] channel 29",\
"[64] channel 30",\
"[64] channel 31",\
"[64] channel 32",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1F_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse12;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse13;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse14;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse15;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse16;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse17;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse18;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse19;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse20;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse21;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse22;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse23;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse24;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse25;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse26;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse27;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse28;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse29;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse30;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse31;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse32;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    INT       channel;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
    INT       num_channels_to_display;
  } image;
  struct {
    char      names[32][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1F;

#define TITAN_ACQ_PPG_CYCLE_MODE_1F_STR(_name) const char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0.05",\
"loop count = INT : 100000",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] _TBEGLOOP1",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH1",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 100",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 100",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH10",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse12]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse13]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[pulse14]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH14",\
"",\
"[pulse15]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH15",\
"",\
"[pulse16]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH16",\
"",\
"[pulse17]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH17",\
"",\
"[pulse18]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH18",\
"",\
"[pulse19]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH19",\
"",\
"[pulse20]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH20",\
"",\
"[pulse21]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH21",\
"",\
"[pulse22]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH22",\
"",\
"[pulse23]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH23",\
"",\
"[pulse24]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH24",\
"",\
"[pulse25]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH25",\
"",\
"[pulse26]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH26",\
"",\
"[pulse27]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH27",\
"",\
"[pulse28]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH28",\
"",\
"[pulse29]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH29",\
"",\
"[pulse30]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH30",\
"",\
"[pulse31]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH31",\
"",\
"[pulse32]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 0",\
"ppg signal name = STRING : [16] CH32",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 10",\
"time reference = STRING : [20] _TBEGSCAN",\
"",\
"[skip]",\
"channel = INT : 11",\
"",\
"[image]",\
"name = STRING : [32] none",\
"mode = STRING : [8] 1f",\
"num channels to display = INT : 32",\
"",\
"[names]",\
"names = STRING[32] :",\
"[64] channel 1",\
"[64] channel 2",\
"[64] channel 3",\
"[64] channel 4",\
"[64] channel 5",\
"[64] channel 6",\
"[64] channel 7",\
"[64] channel 8",\
"[64] channel 9",\
"[64] channel 10",\
"[64] channel 11",\
"[64] channel 12",\
"[64] channel 13",\
"[64] channel 14",\
"[64] channel 15",\
"[64] channel 16",\
"[64] channel 17",\
"[64] channel 18",\
"[64] channel 19",\
"[64] channel 20",\
"[64] channel 21",\
"[64] channel 22",\
"[64] channel 23",\
"[64] channel 24",\
"[64] channel 25",\
"[64] channel 26",\
"[64] channel 27",\
"[64] channel 28",\
"[64] channel 29",\
"[64] channel 30",\
"[64] channel 31",\
"[64] channel 32",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1E_16_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse12;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse13;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse14;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse15;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse16;
  struct {
    INT       channel;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
  } image;
  struct {
    char      names[17][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1E_16;

#define TITAN_ACQ_PPG_CYCLE_MODE_1E_16_STR(_name) const char *_name[] = {\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH1",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 20",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 8",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 37",\
"ppg signal name = STRING : [16] CH10",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 60",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse12]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 2",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse13]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[pulse14]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH14",\
"",\
"[pulse15]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH15",\
"",\
"[pulse16]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH16",\
"",\
"[skip]",\
"channel = INT : 11",\
"",\
"[image]",\
"name = STRING : [32] none",\
"mode = STRING : [8] 1e",\
"",\
"[names]",\
"names = STRING[17] :",\
"[64] ISAC Beam Gate",\
"[64] RFQ Extraction trigger",\
"[64] flexible 1 (2nd RFQ ET)",\
"[64] flexible 2 (3rd RFQ ET)",\
"[64] not assigned",\
"[64] not assigned",\
"[64] not assigned",\
"[64] flexible 3 (Camera trigger)",\
"[64] trapping (Coll kick)",\
"[64] flexible 4 (2nd trapping)",\
"[64] flexible 5  (3rd trapping)",\
"[64] AFG trigger",\
"[64] CPET extraction (Gun kick)",\
"[64] BNG (HCI TOF Gate)",\
"[64] flexible 6 (open)",\
"[64] flexible 7 (open)",\
"[64] ",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1F_16_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    INT       loop_count;
  } begin_scan;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse1;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse4;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse6;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse10;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse12;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse13;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse14;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse15;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse16;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
  } end_scan;
  struct {
    INT       channel;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
  } image;
  struct {
    char      names[17][64];
  } names;
} TITAN_ACQ_PPG_CYCLE_MODE_1F_16;

#define TITAN_ACQ_PPG_CYCLE_MODE_1F_16_STR(_name) const char *_name[] = {\
"[begin_scan]",\
"time offset (ms) = DOUBLE : 0.05",\
"loop count = INT : 100000",\
"",\
"[pulse1]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 93",\
"ppg signal name = STRING : [16] CH1",\
"",\
"[pulse2]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 30",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse4]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH4",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse6]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0.0001",\
"ppg signal name = STRING : [16] CH6",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 0.0001",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 0.0001",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 4",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse10]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 37",\
"ppg signal name = STRING : [16] CH10",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 6",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse12]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[pulse13]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[pulse14]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH14",\
"",\
"[pulse15]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH15",\
"",\
"[pulse16]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGSCAN",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH16",\
"",\
"[end_scan]",\
"time offset (ms) = DOUBLE : 100",\
"time reference = STRING : [20] _TBEGSCAN",\
"",\
"[skip]",\
"channel = INT : 11",\
"",\
"[image]",\
"name = STRING : [32] none",\
"mode = STRING : [8] 1f",\
"",\
"[names]",\
"names = STRING[17] :",\
"[64] ISAC Beam Gate",\
"[64] RFQ Extraction trigger",\
"[64] flexible 1 (2nd RFQ ET)",\
"[64] flexible 2 (3rd RFQ ET)",\
"[64] not assigned",\
"[64] not assigned",\
"[64] not assigned",\
"[64] flexible 3 (Camera trigger)",\
"[64] trapping (Coll kick)",\
"[64] flexible 4 (2nd trapping)",\
"[64] flexible 5  (3rd trapping)",\
"[64] AFG trigger",\
"[64] CPET extraction (Gun kicker)",\
"[64] BNG (HCI TOF Gate)",\
"[64] flexible 6 (open)",\
"[64] flexible 7 (open)",\
"[64] not assigned",\
"",\
NULL }

#define TITAN_ACQ_PPG_CYCLE_MODE_1G_DEFINED

typedef struct {
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse2;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse5;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse7;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse16;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse17;
  struct {
    INT       loop_count;
    char      time_reference[20];
    double    time_offset__ms_;
  } begin_wait;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse11;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse12;
  struct {
    double    time_offset__ms_;
  } end_wait;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse8;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse3;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse9;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse13;
  struct {
    INT       channel;
  } skip;
  struct {
    char      name[32];
    char      mode[8];
    INT       num_channels_to_display;
    INT       max_active_channel;
  } image;
  struct {
    char      names[17][64];
  } names;
  struct {
    double    time_offset__ms_;
    char      time_reference[20];
    double    pulse_width__ms_;
    char      ppg_signal_name[16];
  } pulse14;
} TITAN_ACQ_PPG_CYCLE_MODE_1G;

#define TITAN_ACQ_PPG_CYCLE_MODE_1G_STR(_name) const char *_name[] = {\
"[pulse2]",\
"time offset (ms) = DOUBLE : 10000",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 300",\
"ppg signal name = STRING : [16] CH2",\
"",\
"[pulse5]",\
"time offset (ms) = DOUBLE : 7000",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 4000",\
"ppg signal name = STRING : [16] CH5",\
"",\
"[pulse7]",\
"time offset (ms) = DOUBLE : 2",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 300",\
"ppg signal name = STRING : [16] CH7",\
"",\
"[pulse16]",\
"time offset (ms) = DOUBLE : 100",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 5",\
"ppg signal name = STRING : [16] CH16",\
"",\
"[pulse17]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 20",\
"ppg signal name = STRING : [16] CH17",\
"",\
"[begin_wait]",\
"loop count = INT : 1",\
"time reference = STRING : [20] T0",\
"time offset (ms) = DOUBLE : 12000",\
"",\
"[pulse11]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGWAIT",\
"pulse width (ms) = DOUBLE : 20",\
"ppg signal name = STRING : [16] CH11",\
"",\
"[pulse12]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TBEGWAIT",\
"pulse width (ms) = DOUBLE : 100",\
"ppg signal name = STRING : [16] CH12",\
"",\
"[end_wait]",\
"time offset (ms) = DOUBLE : 0",\
"",\
"[pulse8]",\
"time offset (ms) = DOUBLE : 100",\
"time reference = STRING : [20] _TENDWAIT",\
"pulse width (ms) = DOUBLE : 50",\
"ppg signal name = STRING : [16] CH8",\
"",\
"[pulse3]",\
"time offset (ms) = DOUBLE : 110",\
"time reference = STRING : [20] _TENDWAIT",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH3",\
"",\
"[pulse9]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TENDWAIT",\
"pulse width (ms) = DOUBLE : 100",\
"ppg signal name = STRING : [16] CH9",\
"",\
"[pulse13]",\
"time offset (ms) = DOUBLE : 0",\
"time reference = STRING : [20] _TENDWAIT",\
"pulse width (ms) = DOUBLE : 10",\
"ppg signal name = STRING : [16] CH13",\
"",\
"[skip]",\
"channel = INT : 11",\
"",\
"[image]",\
"name = STRING : [32] none",\
"mode = STRING : [8] 1g",\
"num channels to display = INT : 16",\
"max active channel = INT : 17",\
"",\
"[names]",\
"names = STRING[17] :",\
"[64] anode (e- gate)",\
"[64] G2(inj)",\
"[64] G1(ext)",\
"[64] ch4",\
"[64] FC",\
"[64] ch6",\
"[64] function generator",\
"[64] Camera",\
"[64] ch9",\
"[64] ch10",\
"[64] loop",\
"[64] delay",\
"[64] ch13",\
"[64] ch14",\
"[64] ch15",\
"[64] scope trig",\
"[64] ",\
"",\
"[pulse14]",\
"time offset (ms) = DOUBLE : 5",\
"time reference = STRING : [20] T0",\
"pulse width (ms) = DOUBLE : 1",\
"ppg signal name = STRING : [16] CH14",\
"",\
NULL }

#endif

#ifndef EXCL_TRIGGER

#define TRIGGER_COMMON_DEFINED

typedef struct {
  WORD      event_id;
  WORD      trigger_mask;
  char      buffer[32];
  INT       type;
  INT       source;
  char      format[8];
  BOOL      enabled;
  INT       read_on;
  INT       period;
  double    event_limit;
  DWORD     num_subevents;
  INT       log_history;
  char      frontend_host[32];
  char      frontend_name[32];
  char      frontend_file_name[256];
} TRIGGER_COMMON;

#define TRIGGER_COMMON_STR(_name) const char *_name[] = {\
"[.]",\
"Event ID = WORD : 1",\
"Trigger mask = WORD : 0",\
"Buffer = STRING : [32] SYSTEM",\
"Type = INT : 2",\
"Source = INT : 0",\
"Format = STRING : [8] MIDAS",\
"Enabled = BOOL : y",\
"Read on = INT : 1",\
"Period = INT : 50",\
"Event limit = DOUBLE : 0",\
"Num subevents = DWORD : 0",\
"Log history = INT : 0",\
"Frontend host = STRING : [32] lxcpet.triumf.ca",\
"Frontend name = STRING : [32] feSis",\
"Frontend file name = STRING : [256] fesis.c",\
"",\
NULL }

#endif

