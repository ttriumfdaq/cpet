/* function prototypes */
#ifndef _PPGCODE_INCLUDE_H_
#define _PPGCODE_INCLUDE_H_

#ifdef PPG_CODE 
  INT ppg_load(char *ppgfile);
  INT init_ppg(BOOL load_ppg);
  INT setup_ppg(void);
  INT modify_ppg_loadfile(char *ppgmode);
  INT check_PPG_input_params(void);
  void check_ppg_running(void);
  void ppg_stop(void);
#ifdef NEW_PPG
void ppg_get_clock_info(BOOL* ext_clock, BOOL* good_ext_clock);
INT set_output_clock_keys(BOOL ext_clock, BOOL good_ext_clock);
#endif // NEW_PPG
#endif
#endif
