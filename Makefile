#####################################################################
#
#  Name:         Makefile
#  Created by:   Stefan Ritt
#
#  Contents:     Makefile for MIDAS example frontend and analyzer
#
#  $Id: Makefile 3655 2007-03-21 20:51:28Z amaudruz $
#
#####################################################################
#
#--------------------------------------------------------------------
LXHOST=lxcpet
lxhost=lxcpet.triumf.ca
ifneq ($(HOST),$(LXHOST))
ifneq ($(HOST),$(lxhost))
wronghost::
	@echo "...";
	@echo "Wrong host ($(HOST)) !  Use $(lxhost)";
	@echo "...";
endif
endif

# The MIDASSYS should be defined prior the use of this Makefile
ifndef MIDASSYS
missmidas::
	@echo "...";
	@echo "Missing definition of environment variable 'MIDASSYS' !";
	@echo "...";
endif

#--------------------------------------------------------------------
# The following lines contain specific switches for different UNIX
# systems. Find the one which matches your OS and outcomment the 
# lines below.

#-----------------------------------------
# This is for Linux
ifeq ($(OSTYPE),Linux)
OSTYPE = linux
endif

ifeq ($(OSTYPE),linux)

OS_DIR = linux-m32
OSFLAGS = -DOS_LINUX -Dextname
CFLAGS = -m32 -g -Wall
LIBS = -lm -lz -lutil -lnsl -lpthread -lrt
endif

#-----------------------
# MacOSX/Darwin is just a funny Linux
#
ifeq ($(OSTYPE),Darwin)
OSTYPE = darwin
endif

ifeq ($(OSTYPE),darwin)
OS_DIR = darwin
FF = cc
OSFLAGS = -DOS_LINUX -DOS_DARWIN -DHAVE_STRLCPY -DAbsoftUNIXFortran -fPIC -Wno-unused-function
LIBS = -lpthread
SPECIFIC_OS_PRG = $(BIN_DIR)/mlxspeaker
NEED_STRLCPY=
NEED_RANLIB=1
NEED_SHLIB=
NEED_RPATH=

endif

#-----------------------------------------
# ROOT flags and libs (analyzer)
#
ifdef ROOTSYS
ROOTCFLAGS := $(shell  $(ROOTSYS)/bin/root-config --cflags)
ROOTCFLAGS += -DHAVE_ROOT -DUSE_ROOT
ROOTLIBS   := $(shell  $(ROOTSYS)/bin/root-config --libs) -Wl,-rpath,$(ROOTSYS)/lib
ROOTLIBS   += -lThread
endif
#else
#missroot:
#	@echo "...";
#	@echo "Missing definition of environment variable 'ROOTSYS' !";
#	@echo "...";
#endif
#-------------------------------------------------------------------
# The following lines define directories. Adjust if necessary
#                 
DRV_DIR   = $(MIDASSYS)/drivers/vme
INC_DIR   = $(MIDASSYS)/include
LIB_DIR   = $(MIDASSYS)/$(OS_DIR)/lib
SRC_DIR   = $(MIDASSYS)/src
VME_DIR   = /home/olchansk/daq/vmisft-7433-3.4-KO1/vme_universe
PPG_DIR    = ./
#-------------------------------------------------------------------
# List of analyzer modules
#
#MODULES   = adccalib.o adcsum.o scaler.o
MODULES =
#-------------------------------------------------------------------
# Hardware driver can be (camacnul, kcs2926, kcs2927, hyt1331)
#
DRIVER = vmeio
#-------------------------------------------------------------------
# Frontend code name defaulted to frontend in this example.
# comment out the line and run your own frontend as follow:
# gmake UFE=my_frontend
#
DEFINES = -D CPET -D PPG_CODE -D NEW_PPG
CFLAGS += $(DEFINES)
UFE = fecpet

####################################################################
# Lines below here should not be edited
####################################################################

# MIDAS library
LIB = $(LIB_DIR)/libmidas.a -lvme
# compiler
CC = gcc
CXX = g++
CFLAGS += -g -I. -I$(INC_DIR) -I$(DRV_DIR) -I$(DRV_DIR)/vmic -I$(VME_DIR)/include -L$(VME_DIR)/lib
LDFLAGS +=

all: $(UFE)  ppg_update
## for now not using  ppg_modify_file.o
$(UFE): $(LIB) $(LIB_DIR)/mfe.o vmicvme.o $(UFE).c \
	newppg.o ppg_code.o common.o  $(SRC_DIR)/cnaf_callback.c

	$(CC) $(CFLAGS) $(OSFLAGS) -o $(UFE) $(UFE).c \
	vmicvme.o newppg.o  ppg_code.o common.o \
	$(LIB_DIR)/mfe.o $(LIB) \
	$(LDFEFLAGS) $(LIBS)

ppg_update:  $(LIB) vmicvme.o  ppg_update.c \
	newppg.o common.o $(SRC_DIR)/cnaf_callback.c

	$(CC) $(CFLAGS) $(OSFLAGS) -o ppg_update ppg_update.c \
	vmicvme.o newppg.o common.o \
	$(LIB) \
	$(LDFEFLAGS) $(LIBS)



vmicvme.o: $(DRV_DIR)/vmic/vmicvme.c
	$(CC) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

vmeio.o: $(DRV_DIR)/vmeio.c
	$(CC) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

newppg.o: $(PPG_DIR)/newppg.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<

common.o: $(PPG_DIR)/common.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<

ppg_code.o: $(PPG_DIR)/ppg_code.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<

#v792.o: $(DRV_DIR)/v792.c
#	$(CC) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

#vf48.o: $(DRV_DIR)/vf48.c
#	$(CC) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

#v1729.o: $(DRV_DIR)/v1729.c
#	$(CC) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

# $(DRIVER).o: $(DRV_DIR)/$(DRIVER).c
# 	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $(DRIVER).o $(DRV_DIR)/$(DRIVER).c

#analyzer: $(LIB) $(LIB_DIR)/rmana.o analyzer.o $(MODULES)
#	$(CXX) $(CFLAGS) -o $@ $(LIB_DIR)/rmana.o analyzer.o $(MODULES) \
#	$(LIB) $(LDFLAGS) $(ROOTLIBS) $(LIBS)

%.o: %.c experim.h
	$(CC) $(USERFLAGS) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

clean::
	rm -f *.o *~ \#*

#end file
