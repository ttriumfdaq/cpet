#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
# Check that the keys in the default load files exist
# 
# Run the check like this:
#     check_file.pl <expt>  <filename>
#     e.g. check_file.pl bnmr 1e_defaults.odb
#
#  see AAA_README in this directory for more information
#
# $Log: check_file.pl,v $
# Revision 1.1  2005/09/27 19:40:31  suz
# original - checks load files for mode defaults
#
#
#
my $name="check_file";
my @cmd_array;

our ($expt, $file ) = @ARGV;
my $cmd;
unless ($expt)
{
    print "FAILURE: experiment not supplied\n"; 
    die  "e.g  check_file.pl bnmr 2e_defaults.odb \n";
}
unless ($file)
{
    print "FAILURE: filename not supplied\n"; 
    die  "e.g  check_file.pl bnmr 2e_defaults.odb \n";
}
unless (-e $file)
{
    die "FAILURE: non-existent file \"$file\"\n"; 
}
my $base="/home/cpet/online/ppg/perl";

$cmd="$base/mode_check.pl $base $expt /home/$expt/online/modefiles/$file $expt";

print "$name cmd=\"$cmd\"\n";
unless (open (MFC,"$cmd |"))
{
 print "$name: cannot open filehandle MFC for command: $cmd ($!)\n";
 die "$name: cannot execute $cmd";
}

@cmd_array=<MFC>;       # get the output from the command
close (MFC);
print @cmd_array;
exit;
