#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "midas.h"
#include "mvmestd.h" 
#include "vmicvme.h"
#include "experim.h"
#include "common.h"

#ifdef PPG_CODE
#include "ppg_code.h" // prototypes
#include "newppg.h"
#endif

MVME_INTERFACE *myvme;
DWORD PPG_BASE = 0x00100000;
TITAN_ACQ_SETTINGS tas;
TITAN_ACQ_SETTINGS_STR(titan_acq_settings_str); 
BOOL ppg_running;
HNDLE hDB;
HNDLE hOut, hIn;
char ppgfile[128];
BOOL   debug;
BOOL ppg_external_trig;
BOOL ppg_external_clock;
//DWORD ppg_polarity_mask;

BOOL toggle_clock, toggle_trigger;
BOOL ext_clock_alarm=0;


INT run_state;

// prototypes
INT set_ppg_alarm(void);
INT clear_all_alarms(void);

int main (int argc, char **argv)
{
  INT i,status,size; 
  char str[128];
  char   host_name[HOST_NAME_LENGTH], expt_name[HOST_NAME_LENGTH];
  HNDLE  hKey;
  BOOL   clear=0;

#ifndef NEW_PPG
  printf ("This code is to be used with the PPG32 (new PPG)\n");
  cm_msg(MERROR,"ppg_update","This code is to be used with the PPG32 (new PPG)");
  goto error;
#else
	 
  cm_get_environment (host_name, HOST_NAME_LENGTH, expt_name, HOST_NAME_LENGTH);


#ifndef PPG_CODE
  printf ("PPG_CODE must be defined in Makefile\n");
  goto error;
#endif

  // Open VME interface 
  status = mvme_open(&myvme, 0);

  
  /* connect to experiment */
  printf("calling cm_connect_experiment with host_name \"%s\", expt_name \"%s\" and client name = \"ppg_update\"\n",
	 host_name,expt_name);
  status = cm_connect_experiment(host_name, expt_name, "ppg_update", 0);
  if (status != CM_SUCCESS)
    {
      cm_msg(MERROR,"ppg_update","Could not connect to  hostname %s and expt_name %s\n",host_name,expt_name);
      goto error;
    }
  // printf("Successfully connected to experiment %s\n",expt_name);
  
  /* turn off message display, turn on message logging */
  cm_set_msg_print(MT_ALL, 0, NULL);
  
  /* connect to the database */
  cm_get_experiment_database(&hDB, &hKey);
  
  // get the run state
  size = sizeof(run_state);
  status = db_get_value(hDB, 0, "/Runinfo/state",
			&run_state, &size, TID_INT, 0);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"ppg_update","error getting \"/Runinfo/state\" (%d)",status);
      goto error;
    }
  
  
  // Check the run is stopped
  if (run_state != STATE_STOPPED)
    {
      cm_msg(MERROR,"ppg_update","cannot run ppg_update unless run is stopped");
      goto error;
    }
  
  toggle_trigger=toggle_clock=0;
  /* parse command line parameters */
  //	 printf("argc = %d\n",argc);
  
  for (i=1 ; i<argc ; i++)
    {
      
      if (argv[i][0] == '-' && argv[i][1] == 'c')
        toggle_clock=1;
      if (argv[i][0] == '-' && argv[i][1] == 't') 
        toggle_trigger=1;    
      if (argv[i][0] == '-' && argv[i][1] == 'h')
	goto usage;
    }
  
  printf("Toggle_trigger: %d\n",toggle_trigger);
  printf("Toggle_clock:   %d\n",toggle_clock);
  
  sprintf(str,"/Equipment/Titan_Acq/Settings/ppg/output");
  status = db_find_key (hDB, 0, str, &hOut); 
  if (status != DB_SUCCESS)
    { 
      cm_msg(MERROR,"ppg_update","Key %s not found", str); 
      goto error;
    }
  sprintf(str,"/Equipment/Titan_Acq/Settings/ppg/input");
  status = db_find_key (hDB, 0, str, &hIn); 
  if (status != DB_SUCCESS)
    { 
      cm_msg(MERROR,"ppg_update","Key %s not found", str); 
      goto error;
    }
  
  clear_all_alarms();

  // by default, external clock is bad until we read it back...
  size=sizeof(clear);
  db_set_value(hDB, hOut, "good external clock", &clear, 
			size, 1, TID_BOOL); 
  
  if(status !=SUCCESS)
    cm_msg(MERROR,"ppg_update","Cannot clear key \"/Equipment/TITAN_ACQ/Settings/ppg/output/good external clock\" (%s)",status);




  // Stop the PPG
  TPPGDisableExtTrig(  myvme, PPG_BASE); // stop new external triggers from restarting the PPG
  TPPGReset(myvme,   PPG_BASE); // Halt pgm and stops (even if in long delay)
  ppg_running = 0;
  size = sizeof(ppg_running); 
  
  status = db_set_value(hDB, hOut, "PPG running", &ppg_running, sizeof(ppg_running), 1, TID_BOOL); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"ppg_update","cannot clear \"PPG_running\" flag (%d)",status); 
      goto error;
    }
  
  status = setup_ppg();
  if (status != SUCCESS)
    {
      set_ppg_alarm(); 
      goto error;
    }
  
  if(toggle_trigger)
    cm_msg(MINFO,"ppg_update","Successfully toggled trigger"); 
  else if(toggle_clock)
    cm_msg(MINFO,"ppg_update","Successfully toggled clock"); 
  else
    cm_msg(MINFO,"ppg_update","Success... status updated"); 

  goto exit;
  
 usage:
  printf("usage: ppg_update  [-c ] [-t ]\n");
  printf(" updates PPG according to ODB parameters for internal or external clock and trigger\n");
  printf("   -c toggles clock before updating \n");
  printf("   -t toggles trigger before updating \n");
  goto exit;
  
  
  
 error:
  printf("\n Error detected. Check odb messages for details\n");


 exit:
  cm_disconnect_experiment();
#endif // NEW PPG
  return status;
}



INT setup_ppg(void)
{
  // set up PPG with internal/external trigger and internal/external clock
#ifdef NEW_PPG
  INT status,size; 
  char path_string[]="/Equipment/Titan_Acq/Settings/ppg";
  BOOL ext_clock, good_ext_clock;

  //sprintf(path_string, "/Equipment/Titan_Acq/Settings/ppg");

#ifdef PPG_CODE

  printf ("setup_ppg : ODB Parameters to control PPG: \n");


  size=sizeof(ppg_external_trig);
  status = db_get_value(hDB, hIn, "external trigger",
                                 &ppg_external_trig  , &size, TID_BOOL, 0);
  if(status != SUCCESS)
    {
      cm_msg(MERROR, "ppg_update", "Cannot get value from key \"%s/input/ppg/external trig\" (%d)",path_string,status);
      return status;
    }

  size=sizeof(ppg_external_clock);
  status = db_get_value(hDB, hIn, "external clock",
                                 &ppg_external_clock  , &size, TID_BOOL, 0);
  if(status != SUCCESS)
    {
      cm_msg(MERROR, "ppg_update", "Cannot get value from key \"%s/input/ppg/external clock\" (%d)",path_string,status);
      return status;
    }

  printf("Input settings:\n");
  printf("PPG external start:   %d\n",ppg_external_trig);
  printf("PPG external clock:   %d\n",ppg_external_clock);

  if(toggle_trigger)
    {
      if(ppg_external_trig)
	ppg_external_trig=0;
      else
	ppg_external_trig=1;

       printf("PPG external start after toggle:   %d\n",ppg_external_trig);
       size=sizeof(ppg_external_trig);
       status = db_set_value(hDB, hIn, "external trigger",
			     &ppg_external_trig  , size, 1,TID_BOOL);
       if(status != SUCCESS)
	 {
	   cm_msg(MERROR, "ppg_update", "Cannot set key \"%s/input/ppg/external trig\" to %d (%d)",path_string,ppg_external_trig,status);
	   return status;
	 }
    }

  if(toggle_clock)
    {
      if(ppg_external_clock)
	ppg_external_clock=0;
      else
	ppg_external_clock=1;

       printf("PPG external clock after toggle:   %d\n",ppg_external_clock);
       size=sizeof(ppg_external_clock);
       status = db_set_value(hDB, hIn, "external clock",
			     &ppg_external_clock  , size, 1,TID_BOOL);
       if(status != SUCCESS)
	 {
	   cm_msg(MERROR, "ppg_update", "Cannot set key \"%s/input/ppg/external clock\" to %d (%d)",path_string,ppg_external_clock,status);
	   return status;
	 }

    }


  if(ppg_external_trig)
    {
      printf("Setting PPG with External Start... ");
      status = TPPGEnableExtTrig(  myvme, PPG_BASE);
    }
  else
    {
      printf("Setting PPG with Internal Start... ");
      status = TPPGDisableExtTrig(  myvme, PPG_BASE);
    }
  if(status != SUCCESS)
    {
       cm_msg(MERROR,"ppg_update","Error setting PPG Internal/External Start mode");
       return status;
    }

  ppg_get_clock_info(&ext_clock, &good_ext_clock); // read status register
  
  if(ppg_external_clock)
    { // We want external clock
      if (ext_clock)
	{
	  if (good_ext_clock)            
	      printf("PPG is already set up with External Clock\n");
	  else
	    {
              cm_msg(MERROR,"ppg_update","Cannot set PPG External Clock");
	      cm_msg(MINFO,"ppg_update", "External Clock is BAD. Check that it is plugged in and is active");
	      set_output_clock_keys(ext_clock,good_ext_clock);
	      return FAILURE;
	    }
	}
      else
	{ // toggle clock and set divide-down (call with TRUE)
	  status = TPPGToggleExtClock(myvme, PPG_BASE, TRUE); 
          if(status != SUCCESS)
	    {
	      cm_msg(MERROR,"ppg_update","Cannot set PPG External Clock; TPPGToggleExtClock returns FAILURE");
	      set_output_clock_keys(ext_clock,good_ext_clock);	      
	      return status;
	    }
	}
    } // end of want ppg_external_clock
  else
    { // We want internal clock
      if (ext_clock)
	{ // External clock is set; toggle clock and set divide-down
	  status = TPPGToggleExtClock(myvme, PPG_BASE, TRUE); 
          if(status != SUCCESS)
	    {
	      cm_msg(MERROR,"ppg_update","Cannot set PPG Internal Clock; TPPGToggleExtClock returns FAILURE");
	      set_output_clock_keys(ext_clock,good_ext_clock);	
	      return status;
	    }
	}
      else
	printf("PPG is already set up with Internal Clock\n");
    }
      
  
  ppg_get_clock_info(&ext_clock, &good_ext_clock); // read status register
  set_output_clock_keys(ext_clock,good_ext_clock); // write info to output area	

  printf("setup_ppg: returning SUCCESS\n\n");

#endif // PPG_CODE
#endif // NEW_PPG
  return SUCCESS;

}

void ppg_get_clock_info(BOOL* ext_clock, BOOL* good_ext_clock)
{
  DWORD data;

  // Read the state of the ppg clock from the status register
  printf("ppg_get_clock_info: Reading Status Register of PPG...\n");
  data =  TPPGStatusRead( myvme, PPG_BASE); // displays status of clock and trigger
  if (data & 0x10000) 
       *ext_clock=1;
  else
       *ext_clock=0;

  if (data  & 0x20000)
       *good_ext_clock=1;
  else
       *good_ext_clock=0;
  
  return;
}



INT set_output_clock_keys(BOOL ext_clock, BOOL good_ext_clock)
{
  INT status,size;

  size=sizeof(BOOL);
  status = db_set_value(hDB, hOut, "external clock", &ext_clock, 
			size, 1, TID_BOOL); 
  if(status !=SUCCESS)
    cm_msg(MERROR,"ppg_update","Cannot set key \"/Equipment/TITAN_ACQ/Settings/ppg/output/external clock\" (%s)",status);
  
  status = db_set_value(hDB, hOut, "good external clock", &good_ext_clock, 
			size, 1, TID_BOOL); 
  
  if(status !=SUCCESS)
    cm_msg(MERROR,"ppg_update","Cannot set key \"/Equipment/TITAN_ACQ/Settings/ppg/output/good external clock\" (%s)",status);
  

  if (!good_ext_clock && ext_clock)
    ext_clock_alarm=1;   // global
  else
    ext_clock_alarm=0;

    status = db_set_value(hDB, hOut, "alarms/external clock alarm", &ext_clock_alarm, 
			size, 1, TID_BOOL); 
  if(status !=SUCCESS)
    cm_msg(MERROR,"ppg_update","Cannot set key \"/Equipment/TITAN_ACQ/Settings/ppg/output/alarms/external clock alarm\" (%s)",status);

  return status;
}


INT set_ppg_alarm(void)
{
  BOOL   set=1;
  INT status,size;

  printf("set_ppg_alarm: ext_clock_alarm= %d\n",ext_clock_alarm);
  if (!ext_clock_alarm)  // (global) alarm for particular case 
    {
      size=sizeof(set);
       status = db_set_value(hDB, hOut, "alarms/ppg status alarm", &set, 
			size, 1, TID_BOOL); 
       if(status !=SUCCESS)
	 cm_msg(MERROR,"ppg_update","Cannot set key \"/Equipment/TITAN_ACQ/Settings/ppg/output/alarms/ppg status alarm\" (%s)",status);
       return status;
    }
  return SUCCESS;
}

INT clear_all_alarms(void)
{
  BOOL clear=0;
  INT status,size;

  size=sizeof(clear);
  status = db_set_value(hDB, hOut, "alarms/ppg status alarm", &clear, 
			size, 1, TID_BOOL); 
  if(status !=SUCCESS)
    cm_msg(MERROR,"ppg_update","Cannot clear  \"/Equipment/TITAN_ACQ/Settings/ppg/output/alarms/ppg status alarm\" (%s)",status);


  status = db_set_value(hDB, hOut, "alarms/external clock alarm", &clear, 
			size, 1, TID_BOOL); 
  if(status !=SUCCESS)
    cm_msg(MERROR,"ppg_update","Cannot clear \"/Equipment/TITAN_ACQ/Settings/ppg/output/alarms/external clock alarm\" (%s)",status);

  return status;
}


