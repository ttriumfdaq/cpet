#!/bin/csh
#
# kill-all,sh
#
# version for cpet experiment
#
# kill all DAQ tasks

# Host based
#if ( $HOST =~ "lxcpet"* ) then

if (($HOST != $DAQ_HOST) && ($HOST != $daq_host)) then
   echo "run kill-all from $HOST only "
   exit
endif




sleep 2
#turn off alarm system
odb -c 'set "/Alarms/alarm system active" n'
# shutdown fecpet
odb -c 'sh fecpet'

# stop MIDAS tasks
killall -9 mhttpd
killall mstat
killall odb
killall mserver
killall tri_config
killall mlxspeaker
killall mlogger
killall analyzer
killall roody
killall lazylogger

if ( $HOST =~ "titan04"* ) then
ssh lxcpet ~/online/bin/stop_fecpet
endif


# clean ODB
odb  -c cleanup
odb  -c stop

#

