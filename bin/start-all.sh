#!/bin/sh
# Host based
# make sure this is being run from $DAQ_HOST (defined in .cshrc)
# 
if [ -z "$DAQ_HOST" ]; then
    echo "Environment variable DAQ_HOST must be set in .cshrc for data acquisition"
    exit 1
fi
echo  $HOST > ~/temp
grep --silent $DAQ_HOST ~/temp
if [ "$?" != "0" ] ; then 
   echo "You must be logged onto DAQ_HOST ($DAQ_HOST) to run this file (NOT $HOST)"
   exit 2;
fi
rm ~/temp

if [ -z "$MIDASSYS" ]; then
    echo "Environment variable MIDASSYS must be set in .cshrc for data acquistion"
    exit 1
fi

odbedit -c clean
rm -f ~/temp
ps -ef > ~/temp
grep --silent "mserver -p 7073" ~/temp
if [ "$?" != "0" ];  then
    echo "Starting mserver on port 7073 (64-bit version) "
    $MIDASSYS/linux/bin/mserver -p 7073 -D
else
 echo "mserver is already running"
fi
rm ~/temp

# Start the http midas server
ps -ef > ~/temp
grep --silent "mhttpd -p 8081" ~/temp
if [ "$?" != "0" ] ; then
    echo "Starting mhttpd on port 8081"
     $MIDASSYS/linux/bin/mhttpd -p 8081 -D
else
 echo mhttpd is already running
fi
rm ~/temp

sleep 2
#xterm -e ./frontend &
#xterm -e ./analyzer &

# The  midas logger
# Start the logger
$MIDASSYS/linux/bin/odbedit -c scl | grep --silent  Logger
if [ "$?" != "0" ] ; then
    echo "Starting mlogger"
    $MIDASSYS/linux/bin/mlogger -D
  else
 echo mlogger is already running
fi

# start tri_config as a daemon
$MIDASSYS/linux/bin/odbedit -c scl | grep --silent  tri_config
if [ "$?" != "0" ] ; then
    echo "Starting tri_config with plot"
    ~/online/ppg/tri_config -p -D
  else
 echo tri_config is already running
fi

# start the frontend program via remote login on VMIC
ssh lxcpet ~/online/bin/start_fecpet
echo running start_fecpet on lxcpet to start frontend if not running



#turn on alarm system
odb -c 'set "/Alarms/alarm system active" y'

echo Please point your web browser to http://localhost:8081
#echo To look at live histograms, run: roody -Hlocalhost
#end file
