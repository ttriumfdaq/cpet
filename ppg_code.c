/* ppg_code.c


*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>     /* gmtime(),strftime() */
#include <ctype.h>
#include "midas.h"
#include "vmicvme.h"
#include "experim.h"

#ifdef PPG_CODE
#include "ppg_code.h" // prototypes
#ifdef NEW_PPG
#include "newppg.h"
#else
#include "vppg.h"
#endif
#endif

extern MVME_INTERFACE *myvme;
extern DWORD PPG_BASE;
extern TITAN_ACQ_SETTINGS tas;
extern BOOL ppg_running;
extern HNDLE hDB;
extern HNDLE hOut;
extern char ppgfile[128];
extern BOOL   debug;
extern BOOL ppg_external_trig;
extern BOOL ppg_external_clock;
extern DWORD ppg_polarity_mask;

BOOL ext_clock_alarm=0;

// prototypes
INT set_ppg_alarm(void);
INT clear_all_alarms(void);


INT  init_ppg(BOOL load_ppg)
{
  INT size,status;
#ifdef PPG_CODE
#ifdef NEW_PPG
  DWORD data;
  BOOL clear=0;
#else
  BYTE data;
#endif

  printf ("\ninit_ppg: setting up PPG...\n");
  /* Stop the PPG sequencer  */ 
#ifdef NEW_PPG
  TPPGDisableExtTrig(  myvme, PPG_BASE); // stop new external triggers from restarting the PPG
  TPPGReset(myvme,   PPG_BASE); // Halt pgm and stops (even if in long delay)
#else
  VPPGStopSequencer(myvme,   PPG_BASE); 
#endif
  ppg_running = FALSE; // not running 
  size = sizeof(ppg_running); 
 
  status = db_set_value(hDB, hOut, "PPG running", &ppg_running, sizeof(ppg_running), 1, TID_BOOL); 
  if(status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"init_ppg","cannot clear \"PPG_running\" flag (%d)",status); 
      return status;
    }

#ifdef NEW_PPG  
  clear_all_alarms();

  // by default, external clock is bad until we read it back...
  size=sizeof(clear);
  db_set_value(hDB, hOut, "good external clock", &clear, 
			size, 1, TID_BOOL); 
  
  if(status !=SUCCESS)
    cm_msg(MERROR,"ppg_update","Cannot clear key \"/Equipment/TITAN_ACQ/Settings/ppg/output/good external clock\" (%s)",status);



  TPPGInit( myvme,  PPG_BASE, 0); // internal trig,  set pol mask to zero, PC back to zero, sets outputs to zero  clears TEST MODE bit. Does not change ext/internal clock.
#else
  VPPGInit( myvme,  PPG_BASE); // PPG controls POL output (helicity), disable external trig, beam off (VME control)
  VPPGBeamOff(myvme,  PPG_BASE);
#endif

  if(!load_ppg)
    { // ppg is not to be loaded - this routine has been called from frontend_init
      // see if we can read something from the PPG 
#ifdef NEW_PPG
      data = TPPGStatusRead(myvme,PPG_BASE); // prints out status
   BOOL ext_clock,good_ext_clock;
  ppg_get_clock_info(&ext_clock, &good_ext_clock); // read status register
  printf("after ppg_get_clock_info  ext_clock = %d and good_ext_clock = %d\n",
	 ext_clock,good_ext_clock);
 set_output_clock_keys(ext_clock,good_ext_clock);
#else 
      data = VPPGStatusRead(myvme,PPG_BASE); // prints out status
      data = VPPGExtTrigRegRead(myvme, PPG_BASE); // data=0 enabled or 1=disabled
   


#endif // NEW_PPG 
      printf("init_ppg: success: returning without loading PPG\n");
      return SUCCESS;  // no need to load PPG from frontend_init
    }


 
  // Load the PPG file
  if(ppg_load(ppgfile) != SUCCESS) 
    return FE_ERR_HW;
  printf("init_ppg: PPG file loaded successfully\n");

  ss_sleep(1000);
#ifdef NEW_PPG
  data =  TPPGStatusRead( myvme, PPG_BASE); // reads status of clock and trigger
#endif

  ss_sleep(1000);

  status = setup_ppg(); // set up int/ext trig, clock
  if(status != SUCCESS)
    {
      set_ppg_alarm(); 
      return status;
    }
      
#ifdef NEW_PPG
  data =  TPPGStatusRead( myvme, PPG_BASE); // writes status of clock and trigger
#else
  data =  VPPGStatusRead( myvme, PPG_BASE);
#endif // NEW_PPG

#endif // PPG_CODE
  return SUCCESS;
}


INT setup_ppg(void)
{
  // set up PPG with internal/external trigger and start
  DWORD rpol,status; //pol 
  BOOL ext_clock, good_ext_clock;
  
#ifdef PPG_CODE
  
  
  printf ("setup_ppg : ODB Parameters to control PPG: \n");
#ifdef MPET // MPET
  ppg_external_trig =  tas.ppg.input.ppg_external_start;
  ppg_polarity_mask =  tas.ppg.input.ppg_polarity;
#ifdef NEW_PPG
  ppg_external_clock = tas.ppg.input.ppg_external_clock;  // want external clock 
#endif // NEW_PPG
  
#else // EBIT or CPET
  ppg_external_trig =  tas.ppg.input.external_trigger;
  ppg_polarity_mask =  tas.ppg.input.polarity;
#ifdef NEW_PPG
  ppg_external_clock = tas.ppg.input.external_clock;
#endif // NEW_PPG
#endif // EBIT or CPET
  
  printf("PPG polarity mask:    %d (0x%x)\n",ppg_polarity_mask,ppg_polarity_mask);
  printf("PPG external start:   %d\n",ppg_external_trig);
  printf("PPG external clock:   %d\n",ppg_external_clock);
  
  
#ifdef NEW_PPG
    
  if(ppg_external_trig)
    {
      printf("Setting PPG with External Start...\n");
      status = TPPGEnableExtTrig(  myvme, PPG_BASE);
    }
  else
    {
      printf("Setting PPG with Internal Start... ");
      status = TPPGDisableExtTrig(  myvme, PPG_BASE);
    }
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"setup_ppg","Error setting PPG Internal/External Start mode");
      return status;
    }
  


  ppg_get_clock_info(&ext_clock, &good_ext_clock); // read status register
  printf("after ppg_get_clock_info  ext_clock = %d and good_ext_clock = %d\n",
	 ext_clock,good_ext_clock);
  
  printf("ppg_external_clock= %d ext_clock=%d good_ext_clock=%d\n",
	 ppg_external_clock,ext_clock,  good_ext_clock);

  if(ppg_external_clock)
    { // We want external clock
      if (ext_clock)
	{
	  if (good_ext_clock)            
	      printf("PPG is already set up with External Clock\n");
	  else
	    {
              printf("setup_ppg: Cannot set PPG External Clock, External Clock is BAD. Check that it is plugged in and is active\n");
              cm_msg(MERROR,"setup_ppg","Cannot set PPG External Clock");
	      cm_msg(MINFO,"setup_ppg", "External Clock is BAD. Check that it is plugged in and is active");
	      set_output_clock_keys(ext_clock,good_ext_clock);
	      return FAILURE;
	    }
	}
      else
	{ // toggle clock and set divide-down (call with TRUE)
	  status = TPPGToggleExtClock(myvme, PPG_BASE, TRUE); 
          if(status != SUCCESS)
	    {
	      cm_msg(MERROR,"setup_ppg","Cannot set PPG External Clock; TPPGToggleExtClock returns FAILURE");
	      set_output_clock_keys(ext_clock,good_ext_clock);	      
	      return status;
	    }
	}
    } // end of want ppg_external_clock
  else
    { // We want internal clock
      if (ext_clock)
	{ // External clock is set; toggle clock and set divide-down
	  status = TPPGToggleExtClock(myvme, PPG_BASE, TRUE); 
          if(status != SUCCESS)
	    {
	      cm_msg(MERROR,"setup_ppg","Cannot set PPG Internal Clock; TPPGToggleExtClock returns FAILURE");
	      set_output_clock_keys(ext_clock,good_ext_clock);	
	      return status;
	    }
	}
      else
	printf("PPG is already set up with Internal Clock\n");
    }
      
  
  ppg_get_clock_info(&ext_clock, &good_ext_clock); // read status register
  //  printf("after ppg_get_clock_info  ext_clock = %d and good_ext_clock = %d\n",
  //	 ext_clock,good_ext_clock);

  set_output_clock_keys(ext_clock,good_ext_clock); // write info to output area	


  // Polarity
  
  printf("setup_ppg: Writing PPG polarity mask: 0x%x\n",ppg_polarity_mask);
  rpol = TPPGPolmskWrite( myvme, PPG_BASE , ppg_polarity_mask); 

  
#else  // OLD PPG
  BYTE data;

  // make sure PPG controls these outputs
    VPPGPolzCtlPPG( myvme, PPG_BASE);
    VPPGBeamCtlPPG( myvme, PPG_BASE);
 
  if(ppg_external_trig)
    {
      printf("Setting PPG with External Trigger\n");
      data = VPPGEnableExtTrig(  myvme, PPG_BASE);
    }
  else
    {
      printf("Setting PPG with Internal Trigger\n");
      data = VPPGDisableExtTrig ( myvme, PPG_BASE );
    }
  
  printf("PPG trigger control reg : 0x%x   (0=External trigger 1=Internal)\n",data);
  //printf("setup_ppg: Writing PPG polarity mask: 0x%x\n",pol);
  rpol = VPPGPolmskWrite( myvme, PPG_BASE , ppg_polarity_mask); 
#endif

  //printf("setup_ppg: Read back PPG polarity mask: 0x%x\n",rpol); 
  if (rpol !=ppg_polarity_mask )
    {
      cm_msg(MERROR,"setup_ppg","PPG Polarity mask Error: wrote 0x%x, read back 0x%x",
	    ppg_polarity_mask ,rpol);
      return FE_ERR_HW;
    }
  else
    cm_msg(MINFO,"setup_ppg","PPG Outputs Polarity mask: 0x%x",ppg_polarity_mask);

  printf("setup_ppg: returning SUCCESS\n\n");

#endif // PPG_CODE
  return SUCCESS;

}
#ifdef NEW_PPG
void ppg_get_clock_info(BOOL* ext_clock, BOOL* good_ext_clock)
{
  DWORD data;
  BOOL ext,good;

  // Read the state of the ppg clock from the status register

  data =  TPPGStatusRead( myvme, PPG_BASE); // displays status of clock and trigger
  printf("ppg_get_clock_info: Read PPG Status Register as 0x%x\n",data);
  if (data & 0x10000) 
    {
      printf("ppg_get_clock_info: EXTERNAL clock is selected \n");
      ext=1;
    }
  else
    {
      printf("ppg_get_clock_info: INTERNAL clock is selected \n");
      ext=0;
    }

  if (data  & 0x20000)
    {
      printf("ppg_get_clock_info: external clock is GOOD\n");
      good=1;
    }
  else
    {
      printf("ppg_get_clock_info: external clock is BAD\n");
      good=0;
    }
  
  *ext_clock=ext;
  *good_ext_clock=good;
  //  printf("ppg_get_clock_info: returning *ext_clock = %d and *good_ext_clock = %d\n",
  //	 *ext_clock,*good_ext_clock);
  return;
}

INT set_output_clock_keys(BOOL ext_clock, BOOL good_ext_clock)
{
  INT status;

  printf("set_output_clock_keys: ext_clock=%d good_ext_clock=%d\n",
	 ext_clock, good_ext_clock );

   tas.ppg.output.external_clock = ext_clock;
   tas.ppg.output.good_external_clock = good_ext_clock;

   status = db_set_value(hDB, hOut, "external clock", &tas.ppg.output.external_clock, 
  		sizeof(tas.ppg.output.external_clock), 1, TID_BOOL); 

 
  if(status !=SUCCESS)
    cm_msg(MERROR,"setup_ppg","Cannot set key \"/Equipment/TITAN_ACQ/Settings/ppg/output/external clock\" (%s)");
  
  
   status = db_set_value(hDB, hOut, "good external clock", &tas.ppg.output.good_external_clock, 
  			sizeof(tas.ppg.output.external_clock), 1, TID_BOOL); 
  

   if(status !=SUCCESS)
    cm_msg(MERROR,"setup_ppg","Cannot set key \"/Equipment/TITAN_ACQ/Settings/ppg/output/good external clock\" (%s)");
 


  if (!good_ext_clock && ext_clock)
    ext_clock_alarm=1;   // global
  else
    ext_clock_alarm=0;

 
    status = db_set_value(hDB, hOut, "alarms/external clock alarm", &ext_clock_alarm, 
			  sizeof(ext_clock_alarm), 1, TID_BOOL); 
  if(status !=SUCCESS)
    cm_msg(MERROR,"ppg_update","Cannot set key \"/Equipment/TITAN_ACQ/Settings/ppg/output/alarms/external clock alarm\" (%s)",status);


 
  return status;
}

#endif // NEW PPG

void ppg_stop(void)
{
#ifdef PPG_CODE
#ifdef NEW_PPG


// Stop the PPG and make sure outputs are set to required pattern even if stopped mid-cycle

  TPPGDisableExtTrig(  myvme, PPG_BASE); // Stop external triggers from restarting PPG

  TPPGReset(myvme, PPG_BASE); // Stops program even if in long delay
  TPPGStopSequencer(myvme,   PPG_BASE); // Halt pgm and stop

  ss_sleep(500);

  TPPGZeroAll(myvme, PPG_BASE); // zero all the PPG outputs

#ifdef GONE
  BYTE value;
  DWORD data;
  printf("Running Halt to set PPG outputs to correct state\n");
  TPPGSetStartPC(myvme, PPG_BASE, 500); // Halt command (with required output pattern) is repeated at PC 500
                                       // Set Start to PC 500
   data=TPPGRegRead(myvme, PPG_BASE, TPPG_ADDR);
	  printf("Program will start at address %d\n",data);

  TPPGStartSequencer(myvme, PPG_BASE);
  ss_sleep(500);
  value =  TPPGRegRead(  myvme, PPG_BASE, TPPG_CSR_REG );
  if (value & 1) // still running for some reason
    {
      printf("PPG is still running...halting\n");
     TPPGStopSequencer(myvme,   PPG_BASE); // Halt pgm and stop
    }
  ppg_running = FALSE;
  TPPGSetStartPC(myvme, PPG_BASE, 0); // Set Start back to PC 0
#endif // GONE
#endif
#endif
  return;
}

INT ppg_load(char *ppgfile) 
{
 
  /* download ppg file
   */ 

#ifdef PPG_CODE
 
  /* Stop the PPG sequencer  */ 
#ifdef NEW_PPG
  //TPPGStopSequencer(myvme, PPG_BASE);
  if(TPPGLoad(myvme, PPG_BASE, 0, ppgfile) != SUCCESS) // TPPGLoad stops sequencer
#else  
  VPPGStopSequencer(myvme,   PPG_BASE); 
  /* tr_precheck checked that tri_config has run recently */ 
  if(VPPGLoad(myvme, PPG_BASE, ppgfile) != SUCCESS)
#endif 
    { 
      cm_msg(MERROR,"ppg_load","failure loading ppg with file %s",ppgfile); 
      return FE_ERR_HW; 
    } 
  printf("\nppg_load: PPG file %s successfully loaded",ppgfile); 
  //  cm_msg(MINFO,"ppg_load","PPG file %s successfully loaded",ppgfile); 

#endif // PPG_CODE
  return SUCCESS; 
} 


INT modify_ppg_loadfile(char *mode)
{



#ifndef EBIT  // may be able to do this  in future for EBIT or CPET
#ifndef CPET  // may be able to do this  in future for EBIT or CPET
#ifndef MPET


#ifdef PPG_CODE
#ifdef NEW_PPG


  
  DWORD parameters[10];
  char modfilename[80],modfile[80];
  char filename[80];
  int i,status,size;
  char names[10][32];
  char message[256];

  for (i=0; i<10; i++)
    parameters[i]=0;
  
  if(strcmp(mode,"1h")==0)
    {  
      parameters[0] = 5; // number of parameters
      parameters[1] = (DWORD)tas.ppg.input.scan_loop_count ; // scan loop counter (normally 1)
      parameters[2] = (DWORD)num_bins+1; // number of bins
      parameters[3] = lne_count ; // lne count (width of lne pulse in counts)
      parameters[4] = dw_count ; // dw_count (dwell time - lne width)
      parameters[5] = dac_sleep_count ; // daq_service




      // Debugging - write these to the Output area
        size=sizeof(parameters);
	status = db_set_value(hDB, hOut, "newppg/1h_params/parameters", &parameters, size, 10, TID_DWORD);
	if(status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"modify_ppg_loadfile",
		   "cannot set /Equipment/POL_ACQ/Settings/Output/newppg/1h_params/parameters (%d)",status); 
	    return status;
	  }

        // Read the name so they can be output to screen
        size=sizeof(names);
	status = db_get_value(hDB, hOut, "newppg/1h_params/parameter_names", &names, &size, TID_STRING, FALSE);
	if(status != DB_SUCCESS)
	  {
	    cm_msg(MERROR,"modify_ppg_loadfile",
		   "cannot get /Equipment/POL_ACQ/Settings/Output/newppg/1h_params/parameter_names (%d)",status); 
	    return status;
	  }
    }

  else
    {
      cm_msg(MINFO,"modify_ppg_loadfile","Unknown PPG mode %s",mode);
      return DB_INVALID_PARAM;
    }

  
  printf(" modify_ppg_loadfile:  PPG Mode %s  Parameters \n",ppgmode);
  printf("     Number of parameters: %d\n",parameters[0]); // e.g. 5 = 5 parameters to follow

  for (i=1; i >= (int)parameters[0]; i++)
    printf("        parameter %d %s :  %d (0x%x)\n",i,names[i],parameters[i],parameters[i]);
  
  sprintf(filename,"%s/ppg-templates/%s.dat",tas.ppg.input.ppg_path,mode);
  sprintf(modfile,"%s/ppgload/ppgmods_%s.dat",tas.ppg.input.ppg_path,mode); // output file with PC counters and parameter values
  sprintf(modfilename,"%s/ppg-templates/mode%s_mods.dat",tas.ppg.input.ppg_path,mode); // input file with PC counters

  status =  write_modifications(modfilename, modfile, parameters, message);
  if (status != SUCCESS)
    {
      printf("modify_ppg_load: error from write_modifications");
      printf("message: %s\n",message);
      cm_msg(MINFO, "modify_ppg_load","%s",message);
      cm_msg(MERROR,"modify_ppg_load","Could not write modification file %s",modfilename);
      return DB_INVALID_PARAM;
    }
  else
    printf("\n\nmodify_ppg_loadfile: Now applying modifications to ppg template file...\n");

    printf("filename: %s \n ppgfile %s  \n modfile %s\n",filename,ppgfile,modfile);
  status = ppg_modify_file(filename, ppgfile, modfile);
  if (status != SUCCESS)
      return DB_INVALID_PARAM;
  else
    printf("success from  modify_ppg_loadfile\n");

#endif  // NEW_PPG
#endif  // PPG_CODE
#endif 
#endif 
#endif 
  return SUCCESS; 

}


INT check_PPG_input_params(void)
{

  // called from tr_prestart

#ifdef PPG_CODE
  //  INT status,size;

#ifdef GONE

  double dwell_time,lne_pw,dtmp;
  char str[80];
  INT LNE_freq;
  // Mode 1h
  double pw,dt;
  INT lc;
  float SIS_min_dwell_time_ms; // depends on number of data format bits (and number of channels enabled)

  // ODB output params
  float  fdwt;
  float  one_cycle_time;
#endif // GONE
  
  // Check parameters common to all ppg modes
  printf("check_PPG_input_params: Common input parameters:\n");

#ifdef GONE  // not implemented 
  printf("Run tri_config      %d\n",tas.ppg.input.run_tri_config);
#ifdef EBIT
#ifndef NEW_PPG
  if(!tas.ppg.input.run_tri_config)
    {
      cm_msg(MERROR,"check_PPG_input_params",
	     "tri_config must be run when using old PPG");
      return DB_INVALID_PARAM;
    }
#endif
   cm_msg(MINFO,"check_PPG_input_params","Running without using tri_config not implemented yet");
   return DB_INVALID_PARAM;
#endif // EBIT

#endif // GONE

  /* this value written by tri_config (if run)
     Depends on frequency of PPG. If this is changed, tri_config should be run first to update this value.
  */
  printf("Minimal delay (ms)     %f\n",tas.ppg.output.minimal_delay__ms_);



  
  // check critical parameters 
  //printf("check_PPG_input_params: min_delay=%f\n", min_delay);
  if (tas.ppg.output.minimal_delay__ms_ <= 0)
    {
      cm_msg(MERROR,"check_PPG_input_params",
	     "Invalid minimal delay for PPG (%f ms) read from ODB (filled by tri_config)",tas.ppg.output.minimal_delay__ms_);
      return DB_INVALID_PARAM;
    }
#ifdef GONE 
#ifdef NEW_PPG
  if(!tas.ppg.input.run_tri_config)
    {     // tri_config won't have changed this
      if(tas.ppg.output.time_slice__ms_ < 0) 
	{
	  cm_msg(MERROR,"check_PPG_input_params","Invalid time slice (%f)\n",tas.ppg.output.time_slice__ms_);
	  return DB_INVALID_PARAM;
	}
    }
#endif
#endif // GONE  
  
 

  /* NOTE
     Minimal delay for PPG clocked at 100mHz is 50ns (for old PPG - 5 clock cycles).  1 clock cycle = 10ns 
     
  */
#endif  // PPG_CODE
  return SUCCESS;

}

void check_ppg_running(void)
{
  // TEMP
  BYTE value;

#ifdef PPG_CODE

#ifdef NEW_PPG
  value =  TPPGRegRead(  myvme, PPG_BASE, TPPG_CSR_REG );
  if (value & 1)
#else
  value = VPPGRegRead( myvme, PPG_BASE, VPPG_VME_READ_STAT_REG ); 
  if  (value & 2)
#endif 
    {
      if(debug)printf("check_ppg_running: Pulse blaster IS running\n"); 
      ppg_running = TRUE;
      return;
    }
  else 
    { 
      printf("check_ppg_running: !! Pulse blaster NOT running\n");
#ifdef NEW_PPG
      TPPGStatusRead(myvme,PPG_BASE); // prints out status
#else              
      VPPGExtTrigRegRead(myvme, PPG_BASE);
#endif           
      ppg_running = FALSE;
    }
#ifndef  NEW_PPG
  if (value & 4)
	cm_msg(MINFO, "begin_of_run", "Run %d:  PPG is NOT stopped",run_number);
      else
	cm_msg(MINFO, "begin_of_run", "Run %d:  PPG IS stopped", run_number);
#endif

#endif // PPG_CODE
  return;
}


INT set_ppg_alarm(void)
{
  BOOL   set=1;
  INT status,size;

  printf("set_ppg_alarm: ext_clock_alarm= %d\n",ext_clock_alarm);
  if (!ext_clock_alarm)  // (global) alarm for particular case 
    {
      size=sizeof(set);
       status = db_set_value(hDB, hOut, "alarms/ppg status alarm", &set, 
			size, 1, TID_BOOL); 
       if(status !=SUCCESS)
	 cm_msg(MERROR,"ppg_update","Cannot set key \"/Equipment/TITAN_ACQ/Settings/ppg/output/alarms/ppg status alarm\" (%s)",status);
       return status;
    }
  return SUCCESS;
}

INT clear_all_alarms(void)
{
  BOOL clear=0;
  INT status,size;

  size=sizeof(clear);
  status = db_set_value(hDB, hOut, "alarms/ppg status alarm", &clear, 
			size, 1, TID_BOOL); 
  if(status !=SUCCESS)
    cm_msg(MERROR,"ppg_update","Cannot clear  \"/Equipment/TITAN_ACQ/Settings/ppg/output/alarms/ppg status alarm\" (%s)",status);


  status = db_set_value(hDB, hOut, "alarms/external clock alarm", &clear, 
			size, 1, TID_BOOL); 
  if(status !=SUCCESS)
    cm_msg(MERROR,"ppg_update","Cannot clear \"/Equipment/TITAN_ACQ/Settings/ppg/output/alarms/external clock alarm\" (%s)",status);

  return status;
}
