/********************************************************************\ 
 
  Name:         feebit.c 
  Created by:   PAA 
  
  $Id
 
\********************************************************************/ 
#undef    VMEIO_CODE 
#undef      VT2_CODE 
#undef    DA816_CODE 
//#define     PPG_CODE  // have a PPG   (defined in makefile)
//#define      NEW_PPG  // have new TRIUMF PPG (defined in makefile)
#undef     VMCP_CODE 
#undef  SOFTDAC_CODE 
#undef   SCALER_CODE 
 
#undef VF48_CODE 
#undef VERBOSE 

#undef MPET
//#define EBIT // (defined in makefile)
 
#include <stdio.h> 
#include <stdlib.h> 
#include <sys/stat.h> // time 
#include "midas.h" 
#include "mvmestd.h" 
#include "vmicvme.h" 
#ifdef VMEIO_CODE 
#include "vmeio.h" 
#endif 

#include "experim.h" 

#ifdef PPG_CODE 
BOOL ppg_external_clock = FALSE; 
BOOL ppg_external_trig = FALSE;  
DWORD ppg_polarity_mask;

#ifdef NEW_PPG 
#include "newppg.h"   // prototypes
#include "ppg_modify_file.h" // prototypes
#else
ppg_external_clock = TRUE; // fixed by hardware for old PPG 
#include "vppg.h" 
#endif // NEW_PPG
#include "ppg_code.h" // prototypes
#include "unistd.h" // for sleep 
FILE *ppginput;
#endif  // PPG_CODE

char  experiment_name[32];

TITAN_ACQ_SETTINGS tas;
TITAN_ACQ_SETTINGS_STR(titan_acq_settings_str); 
 
/* Interrupt vector */ 
int trig_level =  0; 
#define TRIG_LEVEL  (int) 1 
#define INT_LEVEL   (int) 3 
#define INT_VECTOR  (int) 0x16 
extern INT_INFO int_info; 
int myinfo = VME_INTERRUPT_SIGEVENT; 

// prototypes
INT read_titan_event(char *pevent, INT off); 
INT get_titan_settings(void);

/* Globals */ 
extern INT run_state;  
extern char exp_name[NAME_LENGTH]; 
 
/* make frontend functions callable from the C framework */ 
#ifdef __cplusplus 
extern "C" { 
#endif 
 
/*-- Globals -------------------------------------------------------*/ 
 
/* The frontend name (client name) as seen by other MIDAS clients   */
#ifdef NEW_PPG 
char *frontend_name = "fecpet"; 
#else
char *frontend_name = "fecpet_oldppg"; 
#endif
/* The frontend file name, don't change it */ 
char *frontend_file_name = __FILE__; 
 
/* frontend_loop is called periodically if this variable is TRUE    */ 
BOOL frontend_call_loop = TRUE; 
 
/* a frontend status page is displayed with this frequency in ms */ 
INT display_period = 000; 
 
/* maximum event size produced by this frontend */
INT max_event_size = 60000;  /* CPJ tigress uses 524000 */
 
/* maximum event size for fragmented events (EQ_FRAGMENTED) */ 
INT max_event_size_frag = 5 * 1024 * 1024; 
 
/* buffer size to hold events */ 
INT event_buffer_size = 10 * 20000;  /* CPJ tigress uses 10* 524000 */
 
/* Hardware */ 
MVME_INTERFACE *myvme; 
 
 
/* VME base address */ 
DWORD VMEIO_BASE   = 0x780000; 
DWORD VT2_BASE     = 0xE00000; 
DWORD VMCP_BASE    = 0x790000; 
DWORD VF48_BASE    = 0xA00000;
#ifdef NEW_PPG
DWORD PPG_BASE     = 0x00100000; // NEWPPG
#else
DWORD PPG_BASE     = 0x008000;   // OLDPPG
#endif
DWORD LRS1151_BASE = 0x7A0000; 
 
/* Globals */ 
extern HNDLE hDB; 
HNDLE hTASet, hOut; 
BOOL  end_of_cycle = FALSE; 
BOOL  transition_PS_requested= FALSE; 
INT   gbl_cycle; 
BOOL   debug=1; 
char data_dir[256];
INT ext_warn=0; // used to send warning message about ext. trig
#ifdef PPG_CODE 
  // PPG 
char cmd[128]; 
char ppgfile[128]; 
BOOL ppg_running,active;   
char copy_cmd[132]; 
#endif 
 
/*-- Function declarations -----------------------------------------*/ 
INT frontend_init(); 
INT frontend_exit(); 
INT begin_of_run(INT run_number, char *error); 
INT end_of_run(INT run_number, char *error); 
INT pause_run(INT run_number, char *error); 
INT resume_run(INT run_number, char *error); 
INT frontend_loop(); 
extern void interrupt_routine(void); 
 
INT read_pollppg_event(char *pevent, INT off); 
INT read_scaler_event(char *pevent, INT off); 

#ifdef PPG_CODE 
  INT ppg_load(char *ppgfile); 
  INT tr_checkppg(INT run_number, char *error); 
  INT tr_poststart(INT run_number, char *error); 
#endif 
 
BANK_LIST pollppg_bank_list[] = { 
   /* online banks */ 
   {"MPET", TID_DWORD,  100, NULL} , 
   {"MCPP", TID_DWORD,  100, NULL} , 
   {"FWAV", TID_DWORD, 2000, NULL} , 
   {""}, 
}; 
 
/*-- Equipment list ------------------------------------------------*/ 
 
#undef USE_INT 
 
EQUIPMENT equipment[] = { 
    {"Poll_PPG",               /* equipment name */ 
    {1, 0,                  /* event ID, trigger mask */ 
     "SYSTEM",               /* event buffer */ 
#ifdef USE_INT 
     EQ_INTERRUPT,           /* equipment type */ 
#else 
     EQ_PERIODIC, //POLLED,     /* equipment type */ 
#endif 
     LAM_SOURCE(0, 0x0),     /* event source crate 0, all stations */ 
     "MIDAS",                /* format */ 
     FALSE,                   /* disabled */ 
     RO_RUNNING,             /* read only when running */ 
     500,                    /* poll for 500ms */ 
     0,                      /* stop run after this event limit */ 
     0,                      /* number of sub events */ 
     0,                      /* don't log history */ 
     "", "", "",}, 
    read_pollppg_event,      /* readout routine */ 
    NULL, NULL, 
    pollppg_bank_list, 
    } 
   , 
 
   {"Scaler",                /* equipment name */ 
    {2, 0,                   /* event ID, trigger mask */ 
     "SYSTEM",               /* event buffer */ 
     EQ_PERIODIC ,           /* equipment type */ 
     0,                      /* event source */ 
     "MIDAS",                /* format */ 
     FALSE,                   /* enabled */ 
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */ 
     RO_ODB,                 /* and update ODB */ 
     10000,                  /* read every 10 sec */ 
     0,                      /* stop run after this event limit */ 
     0,                      /* number of sub events */ 
     0,                      /* log history */ 
     "", "", "",}, 
    read_scaler_event,       /* readout routine */ 
    }, 
 
  {"Titan_acq",                /* equipment name */ 
    {10, 0,                   /* event ID, trigger mask */ 
     "SYSTEM",               /* event buffer */ 
     EQ_PERIODIC ,           /* equipment type */ 
     0,                      /* event source */ 
     "MIDAS",                /* format */ 
     FALSE,                   /* enabled */ 
     RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */ 
     RO_ODB,                 /* and update ODB */ 
     10000,                  /* read every 10 sec */ 
     0,                      /* stop run after this event limit */ 
     0,                      /* number of sub events */ 
     0,                      /* log history */ 
     "", "", "",}, 
    read_titan_event,       /* readout routine */ 
    }, 
 
   {""} 
}; 
 
#ifdef __cplusplus 
} 
#endif 
 
/********************************************************************\ 
              Callback routines for system transitions 
 
  These routines are called whenever a system transition like start/ 
  stop of a run occurs. The routines are called on the following 
  occations: 
 
  frontend_init:  When the frontend program is started. This routine 
                  should initialize the hardware. 
 
  frontend_exit:  When the frontend program is shut down. Can be used 
                  to releas any locked resources like memory, commu- 
                  nications ports etc. 
 
  begin_of_run:   When a new run is started. Clear scalers, open 
                  rungates, etc. 
 
  end_of_run:     Called on a request to stop a run. Can send 
                  end-of-run event and close run gates. 
 
  pause_run:      When a run is paused. Should disable trigger events. 
 
  resume_run:     When a run is resumed. Should enable trigger events. 
\********************************************************************/ 
 
/********************************************************************/ 

HNDLE   hSet, hKey, hGE, hBGO, hKeyPosition, hEPM, hEPD;
extern HNDLE hDB;

/*-- Sequnencer callback info  --------------------------------------*/ 
void seq_callback(INT hDB, INT hseq, void *info) 
{ 
  printf("odb ... trigger settings touched\n"); 
} 
 
 
/*-- Deferred transition -------------------------------------------*/ 
/* will be called repeatively  while transition is reuqested until transition is issued */ 
BOOL wait_end_cycle(int transition, BOOL first) 
{ 
  if (first) { 
    /* Will go through here the first time wait_end_cycle() is called */ 
    /* setup user flags */ 
    transition_PS_requested = TRUE; 
    /* return false as long as the requested transition should be postponed */ 
    return FALSE; 
  } 
 
  /* Check the user flags for issuing the requested transition */ 
  if (end_of_cycle)  { 
    transition_PS_requested = FALSE; 
    end_of_cycle = FALSE; 
    /* Tell system to issue transition */ 
    return TRUE; 
  } 
  else 
    /* in any other case don't do anything ==> no transition */ 
    return FALSE; 
} 
 
/*-- Frontend Init -------------------------------------------------*/ 
INT frontend_init() 
{ 
  int size, status; 


  printf ("frontend_init: starting\n");

  /* do not register for deferred transition */ 
  //  cm_register_deferred_transition(TR_STOP, wait_end_cycle); 
  // cm_register_deferred_transition(TR_PAUSE, wait_end_cycle); 
 
  // Open VME interface 
  status = mvme_open(&myvme, 0);

  // get the experiment
  size = sizeof(experiment_name);
  status = db_get_value(hDB, 0, "/Experiment/name",
                                   experiment_name, &size, TID_STRING, 0);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","error getting \"/Experiment/name\" (%d)",status);
      return status;
    }
#ifdef CPET
  if (strcmp("cpet",experiment_name)!= 0)
    {
      cm_msg(MERROR,"frontend_init","this code is built for CPET not for experiment %s\n",experiment_name);
      return DB_INVALID_NAME;
    }
#endif
#ifdef MPET
  if (strcmp("mpet",experiment_name)!= 0)
    {
      cm_msg(MERROR,"frontend_init","this code is built for MPET not for experiment %s\n",experiment_name);
      return DB_INVALID_NAME;
    }
#endif
#ifdef EBIT
  if (strcmp("ebit",experiment_name)!= 0)
    {
      cm_msg(MERROR,"frontend_init","this code is built for EBIT not for experiment %s\n",experiment_name);
      return DB_INVALID_NAME;
    }
#endif    

#ifdef PPG_CODE 
  status = cm_register_transition(TR_START, tr_checkppg, 400); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for tr_checkppg"); 
      return status; 
    } 
  status = cm_register_transition(TR_START, tr_poststart, 600); 
  if(status != CM_SUCCESS) 
    { 
      cm_msg(MERROR, "frontend_init", "cannot register to transition for tr_poststart"); 
      return status; 
    } 
 

  status = get_titan_settings(); // create records, get ODB keys etc.
  if(status !=SUCCESS)
    return status;

 
  // get the data directory for later
  size = sizeof(data_dir);
  status = db_get_value(hDB, 0, "/logger/data dir",
                                   data_dir, &size, TID_STRING, 0);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","error getting \"data dir\" (%d)",status);
      return status;
    }

   status = init_ppg(FALSE); // initialize but do not load PPG
   if(status != SUCCESS)
    {
      cm_msg(MERROR,"frontend_init","error return from init_ppg (%d)",status);
      return status;
    }
#endif 
  /* print message and return FE_ERR_HW if frontend should not be started */ 
  return SUCCESS; 
} 
 
/*-- Frontend Exit -------------------------------------------------*/ 
 
INT frontend_exit() 
{ 
#ifdef PPG_CODE 
 
  /*Disable the PPG module just in case */ 
#ifdef NEW_PPG
  TPPGStopSequencer(myvme,   PPG_BASE); // Halt pgm and stop
#else
  VPPGStopSequencer(myvme,   PPG_BASE); 
#endif 
#endif 
   return SUCCESS; 
} 
 
/*-- Begin of Run --------------------------------------------------*/ 
INT begin_of_run(INT run_number, char *error) 
{ 
  INT status,size; 
  char set_str[128];

  /* Get current  settings */
  sprintf(set_str, "/Equipment/TITAN_ACQ/Settings"); 
  size = sizeof(TITAN_ACQ_SETTINGS);
  status = db_get_record(hDB, hTASet, &tas, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "begin_of_run", "cannot retrieve %s record (structure size=%d; size of record=%d ) %d", 
	     set_str, sizeof(TITAN_ACQ_SETTINGS),size,status);
      return status;
    }

#ifdef PPG_CODE 
  status = setup_ppg();
  if (status != SUCCESS)
    return status;
 
  if(ppg_load(ppgfile) != SUCCESS) 
    return FE_ERR_HW; 
  else 
    printf("Successfully loaded ppg\n");
  
  if(ppg_external_trig)
    printf("PPG External trigger is selected\n");
  else
       printf("PPG Internal trigger is selected\n");

  if(ppg_external_trig)
    {
      printf("\n\n   PPG is waiting for an external trigger...\n\n");
      printf("End of BOR\n"); 
      return SUCCESS;
    }
  else
    {      
      /* start the PPG  */ 
      printf("\n\nStarting PPG\n"); 
#ifdef NEW_PPG
      TPPGStartSequencer(myvme, PPG_BASE );
#else
      VPPGStartSequencer(myvme, PPG_BASE); 
#endif
      check_ppg_running();  // fills global ppg_running
      
      status = db_set_value(hDB, hOut, "PPG running", &ppg_running, sizeof(ppg_running), 1, TID_BOOL); 
      if(status == DB_SUCCESS) 
	{ 
	  active=1;
	  status = db_set_value(hDB, 0, "/Alarms/Alarms/PPG/active", &active, sizeof(active), 1, TID_BOOL);
	  if(status != DB_SUCCESS) 
	    cm_msg(MERROR,"frontend_init","cannot set /Alarms/Alarms/PPG/active =TRUE  (%d)",status); 
	}
      else
	{ 
	  cm_msg(MERROR,"frontend_init","cannot set ODB key \"PPG_running\" to %d  (%d)",ppg_running, status); 
	  // return FE_ERR_ODB;
	}
    }
#endif /* PPG */ 
  
  printf("End of BOR\n"); 
  return SUCCESS; 
} 

/*-- End of Run ----------------------------------------------------*/ 
INT end_of_run(INT run_number, char *error) 
{ 
  INT status;
  INT clear=0;
 
#ifdef PPG_CODE 
#ifdef NEW_PPG
  ppg_stop(); // also sets required output pattern in case stopped mid-cycle 
#else
  VPPGStopSequencer(myvme,   PPG_BASE); /* stops sequencer; later disable ext. trigger if used */ 
#endif 

  // disable the alarm system while the run is stopped
  active= FALSE;
  status = db_set_value(hDB, 0, "/Alarms/Alarms/PPG/active", &active, sizeof(active), 1, TID_BOOL);
  if(status != DB_SUCCESS) 
    cm_msg(MERROR,"frontend_init","cannot set /Alarms/Alarms/PPG/active to %d  (%d)",active,status); 
  status = db_set_value(hDB, 0, "/Alarms/Alarms/PPG/triggered", &clear, sizeof(clear), 1, TID_INT);
  if(status != DB_SUCCESS) 
    cm_msg(MERROR,"frontend_init","cannot set /Alarms/Alarms/PPG/triggered to %d  (%d)",active,clear); 

  ppg_running = FALSE;
  status = db_set_value(hDB, hOut, "PPG running", &ppg_running, sizeof(ppg_running), 1, TID_BOOL); 
  if(status != DB_SUCCESS) 
    cm_msg(MERROR,"frontend_init","cannot set PPG running FALSE  (%d)",status); 


  printf("\n end_of_run: set ppg_running and ppg alarm false\n");
#endif 
 
#ifdef VMCP_CODE 
  // Disable the module 
  lrs1190_Disable(myvme, VMCP_BASE); 
#endif 
 
  return SUCCESS; 
} 
 
/*-- Pause Run -----------------------------------------------------*/ 
INT pause_run(INT run_number, char *error) 
{ 
  return SUCCESS; 
} 
 
/*-- Resume Run ----------------------------------------------------*/ 
INT resume_run(INT run_number, char *error) 
{ 
  /* reset flag */ 
  end_of_cycle = FALSE; 
  return SUCCESS; 
} 
 
/*-- Frontend Loop -------------------------------------------------*/ 
INT frontend_loop() 
{ 
  char str[80]; 
  INT status; 
  
#ifdef PPG_CODE 
  if (ppg_running)
    {
      if (!ppg_external_trig) 
	{  // internal trigger - mpet/ebit run with a scan loop i.e. started once per run
	  if (run_state == STATE_RUNNING) 
	    { 
              check_ppg_running(); // sets ppg_running 
	      if  (ppg_running) 
		{ 
		  if (debug) 
		    printf("frontend_loop:  pulse blaster IS running\n"); 
		} 
	      else 
		{ 
		  cm_msg(MINFO, "frontend_loop","pulse blaster is NOT running... attempting to stop the run\n"); 
		  printf("frontend_loop:  pulse blaster is NOT running... stopping the run \n");
		  status = db_set_value(hDB, hOut, "PPG running", &ppg_running, sizeof(ppg_running), 1, TID_BOOL); 
		  if(status != DB_SUCCESS) 
		    { 
		      cm_msg(MERROR,"frontend_loop","cannot set PPG_running=FALSE  (%d)",status); 
		      // return FE_ERR_ODB; 
		    }
		  if (cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, 0) != CM_SUCCESS) 
		    status = cm_transition(TR_STOP, 0, str, sizeof(str), SYNC, 0); 
		  if((status !=  CM_SUCCESS) && (status != CM_DEFERRED_TRANSITION)) 
		    cm_msg(MERROR, "frontend_loop", "cannot stop run immediately: %s (%d)", str,status); 
		} 
	    } 
	} // end of internal trigger
    } // end of ppg_running is true
#ifdef EBIT
  else // ppg not running
    {
      // EBIT may be using external trigger
      if ( (run_state == STATE_RUNNING) &&  ppg_external_trig)
	{
	  // check whether PPG has been started yet
	   check_ppg_running(); // sets ppg_running 
	  if  (ppg_running)
	    {
	      printf("pulse blaster IS now running (external trigger)\n");
	      status = db_set_value(hDB, hOut, "PPG running", &ppg_running, sizeof(ppg_running), 1, TID_BOOL); 
	      if(status == DB_SUCCESS) 
		{ 
		  active=1;
		  status = db_set_value(hDB, 0, "/Alarms/Alarms/PPG/active", &active, sizeof(active), 1, TID_BOOL);
		  if(status != DB_SUCCESS)
		    cm_msg(MERROR,"frontend_loop","cannot set /Alarms/Alarms/PPG/active =TRUE  (%d)",status); 
		}
	      else 
		cm_msg(MERROR,"frontend_loop","cannot set PPG_running=TRUE  (%d)",status); 
	    }
	  else
            {
              ext_warn++;
	      if (ext_warn % 100000 == 0) // cut down the number of warning messages
		//   cm_msg(MINFO,"frontend_loop","pulse blaster is NOT running; waiting for external trigger");
		//else if (ext_warn % 1000 == 0)
		printf("pulse blaster is NOT running or cycle is very short; may be waiting for external trigger\n");
            }
	}	  
    }
#endif // EBIT
  if (debug) 
    { 
      check_ppg_running(); // sets ppg_running 
      if  (!ppg_running) 
	printf("pulse blaster IS NOT running\n"); 
    } 
#endif  // PPG code
  return SUCCESS; 
} 
 
/*------------------------------------------------------------------*/ 
/********************************************************************\ 
 
  Readout routines for different events 
 
\********************************************************************/ 
/*-- Trigger event routines ----------------------------------------*/ 
INT poll_event(INT source, INT count, BOOL test) 
     /* Polling routine for events. Returns TRUE if event 
  is available. If test equals TRUE, don't return. The test 
  flag is used to time the polling */ 
    //    if (vmeio_CsrRead(myvme, VMEIO_BASE)) 
    //    if (lam > 10) 
{ 
#ifdef VMEIO 
  int i; 
  int lam = 0; 
  for (i = 0; i < count; i++, lam++) { 
    lam = vmeio_CsrRead(myvme, VMEIO_BASE); 
    if (lam) 
      if (!test) 
  return lam; 
  } 
#endif 
  return 0; 
} 
 
/*-- Interrupt configuration ---------------------------------------*/ 
INT interrupt_configure(INT cmd, INT source, PTYPE adr) 
{ 
  switch (cmd) { 
  case CMD_INTERRUPT_ENABLE: 
    break; 
  case CMD_INTERRUPT_DISABLE: 
    break; 
  case CMD_INTERRUPT_ATTACH: 
    break; 
  case CMD_INTERRUPT_DETACH: 
    break; 
  } 
  return SUCCESS; 
} 
 
/*-- Event readout -------------------------------------------------*/ 
INT read_pollppg_event(char *pevent, INT off) 
{
  if (transition_PS_requested) {
    /* transition requested: What do we do?
       set end_of_cycle = TRUE will issue the postponed transition
       keep end_of_cycle = FALSE will deferre the transition to later */
    printf("%d Could postpone the transition now!\n", gbl_cycle++);
    if (gbl_cycle >= 1)
      end_of_cycle = TRUE;
  }
  return 0;
} 

/*-- Dummy Titan Event --------------------------------------------*/ 
INT read_titan_event(char *pevent, INT off) 
{ 
  return 0; 
} 
 
/*-- Scaler event --------------------------------------------------*/ 
INT read_scaler_event(char *pevent, INT off) 
{ 
  return 0; 
} 
 
 
#ifdef PPG_CODE 
 
INT tr_checkppg(INT run_number, char *error) 
{ 
  struct stat stbuf; 
  int status; 
  time_t timbuf; 
  char timbuf_ascii[30]; 
  int elapsed_time; 
 
 
  //  status = execl("/bin/sh", "sh", "-c", cmd, NULL); 
  //  printf("tr_checkppg: %s sent through execl (status:%d\n",cmd, status); 
 

  /// run tri_config as a separate program
  ///
  // run tri_config
  ///  printf("tr_checkppg: sending system command:\"%s\" \n",cmd); 
  ///  status =  system(cmd); 
 
  stat (ppgfile,&stbuf); 
  printf("tr_checkppg: file %s  size %d\n",ppgfile, (int)stbuf.st_size); 
 
  if(stbuf.st_size < 0) 
    { 
      cm_msg(MERROR,"tr_checkppg","PPG load file %s cannot be found",ppgfile); 
      return DB_FILE_ERROR ; 
    } 
 
  strcpy(timbuf_ascii, (char *) (ctime(&stbuf.st_mtime)) ); 
  // printf ("tr_checkppg: PPG loadfile last modified:  %s or (binary) %d \n",timbuf_ascii,(int)stbuf.st_size); 
  cm_msg(MINFO,"tr_checkppg","PPG loadfile last modified:  %s or (binary) %d",timbuf_ascii,(int)stbuf.st_size); 

 
  // get present time 
  time (&timbuf); 
 
  strcpy(timbuf_ascii, (char *) (ctime(&timbuf)) ); 
  printf ("tr_checkppg: Present time:  %s or (binary) %d \n",timbuf_ascii,(int)timbuf); 
 
  elapsed_time= (int) ( timbuf - stbuf.st_mtime ); 
 
  // printf("tr_precheck: time since ppg loadfile last updated: %d seconds \n",elapsed_time); 
  cm_msg(MINFO,"tr_checkppg","time since ppg loadfile last updated: %d seconds ",elapsed_time); 
  if(elapsed_time > 3) 
    { 
      cm_msg(MERROR,"tr_checkppg","PPG load file %s is too old (%d sec)",ppgfile,elapsed_time); 
      return DB_FILE_ERROR ; 
    } 



  return SUCCESS; 
} 
 
INT get_titan_settings(void)
{
  INT status,size;
  char set_str[128];

  /* Book Setting space */ 

  TITAN_ACQ_SETTINGS_STR(titan_acq_settings_str); 

 
  /* Map /equipment/Titan_acq/settings for the sequencer */ 
  sprintf(set_str, "/Equipment/TITAN_ACQ/Settings"); 
  status = db_create_record(hDB, 0, set_str, strcomb(titan_acq_settings_str)); 
if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_titan_settings","Could not create %s record (%d)",set_str,status);
	  return status;
	}
      else
	printf("Success from create record for %s\n",set_str);

  status = db_find_key (hDB, 0, set_str, &hTASet); 
  if (status != DB_SUCCESS)
    { 
      cm_msg(MERROR,"get_titan_settings","Key %s not found", set_str); 
      return status;
    }
  else 
    printf("Key hTASet for %s found\n",set_str);

  /* check that the record size is as expected */
  status = db_get_record_size(hDB, hTASet, 0, &size);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "get_titan_settings", "error during get_record_size (%d)",status);
      return status;
    }
  
  printf("Size of titan_acq/settings saved structure: %d, size of titan_acq/settings record: %d\n", 
	 sizeof(TITAN_ACQ_SETTINGS) ,size);
  if (sizeof(TITAN_ACQ_SETTINGS) != size)
    {
      /* create record */
      cm_msg(MINFO,"frontend_init",
	     "creating record for %s; mismatch between size of structure (%d) & record size (%d)", 
	     set_str, sizeof(TITAN_ACQ_SETTINGS) ,size);
      status = db_create_record(hDB, 0, set_str, strcomb(titan_acq_settings_str)); 
      if (status != DB_SUCCESS)
	{
	  cm_msg(MERROR,"get_titan_settings","Could not create %s record (%d)",set_str,status);
	  return status;
	}
      else
	printf("Success from create record for %s\n",set_str);
    }

  /* Get current  settings */
  status = 0;
  size = sizeof(TITAN_ACQ_SETTINGS);
  status = db_get_record(hDB, hTASet, &tas, &size, 0);
  if (status != DB_SUCCESS)
    {
      cm_msg(MERROR, "get_titan_settings", "cannot retrieve %s record (structure size=%d; size of record=%d ) %d", 
	     set_str, sizeof(TITAN_ACQ_SETTINGS),size,status);
      return status;
    }
  printf("db_get_record status:%d\n", status);
  printf("PPGpath = %s \n", tas.ppg.input.ppg_path);  
  printf("PPGLOAD path = %s \n", tas.ppg.input.ppgload_path);

  // Assign the name of the load file
#ifdef PPG_CODE
#ifdef NEW_PPG
  sprintf(ppgfile,"%s/ppgload.dat", tas.ppg.input.ppgload_path);
#else 
  sprintf(ppgfile,"%s/bytecode.dat", tas.ppg.input.ppgload_path); 
#endif
  printf("ppgfile:%s\n",ppgfile); 
#endif

  
   /* note that tri_config will produce bytecode.dat for old PPG
     for new ppg, tri_config will also convert bytecode.dat to ppgload.dat
  */
 

  /* find key for ODB output directory */
  sprintf(set_str,"ppg/output"); 
  status = db_find_key (hDB, hTASet, set_str, &hOut); 
  if (status != DB_SUCCESS) 
    { 
      cm_msg(MERROR,"get_titan_settings","cannot get key at %s (%d)",set_str,status); 
      return FE_ERR_ODB; 
    }
  printf("got the key hOut\n");


  printf("PPGpath:%s  Exp name = %s \n", tas.ppg.input.ppg_path, exp_name); 
  sprintf(cmd,"%s/tri_config  -e %s -s -p", tas.ppg.input.ppg_path,exp_name);  // add -p for plot
  printf("get_titan_settings: tri_config cmd:%s\n",cmd); 
 
  return SUCCESS;
}



 
INT tr_poststart(INT run_number, char *error) 
{ 
  INT status; 
  char copy_cmd[132]; 
 
  if(run_state == STATE_RUNNING) 
    { 
      sprintf(copy_cmd,"cp %s/ppgload/bytecode.dat %s/info/run%d_bytecode.dat &", 
	      tas.ppg.input.ppg_path,data_dir,run_number); 
      status =  system(copy_cmd); 
      printf("tr_poststart: status after system command \"%s\" is %d\n",copy_cmd, status); 
 
      sprintf(copy_cmd,"cp %s/ppgload/%s.ppg %s/info/run%d_%s.ppg &",
	      tas.ppg.input.ppg_path,experiment_name,data_dir,run_number,experiment_name); 
      status =  system(copy_cmd); 
      printf("tr_poststart: status after system command \"%s\" is %d\n",copy_cmd, status); 
 
#ifdef NEW_PPG 
      sprintf(copy_cmd,"cp %s/ppgload/ppgload.dat %s/info/run%d_ppgload.dat &", 
	      tas.ppg.input.ppg_path,data_dir,run_number); 
      status =  system(copy_cmd); 
      printf("tr_poststart: status after system command \"%s\" is %d\n",copy_cmd, status); 
#endif
 
    } 
  return SUCCESS; 
}






 
#endif 
 
/* emacs 
 * Local Variables: 
 * mode:C 
 * mode:font-lock 
 * tab-width: 8 
 * c-basic-offset: 2 
 * End: 
 */ 
 
 
