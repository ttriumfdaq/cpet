// 1e.js
//  included by pc_all.html
//
// Globals
var SignalNames = ["ISACBM","TIBG1","TIBG2","RFQEXT","CAP1","TRFER1","TRFER2","SCALER1","SCALER2",
"TIB10","DUMP","KICK1","KICK2","EBEAM1","EBEAM2","CH16"];

  var names = new Array(15);

function check_pol(chan)
{
if(chan > 0)
  return 1
else
  return 0
}



function fix(jvar)
{ // remove trailing zeroes e.g. 0.070000000 becomes 0.07

   var pattern= /(0)+(\D)*$/; // zeroes at end of string possibly followed by a non-digit (e.g. CR?)
   var ivar = jvar+""; // string

   // debug
   //if (jvar > 93)
   //{
   //   alert ('ivar = '+ivar) 
   //   var result=pattern.exec(ivar);
   //   alert (pattern.test(ivar))
   //}

   ivar = ivar.replace(pattern, ""); // remove zeroes at end of string

   //if (jvar > 93)
   //alert ('ivar = '+ivar) 

   ivar = ivar.replace(/\.$/,""); // remove the decimal point if left at end of string
   
   //if (jvar > 93)
   //alert ('ivar = '+ivar)
 
   return ivar;
}
function digits(jvar)
{
   var factor=1;

   if (jvar < 0.0000001 )
      jvar *= 1000000;

if (freq.toFixed) // if this is available
   freq = freq.toFixed(6);
alert ('freq '+freq);
}

function strip(jvar)
{   
   // returns a value with a reasonable number of sig figs.


  // pattern2 is used if jvar < 1
   var pattern2= /([0-9]*\.0+[1-9][1-9][1-9])/; // retain 3 numbers after string of zeroes

   var ivar = jvar+""; // string
   var nsig=6; // initialize to 6 say

   if (jvar > 0.01) // jvar is not too small
   {
       nsig = 3;
       if(jvar > 1000)  // jvar is very large
          nsig=0;
    
       if (jvar.toFixed) // if this is available
          jvar = jvar.toFixed(nsig); // round this up
	return jvar;
    }
      
   // jvar is very small ( <= 0.01 )

    if(pattern2.test(ivar))
    {  // found a string with a bunch of zeroes after decimal point
       result=ivar.match(pattern2)
       s = result[0]; // get first match
       var len = s.length;
       //alert( 's= '+s+ ' len= '+len)
       nsig = len-1;

       var j=s*1;  // j is a number now
       if (j.toFixed) // if this is available
       j = j.toFixed(nsig); // round this up
       return j;
    }
    else
    {
        // should not get here, but just in case pattern doesn't match...
        nsig = 3;
        if (jvar.toFixed) // if this is available
          jvar = jvar.toFixed(nsig); // round this up
	return jvar;
    }
}


//function do_1e( rstate, pstate, ppg_mode,itrig, widthArray,offsetArray)
// globals don't have to be sent explicitly
function do_1e()
{
// 1e

// users want to change the names so they are now in ODB under "/Equipment/titan_acq/ppg cycle/names/names"
  var names_path = "/Equipment/titan_acq/ppg_cycle_mode_1e/names/names[*]"; // don't use link in case user
                                                                            // has just switched page
  //var names_path = block_path + "/names/names[*]";   
  names = ODBGet(names_path); // only room for the comment on 1e/f page

if (ppg_mode == '1f')  // outer table
{  
   // LOOP TABLE
   document.write('<center><table style="text-align: left; width: 75%; background-color:white" border="1" cellpadding="2" cellspacing="2"  >');
   document.write('<tr align="center">');
   document.write('<td rowspan="1"  colspan="1" style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
   document.write('Scan Loop   ');

   var scan_loop_path = block_path + '/begin_scan/loop count';
   var slc = ODBGet(scan_loop_path); // scan loop count
   //alert ('path: '+scan_loop_path)
   document.write(' Loop Count: ');
   document.write('<a href="#" onclick=\'ODBEdit("'+scan_loop_path+'")\' >')
   document.write(slc);
   document.write('</a>');
   document.write('</td>');
   document.write('</tr>');


   document.write('<tr align="center">');
   document.write('<td>');
}
// OUTPUT TABLE
document.write('<center><table style="text-align: left; width: 75%; background-color:white" border="1" cellpadding="2" cellspacing="2"  >');
document.write('<tr align="center">');

document.write('<td rowspan="2" style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Output</td>');
document.write('<td rowspan="2" style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Polarity</td>');
document.write('<td  rowspan="2" style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Output Name<br> <span style="font-weight: normal; font-size: small">(user-defined)</span></td>');
document.write('<td  colspan="4" style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">'); 
document.write('Pulse</td>');
document.write('</td></tr><tr align="center" >')
document.write('<td style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Width</td>');
document.write('<td style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Offset from T0</td>');
document.write('<td style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Block name</td>');
document.write('<td style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Signal</td>');
document.write('</tr>');


// ROW 1 PULSE 1 ISAC BEAM GATE  array index=0
var idx=0;


document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('1</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="isacbm" id="ch1" onClick="change_item(1)"></td>')
ch = polarity & 1  // bitwise operator
document.form2.isacbm.checked=check_pol(ch)  // initialize to the correct value


write_pulse(idx);

// ROW 2 PULSE 2  TITAN BEAM GATE 1
idx++; // 1
document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('2</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="titanbm1" id="ch2" onClick="change_item(2)"></td>')
ch = polarity & 2  // bitwise operator

document.form2.titanbm1.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)


// ROW 3 PULSE 3
idx++; // 2
document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('3</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="titanbm2" id="ch1" onClick="change_item(3)"></td>')
ch = polarity & 4  // bitwise operator

document.form2.titanbm2.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)

// ROW 4 PULSE 4 RFQ 
idx++; // 3
document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('4</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="rfqextr" id="ch4" onClick="change_item(4)"></td>')
ch = polarity & 8  // bitwise operator

document.form2.rfqextr.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)


// ROW 5 PULSE 5  CAPTURE 1 
idx++; // 4
document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('5</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="capture1" id="ch5" onClick="change_item(5)"></td>')
ch = polarity & 16  // bitwise operator

document.form2.capture1.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)

// ROW 6 PULSE 6 Transfer 1 
idx++; // 5
document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('6</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="transfer1" id="ch6" onClick="change_item(6)"></td>')
ch = polarity & 32  // bitwise operator

document.form2.transfer1.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)

// ROW 7 PULSE 7 Transfer 2 
idx++; // 6
document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('7</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="transfer2" id="ch7" onClick="change_item(7)"></td>')
ch = polarity & 64  // bitwise operator

document.form2.transfer2.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)

// ROW 8 PULSE 8 Scaler 1
idx++; // 7
document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('8</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="scaler1" id="ch8" onClick="change_item(8)"></td>')
ch = polarity & 128  // bitwise operator

document.form2.scaler1.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)

// ROW 9 PULSE 9 Scaler 2 
idx++; // 8

document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('9</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="scaler2" id="ch9" onClick="change_item(9)"></td>')
ch = polarity & 256  // bitwise operator

document.form2.scaler2.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)


// ROW 10 PULSE 10 (array index =1)
idx++; // 9
 
document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('10</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="tig10" id="ch10" onClick="change_item(10)"></td>')
ch = polarity & 512  // bitwise operator

document.form2.tig10.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)


// ROW 11 PULSE 11 Dump 
idx++; // 10
; 
document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('11</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="dump" id="ch11" onClick="change_item(11)"></td>')
ch = polarity & 1024  // bitwise operator

document.form2.dump.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)



// ROW 12 PULSE 12 Kicker 1 
idx++; // 11

document.write('<tr>');

document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('12</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="kicker1" id="ch12" onClick="change_item(12)"></td>')
ch = polarity & 2048  // bitwise operator

document.form2.kicker1.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)

// ROW 13 PULSE 13 (array index =1)
idx++; // 12
 
document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('13</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="kicker2" id="ch13" onClick="change_item(13)"></td>')
ch = polarity & 4096  // bitwise operator

document.form2.kicker2.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)


// ROW 14 PULSE 14 ebeam 1
idx++; // 13

document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('14</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="ebeam1" id="ch14" onClick="change_item(14)"></td>')
ch = polarity & 8192  // bitwise operator

document.form2.ebeam1.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)

// ROW 15 PULSE 15 E-beam 2 
idx++; // 14

document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('15</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="ebeam2" id="ch15" onClick="change_item(15)"></td>')
ch = polarity & 16384  // bitwise operator

document.form2.ebeam2.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)



// ROW 16 PULSE CH 16 
idx++; // 15
 
document.write('<tr>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('16</td>');
document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
document.write('<input  type="checkbox" name="ebeam2" id="ch16" onClick="change_item(16)"></td>')
ch = polarity & 16384  // bitwise operator

document.form2.ch16.checked=check_pol(ch)  // initialize to the correct value

write_pulse(idx)


// ROW 17  PPG Cycle Length
document.write('<tr align="center" >');
document.write('<td colspan="7" style="vertical-align: top; background-color: silver; font-weight: normal; font-size: 100% ;">');
var max_path = titan_path +"/settings/ppg/output/last transition (ms)";
var max_time = ODBGet(max_path,"%.9f");

max_time = fix(max_time);
var freq = 1000/max_time;
var freq = strip(freq);

if (ppg_mode == '1f')  // includes scan loop
   document.write(' Total ');
document.write('PPG Cycle length= '+max_time+'ms;  PPG cycle frequency = '+freq+' Hz (updated by tri_config)');
document.write('</td></tr>');
document.write('</table>');


if (ppg_mode == '1f')  // outer table
{
   document.write('</td></tr>');
// add a line for end loop offset
   document.write('<tr align="center">');

   document.write('<td colspan="4" style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
   var end_loop_path = block_path + '/end_scan/time offset (ms)';
   var elo = ODBGet(end_loop_path);  // end loop count
   document.write('End Loop Offset: ');
   document.write('<a href="#" onclick=\'ODBEdit("'+end_loop_path+'")\' >')
   document.write(elo);
   document.write('</a>');
   document.write('</td>');
   document.write('</tr>');
   document.write('</table>');
}

   document.write('</center>');




// testing testing
//freq = 934.12345600000000556789
//var frequency = strip(freq);
//alert('freq= '+freq+ ' frequency= '+frequency);

//freq = 156789123456.999999000556789
//var frequency = strip(freq);
//alert('freq= '+freq+ ' frequency= '+frequency);

//freq = .00199999000556789
//var frequency = strip(freq);
//alert('freq= '+freq+ ' frequency= '+frequency);

return;
}


function write_pulse(index)
{ 
   var ch = index+1;


   document.write('<td  style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
// Name
   names[index] = ODBGet(namesArray[index]);
   document.write('<a href="#" onclick="ODBEdit(namesArray['+index+'])" >')
   document.write(names[index]);
   document.write('</a>');
 
   document.write(' </td>') ;


   document.write('<td  style="vertical-align: top; background-color:floralwhite; font-weight: normal; font-size: 100% ;">');
// pulse width

   jvar = ODBGet(widthArray[index],"%.9f");
   document.write('<a href="#" onclick="ODBEdit(widthArray['+index+'])" >')
   jvar=fix(jvar);
   document.write(jvar + 'ms');
   document.write('</a>');
 
   document.write(' </td>') ;

   document.write('<td style="vertical-align: top; background-color: mintcream; font-weight: normal; font-size: 100% ;">');


 // pulse offset
  
    jvar = ODBGet(offsetArray[index],"%.9f");
    jvar=fix(jvar);
    //alert ('jvar, offsetArray'+jvar+offsetArray['+index+'])
    document.write('<a href="#" onclick="ODBEdit(offsetArray['+index+'])" >')
    document.write(jvar + 'ms');
    document.write('</a>');
   
   document.write('') ;  
   document.write('</td>') 

   document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: small ;">');
   document.write('Pulse'+ch)
   document.write('</td>') 
   document.write('<td style="vertical-align: top; background-color: white ; font-weight: normal; font-size: small ;">');
   document.write(SignalNames[index])
   document.write('</td>') 
   document.write('</tr>');
   return
}
