//  all.js
//  included by pc_all.html
//
//How to add ODBEdit box
//============================================
//document.write('Using Javascript and ODBEdit:')
//path='/runinfo/run number' 
//rn = ODBGet(path)
//document.write('<a href="#" onclick="ODBEdit(path)" >')
//document.write(rn)
//document.write('</a>');
//document.write('') ;
//========================================================
// Note: need to use a different "jpath" variable for each one
//   all jpaths are now global, declared in pc_all.html 
//     1a  skip transfer
//     1b  end after
//     1c  complete cycle with outer scan loop
//     1d  complete cycle ; external trig, no outer scan loop
//     1e  all pulses referenced from T0, no loops 

function do_all(ppg_mode,ppg_index)
{


//alert('do_all starting with  ppg_mode, ppg_index '+ppg_mode+ppg_index)
if (ppg_mode == '1e' )
   return; // need to call do_1e

//alert ('do_all starting with ppg_mode = '+ppg_mode);

// relative values for 1a
// 1. Main Scan Loop
// 2. Extraction loop count
// 3.  begin_extr  offset  (ISAC beam gate)
// 4. pulse1 width (TITAN beam gate 1 TIBG1 )
// 5. pulse2 width (TITAN beam gate 2 TIBG2 )
// 6. pulse3 offset (RFQ extraction RFQEXT )
// 7. pulse4 offset (Capture 1  CAP1)
// 8. pulse4 width (Capture 1 CAP1 ) 
// 9. trans3 offset (  TIG10 )
// 10. begin_sclr  Loop count  (Scaler Loop)
// 11.  pulse7  offset (Scaler 1  TRFER2)
// 12. pulse7  width (Scaler 1 TRFER2 )
// 13. pulse8  offset (Scaler 2  TRFER2) 
// 14. pulse8  width (Scaler 2 TRFER2 )
// 15. trans4 offset (  TIG10 ) 
// 16. pulse9  offset (Dump DUMP) 
// 17. pulse10  offset   (Kicker1 KICK1)
// 18. pulse11  offset (Kicker 2 KICK2)
var a_X_array = new Array (159, 244, 164, 241, 241, 309, 377, 440, 500, 671, 706,711,593,701,676,886,865,933 )
var a_Y_array = new Array (600, 573,  80, 138, 175, 223, 257, 230, 430, 557, 297,329,372,392,430,443,515,572 )
var num_1a_items=1;

// relative values for 1b
// 1. Main Scan Loop
// 2. Extraction loop count
// 3. begin_extr  offset  (ISAC beam gate)
// 4. pulse1 width (TITAN beam gate 1 TIBG1 )
// 5. pulse2 width (TITAN beam gate 2 TIBG2 )
// 6. pulse3 offset (RFQ extraction RFQEXT )
// 7. pulse4 offset (Capture 1  CAP1)
// 8. pulse4 width (Capture 1 CAP1 )
// 9. pulse5 offset (Transer 1  TRFER1)
//10. pulse5 width (Transfer 1 TRFER1 )
//11. pulse6  offset (Transfer 2  TRFER2)
//12. pulse6  width (Transfer 2 TRFER2 )
var b_X_array = new Array (159, 247,164,240,240,303,375,438,440,502,429,486)
var b_Y_array = new Array (590, 556, 80,140,174,226,263,232,296,257,335,320)

// relative values for 1c
// 1. Main Scan Loop
// 2. Extraction loop count
// 3. Trans 1 offset
// 4.  begin_extr  offset  (ISAC beam gate)
// 5. pulse1 width (TITAN beam gate 1 TIBG1 )
// 6. pulse2 width (TITAN beam gate 2 TIBG2 )
// 7. pulse3 offset (RFQ extraction RFQEXT )
// 8. pulse3 width (RFQ extraction RFQEXT )
// 9. pulse4 offset (Capture 1  CAP1)
//10. pulse4 width (Capture 1 CAP1 ) 
//11. pulse5 offset (Transer 1  TRFER1) 
//12. pulse5 width (Transfer 1 TRFER1 ) 
//13. pulse6  offset (Transfer 2  TRFER2)
//14. pulse6  width (Transfer 2 TRFER2 )
//15. trans3 offset (  TIG10 )
//16. begin_sclr  Loop count  (Scaler Loop)
//17.  pulse7  offset (Scaler 1  TRFER2)
//18. pulse7  width (Scaler 1 TRFER2 )
//19. pulse8  offset (Scaler 2  TRFER2) 
//20. pulse8  width (Scaler 2 TRFER2 )
//21. trans4 offset (  TIG10 ) 
//22. pulse9  offset (Dump DUMP) 
//23. pulse10  offset   (Kicker1 KICK1)
//24. pulse9  width (Dump DUMP) 
//25. pulse10  width (Kicker1 KICK1)
//26. pulse11  offset (Kicker 2 KICK2)  
//27. pulse11  width (Kicker 2 KICK2) 
//28. trans5 offset ( E-BEAM 1 )
//29. trans5 Time Ref ( E-BEAM 1 )
//30. trans6 offset ( E-BEAM  1)
//31. trans6 Time Ref ( E-BEAM 1 )  (two divs) 
//32. trans7 offset ( E-BEAM 2 )
//33. trans7 Time Ref ( E-BEAM 2 )  
//34. trans8 offset ( E-BEAM  2) 
//35. trans8 Time Ref ( E-BEAM 2 )
//                          1   2   3   4   5   6   7   8   9   10  11  12 13  14  15  16   17  18  19  20  21  22  23  24  25  26  27  28  29 30   31 32  33  34  35
var c_X_array = new Array (285,280,168,201,240,240,296,337,356,419,421,482,394,460,528,626,622,662,528,656,622,800,791,927,857,820,897,167,169,767,630,240,240,622,685)
var c_Y_array = new Array (50, 82,  49, 90,125,155,202,162,235,195,267,233,302,286,386, 48,289,317,331,348,383,388,430,391,448,491,480,514,490,509,495,546,568,546,550)



// relative values for 1d
// 1. Extraction loop count
// 2. Trans 1 offset
// 3.  begin_extr  offset  (ISAC beam gate)
// 4. pulse1 width (TITAN beam gate 1 TIBG1 )
// 5. pulse2 width (TITAN beam gate 2 TIBG2 )
// 6. pulse3 offset (RFQ extraction RFQEXT )
// 7. pulse3 width (RFQ extraction RFQEXT )
// 8. pulse4 offset (Capture 1  CAP1)
// 9. pulse4 width (Capture 1 CAP1 ) 
//10. pulse5 offset (Transer 1  TRFER1) 
//11. pulse5 width (Transfer 1 TRFER1 ) 
//12. pulse6  offset (Transfer 2  TRFER2)
//13. pulse6  width (Transfer 2 TRFER2 )
//14. trans3 offset (  TIG10 )
//15. begin_sclr  Loop count  (Scaler Loop)
//16.  pulse7  offset (Scaler 1  TRFER2)
//17. pulse7  width (Scaler 1 TRFER2 )
//18. pulse8  offset (Scaler 2  TRFER2) 
//19. pulse8  width (Scaler 2 TRFER2 )
//20. trans4 offset (  TIG10 ) 
//21. pulse9  width (Dump DUMP) 
//22. pulse10  width (Kicker1 KICK1)
//23. pulse11  offset (Kicker 2 KICK2)  
//24. pulse11  width (Kicker 2 KICK2) 
//25. trans5 offset ( E-BEAM 1 )
//26. trans5 Time Ref ( E-BEAM 1 )
//27. trans6 offset ( E-BEAM  1)
//28. trans6 Time Ref ( E-BEAM 1 )  (two divs) 
//29. trans7 offset ( E-BEAM 2 )
//30. trans7 Time Ref ( E-BEAM 2 )  
//31. trans8 offset ( E-BEAM  2) 
//32. trans8 Time Ref ( E-BEAM 2 )
//33. End of Scan Delay

//2
//                          1    2   3   4   5   6   7   8   9  10  11  12   13 14  15  16  17  18  19  20  21  22  23  24  25  26  27  28
var d_X_array = new Array (360, 181,310,355,355,414,453,472,536,541,597,526,584,676,751,738,797,660,790,794,176,176,176,251,189,335,933,759,353,407,752,825,933)
var d_Y_array = new Array ( 50, 104, 84,138,174,221,178,250,210,286,250,327,310,410, 44,316,340,358,374,409,425,480,516,492,547,548,526,515,581,578,583,587, 60)


var Xarray = new Array();
var Yarray = new Array();

if (ppg_index == 0) 
{
   Xarray = a_X_array;
   Yarray = a_Y_array;
}
else if (ppg_index == 1) 
{
   Xarray = b_X_array;
   Yarray = b_Y_array;
}
else if (ppg_index == 2) 
{
   Xarray = c_X_array;
   Yarray = c_Y_array;
}
else if (ppg_index == 3) 
{
   Xarray = d_X_array;
   Yarray = d_Y_array;
}
do_images(Xarray,Yarray)
return
}

//=========================================================
function do_images(Xarray,Yarray)
{
   var x,y;
// alert ('do_images: Xarray = '+Xarray+' Yarray = '+Yarray+' ppg_index= '+ppg_index+ 'x0= '+x0+'y0='+y0)

   var i=0;
   if (ppg_mode == '1e' )
     return; 
  

     x = x0 + Xarray[i];
     y = y0 + Yarray[i];

//  alert('x= '+x+' y = '+y)

   if (ppg_mode != '1d' )  // 1d no scan loop
   {

//<!-- Main Scan Loop Count  -->   1a,1b,1c
//1a,1b,1c 1.

    document.write('<div style="position:absolute;  left:'+x+'px; top:'+y+'px;" >') 
    i++;

    jpath_mlc  = block_path + '/begin_scan/loop count';
    jvar = ODBGet(jpath_mlc);
    document.write('<font color=purple> Main Scan loop Count: ');
    document.write('<a href="#" onclick="ODBEdit(jpath_mlc)" >')
    document.write(jvar);
    document.write('</a>');
    document.write('') ;
    document.write('</font>');
    document.write('</div>') 

  } // end of ppg_mode != '1d' 



//<!-- Extraction Loop Count  -->
//1a,1b,1c 2.
//1d 1.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

jpath_elc  =block_path + '/begin_extr/loop count';
jvar = ODBGet(jpath_elc);
document.write('<font color=darkblue> Extraction loop count: ');
document.write('<a href="#" onclick="ODBEdit(jpath_elc)" >')
document.write(jvar);
document.write('</a>');
document.write('') ;
document.write('</font>');
document.write('</div>')


if (ppg_mode == '1c' || ppg_mode == '1d')
{ // not for 1a or 1b
//<!-- Trans 1 offset  -->
//1c 3.
//1d 2.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

   jpath_t1o =block_path + '/trans1/time offset (ms)';
   jvar = ODBGet(jpath_t1o);
   document.write('<a href="#" onclick="ODBEdit(jpath_t1o)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>')
} // not 1a/1b



//<!-- begin_extr  offset  (ISAC beam gate)  -->
//1a,1b,1d 3.
//1c 4.


 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

jpath_be =block_path + '/begin_extr/time offset (ms)'
jvar = ODBGet(jpath_be);
document.write('<a href="#" onclick="ODBEdit(jpath_be)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')



//<!-- pulse1 width (TITAN beam gate 1 TIBG1 ) --> 
//1a,1b,1d 4.
//1c 5.

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

jpath_p1w =block_path + '/pulse1/pulse width (ms)'
jvar = ODBGet(jpath_p1w);
document.write('<a href="#" onclick="ODBEdit(jpath_p1w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')

//<!-- pulse2 width (TITAN beam gate 2 TIBG2 )  -->
///1a,1b,1d 5.
//1c 6.

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

//if(ppg_mode == '1d')
 //  document.write('<div style="position:absolute;  left:364px; top:260px;">') //1d

jpath_p2w =block_path + '/pulse2/pulse width (ms)'
jvar = ODBGet(jpath_p2w);
document.write('<a href="#" onclick="ODBEdit(jpath_p2w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')


//<!-- pulse3 offset (RFQ extraction RFQEXT ) -->
///1a,1b,1d 6.
//1c 7.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

jpath_p3o = block_path + '/pulse3/time offset (ms)'
jvar = ODBGet(jpath_p3o);
document.write('<a href="#" onclick="ODBEdit(jpath_p3o)" >')
document.write(jvar + 'ms');
//alert (jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')

if(ppg_mode == '1c' || ppg_mode == '1d' )
{  // not 1a or 1b

//<!-- pulse3 width (RFQ extraction RFQEXT ) --> 
//1c 6.
//1d 7.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

   jpath_p3w = block_path + '/pulse3/pulse width (ms)'
   jvar = ODBGet(jpath_p3w);
   document.write('<a href="#" onclick="ODBEdit(jpath_p3w)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>')
} // not 1a or 1b



//<!-- pulse4 offset (Capture 1  CAP1)   --> 
///1a,1b 7.
//1c 9.
//1d 8.

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;


jpath_p4o = block_path + '/pulse4/time offset (ms)'  
jvar = ODBGet(jpath_p4o);
document.write('<a href="#" onclick="ODBEdit(jpath_p4o)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')

//<!-- pulse4 width (Capture 1 CAP1 )  -->
///1a,1b 8.
//1c 10.
//1d 9.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

jpath_p4w = block_path + '/pulse4/pulse width (ms)'  
jvar = ODBGet(jpath_p4w);
document.write('<a href="#" onclick="ODBEdit(jpath_p4w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')


if(ppg_mode != '1a')
{  //<!--  ppg_mode 1a   SKIP_TRANSFER means Pulses 5,6 are skipped  --> 


   //<!--  ppg_mode=1b   END_AFTER_EXTRACTION means everything else is skipped  --> 
   //<!--  ppg_modes 1c,1d  complete cycle   -->


//<!-- pulse5 offset (Transer 1  TRFER1)    -->
//1b 9.
//1c 11.
//1d 10.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

//   if(ppg_mode == '1d')
  //    document.write('<div style="position:absolute;  left:560px; top:370px;">') // 1d
   jpath_p5o = block_path + '/pulse5/time offset (ms)'  
   jvar = ODBGet(jpath_p5o);
   document.write('<a href="#" onclick="ODBEdit(jpath_p5o)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>')

//<!-- pulse5 width (Transfer 1 TRFER1 )  -->
//1b 10.
//1c 12.
//1d 11.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;


   jpath_p5w = block_path + '/pulse5/pulse width (ms)'  
   jvar = ODBGet(jpath_p5w);
   document.write('<a href="#" onclick="ODBEdit(jpath_p5w)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>')

  //<!-- pulse6  offset (Transfer 2  TRFER2)   --> 
//1b 11.
//1c 13.
//1d 12.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;


//   if(ppg_mode == '1d')
 //     document.write('<div style="position:absolute;  left:552px; top:410px;">') // 1d

   jpath_p6o = block_path + '/pulse6/time offset (ms)'  
   jvar = ODBGet(jpath_p6o);
   document.write('<a href="#" onclick="ODBEdit(jpath_p6o)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>')

 //<!-- pulse6  width (Transfer 2 TRFER2 )  -->
//1b 12.
//1c 14.
//1d 13.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

//   if(ppg_mode == '1d')
 //   document.write('<div style="position:absolute;  left:602px; top:395px;">') // 1d
   jpath_p6w = block_path + '/pulse6/pulse width (ms)'  
   jvar = ODBGet(jpath_p6w);
   document.write('<a href="#" onclick="ODBEdit(jpath_p6w)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>')
} // end of SKIP_TRANSFER for 1a
// ==========================================================================

//<!-- Mode 1b ends here (after extraction)  -->

// ==========================================================================

if (ppg_mode != '1b')
{ // all except 1b

//<!-- trans3 offset (  TIG10 )  -->
//1a 9.
//1c 15.
//1d 14.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;


   jpath_t3o = block_path + '/trans3/time offset (ms)'  
   jvar = ODBGet(jpath_t3o);
   document.write('<a href="#" onclick="ODBEdit(jpath_t3o)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>')

//<!-- begin_sclr  Loop count  (Scaler Loop) -->
//1a 10.
//1c 16.
//1d 15.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

   document.write('<font color=green>Scaler loop count: ')
   jpath_slc = block_path + '/begin_sclr/loop count'
   jvar = ODBGet(jpath_slc);
   document.write('<a href="#" onclick="ODBEdit(jpath_slc)" >')
   document.write(jvar);
   document.write('</a>');
   document.write('') ;
   document.write('</font>');
   document.write('</div>')



//<!-- pulse7  offset (Scaler 1  TRFER2)   -->

//1a 11.
//1c 17.
//1d 16.

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

 //  if(ppg_mode == '1d')
 //     document.write('<div style="position:absolute;  left:771px; top:400px;">') // 1d

   jpath_p7o = block_path + '/pulse7/time offset (ms)'  
   jvar = ODBGet(jpath_p7o);
   // alert (jvar)
   document.write('<a href="#" onclick="ODBEdit(jpath_p7o)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>');

//<!-- pulse7  width (Scaler 1 TRFER2 ) -->
//1a 12.
//1c 18.
//1d 17.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;


   jpath_p7w = block_path + '/pulse7/pulse width (ms)'  
   jvar = ODBGet(jpath_p7w);
   document.write('<a href="#" onclick="ODBEdit(jpath_p7w)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>')


//<!-- pulse8  offset (Scaler 2  TRFER2)    -->
//1a 13.
//1c 19.
//1d 18.

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

    jpath_p8o = block_path + '/pulse8/time offset (ms)'   
    jvar = ODBGet(jpath_p8o);
   document.write('<a href="#" onclick="ODBEdit(jpath_p8o)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>') 

 //<!-- pulse8  width (Scaler 2 TRFER2 )  -->
//1a 14.
//1c 20.
//1d 19.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

   jpath_p8w = block_path + '/pulse8/pulse width (ms)'   
    jvar = ODBGet(jpath_p8w);
   document.write('<a href="#" onclick="ODBEdit(jpath_p8w)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;

    document.write('</div>') 




//<!-- trans4 offset (  TIG10 )   -->
//1a 15.
//1c 21.
//1d 20.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

    jpath_t4o = block_path + '/end_sclr/time offset (ms)'   
    jvar = ODBGet(jpath_t4o);
   document.write('<a href="#" onclick="ODBEdit(jpath_t4o)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
    document.write('</div>') 



 if (ppg_mode != '1d')
   {  // Pulses 9 and 10 starts at T0 for 1d external trigger


//<!-- pulse9  offset (Dump DUMP)     -->

//1a 16.
//1c 22.


 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

     jpath_p9o = block_path + '/pulse9/time offset (ms)'   
     jvar = ODBGet(jpath_p9o);
     document.write('<a href="#" onclick="ODBEdit(jpath_p9o)" >')
     document.write(jvar + 'ms');

     document.write('</a>');
     document.write('') ;
     document.write('</div>')


//<!-- pulse10  offset   (Kicker1 KICK1)   -->
//1a 17.
//1c 23.

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

     jpath_p10o = block_path + '/pulse10/time offset (ms)'   
     jvar = ODBGet(jpath_p10o);
     document.write('<a href="#" onclick="ODBEdit(jpath_p10o)" >')
     document.write(jvar + 'ms');
//  alert(jvar + 'ms');
     document.write('</a>');
     document.write('') ;
     document.write('</div>')

} // end of  ppg_mode != '1d'

   if (ppg_mode != '1a')
{  // Pulses 9 and 10 fixed width for 1a 

//<!-- pulse9  width (Dump DUMP)     -->
//1c 24.
//1d 21

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

      jpath_p9w = block_path + '/pulse9/pulse width (ms)'   
      jvar = ODBGet(jpath_p9w);
      document.write('<a href="#" onclick="ODBEdit(jpath_p9w)" >')
      document.write(jvar + 'ms');
      document.write('</a>');
      document.write('') ;
      document.write('</div>')


 //<!-- pulse10  width (Kicker1 KICK1)    -->
//1c 25.
//1d 22.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

      jpath_p10w = block_path + '/pulse10/pulse width (ms)'   
      jvar = ODBGet(jpath_p10w);
      document.write('<a href="#" onclick="ODBEdit(jpath_p10w)" >')
      document.write(jvar + 'ms');
      document.write('</a>');
      document.write('') ;
       document.write('</div>')
 } // end of 1a fixed width



//<!-- pulse11  offset (Kicker 2 KICK2)     -->

//1a 18.
//1c 26.
//1d 23,

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;


    jpath_p11o = block_path + '/pulse11/time offset (ms)'   
    jvar = ODBGet(jpath_p11o);
   document.write('<a href="#" onclick="ODBEdit(jpath_p11o)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
    document.write('</div>') 


if(ppg_mode != '1a')
{  // 1a does not use this (fixed width)

//<!-- pulse11  width (Kicker 2 KICK2)    -->
//1c 27.
//1d 24.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

   jpath_p11w = block_path + '/pulse11/pulse width (ms)'   
    jvar = ODBGet(jpath_p11w);
   document.write('<a href="#" onclick="ODBEdit(jpath_p11w)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
    document.write('</div>') 



//<!-- trans5 offset ( E-BEAM 1 )   -->
//1c 28.
//1d 25.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;


   jpath_t50 = block_path + '/trans5/time offset (ms)'
   jvar = ODBGet(jpath_t50);
   document.write('<a href="#" onclick="ODBEdit(jpath_t50)" >')
   document.write(jvar +'ms');
   document.write('</a>');
   if(ppg_mode == '1c') 
      document.write('') ;
   else if(ppg_mode == '1d') 
      document.write('<span style="color:navy; font-size:75%;"> trans5 offset...</span>');
   document.write('</div>')


//<!-- trans5 Time Ref ( E-BEAM 1 )   -->
//1c 29.
//1d 26.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

     document.write('<span style="color:navy; font-size:75%;">')
    if(ppg_mode == '1d') 
       document.write('...from Time Ref');
   else if(ppg_mode == '1c')
        document.write('trans5 offset from Time Ref ');
     document.write('</span>') 

  jpath_t5tr = block_path + '/trans5/time reference';
  document.write('<span style="font-size:80%;">')
   jvar = ODBGet(jpath_t5tr);

   document.write('<a href="#" onclick="ODBEdit(jpath_t5tr)" >')
   document.write(jvar);
   document.write('</a>');
   document.write('') ;
    document.write('</span></div>') 

 //<!-- trans6 offset ( E-BEAM  1)   -->
// 1c 30.
//1d 27.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

   jpath_t6o = block_path + '/trans6/time offset (ms)' 
   jvar = ODBGet(jpath_t6o);
   document.write('<a href="#" onclick="ODBEdit(jpath_t6o)" >')
   document.write(jvar + 'ms');
   document.write('</a>'); 
    document.write('</div>') 

//<!-- trans6 Time Ref ( E-BEAM 1 )   -->
// 1c 31.
// 1d 28.

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 // two divs are written

   document.write('<span style="color:navy; font-size:75%;">trans6 offset from Time Ref</span> ') 
   document.write('</div>')

  y=y+15
  if (ppg_mode == '1d')
     x=x+25

   
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

   jpath_t6tr=block_path + '/trans6/time reference' 
  document.write('<span style="font-size:80%;">')
   jvar = ODBGet(jpath_t6tr);
   document.write('<a href="#" onclick="ODBEdit(jpath_t6tr)" >')
   document.write(jvar);
   document.write('</a></span>');
   document.write('<span style="color:navy; font-size:75%;"> by... </span>') ;
   document.write('</div>') 


//=============================================================

//<!-- trans7 offset ( E-BEAM 2 )   -->
//1c 32.
//1d 29.
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];

 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

   jpath_t7o = block_path + '/trans7/time offset (ms)'
   jvar = ODBGet(jpath_t7o);
//alert ('jpath_t7o = '+jpath_t7o+ 'jvar= '+jvar);
   document.write('<a href="#" onclick="ODBEdit(jpath_t7o)" >')
   document.write(jvar +'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>')


//<!-- trans7 Time Ref ( E-BEAM 2 )   -->
//1c 33.
//1d 30.

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

   document.write('<span style="color:navy; font-size:75%;">trans7 offset from Time Ref </span>');
   jpath_t7tr = block_path + '/trans7/time reference';
  document.write('<span style="font-size:80%;">')
   jvar = ODBGet(jpath_t7tr);
   document.write('<a href="#" onclick="ODBEdit(jpath_t7tr)" >')
   document.write(jvar);
   document.write('</a></span>');
   document.write('') ;
    document.write('</div>') 



 //<!-- trans8 offset ( E-BEAM  2)   -->
//1c 34.
//1d 31
 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

 
   jpath_t8o = block_path + '/trans8/time offset (ms)' 
   jvar = ODBGet(jpath_t8o);
   document.write('<a href="#" onclick="ODBEdit(jpath_t8o)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
    document.write(' ') ;
    document.write('</div>') 



//<!-- trans8 Time Ref ( E-BEAM 2 )   -->
//1c 35.
//1d 32.

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 // two divs

  if(ppg_mode == '1c')
    document.write('<span style="color:navy; font-size:75%;"> trans8 offset from Time Ref </span>') ;
  else // 1d
        document.write('<span style="color:navy; font-size:75%;"> trans8 offset from</span>') ;
    document.write('</div>');



 if(ppg_mode == '1c')
    x=x+175
 else // 1d
 {
    y=y+20
    x=x-50
 }
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 
 i++;
   if(ppg_mode == '1d')
      document.write('<span style="color:navy; font-size:75%;">Time Ref</span>') ;
   jpath_t8tr=block_path + '/trans8/time reference' 
   jvar = ODBGet(jpath_t8tr);
   document.write('<span style="font-size:80%;">')
   document.write('<a href="#" onclick="ODBEdit(jpath_t8tr)" >')
   document.write(jvar);
   document.write('</a>');

   document.write('</span></div>') 

if(ppg_mode == '1d')
   {

//<!-- End of Scan Delay   -->
// 1d 33.

 x = x0 + Xarray[i];
 y = y0 + Yarray[i];
 document.write('<div style="position:absolute; left:'+x+'px; top:'+y+'px;">'); 
 i++;

//      document.write('<div style="position:absolute;  left:950px; top:173px;">')  // 1d
    jpath_pdeoc = block_path + '/delay_eoc/time offset (ms)'   
    jvar = ODBGet(jpath_pdeoc);
   document.write('<a href="#" onclick="ODBEdit(jpath_pdeoc)" >')
   document.write(jvar + 'ms');
   document.write('</a>');
   document.write('') ;
   document.write('</div>') 
  }// end of 1d only

} // end of not 1a

} // end of not 1b



return;
}