// checkboxes.js
// code for polarity checkboxes
// For new ppg (32 input channels)


var nchannels=32; // array size. Max 32 PPG output channels 
var checkbox_names = new Array(nchannels); // filled by init_checkbox_names()  in checkboxes.js

function init_checkbox_names(nch)
{
   var i;
   var j;
   if(nch > nchannels)
      alert('Maximum number of PPG channels supported is '+nchannels);
   for (i=0; i<nch; i++)
   {
      j=i+1; // channels start at 1
      checkbox_names[i]="chan"+j;      
   }
}


function init_item(channel,ival)
{
 var my_name;
 //alert('init_item starting with channel '+channel+ ' ival= '+ival+' polarity= '+polarity);
 if(channel == 1)
    document.form2.chan1.checked = ival;
 if(channel == 2)
    document.form2.chan2.checked = ival;
 if(channel == 3)
    document.form2.chan3.checked = ival;
 if(channel == 4)
    document.form2.chan4.checked = ival;
 if(channel == 5)
    document.form2.chan5.checked = ival;
 if(channel == 6)
    document.form2.chan6.checked = ival;
 if(channel == 7)
    document.form2.chan7.checked = ival;
 if(channel == 8)
    document.form2.chan8.checked = ival;
 if(channel == 9)
    document.form2.chan9.checked = ival;
 if(channel == 10)
    document.form2.chan10.checked = ival;
 if(channel == 11)
    document.form2.chan11.checked = ival;
 if(channel == 12)
    document.form2.chan12.checked = ival;
 if(channel == 13)
    document.form2.chan13.checked = ival;
 if(channel == 14)
    document.form2.chan14.checked = ival;
 if(channel == 15)
    document.form2.chan15.checked = ival;
 if(channel == 16)
    document.form2.chan16.checked = ival;
 if(channel == 17)
    document.form2.chan17.checked = ival;
 if(channel == 18)
    document.form2.chan18.checked = ival;
 if(channel == 19)
    document.form2.chan19.checked = ival;
 if(channel == 20)
    document.form2.chan20.checked = ival;
 if(channel == 21)
    document.form2.chan21.checked = ival;
 if(channel == 22)
    document.form2.chan22.checked = ival;
 if(channel == 23)
    document.form2.chan23.checked = ival;
 if(channel == 24)
    document.form2.chan24.checked = ival;
 if(channel == 25)
    document.form2.chan25.checked = ival;
 if(channel == 26)
    document.form2.chan26.checked = ival;
 if(channel == 27)
    document.form2.chan27.checked = ival;
 if(channel == 28)
    document.form2.chan28.checked = ival;
 if(channel == 29)
    document.form2.chan29.checked = ival;
 if(channel == 30)
    document.form2.chan30.checked = ival;
 if(channel == 31)
    document.form2.chan31.checked = ival;
 if(channel == 32)
    document.form2.chan32.checked = ival;

// alert ('init_item: Set channel= '+channel+' to  '+ival);
 return;
}



function change_item(channel)
{
 var myval,my_name,newpol,index;

 index=channel-1 // array index
 var jval = 1 << index  // moving bit
 
  if(channel == 1)
    my_name = document.form2.chan1.checked
 if(channel == 2)
    my_name = document.form2.chan2.checked
 if(channel == 3)
    my_name = document.form2.chan3.checked
 if(channel == 4)
    my_name = document.form2.chan4.checked
 if(channel == 5)
    my_name = document.form2.chan5.checked
 if(channel == 6)
    my_name = document.form2.chan6.checked
 if(channel == 7)
    my_name = document.form2.chan7.checked
 if(channel == 8)
    my_name = document.form2.chan8.checked
 if(channel == 9)
    my_name = document.form2.chan9.checked
 if(channel == 10)
    my_name = document.form2.chan10.checked
 if(channel == 11)
    my_name = document.form2.chan11.checked
 if(channel == 12)
    my_name = document.form2.chan12.checked
 if(channel == 13)
    my_name = document.form2.chan13.checked
 if(channel == 14)
    my_name = document.form2.chan14.checked
 if(channel == 15)
    my_name = document.form2.chan15.checked
 if(channel == 16)
    my_name = document.form2.chan16.checked
 if(channel == 17)
    my_name = document.form2.chan17.checked
 if(channel == 18)
    my_name = document.form2.chan18.checked
 if(channel == 19)
    my_name = document.form2.chan19.checked
 if(channel == 20)
    my_name = document.form2.chan20.checked
 if(channel == 21)
    my_name = document.form2.chan21.checked
 if(channel == 22)
    my_name = document.form2.chan22.checked
 if(channel == 23)
    my_name = document.form2.chan23.checked
 if(channel == 24)
    my_name = document.form2.chan24.checked
 if(channel == 25)
    my_name = document.form2.chan25.checked
 if(channel == 26)
    my_name = document.form2.chan26.checked
 if(channel == 27)
    my_name = document.form2.chan27.checked
 if(channel == 28)
    my_name = document.form2.chan28.checked
 if(channel == 29)
    my_name = document.form2.chan29.checked
 if(channel == 30)
    my_name = document.form2.chan30.checked
 if(channel == 31)
    my_name = document.form2.chan31.checked
 if(channel == 32)
    my_name = document.form2.chan32.checked

 myval = my_name;
 //alert('change_item: my_name= '+my_name+' chan='+channel+' myval='+myval+' polarity= '+polarity)
   
// set or clear bit as required
if (myval == 1) newpol = polarity | jval
if (myval == 0) newpol = polarity & ~jval
  var string = d2h(newpol); // string now contains hex conversion
 //alert ('change_item: polarity='+polarity+' myval= '+myval+' jval='+jval+' now newpol = '+newpol+' or 0x'+string)


  //alert ('polarity bitpat = 0x'+string)
  ODBSet(polarity_path,newpol)
  polarity = newpol;
  return;
} 


// convert dec to hex for output
function d2h(d) {return d.toString(16);}

