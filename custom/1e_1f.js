// 1e_1f.js
//  included by pc_all.html
//
// Globals

  var nchannels=32; // array length (32 channels maximum)

  // arrays filled by setup_1e  (nchannels=32)
  var SignalNames = new Array(nchannels); 
  var widthArray = new Array(nchannels);
  var offsetArray = new Array(nchannels);
  var namesArray = new Array(nchannels);
  var names = new Array(nchannels);




function setup_1ef()
{
  // cpet uses 32 channels maximum

   var i=0;
   var j;
   while (i<nchannels)
   {
      j=i+1;
      SignalNames[i]= block_path + '/pulse'+j+'/ppg signal name';
      widthArray[i] = block_path + '/pulse'+j+'/pulse width (ms)'
      offsetArray[i] = block_path + '/pulse'+j+'/time offset (ms)'
      namesArray[i]= block_path + '/names/names['+i+']';

 //     document.write('widthArray'+i+' = '+ widthArray[i]+ '<br>')
 //     document.write('offsetArray'+i+' = '+ offsetArray[i]+ '<br>');
      i++;
   }
   //comment =ODBGet('/Experiment/Run Parameters/Comment'); // only room for the comment on 1e page
} // end of setup_1e

function add_tune_buttons()
{   // Add tune buttons for 1e,1f

    // document.write ('<br>hi') // new line
      document.write(' Load Tune: ');
      document.write('<input  class=tune name="customscript" value="default" type="submit">');
   
   //  document.write('<input  class=tune name="customscript" value="Rb74" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="Rb75" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="Rb76" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="Rb77" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="Rb78" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="Rb79" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="Rb85" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="Rb94" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="Rb96" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="breed95" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="breed50" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="breed35" type="submit">');
   //  document.write('<input  class=tune name="customscript" value="breed20" type="submit">');
    // document.write('<input  class=tune name="customscript" value="TOFgateRb85pass" type="submit">');
   }






function fix(jvar)
{ // remove trailing zeroes e.g. 0.070000000 becomes 0.07
	
   var pattern= /(0)+(\D)*$/; // zeroes at end of string possibly followed by a non-digit (e.g. CR?)
   var ivar = jvar+""; // string

   if (ivar.indexOf(".") == -1) return ivar;
   
   // debug
   //if (jvar > 93)
   //{
   //   alert ('ivar = '+ivar) 
   //   var result=pattern.exec(ivar);
   //   alert (pattern.test(ivar))
   //}

   ivar = ivar.replace(pattern, ""); // remove zeroes at end of string

   //if (jvar > 93)
   //alert ('ivar = '+ivar) 

   ivar = ivar.replace(/\.$/,""); // remove the decimal point if left at end of string
   
   //if (jvar > 93)
   //alert ('ivar = '+ivar)
 
   return ivar;
}

function digits(jvar)
{
   var factor=1;

   if (jvar < 0.0000001 )
      jvar *= 1000000;

if (jvar.toFixed) // if this is available
   jvar = jvar.toFixed(6);
alert ('jvar '+jvar);
}

function strip(jvar)
{   
   // returns a value with a reasonable number of sig figs.


  // pattern2 is used if jvar < 1
   var pattern2= /([0-9]*\.0+[1-9][1-9][1-9])/; // retain 3 numbers after string of zeroes

   var ivar = jvar+""; // string
   var nsig=6; // initialize to 6 say

   if (jvar > 0.01) // jvar is not too small
   {
       nsig = 3;
       if(jvar > 1000)  // jvar is very large
          nsig=0;
    
       if (jvar.toFixed) // if this is available
          jvar = jvar.toFixed(nsig); // round this up
	return jvar;
    }
      
   // jvar is very small ( <= 0.01 )

    if(pattern2.test(ivar))
    {  // found a string with a bunch of zeroes after decimal point
       result=ivar.match(pattern2)
       s = result[0]; // get first match
       var len = s.length;
       //alert( 's= '+s+ ' len= '+len)
       nsig = len-1;

       var j=s*1;  // j is a number now
       if (j.toFixed) // if this is available
       j = j.toFixed(nsig); // round this up
       return j;
    }
    else
    {
        // should not get here, but just in case pattern doesn't match...
        nsig = 3;
        if (jvar.toFixed) // if this is available
          jvar = jvar.toFixed(nsig); // round this up
	return jvar;
    }
}

function do_1g()
{
   document.write('1g mode: for now  use blocks to enter values'); 
}

// globals don't have to be sent explicitly
function do_1ef()
{
// 1e/1f
   var pattern = /DB_NO_KEY/; 
// users want to change the names so they are now in ODB under "/Equipment/titan_acq/ppg cycle/names/names"
  var ppg_cycle_path = '/Equipment/titan_acq/ppg_cycle_mode_'+ppg_mode;
  var names_path = ppg_cycle_path+'/names/names[*]'; // don't use link "ppg cycle" in case user
                                                                            // has just switched page
  //var names_path = block_path + "/names/names[*]";   
  names = ODBGet(names_path);

  // nch_path may not be present
  var nch_key_flag =1;
  var nch_path = ppg_cycle_path+'/image/num channels to display';
  var nch = ODBGet(nch_path);
  //alert ('nch = '+nch)
  if (pattern.test(nch) == true)
  {  // no key is present
     nch = nchannels; // default value
     nch_key_flag = 0; // flag for later
  }
  else if (nch < 1 || nch > 32)
  {
     alert('Using ODBSet \"'+nch_path+'\" to set value to 32 rather than '+nch);
     nch=32;
     ODBSet(nch_path,32); // set this value to avoid confusion
  }
  
// check for key "max active channel"  
// if key exists will be filled by convert_delays.pl (i.e. direct conversion, no bytecode)

  var mac_path = ppg_cycle_path+'/image/max active channel'; // highest channel that changes during cycle
  var max_active_ch = ODBGet(mac_path);
  var match = /DB_NO_KEY/.test(max_active_ch);
//  alert ('max_active_ch = '+max_active_ch+' match= '+match);

  if (match == true) 
     max_active_ch = -1; // no key; set an invalid number

//alert ('max_active_ch = '+max_active_ch)

  //alert ('number of channels to display '+nch+' nch_key_flag= '+nch_key_flag);
  init_checkbox_names(nch);
  var freq;
 
if (ppg_mode == '1f')  // outer table
{  
   // LOOP TABLE
   document.write('<center><table style="text-align: left; width: 100%; background-color:white" border="1" cellpadding="2" cellspacing="2"  >');
   document.write('<tr align="center">'); // Loop Table Row 1  Start
   document.write('<td rowspan="1"  colspan="2" style="vertical-align: top; background-color: lightgoldenrodyellow; font-weight: bold; font-size: 100% ;">'); // Loop Table  

   // include a small table here for the scan loop parameters
      document.write('<center><table style="text-align: center; background-color: lightgoldenrodyellow" border="0" cellpadding="2" cellspacing="2"  >');
      document.write('<td rowspan="1"  colspan="1" style="vertical-align: top; background-color: lightgoldenrodyellow; font-weight: bold; font-size: 100% ;">');
      document.write('<big>Outer Scan Loop Parameters : </big>  </td> ');
      document.write('<td rowspan="1"  colspan="1" style="vertical-align: top; background-color: lightgoldenrodyellow; font-weight: bold; font-size: 100% ;">');

      var scan_loop_path = block_path + '/begin_scan/loop count';
      var slc = ODBGet(scan_loop_path); // scan loop count
      //alert ('path: '+scan_loop_path)
      document.write(' Loop Count: ');
      if (rstate == state_stopped) // stopped
         document.write('<a href="#" onclick=\'ODBEdit("'+scan_loop_path+'")\' >')
      document.write(slc);
      if (rstate == state_stopped) // stopped
         document.write('</a>');
      document.write('</td>');
//      document.write('</tr>');   // Scan Table Row 1 End

//      document.write('<tr align="center">');  // Scan Table Row 2 Start
      document.write('<td rowspan="1"  colspan="1" style="vertical-align: top; background-color: lightgoldenrodyellow; font-weight: bold; font-size: 100% ;">');

      var end_loop_path = block_path + '/end_scan/time offset (ms)';
      var elo = ODBGet(end_loop_path);  // end loop count
      document.write('End Loop Offset: ');
      if (rstate == state_stopped) // stopped
         document.write('<a href="#" onclick=\'ODBEdit("'+end_loop_path+'")\' >')
      document.write(elo);
      if (rstate == state_stopped) // stopped
         document.write('</a>');
      document.write(' ms</td>');
      document.write('</tr>');   // Scan Table Row 2 End
      document.write('</table>');   // Scan Table End

      document.write('</tr>');   // Loop Table Row 1 End
      document.write('<tr align="center">');    // Loop Table Row 2  Start
      document.write('<td  colspan="1" style="vertical-align: top; background-color: lightgoldenrodyellow; font-weight: bold; font-size: 100% ;" >');
} // end of mode 1f
else
{ // mode 1e 
      // 1e "Loop" TABLE 
      document.write('<center><table style="text-align: left; width: 100%; background-color:white" border="0" cellpadding="2" cellspacing="2"  >');
   document.write('<tr align="center">'); // 1e "Loop" Table Row 1  Start
   document.write('<td rowspan="1"  colspan="1" style="vertical-align: top; background-color: white; font-weight: bold; font-size: 100% ;">'); // 1e Table  
   
}

// See how many tables are needed.
var nt=1;
var ntable=0; // counter
if (nch > 16)
{
   nt = 2;   // two tables of up to 16 inputs each
   max = 16; // max channels to display in the output table   
}
else
   max=nch;  // max channels to display in the output table

var offset=0;
while (ntable < nt)
{
 // alert ('ntable= '+ntable);
// OUTPUT TABLE
if (nt== 1)
   document.write('<center><table style="text-align: left;  background-color:white" border="1" cellpadding="2" cellspacing="2"  >');
else
   document.write('<center><table style="text-align: left;  background-color:white" border="1" cellpadding="2" cellspacing="2"  >');
document.write('<tr align="center">');  // Output Table  Row 1 Start

document.write('<td rowspan="2" style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Output</td>');
document.write('<td rowspan="2" style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Polarity</td>');
document.write('<td  rowspan="2" style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Output Name<br> <span style="font-weight: normal; font-size: small">(user-defined)</span></td>');
document.write('<td  colspan="4" style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">'); 
document.write('Pulse</td>');
document.write('</td></tr><tr align="center" >')
document.write('<td style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Width</td>');
document.write('<td style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Offset</td>');
document.write('<td style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Block name</td>');
document.write('<td style="vertical-align: top; background-color:  rgb(204, 204, 255); font-weight: bold; font-size: 100% ;">');
document.write('Signal</td>');
document.write('</tr>');  // Output Table  Row 1 End

   var chan;
   var idx=0;
   var ch;
   var index;
   while (idx < max)
   {
      index = idx + offset;
      chan=index+1;
      document.write('<tr>');  // // Output Table New ROW  
      document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
      document.write(chan+'</td>'); // ROW number
      document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
      document.write('<input type="checkbox" name="'+checkbox_names[index]+'" id="ch'+chan+'" onclick="change_item('+chan+')" ></td>');
      ival = polarity & (1<<index)  // bitwise operator      
      init_item(chan,ival); // set initial value of checkbox

      write_pulse(index);  // write the rest of the line  including  Output Table  Row End
      idx++;
} // end of 16 inputs
document.write('</table>');  // end of OUTPUT Table

if(ppg_mode== '1e')
   document.write('<td  colspan="1" style="vertical-align: top; background-color: white; font-weight: bold; font-size: 100% ;" >');   // end of column for 1e Table
else
    document.write('<td  colspan="1" style="vertical-align: top; background-color: lightgoldenrodyellow; font-weight: bold; font-size: 100% ;" >');   // end of column for LOOP Table or 1e Table

max = nch - 16; // next table
offset=16; // next table
ntable++;  // next output table
} // while ntable < 2

var max_path = titan_path +"/settings/ppg/output/last transition (ms)";
var max_time = ODBGet(max_path,"%.9f");

max_time = fix(max_time);





if (ppg_mode == '1e')  
{
   freq = 1000/max_time;
   freq = strip(freq);
// New ROW:   PPG Cycle Length
   document.write('<tr align="center" >');
   document.write('<td colspan="2" style="vertical-align: top; background-color: silver; font-weight: normal; font-size: 100% ;">');
   document.write('   PPG Cycle length= '+max_time+'ms;  PPG cycle frequency = '+freq+' Hz (updated by tri_config)');
   document.write('</td></tr>');
}
else // ppg_mode = '1f'  
{   // 1f  outer table

   document.write('</td></tr>');

// add a line for end loop offset
   document.write('<tr align="center">');

   document.write('<td colspan="2" style="vertical-align: top; background-color: lightgoldenrodyellow; font-weight: normal; font-size: 100% ;">');

   var jtmp = max_time / 1000; // seconds
   var ktmp = max_time / slc; // one loop
   ktmp= strip (ktmp);
   freq = 1/ktmp;
   freq = strip(freq);
   document.write('Total PPG Cycle length= '+jtmp+'sec; One loop takes '+ktmp+'ms ->  frequency = '+freq+' Hz (updated by tri_config)');

   document.write('</td>');
   document.write('</tr>');
} // end of 1f

if (nch_key_flag)
   {
      document.write('<tr align="center" >');
      document.write('<td colspan="2" style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
      document.write('Displaying ')
      document.write('<a href="#" onclick=\'ODBEdit("'+nch_path+'")\' >')
      document.write(nch+'</a> output channels   ');

      // max_active_ch =1 means no key image/max active channel
      if ((max_active_ch != -1) && (max_active_ch != nch ))
      { // display only if different from selected maximum (may be a skip, or higher channels may be programmed)
        // value comes from convert_delays.pl (direct conversion, no bytecode.dat)
	document.write('  Maximum active channel: '+max_active_ch+'</td></tr>');
      }
   }
else if (max_active_ch >= 0)
   {
      document.write('<tr align="center" >');
      document.write('<td colspan="2" style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
      document.write('  Maximum active channel: '+max_active_ch+'</td></tr>');
   }

document.write('</table>'); // OUTER table

document.write('</center>');

// testing testing
//freq = 934.12345600000000556789
//var frequency = strip(freq);
//alert('freq= '+freq+ ' frequency= '+frequency);

//freq = 156789123456.999999000556789
//var frequency = strip(freq);
//alert('freq= '+freq+ ' frequency= '+frequency);

//freq = .00199999000556789
//var frequency = strip(freq);
//alert('freq= '+freq+ ' frequency= '+frequency);

return;
}


function write_pulse(my_index)
{ 
   var ch = my_index+1;

   var elems = [namesArray[my_index], widthArray[my_index], offsetArray[my_index], SignalNames[my_index]];
   var formats = ["", "%.9f", "%.9f", ""];
   var block_settings = ODBMGet(elems, undefined, formats);
   

   document.write('<td  style="vertical-align: top; background-color: white; font-weight: normal; font-size: 100% ;">');
// Name
   names[my_index] = block_settings[0];
   document.write('<a href="#" onclick="ODBEdit(namesArray['+my_index+'])" >')
   document.write(names[my_index]);
   document.write('</a>');
 
   document.write(' </td>') ;


   document.write('<td  style="vertical-align: top; background-color:floralwhite; font-weight: normal; font-size: 100% ;">');
// pulse width

   jvar = block_settings[1];
   if(rstate == state_stopped)
      document.write('<a href="#" onclick="ODBEdit(widthArray['+my_index+'])" >')
   jvar=fix(jvar);
   document.write(jvar + 'ms');
   if (rstate == state_stopped)
      document.write('</a>');
 
   document.write(' </td>') ;

   document.write('<td style="vertical-align: top; background-color: mintcream; font-weight: normal; font-size: 100% ;">');


 // pulse offset
  
    jvar = block_settings[2];
    jvar=fix(jvar);
    //alert ('jvar, offsetArray'+jvar+offsetArray['+my_index+'])
    if(rstate == state_stopped)
       document.write('<a href="#" onclick="ODBEdit(offsetArray['+my_index+'])" >')
    document.write(jvar + 'ms');
    if(rstate == state_stopped)
       document.write('</a>');
   
   document.write('') ;  
   document.write('</td>') 

 // block name
   document.write('<td style="vertical-align: top; background-color: white; font-weight: normal; font-size: small ;">');
   document.write('Pulse'+ch)
   document.write('</td>') 
   
 // channel name
   document.write('<td style="vertical-align: top; background-color: white ; font-weight: normal; font-size: small ;">');
    jvar = block_settings[3];
    if(rstate == state_stopped)
       document.write('<a href="#" onclick="ODBEdit(SignalNames['+my_index+'])" >')
    document.write(jvar);
    if(rstate == state_stopped)
       document.write('</a>');

   
   document.write('</td>') 
   document.write('</tr>');
   return
}
