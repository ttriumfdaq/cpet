var odb_base = "/Equipment/Titan_acq/ppg cycle";
var unique_id = 0;
var ch_names = [];
var ch_shortnames = [];
var tref_shortnames = [];
var tref_texts = [];
var num_channels = 32;

function init() {
  setInterval(reload_image, 5000);
  
  mjsonrpc_db_get_value(odb_base).then(function(rpc) {
    handle_ppg_structure(rpc.result.data[0]);
    mhttpd_init('PPG programming');
  })
}

function populate_channel_names(ppg) {
  let odb_names = [];
  
  if (ppg.hasOwnProperty("names")) {
    odb_names = ppg["names"]["names"];
  }
  
  for (let i = 0; i < num_channels; i++) {
    let name = "";
    let shortname = "CH" + (i + 1);
    
    if (i < odb_names.length) {
      name += odb_names[i] + " (" + shortname + ")";
    } else {
      name = shortname;
    }
    
    ch_names.push(name);
    ch_shortnames.push(shortname);
  }
}

function block_name_to_loop_name(block_name) {
  let trim_len = 0;
  if (block_name.toLowerCase().indexOf("begin_") == 0) {
    trim_len = 6;
  } else if (block_name.toLowerCase().indexOf("begin") == 0) {
    trim_len = 5;
  } else if (block_name.toLowerCase().indexOf("end_") == 0) {
    trim_len = 5;
  } else if (block_name.toLowerCase().indexOf("end") == 0) {
    trim_len = 4;
  }
  return block_name.substr(trim_len);
}

function populate_tref_names(ppg) {
  tref_shortnames.push("");
  tref_texts.push("End of previous block");
  tref_shortnames.push("T0");
  tref_texts.push("Start of PPG sequence (T0)");
  
  for (let block_name in ppg) {
    if (block_name.indexOf("/name") != -1) {
      continue;
    }
    if (block_name.indexOf("/key") != -1) {
      continue;
    }
    if (["skip", "image", "names"].indexOf(block_name) != -1) {
      continue;
    }
    
    let block = ppg[block_name];
    let lower = block_name.toLowerCase();
    
    if (lower.indexOf("trans") == 0) {
      tref_shortnames.push("_T" + block_name);
      tref_texts.push("Transition " + block_name)
    } else if (lower.indexOf("pulse") == 0 || lower.indexOf("stdpulse") == 0) {
      tref_shortnames.push("_TSTART_" + block_name);
      tref_texts.push("Start of " + block_name)
      tref_shortnames.push("_TEND_" + block_name);
      tref_texts.push("End of " + block_name)
    } else if (lower.indexOf("pat") == 0) {
      tref_shortnames.push("_T" + block_name);
      tref_texts.push("Pattern " + block_name)
    } else if (lower.indexOf("del") == 0) {
      tref_shortnames.push("_T" + block_name);
      tref_texts.push("Delay (start/end???) " + block_name)
    } else if (lower.indexOf("tim") == 0) {
      tref_shortnames.push("_T" + block_name);
      tref_texts.push("Time reference " + block_name)
    } else if (lower.indexOf("begin") == 0) {
      let loop_name = block_name_to_loop_name(block_name)
      tref_shortnames.push("_TBEG" + loop_name);
      tref_texts.push("Start of each loop of " + loop_name)
    } else if (lower.indexOf("end") == 0) {
      let loop_name = block_name_to_loop_name(block_name)
      tref_shortnames.push("_TEND" + loop_name);
      tref_texts.push("End of all loops of " + loop_name)
    } 
  }  
}

function handle_ppg_structure(ppg) {
  let html = "";
  let loop_depth = 0;

  populate_channel_names(ppg);
  populate_tref_names(ppg);
  
  for (let block_name in ppg) {
    if (block_name.indexOf("/name") != -1) {
      continue;
    }
    if (block_name.indexOf("/key") != -1) {
      continue;
    }
    if (["skip", "image", "names"].indexOf(block_name) != -1) {
      continue;
    }
    
    let block = ppg[block_name];
    
    if (block_name.toLowerCase().indexOf("end") == 0) {
      loop_depth -= 1;
    }
    
    html += block_html(block_name, block, loop_depth);

    if (block_name.toLowerCase().indexOf("begin") == 0) {
      loop_depth += 1;
    }
  }
  
  $("#ppg").html(html);
}

function odb_span_html(block_name, param, format) {
  let odb_format = '';
  if (format === undefined) {
    odb_format = 'data-format="' + format + '"';
  }
  let odb_path = odb_base + "/" + block_name + "/" + param;
  return '<span class="modbvalue" data-odb-editable="1" data-odb-path="' + odb_path + '" ' + odb_format + '></span>';
}

function update_sel_from_odb(sel_id) {
  let odb_val = $("#sel_span_" + sel_id).html();
  let sel_val = odb_val;
  let sel_obj_id = "#sel_" + sel_id;
  
  let sel_opts = $(sel_obj_id + " option").map(function() {return $(this).val();}).get();
  
  if (sel_opts.indexOf(odb_val) == -1) {
    // Handle any case-sensitivity if val not in list
    for (var i in sel_opts) {
      if (sel_opts[i].toLowerCase() == odb_val.toLowerCase()) {
        sel_val = sel_opts[i];
        break;
      }
    }
  }
  
  $(sel_obj_id).val(sel_val);
}

function update_odb_from_sel(odb_path, sel_id) {
  let sel_obj_id = "#sel_" + sel_id;
  let val = $(sel_obj_id).val();
  mjsonrpc_db_set_value(odb_path, val);
}

function odb_select_html(block_name, param, values, texts) {
  let odb_path = odb_base + "/" + block_name + "/" + param;
  let html = "";
  
  html += '<select id="sel_' + unique_id + '" style="max-width:250px" onchange="update_odb_from_sel(\'' + odb_path + '\', ' + unique_id + ')">';
  for (let i in values) {
    html += '<option value="' + values[i] + '">' + texts[i] + '</option>';
  }
  html += '</select>';

  html += '<span style="display:none" class="modbvalue" data-odb-path="' + odb_path + '" id="sel_span_' + unique_id + '" onchange="update_sel_from_odb(' + unique_id + ')"></span>';
  
  unique_id += 1;
  return html;
}

function block_html(block_name, block, loop_depth) {
  let retval = "";
  let lower = block_name.toLowerCase();
  
  let t_offset = undefined;
  if (block.hasOwnProperty("time offset (ms)")) {
    t_offset = block["time offset (ms)"]
  }
  
  let t_ref = undefined;
  if (block.hasOwnProperty("time reference")) {
    t_ref = block["time reference"]
  }
  
  let padding = "padding-left:" + 20 * loop_depth + "px;";

  retval += "<tr><td><i style='" + padding + "'>" + block_name + "</i></td>" ;
  retval += '<td><span style="display:inline-block;min-width:100px;text-align:right;' + padding + '">';
  
  if (t_offset === undefined) {
    retval += "<small></small>";
  } else {
    retval += odb_span_html(block_name, "time offset (ms)", "f") + "ms";
  }
  
  retval += "</span> after&nbsp;";
  
  if (t_ref === undefined) {
    retval += "end of previous block"; 
  } else {
    retval += odb_select_html(block_name, "time reference", tref_shortnames, tref_texts); 
  }
  
  retval += "</td><td><span style='" + padding + "'>";

  if (lower.indexOf("trans") == 0) {
    retval += "toggle " + odb_select_html(block_name, "ppg signal name", ch_shortnames, ch_names);
  } else if (lower.indexOf("pulse") == 0) {
    retval += "turn on " + odb_select_html(block_name, "ppg signal name", ch_shortnames, ch_names);
    retval += " for " + odb_span_html(block_name, "pulse width (ms)", "f") + "ms";
  } else if (lower.indexOf("stdpulse") == 0) {
    retval += "turn on " + odb_select_html(block_name, "ppg signal name", ch_shortnames, ch_names);
    retval += ' for standard time (<span class="modbvalue" data-odb-path="/Equipment/Titan_acq/Settings/ppg/input/standard pulse width (ms)"></span>ms)';
  } else if (lower.indexOf("pat") == 0) {
    retval += "set bit pattern to " + odb_span_html(block_name, "bit pattern");
  } else if (lower.indexOf("del") == 0) {
    retval += "do nothing (sleep/delay)"
  } else if (lower.indexOf("tim") == 0) {
    retval += "Documentation was missing for this...";
  } else if (lower.indexOf("begin") == 0) {
    retval += "start looping " + odb_span_html(block_name, "loop count") + " times";
  } else if (lower.indexOf("end") == 0) {
    retval += "end the loop"
  } else {
    retval += "Unhandled block type";
  }
  
  retval += "</span></td></tr>";
  
  return retval;
}

function exec_customscript(name) {
   var params = new Object;
   params.customscript = name;
   mjsonrpc_call("exec_script", params).then(function(rpc) {
      var status = rpc.result.status;
      if (status != 1) {
         dlgAlert("Exec customscript \"" + name + "\" status " + status);
      }
   }).catch(function(error) {
      mjsonrpc_error_alert(error);
   });
}

function del_plot() {
  $("#delplot").attr("disabled", true);
  var params = new Object;
  params.customscript = "delplot";
  mjsonrpc_call("exec_script", params).then(function(rpc) {
     var status = rpc.result.status;
     if (status == 1) {
       setTimeout(reload_image, 1000);
       setTimeout(reload_image, 2000);
       setTimeout(reload_image, 3000);
     } else {
        dlgAlert("Failed to run customscript - status " + status);
     }
     $("#delplot").attr("disabled", false);
  }).catch(function(error) {
     mjsonrpc_error_alert(error);
     $("#delplot").attr("disabled", false);
  });
}

function new_plot() {
  $("#newplot").attr("disabled", true);
  var params = new Object;
  params.customscript = "newplot";
  mjsonrpc_call("exec_script", params).then(function(rpc) {
     var status = rpc.result.status;
     if (status == 1) {
       setTimeout(reload_image, 1000);
       setTimeout(reload_image, 2000);
       setTimeout(reload_image, 3000);
     } else {
        dlgAlert("Failed to run customscript - status " + status);
     }
     $("#newplot").attr("disabled", false);
  }).catch(function(error) {
     mjsonrpc_error_alert(error);
     $("#newplot").attr("disabled", false);
  });
}

function reload_image() {
  $("#plot").attr("src", "ppgplot.gif#" + new Date().getTime());
}