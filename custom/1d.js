//How to add ODBEdit box
//============================================
//document.write('Using Javascript and ODBEdit:')
//path='/runinfo/run number' 
//rn = ODBGet(path)
//document.write('<a href="#" onclick="ODBEdit(path)" >')
//document.write(rn)
//document.write('</a>');
//document.write('') ;
//========================================================
 // Note: need to use a different path variable for each one 

//<!-- Extraction Loop Count  -->
document.write('<div style="position:absolute; left:438px; top:132px;"> ');
var jpath_elc  =block_path + '/begin_extr/loop count';
jvar = ODBGet(jpath_elc);
document.write('<font color=darkblue> Extraction loop count:');
document.write('<a href="#" onclick="ODBEdit(jpath_elc)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</font>');
document.write('</div>')


//<!-- Trans 1 offset  -->
document.write('<div style="position:absolute; left:203px; top:188px;">')
var jpath_t1o =block_path + '/trans1/time offset (ms)';
jvar = ODBGet(jpath_t1o);
document.write('<a href="#" onclick="ODBEdit(jpath_t1o)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')


//<!-- begin_extr  offset  (ISAC beam gate)  -->
document.write('<div style="position:absolute;  left:322px; top:188px;">')
var jpath_be =block_path + '/begin_extr/time offset (ms)'
jvar = ODBGet(jpath_be);
document.write('<a href="#" onclick="ODBEdit(jpath_be)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')


//<!-- pulse1 width (TITAN beam gate 1 TIBG1 ) --> 
document.write('<div style="position:absolute;  left:364px; top:225px;">')
var jpath_p1w =block_path + '/pulse1/pulse width (ms)'
jvar = ODBGet(jpath_p1w);
document.write('<a href="#" onclick="ODBEdit(jpath_p1w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')




//<!-- pulse2 width (TITAN beam gate 2 TIBG2 )  -->
document.write('<div style="position:absolute;  left:364px; top:260px;">')
var jpath_p2w =block_path + '/pulse2/pulse width (ms)'
jvar = ODBGet(jpath_p2w);
document.write('<a href="#" onclick="ODBEdit(jpath_p2w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')


//<!-- pulse3 offset (RFQ extraction RFQEXT ) -->
document.write('<div style="position:absolute;  left:423px; top:303px;">')
var jpath_p3o = block_path + '/pulse3/time offset (ms)'
jvar = ODBGet(jpath_p3o);
document.write('<a href="#" onclick="ODBEdit(jpath_p3o)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')

//<!-- pulse3 width (RFQ extraction RFQEXT ) --> 
document.write('<div style="position:absolute;  left:462px; top:262px;">')
var jpath_p3w = block_path + '/pulse3/pulse width (ms)'
jvar = ODBGet(jpath_p3w);
document.write('<a href="#" onclick="ODBEdit(jpath_p3w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')


//<!-- pulse4 offset (Capture 1  CAP1)   --> 
document.write('<div style="position:absolute;  left:490px; top:340px;">')
var jpath_p4o = block_path + '/pulse4/time offset (ms)'  
jvar = ODBGet(jpath_p4o);
document.write('<a href="#" onclick="ODBEdit(jpath_p4o)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')

//<!-- pulse4 width (Capture 1 CAP1 )  -->
document.write('<div style="position:absolute;  left:552px; top:295px;">')
var jpath_p4w = block_path + '/pulse4/pulse width (ms)'  
jvar = ODBGet(jpath_p4w);
document.write('<a href="#" onclick="ODBEdit(jpath_p4w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')

//<!-- pulse5 offset (Transer 1  TRFER1)    -->

//<!--  image_flag=2   END_AFTER_EXTRACTION means everything else is skipped  --> 
//<!--  image-flag=1   SKIP_TRANSFER means Pulses 5,6 are skipped  -->
//<!--  image-flag=0   complete cycle   -->


document.write('<div style="position:absolute;  left:560px; top:370px;">')
var jpath_p5o = block_path + '/pulse5/time offset (ms)'  
jvar = ODBGet(jpath_p5o);
document.write('<a href="#" onclick="ODBEdit(jpath_p5o)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')

//<!-- pulse5 width (Transfer 1 TRFER1 )  -->

document.write('<div style="position:absolute;  left:613px; top:340px;">')
var jpath_p5w = block_path + '/pulse5/pulse width (ms)'  
jvar = ODBGet(jpath_p5w);
document.write('<a href="#" onclick="ODBEdit(jpath_p5w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')

  //<!-- pulse6  offset (Transfer 2  TRFER2)   --> 

document.write('<div style="position:absolute;  left:552px; top:410px;">')
var jpath = block_path + '/pulse6/time offset (ms)'  
jvar = ODBGet(jpath);
document.write('<a href="#" onclick="ODBEdit(jpath)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')

 //<!-- pulse6  width (Transfer 2 TRFER2 )  -->

document.write('<div style="position:absolute;  left:602px; top:395px;">')
var jpath_p6w = block_path + '/pulse6/pulse width (ms)'  
jvar = ODBGet(jpath_p6w);
document.write('<a href="#" onclick="ODBEdit(jpath_p6w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')

//<!-- begin_sclr  Loop count  (Scaler Loop) -->
document.write('<div style="position:absolute;  left:792px; top:125px;">')
document.write('<font color=green> Scaler loop count')
var jpath = block_path + '/begin_sclr/loop count'
jvar = ODBGet(jpath);
document.write('<a href="#" onclick="ODBEdit(jpath)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</font>');
document.write('</div>')



//<!-- pulse7  offset (Scaler 1  TRFER2)   -->
document.write('<div style="position:absolute;  left:771px; top:400px;">')
var jpath_p7o = block_path + '/pulse7/time offset (ms)'  
jvar = ODBGet(jpath_p7o);
document.write('<a href="#" onclick="ODBEdit(jpath_p7o)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>');

//<!-- pulse7  width (Scaler 1 TRFER2 ) -->
document.write('<div style="position:absolute;  left:810px; top:423px;">')
var jpath_p7w = block_path + '/pulse7/pulse width (ms)'  
jvar = ODBGet(jpath_p7w);
document.write('<a href="#" onclick="ODBEdit(jpath_p7w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')


//<!-- pulse8  offset (Scaler 2  TRFER2)    -->
 document.write('<div style="position:absolute;  left:694px; top:436px;">') 
 var jpath_p8o = block_path + '/pulse8/time offset (ms)'   
 jvar = ODBGet(jpath_p8o);
document.write('<a href="#" onclick="ODBEdit(jpath_p8o)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>') 

 //<!-- pulse8  width (Scaler 2 TRFER2 )  -->
 document.write('<div style="position:absolute;  left:801px; top:455px;">') 
var jpath_p8w = block_path + '/pulse8/pulse width (ms)'   
 jvar = ODBGet(jpath_p8w);
document.write('<a href="#" onclick="ODBEdit(jpath_p8w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;

 document.write('</div>') 



//<!-- trans3 offset (  TIG10 )  -->

document.write('<div style="position:absolute;  left:688px; top:513px;">')
var jpath_t3o = block_path + '/trans3/time offset (ms)'  
 jvar = ODBGet(jpath_t3o);
document.write('<a href="#" onclick="ODBEdit(jpath_t3o)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
document.write('</div>')

//<!-- trans4 offset (  TIG10 )   -->
 document.write('<div style="position:absolute;  left:782px; top:496px;">') 
 var jpath_t4o = block_path + '/end_sclr/time offset (ms)'   
 jvar = ODBGet(jpath_t4o);
document.write('<a href="#" onclick="ODBEdit(jpath_t4o)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
 document.write('</div>') 


// Pulses 9 and 10 starts at T0 for 1d external trigger



//<!-- pulse9  width (Dump DUMP)     -->
 document.write('<div style="position:absolute;  left:185px; top:511px;">') 
 var jpath_p9w = block_path + '/pulse9/pulse width (ms)'   
 jvar = ODBGet(jpath_p9w);
document.write('<a href="#" onclick="ODBEdit(jpath_p9w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
 document.write('</div>')


 //<!-- pulse10  width (Kicker1 KICK1)    -->
 document.write('<div style="position:absolute;  left:190px; top:564px;">') 
 var jpath_p10w = block_path + '/pulse10/pulse width (ms)'   
 jvar = ODBGet(jpath_p10w);
document.write('<a href="#" onclick="ODBEdit(jpath_p10w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
 document.write('</div>')
 



//<!-- pulse11  offset (Kicker 2 KICK2)     -->
document.write('<div style="position:absolute;  left:185px; top:605px;">') 
 var jpath_p11o = block_path + '/pulse11/time offset (ms)'   
 jvar = ODBGet(jpath_p11o);
document.write('<a href="#" onclick="ODBEdit(jpath_p11o)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
 document.write('</div>') 




//<!-- pulse11  width (Kicker 2 KICK2)    -->
document.write('<div style="position:absolute;  left:246px; top:597px;">') 
 var jpath_p11w = block_path + '/pulse11/pulse width (ms)'   
 jvar = ODBGet(jpath_p11w);
document.write('<a href="#" onclick="ODBEdit(jpath_p11w)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
 document.write('</div>') 



//<!-- trans5 offset ( E-BEAM )   -->
 document.write('<div style="position:absolute;  left:225px; top:642px;">') 
 var jpath_t50 = block_path + '/trans5/time offset (ms)'
 jvar = ODBGet(jpath_t50);
document.write('<a href="#" onclick="ODBEdit(jpath_t50)" >')
document.write(jvar +'ms');
document.write('</a>');
//document.write('') ;
document.write(' offset');
document.write('</div>')

//<!-- trans5 Time Ref ( E-BEAM )   -->
 document.write('<div style="position:absolute;  left:225px; top:662px;">') 
document.write('from Time Ref ');
var jpath_t5tr = block_path + '/trans5/time reference';
jvar = ODBGet(jpath_t5tr);
document.write('<a href="#" onclick="ODBEdit(jpath_t5tr)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
 document.write('</div>') 



 //<!-- trans6 offset ( E-BEAM )   -->
document.write('<div style="position:absolute;  left:950px; top:655px;">')
var jpath_t6o = block_path + '/trans6/time offset (ms)' 
 jvar = ODBGet(jpath_t6o);
document.write('<a href="#" onclick="ODBEdit(jpath_t6o)" >')
document.write(jvar + 'ms');
document.write('</a>');
 document.write(' offset from ') 
 document.write('</div>') 

//<!-- trans6 Time Ref ( E-BEAM )   -->
 document.write('<div style="position:absolute;  left:950px; top:675px;">') 
document.write('Time Ref ') 
jpath_t6tr=block_path + '/trans6/time reference' 
jvar = ODBGet(jpath_t6tr);
document.write('<a href="#" onclick="ODBEdit(jpath_t6tr)" >')
document.write(jvar + 'ms');
document.write('</a>');
document.write('') ;
 document.write('</div>') 


