/*
print_blocks.c

 Prints output for debugging

$Id$
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "midas.h"
#include "experim.h"
#include "tri_config.h"
#include "tri_config_prototypes.h"

#ifndef POL
extern TITAN_ACQ_SETTINGS settings;
#else
extern POL_ACQ_SETTINGS settings;
#endif

void  print_pulse_blocks(FILE *fout)
{
  INT i;
  char string[15];

  
  if(fout == NULL)
    {
      printf("print_pulse_blocks: ERROR fout is NULL; cannot print\n");
      return;
    }


  fprintf(fout,"\nprint_pulse_blocks:  PULSE or TRANS blocks so far \n");
  fprintf(fout,"index start         width        t0     time_ref          ppg  output  block\n");
  fprintf(fout,"      time(ms)      (ms)       offset index   name        num   name \n");  
  for (i=0; i<  block_counter[PULSE]; i++)
    {
      if(  (strcmp(pulse_params[i].output_name,"bitpattern")==0) ||
           (strcmp(pulse_params[i].output_name,"delay")==0) )
	{
#ifdef BITS_32
	  char format[]="0x%8.8x";
          DWORD mask = 0xFFFFFFFF;
#elif defined BITS_24
	  char format[]="0x%6.6x";
          DWORD mask = 0x00FFFFFF;
#elif defined BITS_8
	  char format[]="0x%2.2x";
          DWORD mask = 0x000000FF;
#else // BITS_16
	  char format[]="0x%4.4x";
          DWORD mask = 0x0000FFFF;
#endif

	  sprintf(string,format, pulse_params[i].ppg_signal_num & mask);
	}
      else
	sprintf(string,"%2.2d  %-10.10s", pulse_params[i].ppg_signal_num,
		pulse_params[i].output_name);
      
      fprintf(fout,"%2.2d  %10.4f  %10.4f %10.4f %2.2d  %-15.15s  %-15.15s %s\n",
	     i, 	pulse_params[i].time_ms,  pulse_params[i].width_ms,
	     pulse_params[i].t0_offset_ms,
	     pulse_params[i].time_ref_index , 
	     time_ref_params[pulse_params[i].time_ref_index].this_reference_name,
	     string, pulse_params[i].block_name);
    }
  return;
}     




void print_sorted_transitions(TRANSITIONS *ptrans, BOOL bp, FILE *fout )
{
  /* Call with 
     bp=TRUE if ptrans[i].bitpattern is filled,
     bp=FALSE if  bitpattern is not yet filled (except for PATTERN blocks); displays bit instead 

     *fout is stdout unless printing to a file

     global: gbl_combined is set true after call to combine_transitions
     if  gbl_combined is true, do not display PPG bit & bitname
*/

  INT i,j,index;
  char string[128];

  if(fout == NULL)
    {
      printf("print_sorted_transitions: ERROR fout is NULL; cannot print\n");
      return;
    }

  if(debug)
    {
      printf("print_sorted_transitions: num transitions=%d; gbl_combined=%d\n", 
	     gbl_num_transitions,gbl_combined);

#ifdef BITS_32
      printf("32 bits enabled\n");
#else
#ifdef BITS_24
      printf("24 bits enabled\n");
#else
      printf("16 bits enabled\n");
#endif
#endif
    }
      
  if(gbl_combined)
    {
      fprintf(fout,
	      "\nSorted Transitions blocks:  number time refs defined=%d\n",gbl_num_time_references_defined);
      fprintf(fout,
	      "i  |t0_offset(ms) |  P P G  |trans|t i m e    r e f e r e n c e | block  | block\n");
      fprintf(fout,
	      "   |              | pattern |code |index            name    code| index  | name\n"); 
    }
  else
    {
      fprintf(fout,
	      "\nSorted Transitions blocks:  number time refs defined=%d\n",gbl_num_time_references_defined);
      fprintf(fout,
	      "i  |t0_offset(ms) |......  P P G .......... |trans|t i m e ... reference..| block  | block\n");
      fprintf(fout,
	      "   |              |bit name   in hex pattern|code | index  name       code| index  | name\n"); 
    }


  for (i=0; i< gbl_num_transitions; i++)
    {
      index =  ptrans[i].time_reference_index;
      if(is_loop(ptrans[i].code)) // returns true if a loop  
	{
	 sprintf(string,"unchanged (loop)");	// loop
	}
      else if(ptrans[i].bit & 0x80000000) // bit pattern
	{
#ifdef BITS_32
	    sprintf(string,"0x%8.8x",ptrans[i].bitpattern);
#else
#ifdef BITS_24
	    sprintf(string,"0x%6.6x",ptrans[i].bitpattern);
#else
	    sprintf(string,"   0x%4.4x",ptrans[i].bitpattern);
#endif
#endif
	}
      else if( ptrans[i].bit & 0x40000000) // delay
	sprintf(string,"unchanged");
      
      else  // transitions
	{
	  if(gbl_combined)
	    {
	      
#ifdef BITS_32
	      sprintf(string,"0x%8.8x",
		      ptrans[i].bitpattern);
#else
#ifdef BITS_24
	      sprintf(string,"0x%6.6x",
		      ptrans[i].bitpattern);
#else
	      sprintf(string,"0x%4.4x",
		      ptrans[i].bitpattern);
#endif
#endif	     
	    }
	  else
	    {
#ifdef BITS_32
	      sprintf(string,"%2.2d %-10s %8.8x %8.8x",
		      ptrans[i].bit,
		      ppg_signal_names[ptrans[i].bit+OFFSET], 1<<ptrans[i].bit, ptrans[i].bitpattern);
#else
#ifdef BITS_24
	      
	      sprintf(string,"%2.2d %-10s %6.6x %6.6x",
		      ptrans[i].bit,
		      ppg_signal_names[ptrans[i].bit+OFFSET], 1<<ptrans[i].bit, ptrans[i].bitpattern);
#else
	      sprintf(string,"%2.2d %-10s %4.4x %4.4x",
		      ptrans[i].bit,
		      ppg_signal_names[ptrans[i].bit+OFFSET], 1<<ptrans[i].bit, ptrans[i].bitpattern);
#endif
#endif
	      //printf( "++++++= i=%d bitpattern=0x%x;   string:%s\n",
	      // i, ptrans[i].bitpattern,string);
	    }
	}
      if(gbl_combined)
	{
	  fprintf(fout,"%3.3d    %11.5f %9.9s  %1.1d      %2.2d  %20.20s   %1.1d    %2.2d      %s\n",
		  i,ptrans[i].t0_offset_ms ,string,
		  ptrans[i].code, index,
		  time_ref_params[index].this_reference_name,
		  time_ref_params[index].timeref_code,
		  ptrans[i].block_index,  ptrans[i].block_name);
	}
      
      else
	{
#ifdef BITS_32
	  fprintf(fout,"%3.3d    %11.5f %29.29s  %1.1d      %2.2d %15.15s   %1.1d    %2.2d      %s\n",
		  i,ptrans[i].t0_offset_ms ,string,
		  ptrans[i].code, index,
		  time_ref_params[index].this_reference_name,
		  time_ref_params[index].timeref_code,
		  ptrans[i].block_index,  ptrans[i].block_name);
#else
#ifdef BITS_24
	  fprintf(fout,"%3.3d    %11.5f %27.27s  %1.1d      %2.2d %15.15s   %1.1d    %2.2d      %s\n",
		  i,ptrans[i].t0_offset_ms ,string,
		  ptrans[i].code, index,
		  time_ref_params[index].this_reference_name,
		  time_ref_params[index].timeref_code,
		  ptrans[i].block_index,  ptrans[i].block_name);
#else
	  fprintf(fout,"%3.3d    %11.5f %25.25s  %1.1d      %2.2d %15.15s   %1.1d    %2.2d      %s\n",
		  i,ptrans[i].t0_offset_ms ,string,
		  ptrans[i].code, index,
		  time_ref_params[index].this_reference_name,
		  time_ref_params[index].timeref_code,
		  ptrans[i].block_index,  ptrans[i].block_name);
#endif
#endif
	}
      //printf("i=%d\n",i);
    }
  
  return;
}

#ifdef HAVE_AWG
void print_ev_step_params(INT index, BOOL print_values, FILE *fout)
{
  INT i,j;

  if(fout == NULL)
    {
      printf("print_ev_step_params: ERROR fout is NULL; cannot print\n");
      return;
    }


  fprintf(fout,"ev_step_params: \n");

  fprintf(fout,"Ev_Step index                  %d\n",index);
  fprintf(fout,"Block name                     %s\n",ev_step_params[index].block_name);
  fprintf(fout,"AWG unit number                %d\n",ev_step_params[index].awg_unit);
  fprintf(fout,"Index of associated loop:      %d\n",ev_step_params[index].loop_index);
  fprintf(fout,"Uses time ref index:           %d\n",ev_step_params[index].time_ref_index);
  fprintf(fout,"Uses time ref name:            %s\n",

	 time_ref_params[ev_step_params[index].time_ref_index].this_reference_name);
  fprintf(fout,"Starts after time ref:         %f ms\n",ev_step_params[index].time_ms);
  fprintf(fout,"Starts at T0 offset:           %f ms\n",ev_step_params[index].start_t0_offset_ms);
  fprintf(fout,"Number of steps:               %d\n",ev_step_params[index].num_steps);
  fprintf(fout,"Delay between steps:           %f\n",ev_step_params[index].step_delay_ms);
 
  if(print_values)
    {
      fprintf(fout,"AWG Module %d\n",ev_step_params[index].awg_unit);
      fprintf(fout,"Index Vstart   Vend     Ramp  Ramp\n");
	  
      fprintf(fout,"      (Volts)  (Volts)  from  to\n");
	  	
      for(i=0; i<NUM_AWG_CHANNELS; i++)
	{
	  if(ev_step_params[index].awg_unit==0)
	    fprintf(fout,"%1.1d    %8.3f %8.3f  %3.3d %3.3d\n",  i,
		    settings.awg0.vset__volts_[i],
		    settings.awg0.vend__volts_[i],
		    settings.awg0.nsteps_before_ramp[i],
		    settings.awg0.nsteps_after_ramp[i]);
	  
	  else
	    fprintf(fout,"%1.1d    %8.3f %8.3f  %3.3d %3.3d\n",  i,
		    settings.awg1.vset__volts_[i],
		    settings.awg1.vend__volts_[i],
		    settings.awg1.nsteps_before_ramp[i],
		    settings.awg1.nsteps_after_ramp[i]);
	}
    }
    
  return;
}



void print_ev_set_params(INT index, BOOL print_values, FILE *fout)
{
  INT i;

  if(fout == NULL)
    {
      printf("print_ev_set_params: ERROR fout is NULL; cannot print\n");
      return;
    }

  fprintf(fout,"ev_set_params: \n");

  fprintf(fout,"Ev_Set index                   %d\n",index);
  fprintf(fout,"Block name                     %s\n",ev_set_params[index].block_name);
  fprintf(fout,"AWG unit number                %d\n",ev_set_params[index].awg_unit);
  fprintf(fout,"Uses time ref index:           %d\n",ev_set_params[index].time_ref_index);
  fprintf(fout,"Uses time ref name:            %s\n",
	 time_ref_params[ev_set_params[index].time_ref_index].this_reference_name);
  fprintf(fout,"Time offset (ms):              %f\n",ev_set_params[index].time_ms);
  fprintf(fout,"T0 offset (ms):                %f\n",ev_set_params[index].t0_offset_ms);


  if(print_values)
    {
      fprintf(fout,"AWG unit %d\n",ev_set_params[index].awg_unit);
      fprintf(fout,"Index Vset\n");
      fprintf(fout,"      (Volts)\n");
      for(i=0; i<NUM_AWG_CHANNELS; i++)
	if (ev_set_params[index].awg_unit==0)	
	  fprintf(fout,"%1.1d    %8.3f\n",  i,
		  settings.awg0.vset__volts_[i]);
	else
	  fprintf(fout,"%1.1d    %8.3f\n",  i,
		  settings.awg1.vset__volts_[i]);
    }
  return;
}
#endif  // HAVE_AWG


/*---------------------------------------------------------*/
void print_ref_blocks(FILE *fout)
{
  INT i;
  if(fout == NULL)
    {
      printf("print_ref_blocks: ERROR fout is NULL; cannot print\n");
      return;
    }


  fprintf(fout,"    TIME REFERENCE BLOCKS:\n");
  if(gbl_num_time_references_defined > MAX_TIME_REF_BLOCKS )  
    gbl_num_time_references_defined = MAX_TIME_REF_BLOCKS;                   
  fprintf(fout,"Time |      reference  |ref |   time   |f r o m...time..reference|   t0      |block | block\n");
  fprintf(fout,"Index|      name       |code|offset(ms)|index....a n d....n a m e| offset(ms)|index | name\n");
  for (i=0; i<gbl_num_time_references_defined ; i++)
    {
      fprintf(fout,"%2.2d %20.20s  %1.1d %10.4f   %2.2d %20.20s %10.4f     %2.2d   %s\n",
	     i, 
	     time_ref_params[i].this_reference_name,
	     time_ref_params[i].timeref_code,
	     time_ref_params[i].time_ms,
	     time_ref_params[i].my_ref,
	     time_ref_params[time_ref_params[i].my_ref].this_reference_name,
	     time_ref_params[i].t0_offset_ms,
	     time_ref_params[i].block_index, 
	     time_ref_params[i].block_name );
    }
 return; 
}


/*---------------------------------------------------------*/
void print_transitions(TRANSITIONS *ptrans, FILE *fout)
{
  INT i;
  DWORD my_bit=0;
  char string[20];

  if(fout == NULL)
    {
      printf("print_transitions: ERROR fout is NULL; cannot print\n");
      return;
    }

  printf("print_transitions: gbl_num_transitions =%d\n", gbl_num_transitions);
  fprintf(fout,"\n ---------     PPG BLOCKS :  number defined=%d     ----------- \n",gbl_num_transitions);
  fprintf(fout,"index t0_offset(ms) PPG bit or pattern   code time_ref_index & name block_index loop_index block_name\n");
  if( gbl_num_transitions > MAX_TRANSITIONS )  gbl_num_transitions > MAX_TRANSITIONS ;

  for (i=0; i< gbl_num_transitions; i++)
    {
      if(is_loop(ptrans[i].code) )  // a loop  
	 sprintf(string,"");
      
      else if((ptrans[i].bit & 0x80000000) ||(ptrans[i].bit & 0x40000000)) 
	// bit 31 set marks a bit pattern or delay 
	sprintf(string,"0x%8.8x",ptrans[i].bitpattern); 	  	 

    
      else
	sprintf(string,"%2.2d %-8s",
		ptrans[i].bit,
		ppg_signal_names[ptrans[i].bit+OFFSET]);

      if( is_loop(ptrans[i].code)) // loop


	fprintf(fout,"%3.3d    %11.5f  %-20.20s  %1.1d         %2.2d %15.15s        %2.2d      %1.1d %s\n",
	       i,ptrans[i].t0_offset_ms ,string,
	       ptrans[i].code, ptrans[i].time_reference_index,
	       time_ref_params[ptrans[i].time_reference_index].this_reference_name,
	       ptrans[i].block_index,   ptrans[i].loop_index,
	       ptrans[i].block_name);
      else
	fprintf(fout,"%3.3d    %11.5f   %-20.20s  %1.1d         %2.2d %15.15s        %2.2d        %s\n",
	       i,ptrans[i].t0_offset_ms ,string,
	       ptrans[i].code, ptrans[i].time_reference_index,
	       time_ref_params[ptrans[i].time_reference_index].this_reference_name,
	       ptrans[i].block_index,  ptrans[i].block_name);
    }
  printf("print_transitions: returning\n");
  return;
}






void print_rfsweep_params(char * name, INT my_index)
{
  char *sweepname[]={"RF sweep","RF FM sweep", "RF burst", "RF swift AWG"};

  INT i;
  
  printf("print_rfsweep_params: starting debug=%d\n",debug);
  
  printf("Params for this rf sweep block \"%s\", block my_index=%d\n",
	 name,my_index);


  if( rf_sweep_params[my_index].sweep_type==1) 
    {  // RF_SWEEP parameters  frequency start and stop, amplitude, duration
 
      printf("p_sweep_type[%d]=%d    (%s) specific parameters:\n",i, rf_sweep_params[my_index].sweep_type,
	     sweepname[ rf_sweep_params[my_index].sweep_type]);
      // parameters  frequency start and stop, amplitude, duration
      printf("Start Freq = %d kHz;  Stop Frequency = %d kHz \n",
	      rf_sweep_params[my_index].start_freq_khz,  rf_sweep_params[my_index].stop_freq_khz);
      printf("Amplitude = %d mV; duration = %d ms\n",
	      rf_sweep_params[my_index].amplitude_mv,    
	      rf_sweep_params[my_index].duration_ms);
      
    }

  else if( rf_sweep_params[my_index].sweep_type==2)  
    { //RF_FM_SWEEP
      printf("p_sweep_type[%d]=%d    (%s) specific parameters:\n",i, rf_sweep_params[my_index].sweep_type,
		 sweepname[ rf_sweep_params[my_index].sweep_type]);
      printf("start  freq = %d kHz;  stop frequency = %d kHz \n",
	      rf_sweep_params[my_index].start_freq_khz,  rf_sweep_params[my_index].stop_freq_khz);
      printf(" amplitude = %d mV; duration comes from cycle duration NEEDS TO BE FILLED\n",
	      rf_sweep_params[my_index].amplitude_mv)    ;
    }
 
    else if( rf_sweep_params[my_index].sweep_type==3)  
 
    { // RF_BURST  parameters  frequency, amplitude, number of cycles
	  printf("p_sweep_type[%d]=%d    (%s) specific parameters:\n",i, rf_sweep_params[my_index].sweep_type,
		 sweepname[ rf_sweep_params[my_index].sweep_type]);
     
      printf("frequency = %d kHz;  amplitude = %d ms \n",
	      rf_sweep_params[my_index].start_freq_khz,  rf_sweep_params[my_index].amplitude_mv);
      
      printf("Number of cycles : %d\n",      rf_sweep_params[my_index].number);
      
    }

   else if( rf_sweep_params[my_index].sweep_type==4)  
     {// RF_SWIFT_AWG
	  printf("p_sweep_type[%d]=%d    (%s)  specific parameters: \n",i, rf_sweep_params[my_index].sweep_type,
		 sweepname[ rf_sweep_params[my_index].sweep_type]);
      printf("start  freq interval = %d kHz;  stop frequency interval = %d kHz \n",
	      rf_sweep_params[my_index].start_freq_khz,  rf_sweep_params[my_index].stop_freq_khz);
      printf(" amplitude = %d mV; notch frequency = %d Hz\n",
	      rf_sweep_params[my_index].amplitude_mv,  rf_sweep_params[my_index].freq_hz)    ;
      printf(" number of frequencies : %d; number of bursts : %d\n",
	      rf_sweep_params[my_index].frequencies,   rf_sweep_params[my_index].number);
      
    }

  if(my_index > 0)
    {
      printf("Params for all rf sweep blocks so far: \n");
      for(i=0; i< my_index; i++)
	{
	  printf("\nSweep Block %d: \n",i);
	  printf("sweep_type[%d]   = %d    (%s)\n",i, rf_sweep_params[i].sweep_type,
		 sweepname[ rf_sweep_params[my_index].sweep_type]);

	  printf("start_freq_khz[%d]= %d \n",i, rf_sweep_params[i].start_freq_khz);
	  printf("p_stop_freq_khz[%d] = %d \n",i, rf_sweep_params[i].stop_freq_khz);
	  printf("amplitude_mv[%d] = %d\n", i, rf_sweep_params[i].amplitude_mv);
	  printf("duration_ms[%d]  = %d\n", i, rf_sweep_params[i].duration_ms);
	  printf("number[%d]       = %d\n", i, rf_sweep_params[i].number);
	  printf("frequencies[%d]  = %d\n", i, rf_sweep_params[i].frequencies);
	}
    }
  else
    printf("This is the first rf sweep block found so far \n");


  return;
}


/*---------------------------------------------------------*/



void print_loop_params(FILE *fout)
{
  INT i;
  double f,g,h;

  if(fout == NULL)
    {
      printf("print_loop_params: ERROR fout is NULL; cannot print\n");
      return;
    }

  fprintf(fout,"loop_params: \n");
  for (i=0; i<gbl_num_loops_defined; i++)
    {
      fprintf(fout,"\nloop index %d\n",i);  
      fprintf(fout,"loopname:                      %s\n",loop_params[i].loopname);
      fprintf(fout,"loop count:                    %d\n",loop_params[i].loop_count);
      fprintf(fout,"my_start_ref_index:            %d\n",loop_params[i].my_start_ref_index);
      fprintf(fout,"my_end_ref_index:              %d\n",loop_params[i].my_end_ref_index);
      fprintf(fout,"defines start_ref string:      %s\n",loop_params[i].defines_startref_string);
      fprintf(fout,"defines end_ref string:        %s\n",loop_params[i].defines_endref_string);
      fprintf(fout,"define end_ref_index:          %d\n",loop_params[i].defines_end_ref_index);
      fprintf(fout,"start after my_start_time_ref: %fms\n",loop_params[i].start_time_ms);
      fprintf(fout,"start T0 offset:               %fms\n",loop_params[i].start_t0_offset_ms);
      fprintf(fout,"end T0 offset:                 %fms\n",loop_params[i].one_loop_t0_offset_ms);
      fprintf(fout,"duration of one loop:          %fms\n",loop_params[i].one_loop_duration_ms);
      fprintf(fout,"duration of all loops:         %fms\n",loop_params[i].all_loops_duration_ms);
      fprintf(fout,"all loops end at T0 offset:    %fms\n",loop_params[i].all_loops_t0_offset_ms);
      fprintf(fout,"start block name:              %s\n",loop_params[i].start_block_name);
      fprintf(fout,"end block name:                %s\n",loop_params[i].end_block_name);
      fprintf(fout,"open flag:                     %d\n",loop_params[i].open_flag);



    }

  fprintf(fout,"\n");
  return;
}

/*=================================================================*/
#ifdef TDCBLOCK_AUTO
void print_tdcblock_params()
{
  INT i;   

  printf("tdcblock_params (default width=%10.4f):\n", settings.ppg.input.tdcblock_width__ms_);
  printf("index t0_offset(ms) pulse_index block_name\n");
  for (i=0; i<gbl_num_tdcblock_pulses; i++)
    {
     
      printf("  %2.2d  %10.4f %2.2d %-15.15s\n",
	     i,tdcblock[i].t0_offset_ms,
	     tdcblock[i].pulse_index,
	     pulse_params[ tdcblock[i].pulse_index].block_name);
		
	
    }

		 
  return;
}
#endif // TDCBLOCK_AUTO

void print_awg_params(void)
{
  printf(    "AWG             Unit 0           Unit 1\n");
 

  printf("Sample Counter   %10.10d   %10.10d\n",awg_params[0].sample_counter, awg_params[1].sample_counter);
  printf("Type             %10s    %10s\n",awg_params[0].type,awg_params[1].type);
  printf("Outfile          %10s    %10s\n",awg_params[0].outfile,awg_params[1].outfile);
  printf("Plot             %1.1d       %1.1d\n",awg_params[0].plot,awg_params[1].plot);
  printf("In use           %1.1d       %1.1d\n",awg_params[0].in_use,awg_params[1].in_use);
	
}

BOOL is_loop(INT code)
{
  if( (code == BEGLOOP_CODE) ||
      (code == ENDLOOP_CODE) ||
      (code == RES_CODE))
    return TRUE;
  else 
    return FALSE;
}


void print_sorted_loop_transitions(TRANSITIONS *ptrans, FILE *fout)
{
  INT i,j,k;
  TRANSITIONS looptrans[MAX_LOOP_BLOCKS*3];
  char string[30];
  j=0;
  
  for(i=0; i<gbl_num_transitions; i++)  
    {
      if ( is_loop(ptrans[i].code))
	{
	  looptrans[j] = ptrans[i];
	  j++;
	}
    }
  
  if(fout == stdout)
    {
      fprintf(fout, "\n***  NON sorted_loop_transitions:  *** \n");
      for (i=0; i<j; i++)
	fprintf(fout,"i=%d  loop [%d] : time from T0 %f  blockname %s \n",
		i, looptrans[i].loop_index,  looptrans[i].t0_offset_ms, looptrans[i].block_name);
    }
  if(j>1) qsort(looptrans, j, sizeof(TRANSITIONS), trans_compare);

 

  fprintf(fout,"\n***  Sorted_loop_transitions:  *** \n");
  fprintf(fout,"Index Loop  TimeFromT0   Blockname        DefinesReference\n");
  for (i=0; i<j; i++)
    {
      k= looptrans[i].loop_index;
      if(looptrans[i].code ==  BEGLOOP_CODE)
	sprintf(string,"%s", loop_params[k].defines_startref_string);
      else if (looptrans[i].code ==  ENDLOOP_CODE)
	sprintf(string,"%s_ONE", loop_params[k].defines_endref_string);
      else if (looptrans[i].code ==  RES_CODE)
	sprintf(string,"%s", loop_params[k].defines_endref_string);
      
      fprintf(fout,"  %-2.2d    %-2.2d  %10.4f  %-15.15s %-20.20s\n",
	      i, k,  looptrans[i].t0_offset_ms, looptrans[i].block_name,
	      string);
    }  
  return;
}
