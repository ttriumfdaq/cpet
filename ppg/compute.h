// compute.h
#ifndef  COMPUTE_INCLUDE_H
#define  COMPUTE_INCLUDE_H

INT compute(char *ppg_mode);
INT compare(double my_delay, double d_delay);
INT compare_min(double diff_delay);
void bit_assignment( FILE  *outf);
INT process_trans(TRANSITIONS *ptrans , FILE *outf, float conversion_factor, double d_pulsew, double d_minimal);
DWORD backpattern(DWORD bitpat);
void get_delay_name(double my_delay, INT ndelay, INT *namecount, FILE *outf, float conversion_factor, double d_pulsew,  double d_minimal );
void print_delays(FILE *outf);
void assemble_line(char *string, DWORD bitpat);
#ifdef NEWPPG
void print_newppg_delays( FILE *fout);
#endif // NEWPPG
#endif
