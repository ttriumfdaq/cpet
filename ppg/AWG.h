/* AWG.h */

#ifndef  AWG_INCLUDE_H
#define  AWG_INCLUDE_H

#ifdef ALPHI_DA816
#include "da816.h"
#endif

#ifdef ALPHI_SOFTDAC
#include "softdac.h"
#endif


#define AWG_BUFFER_0 0
#define AWG_BUFFER_1 1

#define AWG_EXTERNAL_CLK 1
#define AWG_INTERNAL_CLK 0
#define AWG_MODE_00 0  // stay in Bank 0
#define AWG_AUTO_UPDATE 1
#define AWG_NO_AUTO_UPDATE 0
#define CONFIG_NUM_1 1
#define CONFIG_NUM_2 2

#endif
