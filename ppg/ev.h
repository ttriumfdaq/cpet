/* ev.h
prototypes */

INT get_ev_int( HNDLE hkey, INT *parray, INT size, INT array_len, char *string, char *param_name, char *name);
INT get_ev_double( HNDLE hkey, double *parray, INT size, INT array_len, char *string, char *param_name, char *name);
INT get_ev_step_params(INT my_index);
INT get_ev_set_params(INT my_index);

//INT load_AWG_mem(TRANSITIONS *ptrans, INT unit);
INT load_AWG_mem(TRANSITIONS *ptrans);
INT check_ev(TRANSITIONS *ptrans);
INT check_ev_loop_levels(TRANSITIONS *ptrans);
INT add_last_awg(void);
