// parameters.h
#ifndef  TRI_PARAMS_INCLUDE_H
#define  TRI_PARAMS_INCLUDE_H



INT gbl_num_transitions=0; /* count number of transitions */



/* Transition blocks:
        for pulse, width_ms > 0;  width_ms=0 is a transition  */

PULSE_PARAMS pulse_params[MAX_PULSE_BLOCKS];
DWORD gbl_num_time_references_defined=0; /* total number of reference blocks defined */
TIME_REF_PARAMS time_ref_params[MAX_TIME_REFS];

#ifdef TDCBLOCK_AUTO
TDCBLOCK_PARAMS tdcblock[MAX_PULSE_BLOCKS];
#endif // TDCBLOCK_AUTO
/* Loop blocks :
 */
INT gbl_open_loop_counter=0; /* number of open loops */
INT gbl_num_loops_defined=0; /* total number of loops defined */

LOOP_PARAMS loop_params[MAX_LOOP_BLOCKS];

AWG_PARAMS awg_params[NUM_AWG_MODULES];


//INT gbl_awg_sample_counter[NUM_AWG_MODULES]={0,0}; // count the samples for the AWG memory
//BOOL gbl_using_awg_unit[NUM_AWG_MODULES]={0,0};
EV_SET_PARAMS ev_set_params[MAX_EV_BLOCKS];
EV_STEP_PARAMS ev_step_params[MAX_EV_BLOCKS];
RF_SWEEP_PARAMS rf_sweep_params[MAX_RFSWEEP_BLOCKS];

INT block_counter[NUM_BLOCK_TYPES+1]; // one extra for total 
char *block_names[NUM_BLOCK_TYPES]={
  "PULSE",  "BEGIN_LOOP","END_LOOP","RF_SWEEP","TIME_REF",
  "XY_START","XY_STOP", "EVSTEP", "EVSET"};


/* BLOCKS appears under "ppg cycle" in odb
 */


/*------------------------------------------------
PPG BITS
--------------------------------------------------*/
int bits_state[NUM_BITS]; /* remember state of bits after each block */

/*
 These are listed below in reverse order, i.e. high channel to low channel
 -> starts at channel 32  & ends at channel 1  
      and will be written hi to lo for ppg file 

   Define for 32 bits and use OFFSET for 24 and 16 bits
*/


#ifdef EBIT
char *bit_names[PPG_MAX_BITS]={ 
 "f_ch32_","f_ch31_","f_ch30_","f_ch29_","f_ch28_","f_ch27_","f_ch26_","f_ch25_",
 "f_ch24_","f_ch23_","f_ch22_","f_ch21_","f_ch20_","f_ch19_","f_ch18_","f_ch17_",
 "f_ch16_","f_ebeam2_","f_ebeam1_","f_kick2_","f_kick1_","f_dump_","f_tig10_","f_scaler2_",
 "f_scaler1","f_trfer2_","f_trfer1_","f_cap1_","f_rfqext_","f_tibg2_","f_tibg1_","f_isacbm_"
};
/* filled with EBIT PPG signal names (corresponding to above) but in upper case
 starts at channel 32 ends at channel 1
*/
char *ppg_signal_names[PPG_MAX_BITS]={
 "CH32", "CH31","CH30", "CH29","CH28", "CH27","CH26", "CH25",
 "CH24", "CH23","CH22", "CH21","CH20", "CH19", "CH18","CH17",
 "CH16", "EBEAM2","EBEAM1", "KICK2","KICK1","DUMP","TIG10","SCALER2",
 "SCALER1","TRFER2","TRFER1","CAP1","RFQEXT","TIBG2","TIBG1","ISACBM"
};
#elif defined MPET // MPET
char *bit_names[PPG_MAX_BITS]={ 
 "f_ch32_","f_ch31_","f_ch30_","f_ch29_","f_ch28_","f_ch27_","f_ch26_","f_ch25_",
 "f_ch24_","f_ch23_","f_ch22_","f_ch21_","f_ch20_","f_ch19_","f_ch18_","f_ch17_",
 "f_ch16_","f_spare_","f_tdcblock_","f_tdcgate_","f_hv_","f_awg_am_","f_rfgate4_","f_rftrig4_",
 "f_rfgate3_","f_rftrig3_","f_rfgate2_","f_rftrig2_","f_rfgate1_","f_rftrig1_","f_egun_","f_awg_hv_"
};

/* filled with PPG signal names (corresponding to above) but in upper case*/
char *ppg_signal_names[PPG_MAX_BITS]={
 "CH32", "CH31","CH30", "CH29","CH28", "CH27","CH26", "CH25","CH24", 
 "CH23","CH22", "CH21","CH20", "CH19", "CH18","CH17","CH16","SPARE",
 "TDCBLOCK","TDCGATE","HV","AWGAM","RFGATE4","RFTRIG4","RFGATE3","RFTRIG3",
 "RFGATE2","RFTRIG2","RFGATE1","RFTRIG1","EGUN","AWGHV"
};

#elif defined CPET  //CPET

char *bit_names[PPG_MAX_BITS]={ 
"f_ch32_","f_ch31_","f_ch30_","f_ch29_","f_ch28_","f_ch27_","f_ch26_","f_ch25_",
"f_ch24_","f_ch23_","f_ch22_","f_ch21_","f_ch20_","f_ch19_","f_ch18_","f_ch17_",
"f_ch16_","f_ch15_","f_ch14_","f_ch13_","f_ch12_","f_ch11_","f_ch10_","f_ch9_",
"f_ch8_","f_ch7_","f_ch6_","f_ch5_","f_ch4_","f_ch3_","f_ch2_","f_ch1_"
};

char *ppg_signal_names[PPG_MAX_BITS]={
 "CH32", "CH31","CH30", "CH29","CH28", "CH27","CH26", "CH25","CH24", "CH23","CH22",
 "CH21","CH20", "CH19", "CH18","CH17","CH16", "CH15","CH14", "CH13","CH12", "CH11",
 "CH10", "CH9","CH8", "CH7","CH6", "CH5","CH4", "CH3", "CH2", "CH1"
};

#elif defined POL

char *bit_names[PPG_MAX_BITS]={ 
 "f_ch24_","f_ch23_","f_ch22_","f_ch21_","f_ch20_","f_ch19_","f_ch18_","f_ch17_",
 "f_ch16_","f_ch15_","f_ch14_","f_ch13_","f_ch12_","f_ch11_","f_ch10_","f_ch9_",
 "f_ch8_","f_ch7_","f_ch6_","f_ch5_","f_beam_", "f_dacservp_","f_mcsgate","f_mcsnxt_"
};

char *ppg_signal_names[PPG_MAX_BITS]={
 "CH32", "CH31","CH30", "CH29","CH28", "CH27","CH26", "CH25","CH24", "CH23","CH22", "CH21",
 "CH20", "CH19", "CH18","CH17","CH16", "CH15","CH14", "CH13","CH12", "CH11","CH10", "CH9",
 "CH8", "CH7","CH6", "CH5","BEAM", "DACSERVP","MCSGATE","MCSNXT"
};
#endif // POL/MPET/EBIT/CPET


char begin[]="BEGIN_";
char end[]="END_";
char my_softdac[]="softdac";
char my_da816[]="da816";

char *pbegin=&begin[0];
char *pend = &end[0];
char *psoftdac=&my_softdac[0];
char *pda816=&my_da816[0];
BOOL debug=0;
FILE *dfile; // debug file

/* default values for parameter blocks; these values are used
   if not supplied by user
 */
DWORD default_stop_freq_khz=0;
DWORD default_start_freq_interval_khz=0;
DWORD default_stop_freq_interval_khz=0;
DWORD default_single_freq_khz=0; // single freq
DWORD default_notch_freq_khz=0; // notch  freq
DWORD default_amplitude_mv=0;
DWORD default_duration_ms=0;
DWORD default_num_cycles=0; // number of cycles
DWORD default_num_bursts; // number of bursts
DWORD default_num_frequencies=0; //number of freq
DWORD default_frequency_inc=0; // freq increment)
DWORD default_sweep_type=0; // index to above, 1=rf_sweep 2=rf_fm_sweep 3=rf_burst 4=rf_swift_awg
DWORD default_start_freq_khz=0;
INT default_awg_unit=0; // Unit number can be 0 or 1 (only one unit at present, 0)
char default_rf_generator[20]; // generator name from previous block
char default_ppg_signal_name[16]; // PPG output name from previous block
DWORD default_time_reference = 0; /* time reference from previous block*/
double  default_pulse_width_ms;
INT default_loop_count=1;
BOOL gbl_combined=FALSE; // initially false
INT gbl_num_tdcblock_pulses=0;
#endif



