/*
   Name: ev.c
   Created by: SD

   Contents:  Routines to load AWG according to ev_step and ev_set blocks
  
   $Id$                                   
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "midas.h"
#include "experim.h"
#include "tri_config.h"
#include "tri_config_prototypes.h"
#include "AWG.h"
#include "awg_prototypes.h"
#include "ev.h"
#include "ev_structures.h"
//INT AwgSetSample( INT awg_unit, int nsamples, int offset, int ibuf, int maxchan, int flag, int bank);
extern TITAN_ACQ_SETTINGS settings;
extern HNDLE hDB,hAWG1,hAWG0;
extern FILE *tfile;
//extern AWG_PARAMS awg_params;

#ifdef ALPHI_DA816
extern ALPHIDA816  *da816;
#endif

#ifdef ALPHI_SOFTDAC
extern ALPHISOFTDAC  *softdac;
#endif


INT get_ev_double( HNDLE hkey, double *parray, INT size, INT array_len, char *string, char *param_name, char *name)
{
  INT status;
  INT i,j,len,index;
  char str1[9];
  char *p;

  printf("get_ev_double: starting with string=%s (length=%d) array size=%d  array len=%d\n",
	 string,strlen(string),size, array_len);

  /* default information in odb will be used */  
  if (strlen(string) <= 2)
      return SUCCESS;

  status = db_get_data(hDB, hkey, parray, &size,TID_DOUBLE);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"get_params","error after db_get_data for array \"%s\" (%d)", param_name,status);
      return status;
    }

  if(debug)
    {
      printf("get_ev_double: Present array values in ODB:\n");
      for (i=0; i<array_len; i++)
	printf("index[%d]=%f\n",i,parray[i]);
    }
  

  /* now read the string of new values */

  p = strtok (string, "(");
  /* printf("*p=%s\n",p);
  printf("string=%s len=%d\n",string,strlen(string));
  */
  if (p!= NULL)
    {
      len=strlen(string);
      for(j=0;j<len; j++)
	str1[j] = toupper(string[j]);
      str1[len]='\0';
      // printf("str1=%s\n",str1);
    }
  i=0;

 printf("Values to be set by input string:\n");

  while (i<array_len)
    {
      p = strtok (NULL, ",");
      if (p == NULL)
	goto end;

      //printf("after strtok , : *p=%s\n",p);
      index = atoi(p);
      //printf("index=%d\n",index);
      p = strtok (NULL, ")");

      if (p == NULL)
	goto end;

      //printf("after strtok ) : *p=%s\n",p);
      parray[index]=atof(p);
      printf("array[%d]=%f\n",index,parray[index]);

      p = strtok (NULL, "(");
      if (p == NULL)
	goto end;
      i++;
    }

 end:
  printf("get_ev_double: reached end of string\n");

  if(debug)
    {
      printf("get_ev_double: updated array values to be written back to ODB :\n");
      for (i=0; i<array_len; i++)
	printf("array[%d]=%f\n",i,parray[i]);
    }
  /* now write the data array back to the odb */
  status = db_set_data( hDB, hkey, parray, size, array_len, TID_DOUBLE);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"get_ev_double","error from db_set_data for array \"%s\" (%d)",param_name, status);
      return status;
    }
      
   return SUCCESS;
}



INT get_ev_int( HNDLE hkey, INT *parray, INT size, INT array_len, char *string, char *param_name, char *name)
{
  INT status;
  INT i,j,len,index;
  char str1[9];
  char *p;


  printf("get_ev_int: starting with string=%s (length=%d) array size=%d  array len=%d\n",
	 string,strlen(string),size, array_len);
 
/* default information in odb will be used */  
  if (strlen(string) <= 2)
    return SUCCESS;

  status = db_get_data(hDB, hkey, parray, &size,TID_INT);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"get_params","error after db_get_data for array \"%s\" (%d)", param_name,status);
      return status;
    }

  if(debug)
    {
      printf("get_ev_double: starting with array values:\n");
      for (i=0; i<array_len; i++)
	printf("index[%d]=%d\n",i,parray[i]);
    }

  /* now read the string of new values */

  p = strtok (string, "(");
  //printf("p=%p; *p=%s\n",p,p);

  if (p!= NULL)
    {
      len=strlen(string);
      for(j=0;j<len; j++)
	str1[j] = toupper(string[j]);
      str1[len]='\0';
      //    printf("str1=%s\n",str1);
    }
  i=0;
  while (i<array_len)
    {
      p = strtok (NULL, ",");
      if (p == NULL)
	goto end;

      //printf("after strtok , : p=%p; *p=%s\n",p,p);
      index = atoi(p);
      // printf("index=%d\n",index);
      p = strtok (NULL, ")");

      if (p == NULL)
	goto end;

      //printf("after strtok ) : p=%p; *p=%s\n",p,p);
      parray[index]=atoi(p);
      printf("array[%d]=%d\n",index,parray[index]);
      p = strtok (NULL, "(");
      if (p == NULL)
	goto end;
      i++;
    }

 end:
  printf("get_ev_int: reached end of string\n");

  if(debug)
    {
      printf("get_ev_int: writing array values :\n");
      for (i=0; i<array_len; i++)
	printf("array[%d]=%d\n",i,parray[i]);
    }
  /* now write the data array back to the odb */
  status = db_set_data( hDB, hkey, parray, size, array_len, TID_INT);
  if(status != SUCCESS)
    {
      cm_msg(MERROR,"get_ev_int","error from db_set_data for array \"%s\" (%d)",param_name, status);
      return status;
    }
  
  return SUCCESS;
}



INT get_ev_step_params(INT my_index )
{
  HNDLE hTmp,hAWG;
  INT status,size;
  double *pdouble;
  int *pint;

#ifdef NO_AWG
  return SUCCESS;
#else

  if(ev_step_params[my_index].awg_unit==0 )
    hAWG = hAWG0;
  else
    hAWG = hAWG1;

  // start values
  if(strlen(ev_step_params[my_index].start_values_string) > 2)
    { // some new values have been supplied 
      
      if(ev_step_params[my_index].awg_unit==0 )
	{
	  size = sizeof(settings.awg0.vset__volts_);
	  pdouble = &settings.awg0.vset__volts_[0];
	}
      else
	{
	  size = sizeof(settings.awg1.vset__volts_);
	  pdouble = settings.awg1.vset__volts_;
	}

      status = db_find_key(hDB, hAWG, "Vset (volts)", &hTmp);
      if(status != SUCCESS)
	{
	  cm_msg(MERROR,"get_ev_step_params",
		 "error from db_find_key for \"..awg%d/Vset (volts)\" (%d)",
		 ev_step_params[my_index].awg_unit,status);
	  return status;
	}
	     

      // updates appropriate location in ..awgN/Vset (volts) using db_set_data
      status = get_ev_double( hTmp, pdouble, size,NUM_AWG_CHANNELS, 
			      ev_step_params[my_index].start_values_string , "start values (Volts)",
			      ev_step_params[my_index].block_name );
      if(status != SUCCESS)
	return status;
    }
 
 
	      
  // end values
  if(strlen(ev_step_params[my_index].end_values_string) > 2)
    { // some new values have been supplied 

      if(ev_step_params[my_index].awg_unit==0 )
	{
	  size = sizeof(settings.awg0.vend__volts_);
	  pdouble = &settings.awg0.vend__volts_[0];
	}
      else
	{
	  size = sizeof(settings.awg1.vend__volts_);
	  pdouble = settings.awg1.vend__volts_;
	}


      
      status = db_find_key(hDB, hAWG, "Vend (volts)", &hTmp);
      if(status != SUCCESS)
	{
	  cm_msg(MERROR,"get_ev_step_params",
		 "error from db_find_key for \"..awg%d/Vend (volts)\" (%d)",
		 ev_step_params[my_index].awg_unit,status);
	  return status;
	}      
    
      status = get_ev_double(hTmp, pdouble, size, NUM_AWG_CHANNELS,  
			     ev_step_params[my_index].end_values_string,"end values (Volts)" , 
			     ev_step_params[my_index].block_name	 );
      if(status != SUCCESS)
	return status;
    }
  
  // Wait Nsteps before starting to ramp voltage
  if(strlen(ev_step_params[my_index].nsteps_before_ramp_string) > 2)
    { // some new values have been supplied

      if(ev_step_params[my_index].awg_unit==0 )
	{
	  size = sizeof(settings.awg0.nsteps_before_ramp);
	  pint = settings.awg0.nsteps_before_ramp;
	}
      else
	{
	  size = sizeof(settings.awg1.nsteps_before_ramp);
	  pint = settings.awg1.nsteps_before_ramp;
	}
      
      status = db_find_key(hDB, hAWG, "Nsteps before ramp", &hTmp);
      if(status != SUCCESS)
	{
	  cm_msg(MERROR,"get_ev_step_params",
		 "error from db_find_key for \"..awg%d/Nsteps before ramp\" (%d)",
		 ev_step_params[my_index].awg_unit, status);
	  return status;
	}
      
      // updates appropriate location in ..awg/Vramp from step  using db_set_data
      status = get_ev_int(hTmp, pint, size, NUM_AWG_CHANNELS, 
			  ev_step_params[my_index].nsteps_before_ramp_string ,"Nsteps before ramp" , 
			  ev_step_params[my_index].block_name );
      if(status != SUCCESS)
	return status;
    }

  
  // Nsteps after ramp ends
  
  if(strlen(ev_step_params[my_index].nsteps_after_ramp_string) > 2)
    { // some new values have been supplied 
      if(ev_step_params[my_index].awg_unit==0 )
	{
	  size = sizeof(settings.awg0.nsteps_after_ramp);
	  pint = settings.awg0.nsteps_after_ramp;
	}
      else
	{
	  size = sizeof(settings.awg1.nsteps_after_ramp);
	  pint = settings.awg1.nsteps_after_ramp;
	}
      
      status = db_find_key(hDB, hAWG, "Nsteps after ramp", &hTmp);
      if(status != SUCCESS)
	{
	  cm_msg(MERROR,"get_ev_step_params",
		 "error from db_find_key for \"..awg%d/Nsteps after ramp\" (%d)",
		 ev_step_params[my_index].awg_unit,	 status);
	  return status;
	}

      // updates appropriate location in ..awg/Vramp to step  using db_set_data
      
      status = get_ev_int( hTmp, pint, size, NUM_AWG_CHANNELS, 
			   ev_step_params[my_index].nsteps_after_ramp_string ,"Nsteps after ramp" , 
			   ev_step_params[my_index].block_name );
      if(status != SUCCESS)
	return status;
    }
 

  store_setp(1, my_index); // store this step ...only for printing out the voltage values
  print_ev_step_params(my_index,TRUE, stdout); // print also the array values
  print_ev_step_params(my_index,TRUE, tfile);
  return SUCCESS;
#endif
}




INT get_ev_set_params(INT my_index)
{
  HNDLE hTmp,hAWG;
  INT status,size;
  double *parray;

#ifdef NO_AWG
  return SUCCESS;
#else

  
  if(ev_set_params[my_index].awg_unit==0 )
    hAWG = hAWG0;
  else
    hAWG = hAWG1;
  
  
  if(strlen(ev_set_params[my_index].set_values_string) > 2)
    {  // some new values have been supplied 
      
      if(ev_set_params[my_index].awg_unit==0 )
	{
	  size = sizeof(settings.awg0.vset__volts_);
	  parray = &settings.awg0.vset__volts_[0];
	}
      else
	{
	  size = sizeof(settings.awg1.vset__volts_);
	  parray = settings.awg1.vset__volts_;
	}
      
      status = db_find_key(hDB, hAWG, "Vset (volts)", &hTmp);
      if(status != SUCCESS)
	{
	  cm_msg(MERROR,"get_ev_set_params",
		 "error from db_find_key for \"..awg%d/Vset (Volts)\" (%d)",
		 ev_set_params[my_index].awg_unit,	 status);
	  return status;
	}
      
      
      // updates appropriate location in ..awg/Vset (Volts) using db_set_data
      
      status = get_ev_double( hTmp, parray, size,  
			      NUM_AWG_CHANNELS,  
			      ev_set_params[my_index].set_values_string , 
			      "Set Values (Volts)",
			      ev_set_params[my_index].block_name );
      if(status != SUCCESS)
	return status;
    }

  store_setp(0, my_index); // store this set point ...only for printing out the voltage values
  print_ev_set_params(my_index,TRUE,stdout);
  print_ev_set_params(my_index,TRUE,tfile);
  return SUCCESS;
#endif
}





INT check_ev(TRANSITIONS *ptrans)
{
  /* checks (per unit)  that
     1. all ev_set blocks are in the SAME loop (per unit)
          (also checks no ev_set blocks within an ev_step loop)
     2. ev_step blocks inside a loop
     3. only one ev_step block in its associated loop (for each unit)
     4. ev_set and ev_step blocks not within (std pulse width + minimal delay) of each other
  */


  INT i,n,j,k,unit,u;
  INT open_loop_counter;
  double diff,min;

  printf("check_ev: starting \n");
  
  if(   (block_counter[EV_SET]== 0) && (block_counter[EV_STEP]==0) )
    {
      printf("check_ev: no EV blocks found; returning\n");
      return SUCCESS; // there are no EV blocks 
    }
 
  
  u=n=k=open_loop_counter=0;


  for(unit=0; unit<2; unit++)
    {
      ev_unit[unit].num_blocks=0;
    }

  /* find all the ev_set and ev_step blocks */
  for (i=0; i< gbl_num_transitions; i++)
    {
      if(debug)printf("check_ev: working on transition %d\n",i);
      if(ptrans[i].code == BEGLOOP_CODE)
	open_loop_counter++;

      else if  (ptrans[i].code == ENDLOOP_CODE)
	open_loop_counter--;
	
      // check the maximum number of EV blocks has not been exceeded
      if(n >= MAX_EV_BLOCKS)
	{
	  printf("check_ev:too many EV blocks defined (%d) maximum=%d;  increase MAX_EV_BLOCKS\n",
		 n,MAX_EV_BLOCKS);
	  cm_msg(MERROR,"check_ev","too many EV blocks defined (%d) maximum=%d; increase MAX_EV_BLOCKS",
		 n,MAX_EV_BLOCKS);
	  
	  return DB_INVALID_PARAM;
	}

      if(ptrans[i].code == EV_SET_CODE)
	{
	  unit = ev_set_params[ptrans[i].loop_index].awg_unit;
	  u= ev_unit[unit].num_blocks;
	  printf("unit=%d u=%d\n",unit,u);
	  ev_unit[unit].ev[u].step = FALSE;  // not a step
	  ev_unit[unit].ev[u].evset.t0_offset_ms=ptrans[i].t0_offset_ms;
	  ev_unit[unit].ev[u].evset.ptrans_index=i;
	  ev_unit[unit].ev[u].evset.code=ptrans[i].code;
	  ev_unit[unit].ev[u].evset.awg_unit = unit;
	  ev_unit[unit].ev[u].loop_level = open_loop_counter;
	  ev_unit[unit].num_blocks++;
	  //	  pindex[k]=n;
	  n++; // counts all blocks
	  k++; // count ev_set blocks
	}
      else  if(ptrans[i].code == EV_STEP_CODE)
	{	  
	  unit = ev_step_params[ptrans[i].loop_index].awg_unit;
	  u=ev_unit[unit].num_blocks;
	  ev_unit[unit].ev[u].step = TRUE;
	  ev_unit[unit].ev[u].evset.t0_offset_ms=ptrans[i].t0_offset_ms;
	  ev_unit[unit].ev[u].evset.ptrans_index = i;
	  ev_unit[unit].ev[u].evset.code=ptrans[i].code;
	  ev_unit[unit].ev[u].evset.awg_unit = unit;
	  ev_unit[unit].ev[u].loop_level = open_loop_counter;
	  
	  ev_unit[unit].ev[u].loop_index = ev_step_params[ptrans[i].loop_index].loop_index; // loop index
	  ev_unit[unit].ev[u].t0_start_loop_ms = loop_params[  ev_unit[unit].ev[u].loop_index].start_t0_offset_ms;
	  ev_unit[unit].ev[u].t0_end_all_loops_ms =loop_params[  ev_unit[unit].ev[u].loop_index].all_loops_t0_offset_ms;
	  ev_unit[unit].num_blocks++;
	  //pindex[k]=m<<16; // left shift indicates a step block
	  n++;
	  
	 
	}
    } // end of loop on all transitions
  
  printf("check_ev: %d ev blocks found, of which %d are ev_set; unit 0 has %d blocks, unit 1 %d blocks \n",
	 n,k, ev_unit[0].num_blocks, ev_unit[1].num_blocks);
  
  
  /* check numbers of blocks */
  if (k != block_counter[EV_SET])
    {
      printf("check_ev:error in number of EV_SET blocks found for both units: got %d expect %d",
	     k, block_counter[EV_SET]);
      return DB_INVALID_PARAM;
    }
  else if ( (n-k) != block_counter[EV_STEP])
    {
      printf("check_ev:error in number of EV_STEP blocks found for both units: got %d expect %d",
	     (n-k), block_counter[EV_STEP]);
      return DB_INVALID_PARAM;
    }
  
  if(n != ev_unit[0].num_blocks +  ev_unit[1].num_blocks)
    {
      printf("check_ev:error in numbers of blocks found per unit: total=%d Unit 0 =%d Unit 1 = %d",
	     n, ev_unit[0].num_blocks,  ev_unit[1].num_blocks );
      return DB_INVALID_PARAM;
    }

  if(check_ev_loop_levels(ptrans) != SUCCESS)
    return DB_INVALID_PARAM;




  /* now check that EV_SET or EV_STEP blocks are not within awg_clock_width__ms_ + min_delay
     (for the same unit) to allow time for AWG channel advance pulses
  */
  
  for (unit=0; unit < 2; unit ++)
    {
      printf("working on unit %d\n",unit);
     
      for (j=0; j <   ev_unit[unit].num_blocks-1; j++)
	{
	  if( ( ev_unit[unit].ev[j].step == FALSE) && ( ev_unit[unit].ev[j].step == FALSE))
	    { // two EV_SET blocks 
	      printf("Unit %d: comparing two evset blocks, %s and %s\n",unit,
		     ptrans[ev_unit[unit].ev[j].evset.ptrans_index].block_name,
		     ptrans[ev_unit[unit].ev[j+1].evset.ptrans_index].block_name);

	      diff =  ( ev_unit[unit].ev[j+1].evset.t0_offset_ms -   ev_unit[unit].ev[j].evset.t0_offset_ms);
	      min  = 2 * settings.ppg.output.minimal_delay__ms_ +  
		settings.ppg.input.awg_clock_width__ms_; 
	      if( compare( diff, min) <=0 )
		{
		  cm_msg(MERROR,"ev_check","EV_SET blocks %s and %s are too close (diff=%f, min=%f)",
			 ptrans[ev_unit[unit].ev[j].evset.ptrans_index].block_name,
			 ptrans[ev_unit[unit].ev[j+1].evset.ptrans_index].block_name,
			 diff,min);
		  return DB_INVALID_PARAM;
		}
	    }
      
	  if( ev_unit[unit].ev[j].step == FALSE &&  ev_unit[unit].ev[j+1].step == TRUE)
	    { // EV_SET followed by EV_STEP block
	      printf("Unit %d: comparing EVSET followed by EVSTEP block, %s and %s\n",unit,
		     ptrans[ev_unit[unit].ev[j].evset.ptrans_index].block_name,
		     ptrans[ev_unit[unit].ev[j+1].evset.ptrans_index].block_name);

	      printf("EV_STEP block index=%d: t0_offset_ms=%f  t0_start_loop_ms=%f\n",
		     j+1,  ev_unit[unit].ev[j+1].evset.t0_offset_ms,  ev_unit[unit].ev[j+1].t0_start_loop_ms);
	      diff =  ( ev_unit[unit].ev[j+1].t0_start_loop_ms -  ev_unit[unit].ev[j].evset.t0_offset_ms);
	      min  = 2 * settings.ppg.output.minimal_delay__ms_ +  
		settings.ppg.input.awg_clock_width__ms_; 
	      if( compare( diff, min) <=0 )
		{
		  printf("ev_check: EV_SET block %s and EV_STEP block %s are too close (diff=%f, min=%f)\n",
			 ptrans[ev_unit[unit].ev[j].evset.ptrans_index].block_name,
			 ptrans[ev_unit[unit].ev[j+1].evset.ptrans_index].block_name,
			 diff,min);
		  cm_msg(MERROR,"ev_check","EV_SET block %s and EV_STEP block %s are too close (diff=%f, min=%f)",
			 ptrans[ev_unit[unit].ev[j].evset.ptrans_index].block_name,
			 ptrans[ev_unit[unit].ev[j+1].evset.ptrans_index].block_name,
			 diff,min);
		  return DB_INVALID_PARAM;
		}
	    }
	  
	  if( ev_unit[unit].ev[j].step == TRUE &&  ev_unit[unit].ev[j+1].step == FALSE)
	    { //  EV_STEP followed by EV_SET block 
	      printf("Unit %d: comparing EVSTEP followed by EVSET block, %s and %s\n",unit,
		     ptrans[ ev_unit[unit].ev[j].evset.ptrans_index].block_name,
		     ptrans[ ev_unit[unit].ev[j+1].evset.ptrans_index].block_name);
	      
	      printf("EV_STEP block index=%d: t0_offset_ms=%f  t0_end_all_loops_ms=%f\n",
		     j,  ev_unit[unit].ev[j].evset.t0_offset_ms,  ev_unit[unit].ev[j].t0_end_all_loops_ms);
	      diff =  (   ev_unit[unit].ev[j+1].evset.t0_offset_ms -  ev_unit[unit].ev[j].t0_end_all_loops_ms );
	      min  = 2 * settings.ppg.output.minimal_delay__ms_ +  
		settings.ppg.input.awg_clock_width__ms_; 
	      if( compare( diff, min) <=0 )
		{
		  printf("ev_check: EV_STEP block %s and EV_SET block %s are too close (diff=%f, min=%f)\n",
			 ptrans[ ev_unit[unit].ev[j].evset.ptrans_index].block_name,
			 ptrans[ ev_unit[unit].ev[j+1].evset.ptrans_index].block_name,
			 diff,min);
		  printf("   diff = %f - %f\n",
		     ev_unit[unit].ev[j+1].evset.t0_offset_ms ,
			 ev_unit[unit].ev[j].t0_end_all_loops_ms);
		  
		  cm_msg(MERROR,"ev_check","EV_STEP block %s and EV_SET block %s are too close (diff=%f, min=%f)",
			 ptrans[ ev_unit[unit].ev[j].evset.ptrans_index].block_name,
			 ptrans[ ev_unit[unit].ev[j+1].evset.ptrans_index].block_name,
			 diff,min);
		  return DB_INVALID_PARAM;
		}
	    }

	  if( ev_unit[unit].ev[j].step == TRUE &&  ev_unit[unit].ev[j+1].step == TRUE)
	    { //  two EV_STEP  blocks 
	      printf("ev_check: Unit %d: comparing Two EV_STEP blocks \n",unit,
		     ptrans[ev_unit[unit].ev[j].evset.ptrans_index].block_name,
		     ptrans[ev_unit[unit].ev[j+1].evset.ptrans_index].block_name);
	      
	      printf("EV_STEP block index=%d: t0_offset_ms=%f  t0_end_all_loops_ms=%f\n",
		     j,  ev_unit[unit].ev[j].evset.t0_offset_ms,  ev_unit[unit].ev[j].t0_end_all_loops_ms);
	      
	      printf("EV_STEP block index=%d: t0_offset_ms=%f  t0_start_loop_ms=%f\n",
		     j+1,  ev_unit[unit].ev[j+1].evset.t0_offset_ms,  ev_unit[unit].ev[j+1].t0_start_loop_ms);
	      
	      diff =  ( ev_unit[unit].ev[j+1].t0_start_loop_ms -  ev_unit[unit].ev[j].t0_end_all_loops_ms);
	      min  = 2 * settings.ppg.output.minimal_delay__ms_ +  
		settings.ppg.input.awg_clock_width__ms_; 
	      if( compare( diff, min) <=0 )
		{
		  printf("ev_check","EV_STEP block %s and EV_STEP block %s are too close (diff=%f, min=%f)\n",
			 ptrans[ev_unit[unit].ev[j].evset.ptrans_index].block_name,
			 ptrans[ev_unit[unit].ev[j+1].evset.ptrans_index].block_name,
			 diff,min);
		  cm_msg(MERROR,"ev_check","EV_STEP block %s and EV_STEP block %s are too close (diff=%f, min=%f)",
			 ptrans[ev_unit[unit].ev[j].evset.ptrans_index].block_name,
			 ptrans[ev_unit[unit].ev[j+1].evset.ptrans_index].block_name,
			 diff,min);
		  return DB_INVALID_PARAM;
		}
	    }

      

	      
	
      
	} // end of all EV blocks for this unit
    } // end of both units


 
  printf("check_ev: %d SET blocks and %d STEP blocks found; success\n",k,n-k);
  return SUCCESS;
  
}


INT check_ev_loop_levels(TRANSITIONS *ptrans)
{
  INT i,u,lstep,lset;

  printf("\ncheck_ev_loop_levels: starting\n");

  for (u=0; u<2; u++)
    {
      printf("\n------------ AWG Unit %d-----------\n",u);
      i=0;
      lstep=lset=-1; // start with loop level -1

      while (i <  ev_unit[u].num_blocks)
	{
	  if( ev_unit[u].ev[i].step)
	      printf("Block %d; EVSTEP block name %s:\n",
		     i, ptrans[ev_unit[u].ev[i].evset.ptrans_index].block_name);  
	  else
	      printf("Block %d; EVSET block name %s:\n",
		     i, ptrans[ev_unit[u].ev[i].evset.ptrans_index].block_name);  
	  
	  printf("t0 offset (ms)   = %f\n", ev_unit[u].ev[i].evset.t0_offset_ms);
	  printf("transition index = %d\n", ev_unit[u].ev[i].evset.ptrans_index);
	  printf("code             = %d\n", ev_unit[u].ev[i].evset.code);
	  printf("awg unit         = %d\n",  ev_unit[u].ev[i].evset.awg_unit);
	  printf("awg loop level   = %d\n",  ev_unit[u].ev[i].loop_level);
	    
	  if( ev_unit[u].ev[i].step)
	    {
	      printf("loop index       = %d\n", ev_unit[u].ev[i].loop_index);
	      printf("t0_start_loop_ms = %f\n", ev_unit[u].ev[i].t0_start_loop_ms);
	      printf("t0_end_all_loops_ms   = %f\n", ev_unit[u].ev[i].t0_end_all_loops_ms);
	      if(lstep < 0)  // first block found
		lstep =  ev_unit[u].ev[i].loop_level;
	      else if (lstep !=  ev_unit[u].ev[i].loop_level)
		{
		  printf("check_ev_loops: ERROR ev_step blocks are not at the same loop level\n");
		  printf("    expect loop level of %d for this block\n",lstep);
		  cm_msg(MERROR,"check_ev_loops","illegal nesting of loops for EVSTEP block %s",
			 ptrans[ev_unit[u].ev[i].evset.ptrans_index].block_name);
		  return DB_INVALID_PARAM;
		}	     
	    } // end of step block


	  else
	    { // this is a set block
	      if(lset < 0)  // first block found
		lset =  ev_unit[u].ev[i].loop_level;
	      else if (lset !=  ev_unit[u].ev[i].loop_level)
		{
		  printf("check_ev_loops: ERROR ev_set blocks are not at the same loop level\n");
		  printf("    expect loop level of %d for this loop\n",lset);
		  cm_msg(MERROR,"check_ev_loops","all EVSET blocks must be at same loop level (block %s)",
			 ptrans[ev_unit[u].ev[i].evset.ptrans_index].block_name);
		  return DB_INVALID_PARAM;
		}	   
	    }
	  i++;
	}

      /*  End of this Unit 
	 
      all EVSET blocks are at the same loop level (lset) and
      all EVSTEP blocks are at the same loop level (lstep)

      but EVSTEP blocks should be one loop level down from the lset blocks  */

      if( (lset > -1)  && (lstep > -1) )
	{ // both step and set blocks are present 

	  //check that the steps are in a loop only one loop level down from any set blocks
	  if(lstep != (lset+1))
	    { 
	      printf("check_ev_loops: ERROR ev_step blocks not one loop level down from ev_set blocks\n");
	      printf("  loop level of SET blocks is %d and STEP blocks is %d\n",lset,lstep);
	      cm_msg(MERROR,"check_ev_loops","illegal loop levels for SET and STEP blocks (Unit %d)",u);
	      return DB_INVALID_PARAM;
	    }
	}
    } // end of loop on both units
  printf("\ncheck_ev_loop_levels: returning SUCCESS\n");
  return SUCCESS;
}




INT load_AWG_mem(TRANSITIONS *ptrans)
{
  /* Add to AWG memory for all 8 channels
  
  1. goes through the transitions again looking for loops and ev blocks
  2. calculates number of samples and calls pgm to load AWG mem
  */

  INT ichan,status;
  INT i,j,lc,a,b,c,num_steps;
  char my_loopname[10];
  BOOL gotit;
  INT my_open_loop_counter,my_index;
  HNDLE hTmp, hAWG;
  INT size;
  double *psetv,*pendv;
  INT *prb, *pra;
  INT my_unit;
  int max_unit = 0;

  typedef  struct
  {
    char string[20];
    INT param;
  }AWG_LOAD;
  AWG_LOAD awg_load[20];


#ifdef NO_AWG
  return SUCCESS;
#else

  
  printf("load_AWG_mem  starting\n");

 
  for(i=0; i< NUM_AWG_MODULES; i++)
    awg_params[i].sample_counter = 0;

  init_setp(); // only for printing out the voltage values

  my_open_loop_counter = lc =0;
  status = SUCCESS;

  /* find the beg/end loops and ev_set and ev_step blocks */
  for (i=0; i< gbl_num_transitions; i++)
    {
      if(debug)	printf("load_AWG_mem : working on transition %d\n",i);

      /* set the loop open/close flag */
      if(ptrans[i].code == BEGLOOP_CODE)
	{
	  status = set_loop(i,pbegin,TRUE,
			    awg_load[lc].string,
			    &awg_load[lc].param);
	  if(status != SUCCESS)
	    {
	      cm_msg(MERROR,"load_AWG_mem","transition %d, returning after set_loop with status=%d",i,status);
	      return status;
	    }

	  lc++;
	  my_open_loop_counter++;
	  
	}
      else if  (ptrans[i].code == ENDLOOP_CODE)
	{
	  status = set_loop(i,pend, FALSE,
			    awg_load[lc].string,
			    &awg_load[lc].param );
	  if(status != SUCCESS)
	    {
	      cm_msg(MERROR,"load_AWG_mem","transition %d, returning after set_loop with status=%d",i,status);
	      return status;
	    }
	  lc++;
	  my_open_loop_counter--;
	}
     
      
      

      if(ptrans[i].code == EV_SET_CODE)
	{
	  printf("load_AWG_mem: found EV_SET block at transition %d\n",i);
	  /* this check has been superceded
	  if(my_open_loop_counter > 1) // Only SCAN loop is allowed
	    {
	      cm_msg(MERROR,"awg_load"," EV_SET cannot be in an open user-defined loop");
	      return DB_INVALID_PARAM;
	      } */
	  
	  sprintf(awg_load[lc].string,"ev_set");
	  awg_load[lc].param = ptrans[i].loop_index; // index in ev_set_params block
	  lc++;
	  
	  /* get the parameters */
	  my_index =  ptrans[i].loop_index;
	  my_unit =  ev_set_params[my_index].awg_unit;

	  if (my_unit > max_unit)
	    max_unit = my_unit;

	  printf("Index to this EV_SET block(%s) is %d\n", ptrans[i].block_name, my_index);
	  printf("EV_SET parameters: block_name=%s\n",ev_set_params[my_index].block_name);
	  printf("    unit=%d,    set_values_string = %s\n",
		 my_unit,
		 ev_set_params[my_index].set_values_string);
	  

	  if(get_ev_set_params(my_index) != SUCCESS)
	    return DB_INVALID_PARAM;
	  
	  if(my_unit==0 )
	    psetv = settings.awg0.vset__volts_;
	  else
	    psetv = settings.awg1.vset__volts_;

	  // Write the set values to the AWG
	  for(ichan=0; ichan < NUM_AWG_CHANNELS; ichan++)
	    {
	      status = AwgMemLoad(psetv[ichan],
				    psetv[ichan],
				    awg_params[my_unit].sample_counter, 1, AWG_BUFFER_0, ichan, my_unit); 
				    //				    ev_set_params[my_index].awg_unit);
	      if(status != SUCCESS)
		{
		  cm_msg(MERROR,"load_AWG_mem","error return from loading AWG unit %d sample %d chan %d (block %s)",
			 my_unit, awg_params[my_unit].sample_counter,ichan, ev_set_params[my_index].block_name );
		  return status;
		}
	    }

	  awg_params[my_unit].sample_counter++;  // one more AWG sample
	  printf("gbl_awg_sample_counter[%d]=%d, ichan=%d\n",my_unit, awg_params[my_unit].sample_counter, ichan);
	  /* now remove this transition; it's been dealt with */
	  shift_trans_up(i, ptrans);// decrements gbl_num_transitions
	  i--;
	}
      
      else if(ptrans[i].code == EV_STEP_CODE)
	{
	  printf("load_AWG_mem: found EV_STEP block at transition %d\n",i);
	  /*  
         This check has been superceded 
	  if(my_open_loop_counter != 2)  // SCAN loop plus this ev_step loop
	    {
	      cm_msg(MERROR,"awg_load","for now, EV_STEP cannot be in a nested loop, open loop counter=%d",
		     my_open_loop_counter);
	      return DB_INVALID_PARAM;
	      } */
	  
	  sprintf(awg_load[lc].string,"ev_step");
	  awg_load[lc].param = ptrans[i].loop_index; // index in ev_step_params block
	  lc++;
	  
	  
	  /* get the parameters */
	  my_index =  ptrans[i].loop_index;
	  my_unit = 	 ev_step_params[my_index].awg_unit;
	  printf("Index to this EV_STEP block(%s) is %d\n", ptrans[i].block_name, my_index);
	  printf("EV_STEP parameters: block_name=%s\n",ev_step_params[my_index].block_name);
	  printf("    unit=%d,  loop_count/num steps=%d \n",
		 my_unit, 
		 ev_step_params[my_index].num_steps);
	  
	  if(get_ev_step_params(my_index) != SUCCESS)
	    return DB_INVALID_PARAM;
	  

	  if(my_unit==0 )
	    {
	      hAWG = hAWG0;
	      psetv = settings.awg0.vset__volts_;
	      pendv =  settings.awg0.vend__volts_;
	      prb  = settings.awg0.nsteps_before_ramp;
	      pra  = settings.awg0.nsteps_after_ramp;
	      size=sizeof ( settings.awg0.vend__volts_);
	    }
	  else
	    {
	      hAWG = hAWG1;
	      psetv = settings.awg1.vset__volts_;
	      pendv = settings.awg0.vend__volts_;
	      prb  = settings.awg1.nsteps_before_ramp;
	      pra  = settings.awg1.nsteps_after_ramp;
	      size=sizeof ( settings.awg1.vend__volts_);
	    }

	  /* now load the AWG memory */
	  

	  num_steps = ev_step_params[my_index].num_steps; // constant for all channels (=num samples)

	  // offset is  awg_params[my_unit].sample_counter;
	  
	  for(ichan=0; ichan<NUM_AWG_CHANNELS; ichan++)
	    {
	      // calculate number of steps for each part of the ramp
	      a = prb[ichan];	      
	      c =  pra[ichan];
	      b = num_steps - (a + c);

	      /* check that we have enough steps to do this */
	      if (b <0)
		{
		  cm_msg(MERROR,"load_AWG_mem",
	"AWG unit %d Chan %d: too few steps defined (%d) to wait %d steps before ramping & %d after (block %s)",
			 my_unit,ichan, num_steps,a,c, ev_step_params[my_index].block_name);
		  printf("load_AWG_mem: ERROR  AWG unit %d Chan %d number of steps=%d\n", 
			 my_unit,ichan,num_steps);
		  printf("  Too few steps defined to wait %d steps before ramping and %d after (block %s)\n",
			 a,c,ev_step_params[my_index].block_name);
		  return DB_INVALID_PARAM;
		}

	      
	      /* load in three sections:
                                  ____
                                 /  c
                              b /
                            ___/
                            a
	      
	    AwgMemLoad (Vin,Vout,sample start, nsamples, buf, channel)
	      */
	      if(a>0)
		{
		  status = AwgMemLoad(psetv[ichan],
				      psetv[ichan],
				      awg_params[my_unit].sample_counter, 
				      a, 
				      AWG_BUFFER_0, 
				      ichan,
				      my_unit);
		  if(status != SUCCESS)
		    {
		      cm_msg(MERROR,"load_AWG_mem",
			     "error return from loading AWG unit %d at sample %d, num samples=%d (block %s)",
			     my_unit,  awg_params[my_unit].sample_counter ,
			     a,ev_step_params[my_index].block_name);
		      return status;
		    }
		}
	      
	      if(b>0)
		{
		  status = AwgMemLoad(psetv[ichan],
				      pendv[ichan],
				      awg_params[my_unit].sample_counter + a, 
				      b , 
				      AWG_BUFFER_0, ichan,my_unit);
		  
		  if(status != SUCCESS)
		    {
		      cm_msg(MERROR,"load_AWG_mem",
			     "error return from loading AWG unit %d sample %d, num samples=%d (block %s)",
			     my_unit,  awg_params[my_unit].sample_counter + a, b, 
			     ev_step_params[my_index].block_name);
		      return status;
		    }
		}
	      
	      
	      
	      if(c>0)
		{
		  status = AwgMemLoad(pendv[ichan],
					pendv[ichan],
					 awg_params[my_unit].sample_counter + a + b ,  
					c, 
					AWG_BUFFER_0, ichan, my_unit);
		  if(status != SUCCESS)
		    {
		      cm_msg(MERROR,"load_AWG_mem",
			     "error return from loading AWG unit %d sample %d, num samples=%d (block %s)",
			      my_unit, awg_params[my_unit].sample_counter + a + b , 
			     c, ev_step_params[my_index].block_name);
		      return status;
		    }
		}
	      
	      
	    } // end of for loop on AWG channels

	 awg_params[my_unit].sample_counter+=num_steps ;  // add all the steps on 


	  /* After ramping, the AWG will be set to the end values.
	     Therefore update the set_values to reflect this
	  */

	  // size defined above
	  status = db_set_value (hDB, hAWG,"Vset (volts)", pendv, size, 
				 NUM_AWG_CHANNELS, TID_DOUBLE);
	  if(status != SUCCESS)
	    {
	      cm_msg(MERROR,"load_AWG_mem",
		     "error from setting value for \"..awg%d/Vset (Volts)\" (%d)",
		     my_unit, status);
	      return status;
	    }

	  /* now remove this transition; it's been dealt with */
	  shift_trans_up(i, ptrans);// decrements gbl_num_transitions
	  i--; 
	} // end of EV_STEP block
    } // end of all transitions
  
  // Setup the AWG registers  ... unit?

  for (my_unit=0; my_unit<=max_unit; my_unit++)
    {
      printf("load_AWG_mem: awg_params[%d].sample_counter =%d\n",
	     my_unit, awg_params[my_unit].sample_counter);
      if( awg_params[my_unit].sample_counter > 0)
	{	// parameters awg_unit, num_samples, ibuf, max_awg_channels, flag, bank
	  AwgSetSample(my_unit, awg_params[my_unit].sample_counter, 0, 0, 8, 1,0); 
	}
    }

    print_setpoints();


    print_sorted_transitions(ptrans, FALSE, stdout); // bit pattern not yet calculated
    
    return SUCCESS;
#endif
}

void init_setp(void)
{  // only for printing out the voltage values
  INT j,i;

  for(j=0; j<NUM_AWG_MODULES; j++)
      setp[j].npts = setp[j].limit=0; // zero points and limit

  return;
}


void store_setp(INT r, INT my_index)
{ // only for printing out the voltage values

  // parameter r=0 Set point
  //           r=1 Step

  INT i,counter,unit,nsteps;
  double *psetv, *pendv;


#ifdef NO_AWG
  return;
#else

  if(r==0)  // set
    unit = ev_set_params[my_index].awg_unit;
  else  // step
    unit = ev_step_params[my_index].awg_unit;

  if (unit == 0)	
    {  
      psetv = settings.awg0.vset__volts_;
      if(r>0)
	{  // step
	  pendv =  settings.awg0.vend__volts_;
	  nsteps = loop_params[ev_step_params[my_index].loop_index].loop_count;
	}
    }
  else
    {
      psetv = settings.awg1.vset__volts_;
      if(r>0)
	{  // step
	  pendv =  settings.awg1.vend__volts_;
	  nsteps = loop_params[ev_step_params[my_index].loop_index].loop_count;
	}      
    }


  counter = setp[unit].npts;
  if(counter >=MAX_V)
    {
      setp[unit].limit++; // set the flag
      printf("store_setp:  LIMIT reached....Cannot store any more voltages\n");
      return;
    }

  for(i=0; i<NUM_AWG_CHANNELS; i++)
    {
      setp[unit].channel[i].Vset[counter] = psetv[i]; // store the set point
      setp[unit].channel[i].code[counter] = 0; // set point code	  
      printf("stored set point %f at setp[unit=%d].channel[chan=%d].Vset[counter=%d]\n",
	     psetv[i],unit,i,counter);
	
      if(r> 0)// step...  store nsteps and end value
	{
	 
	  setp[unit].channel[i].code[counter+1]=nsteps; // step end after nsteps 
	  setp[unit].channel[i].Vset[counter+1] = pendv[i]; // store end point
	  printf("stored end point %f at setp[unit=%d].channel[chan=%d].Vset[counter=%d]\n",
		 pendv[i],unit,i,counter+1);
	     
	
	}
    }
  setp[unit].npts++; // saved one point
  if(r>0) 
    setp[unit].npts++; // saved two points

  return;
#endif
}

void print_stored_setp(FILE *fout)
{
  INT unit,counter,i,j,l;
  char string[256];
  char str1[]="       ";
  char str2[15];



  l=strlen(str1);
  for (unit=0; unit<NUM_AWG_MODULES; unit++)
    {
      counter = setp[unit].npts;
      sprintf(string,"   ");

      if(counter > 0)
	{
	  if (counter >= MAX_V ||  setp[unit].limit)
	    {
	      fprintf(fout, "\n Limit reached of %d values for AWG Unit %d\n",MAX_V,unit);
	      counter=MAX_V;
	    }
	  else
	    fprintf(fout,"\n %d values for AWG Unit %d\n",counter,unit);
	  
	  
	  fprintf(fout,  "Channel  Volts....\n");
	  for (i=0; i<NUM_AWG_CHANNELS; i++)
	    {
	      /*	      for(j=0; j<counter; j++)
			      printf("setp[unit=%d].channel[chan=%d].Vset[counter=%d]=%f code=%d \n",
			      unit,i,j,setp[unit].channel[i].Vset[j],
			      setp[unit].channel[i].code[j]);
	      */
	      
	      fprintf(fout, " %1.1d ",i); // channel
	      for(j=0; j<counter; j++)
		{
		  if( setp[unit].channel[i].code[j] == 0) // set point
		    {
		      fprintf(fout," %6.2f",setp[unit].channel[i].Vset[j]);

		      if(i==0)
			{
			  if( (strlen(string)+l)<256) 
			    strcat(string,str1);
			}
		    }
		  else  // this is the end step
		    {
		      fprintf(fout,"....%6.2f", 
			      setp[unit].channel[i].Vset[j]); // ...indicate stepping
		      if(i==0)
			{
			  sprintf(str2,"     %4.4d steps  ", setp[unit].channel[i].code[j]);
			  string[strlen(string)-l]='\0';
			  if( (strlen(string)+strlen(str2))<255) 
			    strcat(string,str2);
			}
		    }
		}
	      fprintf(fout,"\n");	
	    }
	  //fprintf(fout,"\n");
	  fprintf(fout,"%s\n\n",string);
	}
      else
	fprintf(fout,	"No set values found for AWG Unit %d\n",unit);
    }
}

void print_setpoints(void)
{
  char  infofile[256];
  FILE *fout;
 
  print_stored_setp(tfile);
  print_stored_setp(stdout);
    
    
  sprintf(infofile,"%s/awgvolts.txt",settings.ppg.input.ppgload_path);
  fout=fopen(infofile,"w");
  if(fout != NULL)
    {  
      print_stored_setp(fout);
      fclose(fout);
    }
  else
    printf("print_setpoints: could not open file %s\n",infofile);
  return;
}





/*
    No longer needed 

//Add an extra awg pulse

INT add_last_awg(void)
{
  char my_name[]="autoAWG";
  INT i,my_refindex,status;
  double my_time_ms; 

  // add a mininal delay in case the last item is an end_loop or another TDCBLOCK 
  my_time_ms = settings.ppg.output.minimal_delay__ms_; 
  
  // add last awg pulse(s) INSIDE scan loop
  /* get default time reference  ->my_refindex */     
/* if(get_this_ref_index( "", my_name,  &my_refindex)!=DB_SUCCESS)
    {
      cm_msg(MERROR,"add_last_awg","time reference \"\" is not defined (block %s) ",
	     my_name);
      return DB_INVALID_PARAM;
    }


  printf ("add_last_awg: my_refindex=%d\n",my_refindex);
  print_default_time_reference();

  if(awg_params[0].in_use)
    {
      printf("add_last_awg: unit 0, sample_counter =%d\n",awg_params[0].sample_counter);
      // status = add_awg_pulses(0, my_refindex, my_time_ms, my_name);
      if(status != SUCCESS)
	{
	  if(status == DB_KEY_EXIST)
	    printf("add_last_awg: got DB_KEY_EXIST from add_awg_pulses for AWG unit 0\n");
	  else 
	    return status;
	}
    }


  if(awg_params[1].in_use)
    {
      printf("add_last_awg: unit 1, sample_counter=%d\n", awg_params[1].sample_counter);
      status = add_awg_pulses(1, my_refindex, my_time_ms, my_name);
      if(status != SUCCESS)
	{
	  if(status == DB_KEY_EXIST)
	    printf("add_last_awg: got DB_KEY_EXIST from add_awg_pulses for AWG unit 1\n");
	  else 
	    return status;
	}
    }

  return SUCCESS;
}
*/
