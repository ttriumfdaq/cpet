/*
  Name: tri_config.h
  Created by: SD
  
  Contents: Header file for tri_config.c
            i.e. configuration program for TRIUMF Titan Experiment (mpet, ebit).

  $Id: tri_config.h  $ 
*/
   
#ifndef  TRI_CONFIG_INCLUDE_H
#define  TRI_CONFIG_INCLUDE_H

/* Hardware 
 */
#define NUM_AWG_MODULES 2
#define NUM_AWG_CHANNELS 8 // number of channels per module


/* Parameters 
 */
# define MAX_BLOCKS 200
# define TYPE_SHIFT  16
# define PPG_NAMELEN 15 /* maximum length of PPG signal name */
# define BLOCKNAME_LEN 25 // maximum length of a block name stored in various structures
# define COMBINED_BLOCKNAME_LEN 80 // maximum length of combined block names in various structures 

# define MAX_TRANSITIONS 100
# define MAX_PULSE_BLOCKS 50  // maximum number of pulse/transition blocks
# define MAX_TIME_REF_BLOCKS 100 // maximum changes of time reference (automatic plus user-defined)
# define MAX_LOOP_BLOCKS 10
# define MAX_EV_BLOCKS 20  /* max blocks used for HV (step blocks and set blocks) */
# define MAX_RFSWEEP_BLOCKS 4  // all RF blocks sweep/burst/swift
# define MAX_XY_BLOCKS 2
/* each loop & variable pulse defines two time references automatically */
# define MAX_TIME_REFS (MAX_TIME_REF_BLOCKS + 2 * MAX_LOOP_BLOCKS + 2 * MAX_PULSE_BLOCKS)
# define MAX_T 20
# define PLOT_MAX_LOOPS 20 // maximum loop count for expanding in PPG signals display

//  Transition code values:
#define TRANS_CODE 0 //  regular transition
#define BEGLOOP_CODE 1 // begin loop
#define ENDLOOP_CODE 2 //end loop
#define EV_SET_CODE 3 // ev set
#define EV_STEP_CODE 4 // ev step
#define RES_CODE 5 // reserved for loop duration


// Time Reference code values
#define timeref_code_time 0
#define timeref_code_begin_loop 1
#define timeref_code_end_loops 2
#define timeref_code_start_pulse 3
#define timeref_code_end_pulse 4
#define timeref_code_end_oneloop 5


/* indexes into block_counter array as well as block types: */
# define TOTAL       0
// block types defined :
#define PULSE      1 /* pulse with standard OR user-defined width (PULSE or STDPULSE)
                        or a transition (pulse with zero pulse width) or a PATTERN/DELAY */ 
#define BEGIN_LOOP 2
#define END_LOOP   3
#define RF_SWEEP   4 // includes RF_SWEEP, RF_FM_SWEEP,RF_BURST,RF_SWIFT_AWG
#define TIME_REF   5 // define a time reference
#define XY_START   6 // not implemented 
#define XY_STOP    7// not implemented
#define EV_STEP     8
#define EV_SET      9
#define NUM_BLOCK_TYPES 9  // number of block types defined above -1



/*------------------------------------------------
PPG BITS
--------------------------------------------------*/
// note that maximum for OLD PPG is 24 (checked in tri_config main)
#ifdef BITS_32
#define NUM_BITS 32
#define OFFSET 0
#elif defined BITS_24
#define NUM_BITS 24
#define OFFSET 8   // array index
#elif defined BITS_8
#define NUM_BITS 8
#define OFFSET 24  // array index
#else  // default is  BITS_16
#define NUM_BITS 16
#define OFFSET 16  // array index
#endif
extern int bits_state[NUM_BITS]; /* remember state of bits after each block */


/*
// replaced by values in parameters.h
#define AWGHV    15 // channel 1 TRAP TRIGGER -> Clock (Update) input of fast AWG card -> HV  was RF0 for BNMR
#define EGUN    14 //  channel 2 IONIZE   EGUN cathode/bias voltage 
#define RFTRIG1 13 //  channel 3  RF1 TRIGGER
#define RFGATE1 12 //  channel 4 RF1 GATE
#define RFTRIG2 11 //  channel 5 RF2 TRIGGER
#define RFGATE2 10 //  channel 6 RF2 GATE
#define RFTRIG3  9 //  channel 7 RF3 TRIGGER
#define RFGATE3  8 //  channel 8 RF3 GATE
#define RFTRIG4  7 //  channel 9 RF4 TRIGGER
#define RFGATE4  6 //  channel 10 RF4 GATE
#define AWGAM    5 //  channel 11 Clock input on slow AWG card -> Amplitude Modulators 
#define HV       4 //  channel 12 HV pulser input (DRIFT TUBE PULSE UP/DOWN)
#define TDCGATE  3 //  channel 13 TRIUMF TDC gate

#ifdef AUTO_TDCBLOCK
#define TDCBLOCK 2 //  channel 14 TRIUMF TDC block (marks start of block in timing sequence)  was BEAM for BNMR
#else
#define SPARE2 2 //  channel 14 spare - was BEAM for BNMR
#endif

#define SPARE 1   // not used for TITAN
      //  channel 15 used for MCS NEXT         was POL for BNMR
#define FSC  0      // channels 16-24 FSC mem select channels (not used for Titan)
*/

/* 
filled with PPG signal names (corresponding to above) but in upper case*/
#define PPG_MAX_BITS 32 
extern char *ppg_signal_names[PPG_MAX_BITS]; // defined in parameters.h
 


#define PPG_CLOCK_MHZ 10 // clock frequency used by PPG compiler (in header.ppg); 
                         // i.e. internal clock frequency of PPG board if using clock chip.
                         // If using external clock, conversion factor will be used 


/* AWG Constants:  Unit 0 is the FAST unit, called SOFTDAC; Unit 1 is the SLOW unit, called DA816
        AWG is set up in set_awg_params in tri_config.c
   NOTE: if these names are changed, they must also be changed in awg.pl
*/
const char awg_trigger_names[NUM_AWG_MODULES][16]={"AWGHV","AWGAM"};// AWG units 0 (fast) and 1 (slow)
/*

/* 
   global default values

*/
extern char *pbegin;
extern char *pend;
extern BOOL debug;
extern FILE *tfile;

/* these names correspond to the bits defined above
   but will be written hi to lo for ppg file   */
extern char *bit_names[PPG_MAX_BITS];


extern INT block_counter[NUM_BLOCK_TYPES+1]; // one extra for total 
extern char *block_names[NUM_BLOCK_TYPES];
extern INT block_list[MAX_BLOCKS]; // list of block types (shifted by TYPE_SHIFT) & block numbers
 
extern INT gbl_num_transitions; /* count number of transitions */
extern DWORD gbl_num_time_references_defined; /* total number of reference blocks defined */
extern INT gbl_open_loop_counter; /* number of open loops */
extern INT gbl_num_loops_defined; /* total number of loops defined */
extern BOOL gbl_combined; // set true after combine_transitions, transitions are combined
#ifdef AUTO_TDCBLOCK
extern INT gbl_num_tdcblock_pulses;
#endif
 /* default values */
extern DWORD default_stop_freq_khz;
extern DWORD default_start_freq_interval_khz;
extern DWORD default_stop_freq_interval_khz;
extern DWORD default_single_freq_khz; // single freq
extern DWORD default_notch_freq_khz; // notch  freq
extern DWORD default_amplitude_mv;
extern DWORD default_duration_ms;
extern DWORD default_num_cycles; // number of cycles
extern DWORD default_num_bursts; // number of bursts
extern DWORD default_num_frequencies; //number of freq
extern DWORD default_frequency_inc; // freq increment)
extern DWORD default_sweep_type; // index to above, 1=rf_sweep 2=rf_fm_sweep 3=rf_burst 4=rf_swift_awg
extern DWORD default_start_freq_khz;
extern INT default_awg_unit; // Unit number can be 0 or 1 (default 0)

extern char default_rf_generator[20]; // generator name from previous block
extern INT default_loop_count;
extern double default_pulse_width_ms;
extern DWORD default_time_reference;
extern char default_ppg_signal_name[16]; // PPG output name from previous block
extern INT gbl_awg_sample_counter[ NUM_AWG_MODULES]; // count the samples for the AWG memory (2 units)
//extern BOOL gbl_using_awg_unit[NUM_AWG_MODULES]; not used
/* Define the structures */

typedef struct {
  double t0_offset_ms; // offset from T0 i.e. when PPG started
                      
  DWORD bit;  // bit to be  changed
  DWORD code; // set to 0 (transition) unless special block (see codes below)
	 
 /* the following for debugging/error checking: */
  INT time_reference_index; // time reference index (to time_ref_params) of this block
  INT loop_index; // index if a loop or ev block
  DWORD bitpattern; // present bitpattern (filled later)
  DWORD block_index;   // index to this block in the array of all blocks (block_list)
  char  block_name[COMBINED_BLOCKNAME_LEN+1]; // name of this block 
}TRANSITIONS;

#ifdef AUTO_TDCBLOCK
// Block to store auto-pulses for TDCBLOCK
typedef struct {
double t0_offset_ms; // time offset from t0 of leading edge of pulse
INT pulse_index; // index to original pulse associated with this TDCBLOCK pulse
} TDCBLOCK_PARAMS;
#endif //  AUTO_TDCBLOCK 

// Pulses with either standard or user-specified pulse width; transitions with 0 pulse width
typedef struct {
double time_ms; // leading edge time in ms after time reference
double width_ms; // pulse width
INT time_ref_index; // using this time reference index e.g 0 -> T0
char  output_name[PPG_NAMELEN]; // name of PPG output or "bitpattern"
DWORD ppg_signal_num;  // number of PPG output signal or BITPATTERN
double t0_offset_ms; // time offset from t0 of leading edge of pulse
char  block_name[BLOCKNAME_LEN+1]; // name of this block (may be truncated)
} PULSE_PARAMS;


typedef struct {
  //  INT defines_timeref_index ; // defining this reference number e.g. 2 for T2
  INT timeref_code ; // timeref_code  (defined above)
  char this_reference_name[80]; // long string supplied by the user or automatically
  double time_ms;  // time offset from my_ref  time_ref (below)
  INT my_ref; // referenced on this time reference (an index to this structure)
  DWORD block_index;
  double t0_offset_ms;// offset of this reference from T0
  char  block_name[BLOCKNAME_LEN+1]; // name of this block (may be truncated)
}
TIME_REF_PARAMS;



/* Loop blocks :
 */

typedef struct {
char  loopname[10]; // loop names truncated to 10 characters
double start_time_ms; // time in ms after start time reference
INT  my_start_ref_index;  // index in time_ref_params to time reference used by "begin loop"
INT my_end_ref_index;  // index in time_ref_params to time ref. used by "end loop"
char  defines_startref_string[20]; // defines start loop reference e.g.  _TBEGIN_<loopname>
char  defines_endref_string[20]; //  defines end all loops reference e.g.  _TEND_<loopname> 
INT  defines_start_ref_index;  // index in time_ref_params to time reference defined by "begin loop"
INT defines_end_ref_index;  // index in time_ref_params to time ref. defined by "end loop"

double start_t0_offset_ms; // time offset from T0 when this loop began
double one_loop_t0_offset_ms; // calculate time offset from T0 after loop 1 ends
double one_loop_duration_ms;
double all_loops_duration_ms; // total duration of all loops
double all_loops_t0_offset_ms; // offset from t0 when all loops end 
INT   loop_count; 
char  start_block_name[BLOCKNAME_LEN+1]; // name of this block (may be truncated)
char  end_block_name[BLOCKNAME_LEN+1];
BOOL  open_flag; // indicates is loop is open or closed
} LOOP_PARAMS;


typedef struct {
double time_ms; // start time in ms after time reference for this block
double start_t0_offset_ms; // start time in ms after T0
INT time_ref_index; // using this time reference
char start_values_string[256]; // units are volts
char end_values_string[256]; // units are volts
char nsteps_before_ramp_string[256]; // units are step numbers
char nsteps_after_ramp_string[256]; // units are step numbers
DWORD num_steps; // number of steps (c.f. trap triggers) - from loop parameter ==> num AWG samples
double step_delay_ms; /* delay between steps   - from loop parameter  */
INT awg_unit;  // unit number of this AWG
INT  loop_index; // index to loop associated with this ev_step block
char  block_name[BLOCKNAME_LEN+1]; // name of this block (may be truncated)
} EV_STEP_PARAMS;

   
// EV_SET  set a single voltage value on 8 channels of the AWG  
typedef struct {
double time_ms; // time in ms after time reference for this block
double t0_offset_ms; // time in ms after T0
INT time_ref_index; // using this time reference
char set_values_string[256]; // units are volts
INT awg_unit; // unit number of this AWG (default 0)
char  block_name[BLOCKNAME_LEN+1]; // name of this block (may be truncated)

} EV_SET_PARAMS;




typedef struct {
char  generator[20]; // name of RF generator
DWORD sweep_type; // index to above, 1=rf_sweep 2=rf_fm_sweep 3=rf_burst 4=rf_swift_awg
DWORD start_freq_khz;
DWORD stop_freq_khz;
DWORD freq_hz; // notch or single freq
DWORD amplitude_mv;
DWORD duration_ms;
DWORD number; // number of cycles or number of bursts
DWORD frequencies; //(number of freq or freq increment)
char  block_name[BLOCKNAME_LEN+1]; // name of this block (may be truncated)
} RF_SWEEP_PARAMS;


extern PULSE_PARAMS pulse_params[MAX_PULSE_BLOCKS];
extern TIME_REF_PARAMS time_ref_params[MAX_TIME_REFS];
extern LOOP_PARAMS loop_params[MAX_LOOP_BLOCKS];
extern EV_STEP_PARAMS ev_step_params[MAX_EV_BLOCKS];
extern EV_SET_PARAMS ev_set_params[MAX_EV_BLOCKS];
extern RF_SWEEP_PARAMS rf_sweep_params[MAX_RFSWEEP_BLOCKS];

#ifdef AUTO_TDCBLOCK
extern  TDCBLOCK_PARAMS tdcblock[MAX_PULSE_BLOCKS];
#endif //  AUTO_TDCBLOCK

typedef struct {
  INT  sample_counter; //gbl_awg_sample_counter[2]
  char type[10]; // awg type (softdac/da816)
  char outfile[15]; // filename of output awg file ( e.g. da816_ch0)
  BOOL plot;
  BOOL in_use;
} AWG_PARAMS;

extern AWG_PARAMS awg_params[NUM_AWG_MODULES];
extern  char *psoftdac;
extern char *pda816;

#endif



