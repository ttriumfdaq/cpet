/* 
   Name: get_params.h
   Created by: SD

   Contents: function prototypes for get_params.c

   $Id: get_params.h  $
*/
#ifndef  GET_PARAMS_INCLUDE_H
#define  GET_PARAMS_INCLUDE_H

INT get_params( HNDLE hKey, KEY *key, INT my_index, INT block_index, INT total_size,  char *name );
void  print_pulse_blocks( void);
INT compare_auto_trans( double t0_offset_ms, INT ppg_signal_num, double width_ms, char *name);
INT auto_add_pulse(char* my_time_reference, 
		   double my_time_offset_ms,
		   double my_pulse_width_ms,
		   char* ppg_signal_name,  
		   BOOL add_auto_time_refs, 
		   char* orig_name);
INT get_int( HNDLE hKey, char *my_key_name, INT *ival, INT *my_default, INT flag, char *name);
INT get_dword( HNDLE hKey, char *my_key_name, DWORD *ival, DWORD *my_default, INT flag, char *name);
INT get_double( HNDLE hKey, char *my_key_name, double *fval, double *my_default, INT flag, char *name);
INT get_string( HNDLE hKey, char *my_key_name, char *string, INT *psize, char *my_default, INT flag, char *name);
INT get_last_open_loop(INT *open_loop_index, char *loopname, char *name);
INT check_loops(INT loop_index);


INT add_pulse_block( char *my_time_ref, INT my_index, INT block_index, BOOL add_auto_timerefs, 
		      char *name);
void print_loop_params(void);
INT add_time_ref(INT this_refindex, INT my_refindex,  INT code, double time_offset, INT block_index,
		  char *name);
INT  get_this_ref_index(char *my_ref_string, char *name,  INT *index);
INT  get_next_ref_index(char *string, char *name,  INT *index);
INT add_transition(INT block_index, INT my_index, INT refindex,  double my_offset);
INT  set_loop(INT index, char *str, BOOL ival, char *my_string, INT *param);
INT get_loop_name(char *my_block_name, char *str,  char *loop_name);
void concat(char *string1, char *string2, INT max_len);
INT check_ppg_name(char *ppg_name);
void  zero_rfsweep_params(INT i);
INT auto_add_loop(BOOL flag, INT loop_count, TRANSITIONS *ptrans);
INT   print_default_time_reference(void);
INT add_awg_pulses(INT my_awg_unit, INT my_refindex, double my_time_ms, char *name);
double max(double a, double b);
double min(double a, double b);
void add_pulse_info(INT i, INT orig_pulse_index);
INT auto_add_tdcblock(char* my_time_reference,double my_time_offset_ms,INT pulse_index,char* orig_name);
INT add_tdcblock_pulses(void);
INT  auto_add_tdcblock( char * my_time_reference, double time_ms, INT pulse_index, char *orig_name );
#endif
