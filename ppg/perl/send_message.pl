# common subroutines
use strict;
use warnings;
##############################################################
sub send_message
##############################################################
{
# send a message to odb message logger
    my ($name, $merror, $message) =  @_;

    our ($EXPERIMENT,$ODB_SUCCESS,$COMMAND,$ANSWER,$DEBUG);
    my ($ENV,$MIDAS_HOSTNAME);

    my $status;
    my $host="";
    my $dquote='"';
    my $squote="'";
    my $command="$dquote msg $merror $name $squote $message $squote $dquote";
    print "name=$name, merror=$merror, message=$message\n";
    
   print "command is: $command \n";
     $MIDAS_HOSTNAME = $ENV {"MIDAS_SERVER_HOST"};
    unless ($MIDAS_HOSTNAME eq "")
    {
	$host = "-h $MIDAS_HOSTNAME";
    }
    $COMMAND ="`odb -e $EXPERIMENT -c $command $host` ";
    $ANSWER=`odb -e $EXPERIMENT -c $command $host`;  
    $status=$?;
    chomp $ANSWER;  # strip trailing linefeed
    if($DEBUG)
    {
	print "command: $COMMAND\n";
	print " answer: $ANSWER\n";
    }

    if($status != $ODB_SUCCESS) 
    { # this status value is NOT the midas status code
	print "send_message:  Failure status returned from odb msg (status=$status)\n";
    }
    return;
} 

sub strip
{
# removes / from end of string, // becomes /
    my $string=shift;
    $string=~ (s!//!/!g);
    $string=~s!/$!!;
    print "strip: now \"$string\"\n";
    return ($string);
}


# IMPORTANT
# this 1 is needed at the end of the file so require returns a true value   
1;   

