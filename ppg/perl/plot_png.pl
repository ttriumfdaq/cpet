#!/usr/bin/perl -w
#
# Run this file e.g.
# /home/ebit/online/ppg/perl/plot_png.pl
#
######################################################################
#                                                                        
# plot_png.pl 
#
# 
use strict;
#our ( $file ) = @ARGV;# input parameters
#our $nparam=6; # need 6 input params

$|=1; # flush output buffers
#$count = @ARGV;
print "plot_png.pl: starting\n";
#if($count != $nparam)
#{
#    print "not enough parameters supplied ($nparam); expect $count:\n";
#    die "filename; max_time; number of plots; run number; loop count flag;\n";
#}

print "plot_png.pl : sending system cmd: \"display /home/ebit/online/ppgplot.png &\" \n";
system ("display ppgplot.png &");

print "plot_png.pl : sending system cmd: \"display /home/ebit/online/awg_zero.png &\" \n";
system ("display awg_zero.png &");
print "plot_png.pl : sending system cmd: \"display /home/ebit/online/awg_one.png &\" \n";
system ("display awg_one.png &");
exit;
