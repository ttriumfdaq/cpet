# subroutines for odb access
#
###############################################################################################
#    standard odb access subroutines
#
# Note: these subroutines are included by the main perl script with the command:
#
#                   require "odb_access.pl";
#
#       The main program is expected to define the following globals:
##            ######### G L O B A L S ##################
##            status = 0 is success for odb
##            our $ODB_SUCCESS = 0;
##            our $EXPERIMENT = ""; 
##            our $FAILURE = $FALSE = 0;
##            our $SUCCESS = $TRUE = 1;
##            our $DEBUG = $FALSE; # set to 1 for debug, 0 for no debug
##            our $ANSWER = " ";   #reply from odb command
##            our @ARRAY    #array contents used by get_array
##            our $COMMAND = " "; # copy of command sent be odb_cmd (for error handling)
##            # run states:
##            our $STATE_STOPPED = 1; # Run state is stopped
##            our $STATE_PAUSED  = 2; # Run state is paused (error condition for bnmr, MUSR uses this)
##            our $STATE_RUNNING = 3; # Run state is running
##################################################################################################
#
# $Log: odb_access.pl,v $
# Revision 1.36  2008/02/13 20:53:51  suz
# change the printing; use print_out
#
# Revision 1.35  2005/09/27 18:39:02  suz
# support brackets in key names for ls
#
# Revision 1.34  2005/09/26 17:50:45  suz
# change some messages to print_2 so they appear on screen for debugging
#
# Revision 1.33  2004/11/18 19:58:21  suz
# turn off debug flag
#
# Revision 1.32  2004/11/18 19:29:42  suz
# add another param to odb_cmd to handle links; test the number of params supplied and zero any that are not supplied
#
# Revision 1.31  2004/11/16 18:51:22  suz
# initialize more unsupplied parameters in odb_cmd
#
# Revision 1.30  2004/11/10 00:13:27  suz
# try again to fix warnings when get_command_string called with too few params
#
# Revision 1.29  2004/10/19 19:13:56  suz
# a message becomes a debug message
#
# Revision 1.28  2004/10/05 06:17:44  suz
# replace fix for v1.26 (empty value1) that got lost
#
# Revision 1.27  2004/10/04 21:55:20  suz
# comment out a debug
#
# Revision 1.26  2004/06/29 22:22:58  suz
# fix bug where get_command_string could not handle value1=0
#
# Revision 1.25  2004/06/10 17:53:46  suz
# add fix_answer ; use our; last Rev message is bad
#
# Revision 1.24  2004/06/09 21:36:23  suz
# add check on beamline
#
# Revision 1.22  2004/04/02 22:32:12  suz
# write time prints timing info to file only
#
# Revision 1.21  2004/03/30 22:50:35  suz
# add subroutine write_time
#
# Revision 1.20  2004/03/29 20:26:29  suz
# add print_2
#
# Revision 1.19  2004/03/29 19:51:57  suz
# add value1 to message
#
# Revision 1.18  2004/03/29 18:15:19  suz
# open_output_file now appends to message file; extra parameter process_id;fix bug in odb_cmd where setting a value of 0 did not work
#
# Revision 1.17  2004/02/13 20:59:26  suz
# add carriage return in print_3 to prevent line number output on die
#
# Revision 1.16  2003/05/07 16:39:01  suz
# updated for bnmr/bnqr
#
# Revision 1.15  2002/04/16 17:42:02  suz
# fix a debug statement
#
# Revision 1.14  2002/04/15 18:48:59  suz
# add pause for musr; add suppress to open_output_file
#
# Revision 1.13  2001/11/01 22:09:52  suz
# add a parameter to odb msg command to avoid speakers talking
#
# Revision 1.12  2001/11/01 17:56:15  suz
# handle another appended string in get_string
#
# Revision 1.11  2001/10/04 19:22:00  suz
# fix bug adding trailing / to path when key is blank
#
# Revision 1.10  2001/09/28 19:33:02  suz
# 'our' not supported on isdaq01. Replace with 'use vars'
#
# Revision 1.9  2001/09/14 19:22:52  suz
# use strict;imsg now msgmodify get_string
#
# Revision 1.8  2001/05/09 17:25:49  suz
# change odb msg command to imsg to avoid speaker problem
#
# Revision 1.7  2001/04/30 20:01:47  suz
# Add support for midas logger
#
# Revision 1.6  2001/03/30 18:59:35  suz
# add a debug statement only
#
# Revision 1.5  2001/03/01 18:58:56  suz
# Changes to open_output_file. Delete any old copy of outfile, and change
# permissions to allow others to delete it.
#
# Revision 1.4  2001/02/23 20:37:14  suz
# print statement changed in open_output_file
#
# Revision 1.3  2001/02/23 20:32:22  suz
# add subrouting open_output_file
#
# Revision 1.2  2001/02/23 19:56:27  suz
# ensure get_bool returns an integer value
#
# Revision 1.1  2001/02/23 17:55:04  suz
#
#
use strict;
sub check_directories($$); # prototype with 2 arguments
sub get_command_string($$$$$); #prototype with 5 arguments
#
# globals
#   "our" supported only in Perl 5.6.0 (not yet on isdaq01)
# e.g. our $FALSE= our $FAIL = 0;
# replace with "use vars" , e.g. use vars '$FALSE'; or with qw  
our ($COMMAND,$EXPERIMENT,$ANSWER,$ODB_SUCCESS);
our ( $TRUE, $FALSE, $FAILURE,  $SUCCESS) ;
our ( $DEBUG, @ARRAY);
our ( $STATE_STOPPED, $STATE_RUNNING, $STATE_PAUSED);
# for odb  msg cmd:
our ($MERROR, $MINFO, $MTALK);
our ($DIE, $CONT);
# constants for print_3
$DIE = $TRUE;  # die after print_3
$CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#
#

sub odb_cmd
{
# executes an odb command
#
#  input parameters:
#   $cmd    odb command to execute e.g. ls
#   $path   full path of directory of odb variable 
#                  e.g. /equipment/MUSR_TD_ACQ/Settings/mode/histograms
#   $key    name of odb variable
#   $value  value to write to odb parameter ( for odb command "set")
#
#   $EXPERIMENT global  experiment
#
#   output parameters:
#   globals:
#   $ANSWER  global   string returned from odb
#   $COMMAND command string for error messaging
#
#   status           SUCCESS or FAIL
#   $path   
#   $key
#   $status          status return from odb_cmd
#   
#   subroutine returns 1 for SUCCESS,  0 for FAIL

# note :  $cmd    cleaned-up parameters from get_command_string

#    my $cmd  = $_[0];
#    my $path = $_[1];
#    my $key  = $_[2];
#    my $value1 = $_[3];
#    my $value2 = $_[4];   (only used for msg cmd at present; blank for other commands )
#    my $link_key_name  for ls command for links; look for different key name  
    my $name = "odb_cmd";
    my ($orig_cmd, $orig_path, $orig_key, $value1, $value2, $link_key_name ) = @_; # get the parameters
    my $len = @_ ; # supplied params; max 6 at present
    my ($command,$path,$key,$cmd,$status);
    my $debug=0;
    my $space="space";

    if ($len < 6) { $link_key_name = ""};
    if ($len < 5) { $value2="";}
    if ($len < 4) { $value1="";}
    if ($len < 3) { $orig_key="";}
    if ($len < 2) { $orig_path="";}
    if ($len < 1) 
    { 
	print_out    ($name, "at least one parameter must be supplied to odb_cmd",$CONT);
        return($FAILURE);
    }
    #print ("EXPERIMENT=\"$EXPERIMENT\"\n");
    if($EXPERIMENT eq " ") # initialized to " "
    {
	print_out    ($name, "parameter \$EXPERIMENT must be supplied to odb_cmd",$CONT);
        return($FAILURE);
    }

    if($debug)    
    { 
        print_out ($name, "starting, received $len parameters:",$CONT);
        print_out  ($name," cmd = $orig_cmd; path=$orig_path; key = $orig_key;" ,$CONT) ;
	print_out  ($name,"value1 = $value1; value2 =$value2; link_key_name=$link_key_name",$CONT);
    }
    
    
#   get_command_string cleans any leading/trailing spaces, extra slashes out of $path,$key, $cmd
    
    ( $command, $cmd, $path, $key)  = get_command_string($orig_cmd,$orig_path,$orig_key,$value1,$value2);
 
    #print "command=$command\n";

#    $_=$command; 
#    if($cmd eq "ln")  # use eq for strings
#    {  # link command: expect "ln" "path" "linkname" ; get_command_string will have stripped the space


#    $command=~ s/\/$space\"/\" /i; # if the word "space" is included, replace it with a space
#	  print"command=$command\n";


    if($debug)
    { 
        print_out ($name, "After get_command_string, command:$command",$CONT); 
        print_out ($name,"   and cmd=$cmd; path =$path; key =$key; value1=$value1",$CONT);
    }
 

   $COMMAND ="`odb -e $EXPERIMENT -c $command` ";  # save this in case of error
    #print ("COMMAND=$COMMAND\n");
    $ANSWER=`odb -e $EXPERIMENT -c $command`;  
    $status=$?;
    chomp $ANSWER;  # strip trailing linefeed

# print information for now:
    if($debug) 
    {
	print_out ($name, "command: $COMMAND, answer: $ANSWER",$CONT);
    }
    if($status != $ODB_SUCCESS) 
    { # this status value is NOT the midas status code
        print_out ($name, "command: $COMMAND, answer: $ANSWER",$CONT);
        print_out ($name,  "Failure status returned from odb (status=$status)",$CONT);
        return($FAILURE, $path, $key);
    }
 
    
# look for the most common error strings (these return with $ODB_SUCCESS) and flag them as failures

    $_=$ANSWER;
    if ( /usage/i)
    {
	print_out ($name, "command: $COMMAND, answer: $ANSWER",$CONT);
        print_out($name,"detected key word : usage; -> unknown command",$CONT); # unknown command
        return($FAILURE, $path, $key);
    }
    elsif ( /unknown/i)
    {
	if(/\<unknown\>/i)# <unknown> allow this one
	{ 
            print_out ($name, "command: $COMMAND, answer: $ANSWER",$CONT);
	    print_out($name,"detected key word : <unknown>; -> likely unknown link...midas2.0.0",$CONT); 
	}
	else
	{
	    print_out ($name, "command: $COMMAND, answer: $ANSWER",$CONT);
	    print_out($name, "detected key word : unknown; -> unknown command",$CONT); # unknown command
	    return($FAILURE, $path, $key);
	}
    }
    elsif (/\?/i) 
    { 
	print_out ($name, "command: $COMMAND, answer: $ANSWER",$CONT);
        print_out($name, "detected key word : ?; odb probably stuck...",$CONT); # odb probably stuck waiting for a reply
        return($FAILURE, $path, $key);
    }  
    elsif (/not found/i) 
    {
        unless (/analyzer/i) # analyzer not found is OK (make command) 
        {
	    print_out ($name, "command: $COMMAND, answer: $ANSWER",$CONT);
            print_out($name, "detected key words \"not found\" in reply",$CONT);
            print_out($name, "  so command may not have worked as expected",$CONT);
            return($FAILURE, $path, $key);
        } 
    }
    elsif (/is of type/i) 
    {
	print_out ($name, "command: $COMMAND, answer: $ANSWER",$CONT);
	print_out($name, "detected key words \"is of type\" in reply",$CONT);
	print_out($name, "  so command probably did not work as expected",$CONT);
	return($FAILURE, $path, $key);
    }

#       For ls, odb should reply with the key at the beginning of the string.
    if($cmd eq "ls")  # use eq for strings
    {
	unless ($link_key_name eq "")
	{   # link name is different from actual key name. Look for link_key_name in ANSWER
	    print_out($name,"Looking for link_key_name=$link_key_name in ANSWER rather than key name=$key",$CONT);
	    unless (/^$link_key_name/i)
	    {
	        print_out ($name, "command: $COMMAND, answer: $ANSWER",$CONT);
		print_out ($name, " Unexpected reply with command ls after reading $key ",$CONT);
		print_out ($name, " looking for link keyname $link_key_name in reply rather than key",$CONT);
		print_out ($name,   "         odb replied: $ANSWER", $DIE);
		return($FAILURE , $path, $key); 
	    } 
	}
	else
	{
	    
	    #print "answer:$ANSWER\n";
	    #print "key:$key\n";
	    my $temp=$key;
	    $temp=~ tr/()/./; #substitute for brackets
	    #print "temp:$temp\n";
	    unless ($ANSWER=~/^$temp/i)
	    {
	        print_out ($name, "command: $COMMAND, answer: $ANSWER",$CONT);
		print_out ($name, " Unexpected reply with command ls after reading $key ",$CONT);
		print_out ($name,   "         odb replied: $ANSWER", $DIE);
		return($FAILURE , $path, $key); 
	    } 
	}
    }
    
    return ($SUCCESS, $path, $key);
}

##############################################################
sub get_array
##############################################################
{
#   removes key from $ANSWER and splits contents into @ARRAY
#
#   input:   key
#            $ANSWER (global)
#   output:  @ARRAY  (global)
#
    my $key   = $_[0];
    my $my_name="get_array";

    if ($DEBUG) { print_out($my_name, "key= $key answer = $ANSWER",$CONT); }
    $key =~ tr/A-Z/a-z/; # translate to lower case (in case key is not correct case)
    $_=$ANSWER; # move into default variable
  # use i flag instead  tr/A-Z/a-z/; # translate ANSWER to lower case
    tr/ \n/ /s; # translate spaces and linefeed to spaces and squeeze (/s)
    s/$key //i; # remove $key & a space from global ANSWER, ignoring case 


    @ARRAY = split / /;  # fill global ARRAY
    if($DEBUG) { print_out($my_name, "array = @ARRAY",$CONT); }
    return;
}
##############################################################
sub get_int
###############################################################
{
#   removes key from $ANSWER and returns contents
#
#   input:   key
#            $ANSWER (global)
#   output: return value is the integer contents of $ANSWER
#
    my $key   = $_[0];
    my $integer = -1;
    my $debug=0; # no debug
    my @x; #temp array for split
    my $msg="";
    my $answer="";
    my $my_name="get_int";
    if($debug) 
    { 
        print_out($my_name,"get_int starting with key: $key and ANSWER: $ANSWER",$CONT); 
    }
   # $key =~ tr/A-Z/a-z/; # translate to lower case  (in case key is not correct case)
    ($answer,$msg)= fix_answer($key); # strip off any extra message after the value we want
    $_=$answer; # move into default variable
     if($debug){ print_out($my_name,"  after fix_answer, answer=$answer, msg=$msg",$CONT);}
    tr/ \n/ /s; # translate spaces and linefeed to spaces and squeeze (/s)
    s/$key //i; # remove $key & a space from global ANSWER (case independent) 
    (@x)=split / /; # split by spaces
    if($debug){ print_out($my_name,"after split, array=@x, length=$#x, last value=$x[-1] ",$CONT);}
    $_=$x[-1]+0; #make sure last value is an integer by doing an integer add

    if($debug) { print_out($my_name, "returning value=$_ ",$CONT); }
    if($msg){print_out($my_name, "msg=\"$msg\" ",$CONT);}
    return ($_);
}

##############################################################
sub get_bool
###############################################################
{
#   removes key from $ANSWER and returns contents
#
#   input:   key
#            $ANSWER (global)
#   output: return value is TRUE or FALSE depending on $ANSWER
#
    my $key   = $_[0];
    my @x;  #temp array for split
    my $answer;
    my $msg="";
    my $my_name="get_bool";

    if($DEBUG) 
    { 
        print_out($my_name,"starting with key: $key and ANSWER: $ANSWER",$CONT); 
    }
    $key =~ tr/A-Z/a-z/; # translate to lower case  (in case key is not correct case)

    ($answer,$msg)= fix_answer($key); # strip off any extra message after the value we want
    $_=$answer; # move into default variable
     if($DEBUG){ print_out($my_name, "after fix_answer, answer=$answer, msg=$msg",$CONT);}

    tr/ \n/ /s; # translate spaces and linefeed to spaces and squeeze (/s)
    s/$key //i; # remove $key & a space from global ANSWER,ignoring case 
#    split / /; deprecated
    (@x)=split / /; # split by spaces
    if($DEBUG){ print_out($my_name," after split, array=@x, length=$#x, last value=$x[-1] ",$CONT);}
    $_=$x[-1];
    s/y/$TRUE/;
    s/n/$FALSE/;
    $_=$_+0; #make sure it's an integer by doing an integer add
    if($DEBUG) { print_out($my_name," returning value = $_ ",$CONT); }
    if($msg){print_out($my_name,  "msg=\"$msg\" ",$CONT);}
    return ($_);
}


##############################################################
sub get_string
###############################################################
{
#   removes key from $ANSWER and returns contents
#
#   input:   key
#            $ANSWER (global)
#   output: string contents of $ANSWER (with key stripped off)
#           message (that may be appended to end of $ANSWER string) 
#
    my $key   = $_[0];
    my $string = "blank";
    my $msg = "";
    my $answer = "";
    my $my_name="get_string";
    if($DEBUG) 
    { 
        print_out($my_name,"starting with key: $key and ANSWER: $ANSWER",$CONT); 
    }
    ($answer,$msg)= fix_answer($key);
    $_=$answer; # move into default variable
    if($DEBUG){ print_out($my_name,"after fix_answer, answer=$answer, msg=$msg",$CONT);}

    s/^ *//; # strip spaces from beginning of string
    s/ *$//; # and end
    print_out($my_name, "get_string returns:\"$_\" ",$CONT);

    if($msg)
    {
	print_out($my_name,"   and msg:\"$msg\" ",$CONT);              
    }
    
    return ( $_, $msg );
}

##############################################################
sub fix_answer($)
##############################################################
{
#   input:   key
#            $ANSWER (global)
#   output: string contents of $ANSWER (with key stripped off)
#           message (that may be appended to end of $ANSWER string) 
#
    my $key   = $_[0];
    my $string = "blank";
    my $msg = "";
    my $my_name="fix_answer";
    $_=$ANSWER; # move into default variable

    $DEBUG=0;
    tr/ \n/ /s; # translate spaces and linefeed to spaces and squeeze (/s)
    s/$key //i; # remove $key & a space from global ANSWER, ignoring case 

# test only
##    $_ = $_ . "  cannot blah blah  "; # add rubbish to string for testing $msg
##    $_ = $_ . " 11:34:56 garbage  "; # add rubbish to string for testing $msg


# sometimes $ANSWER will have a string like this appended:
# "Cannot open message log file /home/bnmr/online/bnmr2/midas.log"
# or
# "11:14:49 [ODBEdit] Warning: The RF has tripped"
# strip these off into $msg
#
#   may find other messages occur - if so add them in later


#   adds ";;" to split string - hopefully no normal strings use ";;"

#   look for time stamp e.g. 11:14:49
    if( /[\d][\d]:[\d][\d]:[\d][\d]/)
    {
	if($DEBUG) { print_out($my_name,"found date string appended",$CONT); }
	s /([\d][\d]:[\d][\d]:[\d][\d])/;;$1/; # save string with parentheses -> $1
	if($DEBUG) { print_out($my_name,"string:\'$_\'",$CONT); }
    }
#   look for "Cannot "
    elsif (/Cannot /i)
    {
	if($DEBUG) { print_out($my_name, "found \'Cannot \' string",$CONT); }
	s /(Cannot )/;;$1/i;  # save string with parentheses -> $1
	if($DEBUG) { print "string:\'$_\'\n"; }
    }
    ($_,$msg)=split(/;;/); # split by added ";;"

    if($DEBUG)
    {
	print_out($my_name,"After split, string:\'$_\' and msg:\'$msg\'",$CONT);
    }
    return ( $_, $msg );
}

#===============================================================================
#
# get_command_string
#
#===============================================================================

sub get_command_string($$$$$)
{
# called from odb_cmd which has already checked the no. of input parameters
# and set any not supplied to "" 
  my ($cmd, $path, $key, $value1, $value2) = @_; # get the parameters
  my ($command,$value,$fullpath);
  my $my_name="get_command_string";

  my $debug=0; # no debug


  if($debug)
  {
      print_out($my_name,"Input values:",$CONT);
      print_out($my_name, "  path =\"$path\" key =\"$key\" ",$CONT);
      print_out($my_name, "  cmd=\"$cmd\" ",$CONT);
      print_out($my_name, "  value1 =\"$value1\" value2 =\"$value2\"  ",$CONT);
  }

#Strip leading/trailing blanks from cmd
$cmd =~ s/ *$//; #remove trailing spaces
$cmd =~ s/^ *//; # and leading spaces

# and from $value1 & 2
$value1 =~ s/ *$//; #remove trailing spaces
$value2 =~ s/ *$//; #remove trailing spaces
$value1 =~ s/^ *//; # and leading spaces
$value2 =~ s/^ *//; # and leading spaces

## note: have to check against blank i.e. "unless ($value1 eq '')"
##                            rather than "if($value1)" 
##  or  $value1=0 will be taken as blank.
##
unless ($value2 eq '')    # something in value2   
{    
    unless ($value1 eq '') {  $value = "$value1\" \"$value2";} # something in $value1 & $value2
    else { $value = ''; } # blank
}

else        # value2 is blank
{
    unless ($value1 eq '') {   $value = $value1; } # something in $value1 only
    else { $value=''; } # blank
}

if ($debug) { print_out($my_name,"value:\"$value\" ",$CONT);}

# and from $value
$value =~ s/ *$//; #remove trailing spaces
$value =~ s/ *$//; #remove trailing spaces

# Remove spaces from key and path as well
$path =~ s/ *$//; #remove trailing spaces
$path =~ s/^ *//; # and leading spaces
$key =~ s/ *$//; #remove trailing spaces
$key =~ s/^ *//; # and leading spaces


# Sort out path :
if($debug) { print_out($my_name, "path =$path",$CONT);}
$path=~ s/space/ /i; # if the word "space" is included, replace it with a space
$key=~ s/space/ /i; # if the word "space" is included, replace it with a space
$value=~ s/space/ /i; # if the word "space" is included, replace it with a space

if($debug) 
{ print_out($my_name, "path =$path key=$key  value=$value",$CONT);}
unless($path eq '')
{        # path is not blank
    unless($key eq '' || $key eq " ") # if key is blank, do not add trailing slash to path
    {        # path & key are not blank
	unless($path=~ m@/$@)   # if no trailing slash ...
	{ 

	    if ($debug) { print_out($my_name, "Adding a trailing slash",$CONT);} 
	    $path=~ s@$@/@;
	}
	else 
	{ 
	    if($debug) { print_out($my_name, "matched a trailing slash",$CONT);} 
	}
    }
    else 
    { if ($debug) 
      { print_out($my_name,"key(\"$key\") is blank; NOT adding trailing slash to path",$CONT);} 
  }
}
if($debug) { print_out($my_name, "path =$path",$CONT);}

# sort out $key :
if ($debug) { print_out($my_name, "key =$key",$CONT); }

unless ($key eq '')
{       # key is not blank
    # if path is not blank, do not allow leading slash 
    unless ($path eq '') { $key=~ s@^/@@; } 
    else { if ($debug) { print_out($my_name,"path is blank; allowing a leading / on key",$CONT);} }
}
if ($debug) { print_out($my_name, "key =$key",$CONT); }

$fullpath = "$path$key";
if ($debug) {print_out($my_name, "fullpath =$fullpath",$CONT);}

unless ($fullpath eq '')
{       # fullpath exists
    unless($value eq '')  { $command = "\'$cmd \"$fullpath\" \"$value\"\'";} # something in $value
    else  {  $command = "\'$cmd \"$fullpath\" \' "; } # value is blank
}
else
{       # fullpath is blank
    unless ($value eq '')  { $command = "\'$cmd  \"$value\" \'";}  # there is something in $value
    else  { $command = "\'$cmd\'";}  # $value is blank
}
if($debug)
{ 
    print_out($my_name, "get_command_string returns command=\"$command\"",$CONT);
    print_out($my_name, "   and cmd=\"$cmd\"; key=\"$key\"; path=\"$path\"; and value=\"$value\"",$CONT);
}

return ($command,$cmd,$path,$key);
}

########################################################################################
sub get_run_state
##############################################################
{
# is a run in progress?
#
# inputs:
#     none
# outputs:
#     run state  
#     transition  
#
#
# run state  :   1 = not running  3 = running
# transition in progress  = 1 if true
    my $this_path = "/Runinfo";
    my $key_state = "state";
    my $key_trans = "Transition in progress";
    
    my $run_state = 0; #initialize
    my $trans = 0; 
    my $debug = 0; # set debug

    my ($status,$path,$key,$cmd);
    my $my_name="get_run_state";

  #  print_out($my_name,  "get_run_state starting...",$CONT);
    ($status,$path,$key) = odb_cmd ( "ls","$this_path","$key_state" );
    unless  ($status) { die "Failure accessing $path/$key from odb_cmd\n"; }
    $run_state = get_int ( $key);
    
    ($status,$path,$key)= odb_cmd ( "ls","$path","$key_trans" );
    unless ($status ) { die "Failure accessing $path/$key_trans from odb_cmd\n"; }
    $trans = get_int ( $key);
    
    if ($debug) { print_out($my_name,  "Transition = $trans; Run state = $run_state ",$CONT); }
    
    if ($trans == 1 ) { if ($DEBUG) { print_out($my_name,  "Transition in progress",$CONT);}  }
    $_[0]=$trans;    
    $_[1]=$run_state;
    
    if($debug)
    {
        if ($run_state == $STATE_STOPPED)  { print_out($my_name,  "Run is stopped     ",$CONT); }
        else                               {  print_out($my_name,  "Run is not stopped     ",$CONT);  }
    
	print_out($my_name,  "get_run_State returning run_state=$run_state, trans=$trans",$CONT);
    }
    return ($run_state, $trans) ;


}

sub exit_with_message
{
    my $name = shift;
    my $status;

    unless ($name) { $name = "perl script";}
    print_out ($name, "INFO: Exiting after an error condition",  $DIE);
}




sub print_out($$$)
{
    
    # inputs:   name string flag
#
# prints the message to std output  only
#   
#     name = name of calling subroutine ($name)
#     message =  message subroutine wants to write
#     flag      = true if want to use die to print, 
#               false if just print
    
#For example,
# print_out ($name, "Failure returned from remove_a_link for $expt_name",$CONT );

    
    my ($name, $message, $Die) =  @_;
    my $status;
    my $debug=0;
  
    if($debug){print "print_out: starting with name = $name, message = \"$message\",  Die = $Die\n";}
    unless ($message eq '') 
    {
       
	{
	    print "$name: $message\n" ;
	}

	if ($Die)
	{ 	     
	    die "Aaaah!";
	}
    }
    else {  print  "$name: message not supplied\n"; }
    return;
}


# IMPORTANT
# this 1 is needed at the end of the file so require returns a true value   
1;   


















