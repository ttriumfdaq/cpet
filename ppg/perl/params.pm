# params.pm   perl module
# parameters - names of inputs and loops
use warnings;
use strict;
our (@names,@loopnames);
our $max_loopcount=1000; # maximum combined loopcount to keep files small
our $expt=$ENV{MIDAS_EXPT_NAME}; # use environment variable
my $host=$ENV{HOSTNAME}; # environment variable

unless ($expt) 
    {  die 'Environment variable MIDAS_EXPT_NAME must be set up' };

if ($expt eq "mpet")
{ # assigned outputs in ascending order to largest output used
  #            1       2        3       4         5      .....     22
    @names=qw(AWGHV  EGUN     RFTRIG1  GATE1    RFTRIG2 GATE2  RFTRIG3  GATE3 RFTRIG4  GATE4 
             AWGAM  HV       TDCGATE  TDCBLOCK unused  unused unused  unused unused  TOFGATE 
             unused EBITTRIG );
  # defined loops 
    @loopnames=qw(SCAN RAMP); # expect two loops only for mpet  (upper case)
}
elsif ($expt eq "ebit")
{ # assigned outputs in ascending order to largest output used
  # fill this in for ebit !!
  #            1       2      3      4    5     6      7       8        9    10    11   ... 24
    @names=qw(ISACBM  TIBG1 TIBG2 RFQEXT CAP1 TRFER1 TRFER2 SCALER1 SCALER2 TIG10 DUMP 
              KICK1 KICK2 EBEAM1 EBEAM2 CH16 CH17 CH18 CH19 CH20 CH21 CH22 CH23 CH24  );
  # defined loops 
    our (@loopnames_1b, @loopnames_1d, @loopnames_1e );
    @loopnames_1b=qw(SCAN EXTR); 
    @loopnames_1d=qw(EXTR SCLR);
    @loopnames_1e=qw();  # no loops
    @loopnames=qw(SCAN EXTR SCLR); # expect three loops for ebit modes 1a,1c (upper case)
}
elsif ($expt eq "pol")
{ # assigned outputs in ascending order to largest output used
  # 
  #            1           2       3       4     5         6        7       8.....     22
    @names=qw(MCSNXT1  MCSGATE1 DACSVP1  BEAM1 MCSNXT2  MCSGATE2 DACSVP2  BEAM2    unused  );
  # defined loops 
    @loopnames=qw(SCAN CYCLE); # expect two loops only for POL  (upper case)
}
elsif ($expt eq "cpet")  # like ebit
{ # assigned outputs in ascending order to largest output used
  #            1   2    3    4     5      .....     32
    @names=qw(ANODE E2 E1 DT-1 FIVE EXTRACTION G1 G2 NINE RESET ELEVEN TWELVE TRANSMISSION WEHNELT FIFTEEN SIXTEEN
SEVENTEEN EIGHTEEN NINETEEN TWENTY TWENTYONE TWENTYTWO TWENTYTHREE TWENTYFOUR TWENTYFIVE TWENTYSIX TWENTYSEVEN TWENTYEIGHT 
TWENTYNINE THIRTY THIRTYONE THIRTYTWO);
    @loopnames=qw(TRAPLOOPSTART SCAN EXTR SCLR LOOP4 LOOP5 ); # may contain more than the expected number of loops
}

else # default
{ # assigned outputs in ascending order to largest output used
  #            1   2    3    4     5      .....     16
    @names=qw(ONE TWO THREE FOUR FIVE SIX SEVEN EIGHT NINE TEN ELEVEN TWELVE THIRTEEN FOURTEEN FIFTEEN SIXTEEN);
    @loopnames=qw(LOOP1 LOOP2 LOOP3 LOOP4 LOOP5 ); # may contain more than the expected number of loops
}

if ($expt eq "pol")
   {our $logfile = "/tmp/$expt/ppg_plot.out";}
else
{
    if ($host=~/ladd04/i  || $host=~/lxm9a/i )
    { # test version in lab
	our $logfile = "/home/$expt/data/ppg_plot.out";
    }
    else
    { # titan04
        our $logfile = "/titan/data4/$expt/ppg_plot.out";
    }
}
our $path;
if ($expt eq "pol") { $path =  "/home/$expt/online/$expt/ppg/"; } # PATH for POL
else  { $path =  "/home/$expt/online/ppg/"; } # PATH

# IMPORTANT
# this 1 is needed at the EOF so require returns a true value
1;
