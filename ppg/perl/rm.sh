#!/bin/tcsh
#
# deletes old plot files. Use from webpage "delplot" button
# Check for input parameters
# run with e.g. rm.sh /home/pol/online/pol/ppg/ppgplot
#            or rm.sh /home/mpet/online/ppg/ppgplot
#  or cd to ppgplot directory, then  ../perl/rm.sh
set my_path = ./
if ($#argv == 1) then
  if (-e $1) then
    echo "$1"
    set my_path = `echo $1`
  endif
else
  echo "No input params";
endif
echo "exp:   $MIDAS_EXPT_NAME"
echo "my_path: $my_path"
echo ""
echo "deleted temporary plot files in $my_path/";
echo "removing $my_path/ppgplot.dat*"
rm -f $my_path/ppgplot.dat*
echo "removing $my_path/ppgloops.dat"
rm -f $my_path/ppgloops.dat
echo "removing $my_path/plotparams.dat*"
rm -f $my_path/plotparams.dat*
echo "removing $my_path/ppgplotloop.dat"
rm -f $my_path/ppgplotloop.dat
echo "removing $my_path/ppgplottimes*.dat"
rm -f $my_path/ppgplottimes*.dat
echo "removing $my_path/tempfile.dat"
rm -f $my_path/tempfile.dat
echo "removing $my_path/gnuplot.*"
rm -f $my_path/gnuplot.*
exit;
