#!/usr/bin/perl -w
use warnings;
use strict;


    my ($bitpat) =  @ARGV;
unless ($bitpat){ $bitpat =  0x1f9f;}

printf("bitpat= 0x%x \n",$bitpat);

my $temp = $bitpat << 1 ;
printf("bitpat shifted left by 1 = 0x%x \n",$temp);

# find highest bit set


my ($i,$j,$s);
my $mask=0;

for ($i=0; $i < 32; $i++)
{
   $mask = $mask | 1<<$i;
   $j=$bitpat >> $i;    
   if ($j == 0) { last; }

}
print ("highest bits set is i=$i\n");
printf ("mask is 0x%x \n",$mask);

$s = join(' ',split('',sprintf ("%32.32b",$bitpat))); 
print "s32=$s\n";                     
$s = join(' ',split('',sprintf ("%24.24b",$bitpat))); 
print "s24=$s\n";                     
$s = join(' ',split('',sprintf ("%*.*b",$i,$i,$bitpat))); 
print "s$i=$s\n"; 
