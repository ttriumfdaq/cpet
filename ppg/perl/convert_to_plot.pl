#!/usr/bin/perl -w
use warnings;
use strict;
#
# Converts new PPG output ppgload.dat to ppgplot.dat 
# parameters: experiment name, loopname to expand with loops.pl
#
#NOTE
# our $expt, our (@names,@loopnames) our $logfile  needed for params.pm
#
our ($inc_dir, $log, $polarity) = @ARGV; # our $expt needed for params.pm

our $expt ;#  supplied by params.pm
our (@names,@loopnames);  # needed for params.pm
our $logfile; # from params.pm
our $path; # from params.pm
sub cleanup ($$$);
sub writeloops (\% $$);

unless ($inc_dir) { die "convert_to_plot: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/params.pm";

# make sure we can open this output file
my $ppgpath = $path;
my $loadpath = $ppgpath."ppgload/";
my $plotpath =  $ppgpath."ppgplot/";
my $infile = $loadpath."ppgload.dat";
my $outfile = $plotpath."ppgplot.dat";
my $loopfile =  $plotpath."ppgloops.dat";
my $plotparams =  $plotpath."plotparams.dat";
my $line;
my $bitpat=0;
my @fields;
my $max_inputs=24;
my $counter=0;
my $nloops=0;
my $pattern;
my ($i, $j, $mask, $s, $string, $lc, $instruction, $bl, $clock, $tmp);
my ($who, $gotit);
my (@cleaned);
my @used;
my $len_used;
my $instruction_num;
my @loopbits;
my ($index,$len,$input);
my $num_inputs_used=0;
my $pol_masked=0; 

unless ($log) { $log =0 ;}
unless ($polarity) { $polarity =0 ;};
print "\n\nconvert_to_plot.pl starting with log=$log and polarity=$polarity \n";  
print "@names\n";
print "@loopnames\n";

if($log)
{
    open (LOG,">>$logfile") or ($tmp= $!);
	  
    if ($tmp)
    { 
	print "convert_to_plot.pl:  FAILURE cannot open file \"$logfile\"; $tmp "  ;
	$log=0;
    }
}

my $maxloops = $#loopnames ; #max number of loops defined -1
my %loopinfo; # store the loop information in a hash of hashes
my $got_it=0;

# go through file once to find loop information and determine the bits that ARE used


unless (-e $infile)
{
    if ($log) 
    { 
	print LOG " convert_to_plot: no such file as $infile\n";
	return 0;
    }
    die " convert_to_plot: no such file as $infile\n"; 
}

open (IN,$infile) or die  ($tmp= $!);	  
if ($tmp)
{
    if($log)
    {
	print LOG  "convert_to_plot.pl:  FAILURE cannot open file \"$infile\"; $tmp "  ;
	return 0;
    }
    die "convert_to_plot.pl:  FAILURE cannot open file \"$infile\"; $tmp "  ;
}
print "convert_to_plot: successfully opened $infile\n";  ## ppgload.dat

while (<IN>)
{
    chomp $_;
    if(/^( +)?\#/) # skip  "#" at beginning of the line
    {
	print "found comment; skipping line: $_\n";
	next;
    }
    if(/Num/)
    {
	print "found \"Num\"; skipping line: $_\n";
	next;
    }
    unless(/\d/)
    {
	print "found blank line; skipping\n";
	next;
    }

    print "working on line $_\n";
    @fields=split;
    $instruction_num = $fields[0] + 0;
    $pattern = hex $fields[1];
    $pattern +=0;
#    print "pattern: $pattern\n";    
    $bitpat = $bitpat | $pattern;  # remember which bits are used
    $instruction = hex $fields[4];
    $tmp = $instruction & 0x300000;
    
    if ($tmp ==  0x200000)  # begin loop
    {

       if($nloops > $maxloops) 
       { 
	   if ($log)
	   { 
	       print LOG "Too many loops defined $nloops > max of $maxloops\n";
	       return 0;
	   }    
	   die "Too many loops defined"; 
       }

       $lc = $instruction & 0xFFFFF;
       # $string = " # begin loop; loopcount = $lc ";
       $loopinfo { $loopnames[$nloops] } = { 
	   start => $instruction_num,
	   count => $lc,
           index => $nloops
	   };
       push @loopbits,0;
       $nloops++;
    }

    elsif ($tmp == 0x300000)  # end loop
    {   # end loop
	$bl = $instruction & 0xFFFFF; # instruction number of begin loop
	$got_it=0;
        for $who (keys %loopinfo) {
            print("Comparing start value ", $loopinfo{$who}->{start}, " with $bl\n");
	    if ( $loopinfo{$who}->{start} +0  ==  $bl+0 )
	    {
		print "found end of loop $who at instruction number $instruction_num \n";
		$loopinfo{$who}->{end} = $instruction_num;
		$gotit=1;
		#print "end: ",$loopinfo{$who}->{end},"\n";
		#print "count: ",$loopinfo{$who}->{count},"\n";
	    }
	}
	unless ($gotit) 
	{ 
	    if($log)
	    {
		print LOG  "found endloop with no beginloop at instruction line $instruction_num\n";
		return 0;
	    }
	    die "found endloop with no beginloop at instruction line $instruction_num\n";
	}
    }
}
close IN; 


print "Loops found : $nloops\n";
for $who (keys %loopinfo) {
    print "\n$who\n";
    print "start: ",$loopinfo{$who}->{start},"\n";
    print "end: ",$loopinfo{$who}->{end},"\n";
    print "count: ",$loopinfo{$who}->{count},"\n";
    print "index: ",$loopinfo{$who}->{index},"\n";
}

print "calling writeloops\n";
if (writeloops (%loopinfo, $loopfile, *LOG)  ==0)
{
    if ($log) { print LOG "convert_to_plot:  Error returned by writeloops\n"; }
    die "convert_to_plot:  Error returned by writeloops\n";
}
print "writeloops has written loopfile $loopfile\n";

printf ("bitpat= 0x%x\n", $bitpat);
# find highest bit that is set and the mask for polarity
$mask=0;
for ($i=0; $i < 32; $i++)
{
   $mask = $mask | 1<<$i;
   $s=$bitpat >> $i;    
   if ($s == 0) { last; }
}
$j=$i-1; # bit starts at 0
printf ("Highest bit set in bitpat 0x%x is bit %d. Polarity mask is 0x%x\n",$bitpat,$j,$mask);
$pol_masked = $polarity & $mask;

if($log) { print LOG  "highest bit set is bit= ($i-1)\n";}

$s = join(' ',split('',sprintf ("%*.*b",$i,$i,$bitpat)));
print "$s\n";
@used =  split(" ",$s);
$len_used =$#used + 1; # maximum number of inputs defined
print "\nused bits: $s\n";
print "used array: @used \n";

# Open output file and write the names of the Inputs and Loops
print "opening $outfile\n";
open (OUT,">$outfile") or ($tmp= $!);
if ($tmp)
{ 
    if($log)
    { 
	print LOG  "FAILURE cannot open file \"$outfile\"; $tmp\n"  ;
	return 0;
    }
    die "FAILURE cannot open file \"$outfile\"; $tmp"  ;
}

# print first line of file
print OUT "# ";
my $tl="";
# Add the names of the loops we have found
for $who (keys %loopinfo) {
    print OUT $who," " ;
    $tl=$tl."L "; 
}
$index=0;  $input=$len_used;
my @temp;
while ( $index < $len_used)
{
    if ($used[$index] == 0) 
    { # print "Input $input is NOT used\n";
    }
    else  
    { 
        push @temp,$input;
	print  "$input.$names[$input-1]  (Input $input IS used)";
        print OUT "$input.$names[$input-1] ";
        $num_inputs_used++; # remember how many inputs are used for gnuplot params
    }
    $index++; $input--;
}
print OUT "\n";

#print OUT "# Clk Cycles L L @temp # Inputs\n"; 
 print OUT "# Clk Cycles $tl @temp # Inputs\n"; # $tl contains one "L " per loop

# go through file again, this time writing the output file
# ------------  reopening file -------------------
open (IN,$infile)   or ($tmp= $!);  # reopen IN
if ($tmp)
{ 
    if($log){ print LOG "ppg_plot.pl:  FAILURE cannot open file \"$infile\"; $tmp\n";return 0;}
    die "ppg_plot.pl:  FAILURE cannot open file \"$infile\"; $tmp\n"  ;
}

my ($endflag,$begflag);

$counter=0;

my $idx=$#loopbits; # index to loopbits

while (<IN>)
{
    
    chomp $_;
    if(/^( +)?\#/) # skip  "#" at beginning of the line
    {
	print "found comment; skipped line: $_\n";
	next;
    }
    if(/Num/)
    {
	print "found \"Num\"; skipped line: $_\n";
	next;
    }
    unless(/\d/)
    {
	print "found blank line; skipped\n";
	next;
    }
    
    print "working on line $_\n";
    @fields=split;
    $instruction_num = $fields[0] + 0;
    #   print hex $fields[1];
    $pattern = hex $fields[1];
    printf ("Calling cleanup with pattern  0x%x\n", $pattern);
    @cleaned=qw(); # make sure cleaned is empty

    if(cleanup($pattern, *LOG , $pol_masked) == 0)  # removes bits that aren't used and check polarity
    {
	print LOG "convert_to_plot: error returned by cleanup\n";
	exit;
    }
    printf "After cleanup, clean is @cleaned\n";


    $clock = hex $fields[3] ; # 3 clock cycles per instruction
    $instruction = hex $fields[4];
    $tmp = $instruction & 0x300000;

    $endflag=$begflag=0;

    if ( $tmp == 0x100000 ) 
    {  
	$string = " Continue";  
    }
    elsif ($tmp == 0x300000) 
    {  # end loop
	$bl = $instruction & 0xFFFFF;
	$string = " end loop; go to line $bl ";
	$endflag=1;
        
        	
    }
    elsif ($tmp ==  0x200000) 
    {   # begin_loop
	$lc = $instruction & 0xFFFFF;
	$string = " begin loop ";
##  the "begin loop" command is not repeated in the loop
        $begflag = 1; # set a flag
	

	for $who (keys %loopinfo) {
	    # print "\n$who\n";
	    # print "start: ",$loopinfo{$who}->{start},"\n";
	    if ( $loopinfo{$who}->{start} + 0 == $instruction_num )
	    {
		$string =  $string.$who;
	    }
	}
	$string = $string . " count = $lc ";
	print "string: $string\n";
    }
    else {  $string = " # halt ";}
    
    $pattern+=0;

    print "loopbits : @loopbits\n";
    print "cleaned : @cleaned\n";

    printf ("%d ",$clock);
    print  "@loopbits @cleaned # $counter $string";
    printf (" (pattern: 0x%x) \n",$pattern);

    printf OUT ("%12.12d ",$clock);
    print  OUT "@loopbits @cleaned # ";
    printf  OUT ("%3.3d %s",$counter, $string);
    printf OUT (" (pattern: 0x%x) \n",$pattern);    
    if($endflag)
    {   # end of loop
        # find loop that has ended (i.e. the last one that began)
        my $gotcha=0;
        for (my $ii=0; $ii<$nloops; $ii++)
	{
           if ($loopbits[$ii] >0) 
	   {
	       $loopbits[$ii]=0;
	       print "end loop! Setting loopbits[$ii]  to 0\n";
	       $gotcha=1;
	       last;	       
           }
        }
        unless ($gotcha) { die " did not find a loop to end";}				   
    }
    if($begflag)
    {  #begin of loop 
        if($idx >= 0)
        { 
	    $loopbits[$idx]=1; # set loop on 
            print "begin loop! Setting loopbits[$idx]  to 1\n";
            $idx--;
        }
	else { die "ran out of loops to begin !! ";}
    }

    $counter++; # program line counter
}
close OUT;
close IN;
if($log){close LOG;}

print "Output loop file is $loopfile\n";
print "Output plotparams file is $plotparams\n";
print "\nconvert_to_plot.pl ending\n\n";   

 
sub cleanup ($$$)
{
    my $pattern = shift;
    *LOG = shift;
    my $pol_masked = shift;  # masked polarity
    my @fields;
    
    my ($s,$e,$bit,$flip);
    my $tmp;

    my $index;
    my $input;
    my $dbg=0;

    if($dbg){ print "cleanup:  used   is @used \n";}
    $len = $#used + 1;

    if($len < 1 )
    { 
	if($log){ print LOG "problem with length of array \"used\".  (len=$len, must be >=1 )\n" ;return 0;}
	die "problem with length of used array  ($len)\n" ;
    }
    # len gives the number of bits used
    if($dbg)
    { 
      print "cleanup: number of inputs used  len = $len\n";
      if($log)  { print LOG "cleanup: number of inputs used  len = $len\n"; }
    }
    $s = join(' ',split('',sprintf ("%*.*b",$len,$len,$pattern)));
   # if    ($len == 32) { $s = join(' ',split('',sprintf ("%32.32b",$pattern))); }
   # elsif ($len == 24) { $s = join(' ',split('',sprintf ("%24.24b",$pattern))); }
   # elsif ($len == 16) { $s = join(' ',split('',sprintf ("%16.16b",$pattern))); }
   # else  
   # {   
   #    print  LOG "cleanup: unexpected number of inputs found ($len). Expect 16, 24 or 32\n"; 
   #    die "cleanup: unexpected number of inputs found ($len). Expect 16, 24 or 32\n"; 
   # }

    if($dbg) {print "pattern: $s\n";}
    @fields =  split(" ",$s);
    my $len1 =$#fields + 1; # maximum number of inputs defined
    if($dbg) {print "len1= $len1\n";}
    if($len1 != $len){ die "arrays  fields and used should have the same length ($len1 and $len respectively)"; } 

    if($dbg) {print "fields : @fields\n";}

#    $index=0;$input=32;
 $index=0;$input=$len;

    $flip=0;
    while ( $index < $len)
    {
	if ($pol_masked > 0)
        {   # is this bit set in the polarity mask?
	    $bit=1<< ($input-1);
	    $flip=$pol_masked & $bit; # $flip=1 if this bit is set in polarity mask. Bit must be flipped
	    if($dbg) { printf ("Moving bit for pol is 0x%x, masked polarity =0x%x flip=0x%x\n",$bit,$pol_masked,$flip);}
            if($flip)
	    { printf ("Bit %d will be flipped ( masked polarity = 0x%x)\n",$bit,$pol_masked);}
        }


	if (($used[$index] ) == 0) 
	{ 
	    if($dbg){  print "Input $input at index $index is NOT used\n" };
	    $e = shift @fields; # remove the element
          #  if($e > 0) {die "popping used element  at index $index when bitpat says unused";}
	  #  print "removing element $e at index $index from fields\n";	    
	}
	else  
	{ 
	   #if($dbg){ print "Input $input  at index $index IS used\n"; }	 
   


            $e = shift @fields;
	   if($dbg){ print "Input $input IS used;  moving  element $e at index $index to cleaned\n"};
           if($flip)  # polarity must be reversed
	   {
	       if ($e) {$e=0; }
               else {$e=1; }
	   }

	    push @cleaned, $e;
	}
	$index++; $input--;
    }
    if($dbg) {print "cleaned: @cleaned\n";}
    return 1;
}


sub writeloops (\% $ $)
{
    my $href = shift;
    my $loopfile = shift;
    *WHERE = shift;

    if (ref($href) ne 'HASH') 
    {
	if($log){ print WHERE "writeloops: Expected a hash reference, not $href\n"; return 0; }
	die "Expected a hash reference, not $href\n"; }
   
    my %h = %$href; 
    my ($w,$tmp);
    my $nloops=0;

    print "writeloops: opening $loopfile\n";
    open (FILE,">$loopfile") or ($tmp= $!);
    if ($tmp)
    { 
	if($log){ print WHERE  "FAILURE cannot open file \"$loopfile\"; $tmp\n";return 0;}
	die "FAILURE cannot open file \"$loopfile\"; $tmp"  ;
    }
    else { print "writeloops:  $loopfile successfully opened\n"; }


    for $w (keys %h) {
	print "\n testsub: $w \n";
	print "start: ",$h{$w}->{start},"\n";
	print "end: ",$h{$w}->{end},"\n";
	print "count: ",$h{$w}->{count},"\n";
	print "count: ",$h{$w}->{index},"\n";

	print FILE "$w start:",$h{$w}->{start},"  end:",$h{$w}->{end},"  loopcount:",$h{$w}->{count},"  loopindex:",$h{$w}->{index},"\n";

        $nloops++;
    }
    print FILE "# $nloops loops\n";
    print "testsub:  Loops found : $nloops \n";
    close FILE;
    return 1;
}



