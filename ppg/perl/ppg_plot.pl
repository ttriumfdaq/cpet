#!/usr/bin/perl -w
use warnings;
use strict;
use POSIX; # needed for isalpha
#
# ppg_plot.pl called from webpage to make and display the plot for the ppg cycle
#
# NOTE - use delplot button before this to delete old plots (doesn't work to call rm.sh from this
#        file using the web for some reason
#
#   note - calls subroutine do_plots in this file (similar to do_plots.pl)
#
sub  readloopinfo($$);  # in readloopinfo.pl
sub  my_do_plots($$$$$\@);
sub  select_ebit_loopnames();

my ($inc_dir, $run_number,  @loopinfo) = @ARGV;
my $len =$#ARGV;
my $i;
my $time;
my $tmp;
my $modepath="/equipment/titan_acq/mode_name";
my $mode; # ebit mode 1a..1e  ## added as param

our ($expt,$path,$max_loopcount,$logfile);
unless ($inc_dir) { die "ppg_plot: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/params.pm"; # params.pm   $path, $expt from environment variable
my $perlpath = $path."/perl";

## add this for MIDAS messages
if ($expt eq ""){ die "MIDAS_EXPT_NAME environment variable not defined";}
require "$inc_dir/perlmidas.pl";
MIDAS_env(); 



open (LOG,">>$logfile") or ($tmp= $!);
if($tmp) 
{ 
    MIDAS_sendmsg("ppg_plot","FAILURE cannot open file \"$logfile\"; $tmp")  ;
    die "ppg_plot: FAILURE cannot open file \"$logfile\"; $tmp"  ;
}
else
{
    MIDAS_sendmsg("ppg_plot","output log file is \"$logfile\" ");
}

$time = localtime();
print LOG "\n******************************************************************************\n\n";
print LOG "\n     ppg_plot.pl starting at $time,  run number = $run_number \n";
print LOG "\n******************************************************************************\n\n";
print LOG "Input parameters :  @ARGV\n";

my($loopname, $loopcount);
print "max_loopcount $max_loopcount\n";
print "logfile $logfile \n";

if ($len < 0)
{ die "Run ppg_plot.pl with arguments include_dir  run_number [loopname nloops ...]\n";}
print " ...  parameters: @ARGV  \n";

unless ($expt eq "mpet" || $expt eq "ebit"|| $expt eq "cpet" || $expt eq "pol" )
{
    print LOG "ppg_plot:  Experiment name \"$expt\" unknown\n";
    die "ppg_plot:  Experiment name \"$expt\" unknown";
}

# exp ebit has different modes with different numbers of loops
## I don't think we need this any more
##if($expt eq "ebit") { select_ebit_loopnames(); }

$mode="none";
if  ($expt eq "ebit" || $expt eq "cpet" )
{
    $mode =MIDAS_varget( $modepath);
    if ($mode=~/not found/)
    {
	print LOG "ppg_plot: cannot read mode for $expt: $mode\n";
	print "ppg_plot: cannot read mode for $expt : $mode\n";
        $mode="none"; 
    }
}
print "experimental mode: $mode\n";

unless ($path){ $path="";}
unless ($run_number)
{
    print LOG "ppg_plot:  Run number must be supplied\n";
    die "ppg_plot:  Run number must be supplied";
}

if (isalpha($run_number))
{
    print LOG  "Input parameter run number ($run_number) should be a number\n";
    die "ppg_plot:  input parameter run number ($run_number) should be a number\n";
}

#unless ($loopname){$loopname="";}
#elsif (isdigit($loopname))
#{
#    die "ppg_plot:  input parameter loopname ($loopname) should not be a number\n";
#}
#unless ($loopcount){$loopcount=0;}
#elsif (isalpha($loopcount))
#{   
#    print LOG  "Input parameter loopcount ($loopcount) should be a number\n";
#    die "ppg_plot:  input parameter loopcount ($loopcount) should be a number\n";
#}


print LOG "rn=$run_number, path=$path, mode=$mode, array: @loopinfo\n";
print LOG "calling subroutine my_do_plots with parameters: $inc_dir, $expt, $mode, $run_number, filehandle *LOG, @loopinfo\n";


print "ppg_plot.pl: calling subroutine my_do_plots with parameters:$inc_dir,  $expt, $mode $run_number *LOG @loopinfo\n";

$i =  my_do_plots($inc_dir, $expt, $mode, $run_number, *LOG, @loopinfo);
if($i != 1)
{ 
    print LOG "error from my_do_plots.pl\n";
    die "error from my_do_plots.pl"; 
}


## gnuplot.sh runs gnuplot to display the plot.
## Instead, now using webpage to display the plot

## print LOG "cmd=$perlpath/gnuplot.sh  $path\n";
## print "ppg_plot.pl: system cmd=$perlpath/gnuplot.sh  $path\n";

##$i = system "$perlpath/gnuplot.sh  $path";
##$i+=0;
##unless ($i == 0) 
##{
## print LOG "ppg_plot:  error from gnuplot.sh\n"; 
## die "ppg_plot:  error from gnuplot.sh"; 
##}
##else {  print LOG "ppg_plot: success from gnuplot.sh\n";  }

close $logfile;


sub my_do_plots($$$$$\@)
{
#
# parameters
#         include path
#         expt name
#         mode (1a/1b etc or none)
#         run number
#         filehandle
#         name of loop(s) to be expanded (one of those listed in params.pm for this expt)  NONE if no expand required
#         expanded loop count  (or if absent, default will be the loop count in file ppgload.dat)
#         Default loopnames are LOOP1 LOOP2 LOOP3 ...
#                        or for mpet they are SCAN and RAMP
#         e.g. my_do_plots.pl mpet none 100 *FH RAMP
#              my_do_plots.pl none none 50 *FH LOOP1 2 LOOP2 20
#
    my $inc_dir = shift;
    my $exp = shift;
    my $mode = shift;
    my $rn = shift;
    *LOG = shift; # filehandle passed as a glob
    my (@expand) =@{(shift)};  
    
    my $count = @_;
    print "count: $count\n";
    
    our $expt = $exp;
    our %loopinfo; # store the loop information in a hash of hashes

    my %expand_loops;
    my $zeroflag=0;
    my $loop_count = 0;
    my $num = $#expand +1;
    my ($lcnt,$lc,$nexpand);
    our $path;
    my $loadpath = $path."ppgload/";
    my $plotpath = $path."ppgplot/";
    my $file0 = $loadpath."ppgload.dat"; # input file converted from bytecode.dat
    my $file1 = $plotpath."ppgplot.dat";
    my $file2 = $plotpath."ppgloops.dat";
    my $file3 = $plotpath."plotparams.dat";
    my $file4 = $plotpath."ppgplotloop.dat";
    my $file5 = $plotpath."ppgplottimes.dat";
    my $file6 = $plotpath."gnuplot"; #  gnuplot.pl output files are gnuplot.png and gnuplot.txt
    our $max_loopcount; # maximum combined loopcount to keep files small (in params.pm)    
    my ($i,,$k,$linenum);
    my ($element,$len);
    my $gotcha=0;
    our (@names,@loopnames); # global - filled with parameters from params.pm
    my $filename;
    my $who;
    my @fields;
    my $tmp;

    my $polarity_path="/equipment/titan_acq/settings/ppg/input/polarity";
    my $polarity;

    require "$inc_dir/params.pm"; # params.pm
    require "$inc_dir/readloopinfo.pl";
    
    print "my_do_plots starting with input parameters: \n";
    print "experiment: $exp ; mode $mode; run number $rn;  FH; expand array: @expand\n\n"; 

    print "Input names: @names\n";
    print "Loop names: @loopnames\n";
    print "Max loopcount: $max_loopcount\n";
    


    print LOG "Subroutine my_do_plots starting with  \n";
    print LOG "input parameters: $exp, $mode, $rn, @expand\n\n";
    print LOG "experiment: $expt\n"; 
    print LOG "Input names: @names\n";
    print LOG "Loop names: @loopnames\n";
    print LOG "Max loopcount: $max_loopcount\n";



unless ($run_number){ $run_number = 0; }
elsif (isalpha($run_number))
{
    print LOG  "my_do_plots: Input parameter run number ($run_number) should be a number\n";
    die "my_do_plots:  input parameter run number ($run_number) should be a number\n";
}


    my ($expand_loop_name,$name);
    my $nl=0;
    if($num) { print   "my_do_plots: num=$num. Loop(s) are to be expanded\n"; }
    else { print   "my_do_plots: num=$num.  No loops to be expanded\n"; }


    if($num) # num=number of items in @expand. If $num>0,  one or more loop(s) are to be expanded
    {
	print "array length num=$num; array: @expand\n";
	$i=0;
	my $h=$num -1 ; # highest valid array index
	while ($i < $num ) 
	{  
	    $name  = uc($expand[$i]);
            print("my_do_plots:i=$i  num=$num  working on name=$name\n");
            print LOG ("my_do_plots:i=$i working on name=$name\n");
	    ##if ($name =~ /\D+/) # \D look for any non-digit
            if (isalpha($name)) 
	    {   
		#print("found a non-digit in $name\n");
		$expand_loops {$name} = 0;  # found a loopname. initialize count to zero
                $nl++; # count number of expansions

                if (($i+1) < ($num)) # check for end of expand array
		{  $count = $expand[$i+1];  } # get the next element
                else
		{ # end of array; no count supplied for this loop
                    print "my_do_plots:loop $name to be expanded by loop count in file\n";
                    print LOG "my_do_plots:loop $name to be expanded by loop count in file\n";
		    $zeroflag = 1;  # at least one of the loop(s) uses loop count in file (count=0)
		    $i++; # next element
		    next;
		}  

		#unless ($count =~ /\D+/) # look for any non-digit 
                unless(isalpha($count)) 
		{  # $count is a number
		    $expand_loops {$name} = $count; # this element is a loop count
                    print "my_do_plots:loop $name to be expanded by $count\n";
                    print LOG "my_do_plots:loop $name to be expanded by $count\n";
		    $i+=2;
		    next;
		}  # $count is the next loop name
		else 
		{
		    $zeroflag  = 1;  # at least one of the loop(s) uses loop count in file (count=0)
		    print("my_do_plots:loop $name to be expanded by loop count in file\n"); 
		    print LOG ("my_do_plots:loop $name to be expanded by loop count in file\n"); 
		} 
		$i++;
		
	    }
            else
            {
		print LOG "my_do_plots: invalid loopname $name\n";
		die "my_do_plots: invalid loopname $name\n";
	    }
	     
	}
	
	# now check that the loopnames to be expanded exist
	
	
	for $who (keys %expand_loops) 
	{    
	    $gotcha=0;
	    for $element (@loopnames)  # defined loopnames are in params.pm
	    {
		print("my_do_plots:  element:  $element\n");
		if ($element eq $who) { $gotcha=1; last; }
	    } 
	    unless ($gotcha) 
	    {
                # remove the loop if it hasn't been found in loopnames
                # ( ebit doesn't have some of the loops in different modes)
		print LOG "my_do_plots: no such loop name as \"$who\" has been found in array of loopnames\n "; 
		print "my_do_plots: no such loop name as \"$who\" has been found in array of loopnames \n";
                print LOG "my_do_plots: deleting $who from hash array\n";
                print  "my_do_plots: deleting $who from hash array\n"; 
                delete $expand_loops {$who};
                $nl--; # decrement the count of loops to be expanded
	    }
            else
            {
		print LOG "my_do_plots: loop $who will be expanded with loop count $expand_loops{$who}\n";
	        print "my_do_plots: loop $who will be expanded with loop count $expand_loops{$who}\n";
            }
	}
      
        print "my_do_plots: number of loops to be expanded is $nl\n";
        print LOG "my_do_plots: number of loops to be expanded is $nl\n";


    }
    else  
    { 
       print "my_do_plots: no loop is to be expanded\n";
       print LOG "my_do_plots: no loop is to be expanded\n";
    }

    if($nl > 0)
    {
       for $who (keys %expand_loops) 
       { 
          print "my_do_plots: loop $who  loop count  $expand_loops{$who}\n";
          print LOG "my_do_plots: loop $who  loop count  $expand_loops{$who}\n";
       }
    }


#check input file is present
    unless (-e $file0)
    {
	print LOG "my_do_plots: no such loop name as \"$who\" has been found in array of loopnames\n "; 
	print "my_do_plots: no such file as $file0\n";
	return 0;
    }
    
#remove old copies of output files
# this doesn't work from webpage button (why? permissions?). Use delplot button
   my $ppgplot_dir = $inc_dir;
   $ppgplot_dir =~ s/perl/ppgplot/;
   print "ppgplot_dir=\"$ppgplot_dir\"\n";
   print "sending system command :\"$perlpath/rm.sh $ppgplot_dir\"  \n";
    system "$perlpath/rm.sh $ppgplot_dir";

#  get the polarity from ODB
    $polarity =MIDAS_varget( $polarity_path);
    if ($polarity=~/not found/)
    {
	print LOG "ppg_plot: cannot read polarity for $expt: $polarity\n";
	print "ppg_plot: cannot read polarity for $expt : $polarity\n";
        $polarity=0; 
    }


    print "running convert_to_plot with cmd \"$perlpath/convert_to_plot.pl $perlpath 1 $polarity\" \n";
    $i = system "$perlpath/convert_to_plot.pl $perlpath 1 $polarity";  # -> ppgplot.dat  still in clock cycles
    $i+=0;

    # reopen logfile
    open (LOG,">>$logfile") or ($tmp= $!);
	  
    if ($tmp)
    { 
	print "ppg_plot.pl:  FAILURE re-opening file \"$logfile\"; $tmp "  ;
    }

    unless ($i == 0) 
    { 
	print LOG  "error from convert_to_plot\n";
	print "error from convert_to_plot\n";
	return 0;
    }
    
    unless (-e $file1)
    {
	print " my_do_plots: no such file as $file1\n";
	return 0;
    }
    unless (-e $file2)
    {
	print LOG " my_do_plots: no such file as $file2\n";
	print " my_do_plots: no such file as $file2\n";
	return 0;
    }
    
    print "my_do_plots: nl=$nl  \n";
# check loopsizes for multiple loop expansion
#      (if one loop only, check in loops.pl will be sufficient)  
    if($nl > 1)
    { # number of expanded loops is more than one;
	if( $zeroflag)
	{ # one or more of the loopcounts is zero
	    # read information from the loopfile
	    $lc = readloopinfo($perlpath, $file2);   
	    print "Loop information read from file $file2:\n";
	    my $w;
	    for $w (keys %loopinfo) {
		print "\n loop: $w \n";
		print "start: ",$loopinfo{$w}->{start},"\n";
		print "end: ",$loopinfo{$w}->{end},"\n";
		print "count: ",$loopinfo{$w}->{count},"\n";
		print "index: ",$loopinfo{$w}->{index},"\n";
	    }
	}
	# check combined loopcounts
	$lcnt=1;
	for $who (keys %expand_loops)
	{
	    print "\n$who";
	    print "  loopcount for loop $who ",$expand_loops{$who},"\n";
	    $lc =$expand_loops{$who};
	    if ($lc == 0)
	    {
		$lc = $loopinfo{$who}->{count};
		print "Loop $who to be expanded with count from file i.e. $lc\n";    
	    }
	    $lcnt *= $lc;
	}
	print "Combined loop count is $lcnt\n";
	if($lcnt > $max_loopcount) 
	{ 
            MIDAS_sendmsg("ppg_plot","combined loop count(s) must be less than $max_loopcount. Supply loopcount(s) for gnuplot using \"plot2\" button, then use \"gnuplot2\" button");
	    print LOG "combined loop count must be less than $max_loopcount to keeps files small\n";
	    print LOG "supply smaller loop count(s)\n";
	    print "combined loop count must be less than $max_loopcount to keeps files small\n";
	    print "supply smaller loop count(s)\n";


	    return 0;
	}
    }

# important !!    
# if /home/pol/online/pol/ppg/ppgplot/plotparams.dat is present (i.e. not deleted) make sure it is empty
# because loops.pl opens it with append.

    if (-e $file3)  #  plotparams.dat
	    {  
		print "  $file3 exists. Clean it out by opening a new version\n";
		print "opening $file3\n";
                open (OUT, ">$file3") or ($tmp= $!);
		if($tmp)
		{
		    print "FAILURE cannot open file \"$file3\"; $tmp"  ;
		}
		else 
		{ 
		    ##print OUT "hi from ppg_plot.pl\n";
		    close(OUT) };
	    }

# run loops.pl to expand loop (if any) and convert to time values (ms)
    if($nl)
    {  # first loop to be expanded
	$filename=$file1;
	$k=0; # counter
	for $who (keys %expand_loops)
	{
	    print "\n$who";
	    print "  loopcount: ",$expand_loops{$who},"\n";
	    
	    print "Expanding loop \"$who\" and converting delays  to times...input filename is $filename \n";
	    $i = system "$perlpath/loops.pl $perlpath $filename $who $expand_loops{$who}";  # -> ppgplotloop.dat -> ppgplottimes.dat
	    print "command was  $perlpath/loops.pl $perlpath $filename  $who $expand_loops{$who}\n";
	    $i+=0;
	    unless ($i == 0) 
	    { 
		print LOG  "error from loops.pl\n"; 
		print "error from loops.pl\n"; 
	    }
	    $k++;
	    if($k >= $nl){last;} # all expansions are done
	    
	    # output file is $file4 (ppgplotloop.dat)
	    
	    $filename = $file4;
	    $filename=~ s/loop/$who/;
	    system "cp $file4 $filename"; # copy output file for debugging
	    print "input filename is now $filename\n";
	}
	
	if($nl > 1)  # adjust plotparams file (multiple loops)
	{
	    unless (-e $file3) 
	    { 
		print LOG  " no such file as $file3\n"; 
		print " no such file as $file3\n";
		return 0;
	    }
	    $filename="$file3.multiple_plots";
	    system "cp $file3 $filename"; # copy file for debugging	
	    print "opening $filename\n";
	    unless (-e $filename) 
	    { 
		print LOG " no such file as $filename\n";
		print " no such file as $filename\n";
		return 0;
	    }
	    open (IN,$filename) or ($tmp= $!);
	    if($tmp)
	    {
		print LOG  "FAILURE cannot open file \"$filename\"; $tmp\n"  ;
		print "FAILURE cannot open file \"$filename\"; $tmp"  ;
		return 0;
	    }
	    
	    my @info="";
	    
	    while (<IN>)
	    {
		chomp $_;
		@fields = split;
		$k = pop @fields ;
		unshift @info, $k;
		$k = pop @fields ;
		unshift @info, $k;
		print "info: @info;\n";
	    }
	    #print "@fields\n";
	    
	    print "opening $file3\n";
	    $k=0;
	    open (OUT,">$file3") or ($k= $!);
	    
	    if ($k)
	    { 
		print "FAILURE cannot open file \"$file3\"; $k"  ;
		return 0;
	    }
	    print OUT "@fields @info\n";
	    print "@fields @info\n";
	    close OUT;
	}
    }
    else
    {
	print "Converting delays  to times... \n";
	$i = system "$perlpath/loops.pl $perlpath $file1";  # -> ppgplotloop.dat -> ppgplottimes.dat
	print "command was  $perlpath/loops.pl $perlpath $file1\n";
	$i+=0;
	unless ($i == 0) 
	{ 
	    print "error from loops.pl";
	    return 0;
	}
    }
    
    unless (-e $file3)
    {
	print " my_do_plots: no such file as $file3\n";
	return 0;
    }
    
    
#
    my $tempfile = $file6."*"."png";
    print "deleting old copies of plot files...\n";
    
    system "rm -f $tempfile";  
    $tempfile = $file6."*"."txt";
    system "rm -f $tempfile";  
    
    
    open (IN,$file3) or   ($tmp= $!);
    if($tmp)
    {
	print LOG  "FAILURE cannot open file \"$file3\"; $tmp\n"  ;
	print "FAILURE cannot open file \"$file3\"; $tmp"  ;
	return 0;
    }
    while (<IN>)
    {
	chomp $_;
	if(/\#/)
	{
	    print "skipping line: $_\n";
	    next;
	}
	
	print "Working on line: $_\n";
	s/rn/$rn $mode/; # add more info
        #s/rn/$rn/;
	print "**** Subs rn: $_\n";
	
# run gnuplot
        print LOG "running $perlpath/gnuplot.pl $perlpath $_ \n";
	print "running gnuplot.pl with params $perlpath $_ \n";
	$i = system "$perlpath/gnuplot.pl $perlpath $_";
	unless ($i == 0) 
	{
	    print LOG   "FAILURE - error returned from gnuplot.pl\n";
	    print "my_do_plots:  error returned from gnuplot.pl\n";
	    return 0; 
	}
    }
    

#run gnuplot interactively
#system "$perlpath/gnuplot.sh";
    
    
    print "subroutine my_do_plots is ending\n";
    my $perlpath = $path."perl";
print "===============================================================================\n";
print "Now run \n";
    print "     $perlpath/gnuplot.sh $path\n";  
    print "   to have gnuplot display the plots.\n";
    print "Or view plot on experiment's gnuplot webpage\n";
print "===============================================================================\n";
    
#$tempfile = $file6.".png";
#print ("running display on $tempfile\n");
#system "display $tempfile &";
    print LOG  "do_plot: done\n\n";
    print "do_plot: done\n";
    return 1; 
}


sub select_ebit_loopnames()
{
   # ebit has different modes with fewer loops defined
   # get the mode from ODB 

    our (@loopnames, @loopnames_1b, @loopnames_1d, @loopnames_1e ); #defined in params.pm

    my $mode =MIDAS_varget( $modepath);
    if ($mode=~/not found/)
    {
        MIDAS_sendmsg("ppg_plot","FAILURE cannot read mode for ebit experiment: $mode");
	print LOG "ppg_plot:FAILURE cannot read mode for ebit experiment: $mode\n";
	die "ppg_plot: FAILURE cannot read mode for ebit experiment: $mode\n";
    }
    print "select_ebit_loopnames: ebit mode:$mode\n";


    unless ($mode=~/^1[abcde]$/  )  # modes 1a 1b 1c 1d 1e
    {
	MIDAS_sendmsg("ppg_plot"," Unsupported  mode $mode  for $expt ");
	print LOG "select_ebit_loopnames: Unsupported  mode $mode for $expt\n";
	die "select_ebit_loopnames: Unsupported  mode $mode for $expt\n";
    }
    if($mode eq "1b") { @loopnames = @loopnames_1b; }  # these are in params.pm
    elsif ($mode eq "1d") { @loopnames = @loopnames_1d; }
    elsif ($mode eq "1e") { @loopnames = @loopnames_1e; }

    print "select_ebit_loopnames:  experiment $expt mode $mode loopnames @loopnames\n";
    print LOG "select_ebit_loopnames:  experiment $expt mode $mode loopnames @loopnames\n";
    return;
}
