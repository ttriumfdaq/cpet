#!/usr/bin/perl 
#
# Run this file e.g.
# /home/ebit/online/ppg/perl/gnuplot_awg.pl /home/ebit/online /home/ebit/online/awg/awg_zero.dat /data/ebit/info 
######################################################################################################
# Parameters: "online" directory, input filename, output directory, AWG channel (optional)
#
# Plots up to 8 AWG channels from data file /home/ebit/online/awg/awg_Unit.dat
#      where Unit =AWG unit number (zero or one)
#
# If AWG_channel is blank (not supplied)  : 
#    Plots all 8 channels on one plot -> /home/ebit/online/awg/awg_Unit_all.png
#    And writes plots of each channel into  /home/ebit/online/awg/awg_Unit_Ch.png
#        where Unit=AWG unit (zero,one) and Ch=channel (0-7)
#    Copies awg_Unit_all.png to  run directory as /data/ebit/info/runXX_awg_Unit.png
#
# If AWG_channel is supplied (i.e. 0-7 ), 
#    Plots only that AWG channel -> awg_Unit_Ch.png
#    and Copies awg_Unit_Ch.png to run directory as /data/ebit/info/runXX_awg_Unit.png
#
# Makes a softlink /home/ebit/online/awg_Unit.png  to /data/ebit/info/runXX_awg_Unit.png
#  (after deleting old softlink)
use strict;
our ($onlinepath, $filename, $datapath, $ichan ) = @ARGV; # input parameter
our $nparam=4; # input params
my $name="gnuplot_awg.pl";
my $count;
my @names;
my $index;
my ($istart,$iend);
my ($max_time,$line,$i,$j,$k,$l,$m);
my ($xmax,$xmin);
my $width;
my (@lines,@single);
my $flag;
my ($rn,$unit);
my $outfile;
my @ppg_signals = ("AWGAM","AWGHV");
my @unit_names = ("zero","one");

##################### G L O B A L S ####################################
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $EXPERIMENT="ebit";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
#our $MTALK=32; # talk

#######################################################################
use lib "/home/ebit/online/ppg/perl";
require "send_message.pl";

$|=1; # flush output buffers

$count = @ARGV;

# remove excess /
$onlinepath = strip($onlinepath);
$filename =  strip($filename);
$datapath = strip($datapath);

print "gnuplot_awg.pl: starting with num arguments = $count\n";
print "\n  **** NEW1 VERSION ****\n";
print "  parameters online path = $onlinepath, file name = $filename\n";
print "             data path =  $datapath,  channel = $ichan\n";


if($count > $nparam || $count < ($nparam-1))
{
#  send_message($name,$MINFO,"FAILURE incorrect number of parameters supplied to $name ($count)" ) ;
  die "gnuplot_awg.pl: incorrect number of parameters supplied ($count); expect at least ($nparam-1)\n";
}
 
print "count = $count  nparam=$nparam\n";

if($count == $nparam-1)
{
    $istart = 0;
    $iend = 8;
    print "all AWG channels will be plotted\n";
    $flag=1;
}
else 
{
    if($ichan < 0 || $ichan > 7){ die "invalid channel ($ichan). Enter 0-7 or no param for all channels\n"}  
    print "AWG channel $ichan will be plotted\n"; 
    $istart = $ichan;
    $iend = $ichan+1;
    $flag=0;
}


print "filename=$filename\n";
unless (-e $filename)
{
  send_message($name,$MINFO,"FAILURE file \"$filename\" does not exist" ) ;
  die "gnuplot_awg.pl: no such file as $filename\n";
}

# first line in file contains the maximum time on the x axis
open (RFC,$filename) or die $!;
$line=<RFC>;
print $line;
$line =~tr/ / /s;
chomp $line;
@names = split (/ /,$line);
shift @names; # remove #

$i = @names;
# make sure the file contains the 4 parameters needed for plotting
unless ($i > 4)
{
    send_message($name,$MINFO,"FAILURE file \"$filename\" does not have enough parameters" ) ;
    die "gnuplot_awg.pl: file \"$filename\" does not have enough parameters (expect 4) \n";
}
foreach $index (@names)
{ print "$index\n";  }

print "@names[0]\n";
$max_time = @names[0];
$width =  @names[1];
print "max time = $max_time ms;  pulse width = $width ms\n";
$rn = @names[2]; # run number
$unit=@names[3]; # AWG unit number
print "run number=$rn;  AWG unit = $unit\n";
unless ( ($unit ==0)  or ($unit == 1) )
{
    send_message($name,$MINFO,"FAILURE: illegal AWG unit $unit, Must be 0 or 1" ) ;
    die "illegal AWG unit $unit. Must be 0 or 1\n";
}
unless ((  ($filename =~ /zero/) and ($unit == 0)) or
	(  ($filename =~ /one/) and ($unit == 1)))
{
    send_message($name,$MINFO,"FAILURE file \"$filename\" does not match AWG unit number ($unit)" ) ;
    die "gnuplot_awg.pl: file \"$filename\" does not match AWG unit number ($unit)\n";
}


my $ppg_name = $ppg_signals[$unit];
my $my_unit = $unit_names[$unit];
print "AWG Unit $my_unit  PPG signal name $ppg_name";


print " removing softlink  \"$onlinepath/awg_$my_unit.png\" \n";
system ("rm -f $onlinepath/awg_$my_unit.png"); # remove old softlink

# calculate x axis offsets of 5% to get plots away from y axes
$xmin = 0.05 * $max_time;
$xmax = $xmin + $max_time + .25*$max_time; # add last offset to get key off the plot
$xmin *= -1;

$l=3;
#print "istart=$istart iend=$iend\n"; 
for ($i=$istart; $i<$iend; $i++) # for AWG clock pulse and all 8 AWG channels
{
    print("plotting awg channel $i -> awg$i.png \n");
    $j = $i+4;
    $k = $i+1;
    $m = $k+3; # plot colour (lm); make sure colour of AWGAM is different
# lines[0] : plot of one AWG Voltage channel (-10,+10V)
# lines[1] : plot of AWGAM (clock) offset to -12 so appears at bottom of plot
    if ($i == $istart)
    { # first
	@lines[$i] = "\'$filename\' using 2:(\$$j) with steps title \"AWG chan $i\" lt $k";
    }
    else
    {
	@lines[$i] = ", \'$filename\' using 2:(\$$j) with steps title \"AWG chan $i\" lt $k";
    }
#                                
}
# now add AWGAM to the plot
    @lines[$iend]= ", \'$filename\' using 2:(\$$l-12) with steps title \"AWGAM\" lt $m";
$single[1]=@lines[$iend];
print @lines;

open (OUT,"|gnuplot") or die $! ; 
print OUT<<eot;
set xrange [$xmin:$xmax]
set yrange [-14:+12.5]
set xlabel "time (ms)"
set ylabel "Voltage (Volts)"    
set title "AWG Unit $unit  Voltage versus Time    Run $rn"

# plot automatically
#plot @lines
#pause 10;
set term png colour 
set out "$onlinepath/awg/awg_$my_unit\_all.png"
#    replot
plot @lines
eot
# eot ends print statement; no spaces allowed

if ($flag)
{ # all channels
    print "\nNow plotting individual channels:\n";
    # now produce the plots awgU_C.dat (but don't display them automatically)
    $single[0]= $lines[$istart]; # first one
    for ($i=$istart; $i<$iend; $i++) # for AWG clock pulse and all 8 AWG channels
    {
	print "plotting @single\n";

open (OUT,"|gnuplot") or die $!; 
print OUT<<eot;
set xrange [$xmin:$xmax]
set yrange [-14:+12]
set xlabel "time (ms)"
set ylabel "Voltage (Volts)"    
set title "AWG Unit $unit Channel $i  Voltage versus Time  Run $rn"
	    
# do not plot these automatically
##plot @single
##pause 10;
set term png colour 
set out "$onlinepath/awg/awg_$my_unit\_$i.png"
#    replot
plot @single
eot
# eot ends print statement; no spaces allowed
	    
# subsequent plots must have leading comma removed
        $single[0]=$lines[$i+1];
        $single[0]=~s/,/ /;
     } # end of loop
}
else
{  # one channel only
    print "\ncopying $onlinepath/awg/awg\_$my_unit\_all.png to  awg$my_unit\_$istart.png and exiting\n";
    system ("cp  $onlinepath/awg/awg\_$my_unit\_all.png  $onlinepath/awg/awg$my_unit\_$istart.png"); # copy to awgN.png
#    exit;
}

$outfile = "$datapath/run$rn\_awg\_$my_unit.png";
print "outfile = $datapath\n";
print "\ncopying  $onlinepath/awg/awg\_$my_unit\_all.png to $outfile and exiting\n";
system ("cp  $onlinepath/awg/awg\_$my_unit\_all.png $outfile"); # copy to data directory
print "making softlink \"$onlinepath/awg\_$my_unit.png\" to \"$outfile\" \n";
system ("ln -s $outfile  $onlinepath/awg\_$my_unit.png");


