#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
#
#   Normally invoked from user button
# 
# invoke this script with cmd e.g.
#                  include_path               experiment   select_mode  mode_name    
# change_mode.pl  /home/cpet/online/ppg/perl     cpet         1a        defaults
#
#   file with defaults is loaded when changing mode, e.g. 2a_defaults.odb
#       (providing it exists)
#   if mode_name is blank or defaults, looks for 2a_defaults.odb to load
#   if mode_name is "none", no defaults are loaded
#   if mode_name is "test", looks for 2a_test.odb to load
#
#   the path for mode files is /home1/ebit/online/modefiles ($modepath)
#
#
# based on change_mode.pl Rev 1.31 BNMR
# $Log: change_mode.pl,v $
#
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
#  parameters needed by init2_check.pl (required code common to perlscripts)
#
# input parameters :
our ($inc_dir, $expt, $select_mode, $mode_name ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "change_mode" ; # same as filename
our $other_name = "get_next_run_number";
our $outfile = "change_mode.txt"; # path will be added by file open
our $parameter_msg = "include path , experiment , select_new_mode  mode_name";
our $nparam = 4;  # no. of input parameters
our $beamline = $expt; # beamline is not supplied. Same as $expt 
############################################################################
# local variables:
my ($transition, $run_state, $path, $key, $status);
my $input_path = "/Equipment/Titan_acq/";
my $old_ppg;
my ($ppg_path,$string);
my ($process,$run_state,$transition);
my ($msg,$loop,$flag);
my $changing_mode=$FALSE;
my $mode_filename;
my $experiment_name;
my $fixed_link = "/alias/ppg_blocks&"; # name of fixed BLOCKS link
my $ppg_cycle_link = "/Equipment/titan_acq/ppg cycle";
my $ppg_cycle_mode = "/Equipment/titan_acq/ppg_cycle_mode_";
my $modepath; # /home/cpet/online/modefiles  path for cpet's modefiles
#--------------------------------------------------------------------------
$|=1; # flush output buffers



# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
require "$inc_dir/do_link.pl";

#bypass check on get_run_number.pl for titan expt  - doesn't use MUSR run numbering
if ($expt =~  /mpet/i || $expt =~ /ebit/i || $expt =~ /cpet/i ){ $other_name=""; }
# Init2_check.pl checks:
#   one copy of this script running
#   no copies of $other_name running (may interfere with this script)
#   no. of input parameters is correct
#   opens output file:
#
require "$inc_dir/init2_check.pl";

# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null


print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt, select new mode = $select_mode  load file mode name $mode_name \n";

print FOUT ("fixed_link = $fixed_link\n");
#print FOUT ("fixed_link1 = $fixed_link1\n");

unless ($select_mode)
{
    print FOUT "FAILURE: selected mode  not supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE:  selected mode not supplied " ) ;
	unless ($status) { print FOUT "$name: Failure return after msg \n"; } 
        die  "FAILURE:  selected mode  not supplied \n";
}
unless ($select_mode =~/^[12]/)
{
    print_3 ($name,"FAILURE: invalid selected mode ($select_mode)",$MERROR,1);
}
unless ($expt =~ /mpet/i || $expt =~ /ebit/i  || $expt =~ /cpet/i )
{
    print FOUT "FAILURE: experiment $expt not supported\n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE:  experiment $expt not supported " ) ;
    unless ($status) { print FOUT "$name: Failure return after msg \n"; } 
    die  "FAILURE:  experiment $expt not supported\n";
}

$modepath =  "/home/$expt/online/modefiles"; # path for modefiles
$select_mode =~ tr/A-Z/a-z/; # lower case



# make a few sanity checks:
# ebit/cpet only support Mode 1
if ($select_mode =~ /^1/) # Type 1 experiment  (maybe other types in future??)
{
    if ( $expt =~  /ebit/i   ) 
    {
	unless ($select_mode=~/[abcde]$/  )  # modes 1a 1b 1c 1d 1e
	{
	    print_3 ($name, " Unsupported  mode  for $expt ($select_mode)",$MERROR, 1);
	}
    }

    if ( $expt =~  /cpet/i  ) 
    {
	unless ($select_mode=~/[abcdefg]$/  )  # modes 1a 1b 1c 1d 1e 1f 1g
	{
	    print_3 ($name, " Unsupported  mode  for $expt ($select_mode)",$MERROR, 1);
	}
    }
    else
    {  # mpet not yet supported
	#unless ($select_mode=~/[0ncfhd]$/  )  # modes 10 1n 1h 1c 1f 1d exist for POL
	#{
	    print_3 ($name, " Unsupported  mode  for $expt ($select_mode)",$MERROR, 1);
	#}
    }
}
else
{
 # no other types supported
 print_3 ($name, " Unsupported  mode  for $expt ($select_mode)",$MERROR, 1);
}
# EBIT/CPET supports loading a file of defaults when changing mode
if ( $expt =~  /mpet/i    )
{ 
    $mode_filename = "none";
}
else
{
    $mode_name =~ tr/A-Z/a-z/; # translate to lower case
# generate mode_filename 
    unless ($mode_name =~/none/i)
    {
        $mode_filename = "$select_mode"."_"."$mode_name.odb"; #concatenate
    }
    else
    {
        $mode_filename = $mode_name;
    }
    print_2 ($name,"mode_filename=\"$mode_filename\"; select_mode=$select_mode, 
mode_name=$mode_name",$CONT);
}




#
# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if($transition ) 
{ 
    print FOUT "Run is in transition. Try again later \n";
    print FOUT "Or set /Runinfo/Transition in progress to 0\n";
    ($status)=odb_cmd ( "msg","$MINFO","","$name", "Run is in transition. Try again later or set /Runinfo/Transition in progress to 0 " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "Run is in transition. Try again later or set /Runinfo/Transition in progress to 0 " ;
}

if ($run_state != $STATE_STOPPED) 
{   # Run is not stopped; 
    print FOUT "Run is in progress. Cannot change experimental mode\n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Run in progress. Cannot change experimental mode" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "Run is in  progress. Cannot change experimental mode " ;
}

# key /equipment/titan_acq/mode_name contains the present ppg mode 
($status, $path, $key) = odb_cmd ( "ls","$input_path","mode_name" ) ;
unless ($status)
{ 
    print FOUT "$name: Failure from odb_cmd (ls); Error getting $input_path/mode_name \n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE getting $input_path/image/mode_name" ) ;
    die "$name: Failure getting $input_path/image/mode_name  ";
}
($old_ppg, $msg) =get_string($key);

$old_ppg =~ tr/A-Z/a-z/; # lower case

print "Present ppg mode =$old_ppg\n";
print FOUT "Present ppg mode =$old_ppg\n";



write_time($name,$CONT);
my $my_cycle_mode;
$my_cycle_mode= $ppg_cycle_mode.$select_mode;
print "my_cycle_mode=$my_cycle_mode\n";
print FOUT "my_cycle_mode=$my_cycle_mode\n";
# my_cycle_mode should be e.g. /equipment/titan_acq/ppg_cycle_mode_1a

# check if same expt. is already set up
if($old_ppg eq $select_mode) # both are lower case
{ 
    print FOUT "\n -------- old and new modes are the same  ------------\n";
    print FOUT ("calling test_a_link with select_mode = $select_mode, fixed_link =$fixed_link ppg_cycle_link=$ppg_cycle_link\n");
    $status = test_a_link ($my_cycle_mode, $fixed_link, $ppg_cycle_link);
    if($status == $TRUE)
    {
	print FOUT "$name: Experiment $old_ppg is set up already\n";
	print      "$name: Experiment $old_ppg is set up already\n";
	odb_cmd ( "msg","$MINFO","","$name","INFO: experiment $old_ppg is set up already");
	exit;
    }

}    
    
#my $fixed_link = "/alias/ppg_blocks&"; # name of fixed BLOCKS link
#my $ppg_cycle_link = "/Equipment/titan_acq/ppg cycle";
#my $ppg_cycle_mode = "/Equipment/titan_acq/ppg_cycle_mode_";
   
print FOUT "\n -------- remove old links  ------------\n";
$status = remove_my_link($fixed_link);
$status = remove_my_link($ppg_cycle_link);

$status = make_my_link($my_cycle_mode,$fixed_link);
$status = make_my_link($my_cycle_mode,$ppg_cycle_link);
unless($status) 
	{    
	    print FOUT "$name: Error return from make_link for /ppg/$select_mode\n"; 
	    die "$name: Error return from make_a_link for $select_mode\n ";
	}


print FOUT "\n -------- update mode_name  ------------\n";
($status) = odb_cmd ( "set","$input_path","mode_name" ,$select_mode) ;
unless($status)
    { 
	print FOUT "$name: Failure from odb_cmd (set); Error setting $input_path mode_name \n";
	odb_cmd ( "msg","$MERROR","","$name", "FAILURE setting mode_name" ) ;
	die "$name: Failure setting $input_path mode_name  ";
    }

# load the file of default values for this mode if mode_filename is not "none"
    unless ($mode_filename =~/none/i)
    {
        my $modefile = "$modepath/$mode_filename";
        print FOUT "\n---- loading default values for mode $select_mode from file \"$modefile\"-------\n"
;
        print "\n----loading default values for mode $select_mode from file \"$modefile\"-------\n";
  
        $status =  load_mode_defaults ($modefile);
        unless($status) 
        {  
            print_2($name, "Error return from load_mode_defaults",$CONT); 
            print_3($name, "WARNING: could not load mode defaults from file $mode_filename ",
                    $MINFO,$CONT); 
        }
        else
        {  
            print_3($name, "INFO - mode default values from file $mode_filename have been loaded ",$MINFO,$CONT); 
        }
    }
    else
   {
        print FOUT "\n---- no default values for $select_mode will be loaded -------\n";
        print "\n---- no default values for $select_mode will be loaded -------\n";
    }







print FOUT "INFO: Success -  ppg mode) $select_mode is now selected \n";
odb_cmd ( "msg","$MINFO","","$name", "INFO: ppg mode $select_mode is now selected " );
print  "INFO: Success - ppg mode  $select_mode is now selected  \n";



write_time($name, 3);

## run tri_config so that gnuplot will plot the latest if the button is pressed!

print "change_mode.pl: system cmd=\"/home/$expt/online/ppg/tri_config -s\"\n";
$i = system "/home/$expt/online/ppg/tri_config -s";
exit;




























