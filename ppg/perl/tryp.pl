#!/usr/bin/perl -w
#
# Open output file 
my $plotfile="plotfile.tmp";
my $tmp;
my $outfile="plotfile.png";
$my_term = "png large   size 1000,800   xffffff x000000 x404040 xff0000 xffa500 x66cdaa xcdb5cd xadd8e6 x0000ff xdda0dd x9500d3 x008000 x800000 x000080 x00ff00 x800080 x008080 xff00ff x808080";
print "opening $plotfile\n";
open (OUT,">$plotfile") or ($tmp= $!);
if ($tmp)
{ 
    die "FAILURE cannot open file \"$plotfile\"; $tmp"  ;
}
my $title="\"test title\"";
my $ytics="set ytics (";
my $nplot=1;
my $min_time=0;
my $max_time=100;
my $xoffset=0.1;
my $yoffset=0.2;
my $array =  "\'/home/cpet/online/ppg/ppgplot/ppgplottimes\.dat\' using 1:(\$2+0) with steps notitle";
print "array: $array\n";
print OUT<<eot;
set xlabel "time (ms)"
set ylabel "Loops /  PPG Outputs"
set title $title   font "large" 
set time
$ytics
set yrange [-1:$nplot + 1]
set xrange [$min_time:$max_time]
set offset $xoffset,$xoffset,$yoffset,$yoffset
set term $my_term
set term png  xFFFFFF 
set out "$outfile"
bind c "set offset 0,0,0.4,0.4 ; replot"
plot  $array 
pause -1 "Press Return to finish"
eot
close OUT;
print "closed $plotfile\n";


print "now send to gnuplot...\n";
open (OUT,"|gnuplot") or die $!;
open (IN,$plotfile) or die $!;
    while (<IN>)
    {
	chomp $_;
        print OUT $_;
    }
close IN;
