#!/usr/bin/perl -w 
# above is magic first line to invoke perl
# or for debug
use warnings;
use strict;
#
# prototypes for subs
sub roundup($);
sub polarity($$);
#
# Translates delays.dat to instructions for new PPG
# Input parameters
#       param 1 include path  ....online/ppg/perl
#       param 2 path and name of input file (...online/ppg/ppgload/delays.dat)
#       param 3 path and name of output file (...online/ppg/ppgload/ppgload.dat -> $ppgload)
#       param 4 nominal PPG frequency - (e.g. if nominal clock is 10MHz and actual clock of NEW PPG is 100MHz,
#           output times will be multiplied by 10 to give the correct result i.e. as if NEW PPG runs at 10MHz).
# 
# example: convert_delays.pl /home/cpet/online/ppg/perl /home/cpet/online/ppg/ppgload/delays.dat /home/cpet/online/ppg/ppgload/ppgload.dat 100  (PPG running at 100MHz)
my ($inc_dir, $filename, $ppgload, $nominal_freq) = @ARGV;
my $num = $#ARGV;
my $clock_freq_MHz = 100; 
my $max_lc = 0xFFFFF; # 20 bits
my $multiply;
# these are input parameters
my $name = "convert_delays";
my $exp = $ENV{MIDAS_EXPT_NAME}; # experiment name 
my $minimal_delay_cs=3; # 3 clock cycles (new ppg)
my $halt = 0; # halt instruction (new ppg)
my ($ins,$lc,$ln,$delay_ms,$delay_cs, $setpat, $clrpat, $tr_code);
my $tmp;
my @fields;
# instruction list for new and old ppg
# new_instructions are  Halt=0, Continue=1, Begin Loop=2, End Loop=3. Others not used.
my @transition_code=("Transition","Begin Loop","End Loop"); # these are all we expect in delays file
#  transition code is 0=transition, 1=begin loop, 2=end loop
my $pc =0; # program counter
my $time_ms;
my $debug=0;
my $count=0;
my $freq_MHz = $clock_freq_MHz;
my $numloops = 0;
my $endloop_pc=0;
my( $odb_scan_pc, $eqppath, $eqpname, $odb_endloop_pc, $status);

my @savelines; # store the lines for output file ($ppgload) as need to fill the num instruction lines last
my $line; # temporary storage for line using sprintf

print "\nconvert_delays.pl starting\n";

if ($exp eq ""){ die "MIDAS_EXPT_NAME environment variable not defined";}
our $path;  # defined in params.pm as /home/$expt/online/ppg/
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/params.pm"; # params.pm  (for $path)
require "$path/perl/perlmidas.pl";

MIDAS_env(); 

$num++;
print "Number of input parameters supplied: $num\n";

if ($num < 3)
{
     MIDAS_sendmsg("convert_delays","Not enough parameters supplied");
    die "Must supply at least 3 parameters: Include dir, Input file, Output file, [ nominal freq]\n";
}
unless ($nominal_freq) { $nominal_freq = 100 }; # 100MHz frequency; do not adjust times for freq difference
my $string="";
my $default="(Default)";
print "Input Parameters:  \n";
print "      1. include path      $inc_dir\n";
print "      2. input file        $filename\n";
print "      3. output file       $ppgload\n";
if ($num < 4) { $string=$default;}
print "      4. nominal_freq      $nominal_freq $string\n";
if ($nominal_freq != 100)
{ 
    $multiply = $clock_freq_MHz/ $nominal_freq;
    print "PPG delay counts will be multiplied by $multiply compared with the input file\n";
    print "PPG will appear to be clocked at $nominal_freq MHz (actual clock frequence is $clock_freq_MHz)  \n";
}
else
{$multiply = 1;}

my $time_slice_ms = 1/$clock_freq_MHz;
$time_slice_ms = $time_slice_ms / 1000;
my $time_slice_ns = $time_slice_ms * 1000000;
print "time slice = $time_slice_ms  ms or $time_slice_ns ns\n";

my $err;
open (IN,"$filename") or ($err= $!);
if ($err)
{
    MIDAS_sendmsg("convert_delays","Cannot open input file \"$filename\";$err");
    die "FAILURE cannot open input file \"$filename\";$err\n";
}
push @savelines, "#   File written by convert_delays.pl   \n";
push @savelines, "#Ins 0=halt 1=cont 2=loop 3=endloop   \n"; 
push @savelines, "#Note: PC is decimal; bitpats,delay,ins/data are hex  \n";
push @savelines, "#PC set bitpat clr bitpat      delay  ins/data\n";
push @savelines,  "Num Instruction Lines = XXX           # IDDDDD\n"; # later substitute XXX with pc
push @savelines,  "000 0x00000000 0xffffffff  0x00000005 0x100000 \n"; # first instruction doesn't work properly so add a dummy

my $linenum=0; # line number of input file
my ($offset,$t0_offset);
$pc=1;
my $tot_bitpat=0; # to find highest bit
my $current_bitpat=$offset=0;
my $begin_loop_flag=0;
my $end_loop_flag=0;
my @loop_start;
my $index;
$lc=$ln=0;
while (<IN>)
{
    $linenum++;

    if ( /^\d/ )
    {
	# print "found a line beginning with a digit... $_ ";
	print "\nworking on line $_ ";
	@fields=split;
        $index = $fields[0] + 0; # delays instruction index 
        $delay_ms = $fields[1] + 0; # ms
        $setpat  = hex($fields[2]) + 0;
	$tr_code = $fields[3] + 0;
        $t0_offset  = $fields[4] + 0;

	if  ($tr_code > 2 )  # only delay , begin and end loop transitions expected in delay file
	{   # tr_code: 0=transition, 1=beginloop,  2=endloop
	    MIDAS_sendmsg("convert_delays","unsupported transition code :  $fields[5]");
	    die "FAILURE unsupported transition code :  $fields[5]\n";
	}


	print "working on line $linenum: delay=$delay_ms ms  set pattern = $setpat  transition code = $tr_code T0 offset = $t0_offset  ";



        if($tr_code ==1)
	{ 
            $ln =$fields[5];
	    $lc =$fields[6];
            print "-> begin loop $ln ; count = $lc\n";
	}
	elsif  ($tr_code==2)
	{ 
	    $ln =$fields[5];  
            print "-> end loop $ln \n";
	}
        else
        {
  	    print "-> transition\n";
	    #print "fields[5] is $fields[5]\n";
            if($fields[5] eq "LAST")
	    {
		print "Found LAST instruction\n";
		if ($end_loop_flag)
		{
		    print "Detected LAST instruction delay refers to reserved block. Skipping this instruction\n";
		    last;
		}
	    }
	}     
	$end_loop_flag=0; # clear flag
    
        # as long as transition code < 3 (expected), add 1 to get instruction for new ppg
        $ins = $tr_code + 1;
        # ppgnew instruction: 0=halt 1=transition, 2=beginloop,  3=endloop

	print "Assembling instruction at pc = $pc :\n";
	$delay_ms+=0;

# NOTE: After an END LOOP instruction, the t0-offset time will suffer a discontinuity due to the reserved time for
#       multiple loops. Therefore it cannot be used to check the delay time around an end of loop.
#       Not trying to combine any instructions at this stage except at begin-of-run. Transitions with zero delay often occur
#       at the end loop (zero delay at the end of the reserved time).  These will be set to minimal delay.

        if ($tr_code ==1) # begin loop
	{
            print "Working on begin loop \n";
	   

	   
	  #  if (($offset != $t0_offset) && ($index > 0))  # very first instruction may be begin_scan
	  #  {
	#	MIDAS_sendmsg("convert_delays","expect no change in t0_offset ($t0_offset) for begin loop instruction at line  $linenum");
	#	die "convert_delays: expect no change in  t0_offset ($t0_offset) for begin loop  at line  $linenum\n";
	#    }
	    if ($begin_loop_flag)
	    {
		MIDAS_sendmsg("convert_delays","illegal consecutive begin loop instructions at line  $linenum");
		die "convert_delays: illegal consecutive begin loop instructions  at line  $linenum\n";
	    }

	    $begin_loop_flag=1;
            print "Set begin_loop_flag\n";

	    if ($delay_ms != 0)
	    {
		# add a delay transition prior to begin loop
                print "Delay for begin loop instruction is non-zero. Adding a transition with delay $delay_ms prior to begin loop\n";
		$tmp = 1 << 20 ; # transition
	    }
	    else
	    {
		if ($setpat != $current_bitpat)
		{
		    MIDAS_sendmsg("convert_delays","expect current bitpat ($current_bitpat) not to change for begin loop instruction at line  $linenum");
		    die "convert_delays: expect current bitpat ($current_bitpat) not to change for begin loop  at line  $linenum\n";
		}
		next;
	    }
	}

	
        if  ($tr_code==0) # delay
	{
#	    if ( ($delay_ms == 0) && ($setpat == $current_bitpat) )
#	    { 
# 		if ($offset != $t0_offset)
# 		{
# 		    MIDAS_sendmsg("convert_delays","expect t0 offset not to change when delay is 0 at line  $linenum");
# 		    die "convert_delays: expect t0 offset not to change when delay is 0 at line $linenum\n";
# 		}
#                 print "no delay,  no change to current bitpat, just a spacer, skipping \n";
# 		next;  # no delay,  no change to current bitpat, just a spacer
#	    }

	    if ($begin_loop_flag){ $ins = 2; } # attach begin of loop to this instruction		
	    
	    $tmp = $ins << 20 ;

	    if ($begin_loop_flag)
	    {	    
		if ($lc > $max_lc) 
		{
		    MIDAS_sendmsg("convert_delays","loopcount for loop $ln will be set to $max_lc (maximum 20 bits)");
		    print "loopcount for loop $ln will be set to $max_lc (maximum 20 bits)\n";
		    $lc =  $max_lc;
		}
		else { print "loopcount for loop $ln is $lc\n"; }

		$tmp = $tmp | $lc;
                push @loop_start, $pc;
		$begin_loop_flag=0; # clear flag
                print "Cleared begin_loop_flag\n";
	    }
	    
	}
	elsif ($tr_code==2) # end loop
	{
 	    print "working on end loop instruction \n";
 	    
 	    $tmp = pop @loop_start;
 	    print "loop starts at pc=$tmp\n";
 	    $tmp = $tmp | ( $ins << 20) ;
            $end_loop_flag = 1; # set a flag in case end loop is last valid instruction	    	    
	}


	#convert delay (ms) to clock cycles	
 	$delay_cs =  $delay_ms /  $time_slice_ms;
         print "delay_ms = $delay_ms;  delay_cs = $delay_cs \n";
        # round up to nearest cs
         $delay_cs = roundup($delay_cs);
 	$delay_cs = $delay_cs - 3.0; # subtract the 3 clock cycles that each instruction takes
 	if($delay_cs < 0) { $delay_cs=0; } # minimal delay is 3 cs.
  
         printf "After rounding up and subtracting 3cs,   delay_cs = %d\n", int($delay_cs);

 	printf "pc=%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $setpat, (~$setpat & 0xFFFFFFFF),int($delay_cs),$tmp;
        $line= sprintf ("%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $setpat, (~$setpat & 0xFFFFFFFF),int($delay_cs),$tmp);
	push @savelines,$line;

	$pc++;  # next instruction
	$current_bitpat=$setpat; # remember current bit pattern
        $tot_bitpat = $tot_bitpat | $setpat;
	$offset=$t0_offset; # remember current offset 
    }

    else
    {
	print "skipping line $_";
    }

} # while

# add the halt instruction, leaving the outputs unchanged
$line= sprintf ("%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $setpat, (~$setpat & 0xFFFFFFFF),0,0);
$pc++;
push @savelines,$line;


close IN;
print "closed input file $filename\n";

print "array:\n";
print "@savelines";
my $len = $#savelines;
print "len=$len \n";

my ($hibit,$bit_pattern);
($hibit,$bit_pattern)=polarity($tot_bitpat,0); # find highest bit set ($hibit)

my $hibit_path="/equipment/titan_acq/ppg cycle/image/max active channel"; # key may not be present
my $result = MIDAS_varget($hibit_path); # see if key exists
#print "result is $result\n";
unless ($result =~/not found/i)
{
    MIDAS_varset($hibit_path, $hibit); # write value
    print ("Wrote highest active input $hibit to $hibit_path\n");
}

# open output file
print "\n Opening output file $ppgload\n";
open (OUT,">$ppgload") or ($err= $!);
if ($err)
{
    print "$err\n";
    MIDAS_sendmsg("convert_delays","Cannot open output file \"$ppgload\";$err");
    die "FAILURE cannot open output file \"$ppgload\"; $err\n";
}

print "\n Successfully opened output file $ppgload \n";
my $i=0; # counter
my $j;

while ($i <= $len)
{
    $i++;
    $_=shift @savelines;  # retrieve a line
    print "working on line $i:  $_";

    if ( /^Num Instruction Lines/ )
    {
	$tmp = sprintf ("%3.3d",$pc);
	s/XXX/$tmp/; 
	chomp $_;
	$_ = $_ . "  set bitpattern: bits $hibit to 0 " . "\n";
        print "Writing number of instruction lines as $pc\n";
    }
    if (/^\d/) # this is an instruction line
    {
	@fields=split;
        $setpat  = hex($fields[1]) + 0;
	($j,$bit_pattern)=polarity($setpat,$hibit); # find highest bit set ($hibit)
        chomp $_;
	$_ = $_ . " # " . $bit_pattern . "\n";
        print "Now line is $_\n";
	
    }
    
    print OUT "$_";
    print "wrote line $_";
}

close OUT;


print "Input file was $filename\n";
print "Output file is $ppgload\n";
print "Success from convert_delays.pl\n";


sub roundup($)
{   
    # rounds up to nearest int
    # .... except we often get e.g. 7.4999999999 instead of 7.5
    #      so detects this and rounds 7.49999999 to 8 instead of 7
    my $i=shift;
    my ($j,$k);

    $j=int($i);
    $k=$i-$j;
    if($k>=0.499)
    {
	$k=0.5;
	$i=$j+$k;
    };
    $i+=0.5;
    $j=int($i);

    return $j;
}

sub polarity($$)
{
    my $bitpat =  shift;  # bit pattern to write out
    my $hb = shift;       # max number of bits to write; defaults to max bit in bit pattern if not supplied
    
#   if $bitpat is not supplied, $bitpat=0;
#   if $hb not supplied, uses the highest bit it finds

    $bitpat = $bitpat & 0xFFFFFFFF; # 32 bits max

    
    # printf "polarity: bitpat= 0x%x \n",$bitpat;
    
    my $temp = $bitpat << 1 ;
    # printf "bitpat shifted left by 1 = 0x%x \n",$temp;
    
    
    
# find highest bit set
    
    my ($i,$j,$s);
    my $mask=0;
    
    for ($i=0; $i < 32; $i++)
    {
	$mask = $mask | 1<<$i;
	$j=$bitpat >> $i;    
	if ($j == 0) { last; }
	
    }
    print ("polarity: highest bit set is i=$i\n");
    
    if ($hb > 0) # hightest bit for display is supplied 
    {
	if ($hb >= $i)
	{
	    print "polarity: using supplied highest bit for display ($hb) rather than calculated ($i)\n";
	    $i=$hb;
	}
	else {  print "polarity: supplied highest bit for display ($hb) is too small; using calculated ($i)\n"; }
    } 
	
    #  printf ("mask is 0x%x \n",$mask);
    
    # $s = join(' ',split('',sprintf ("%32.32b",$bitpat))); 
    # print "s32=$s\n";                     
    # $s = join(' ',split('',sprintf ("%24.24b",$bitpat))); 
    # print "s24=$s\n";                     
    $s = join(' ',split('',sprintf ("%*.*b",$i,$i,$bitpat))); 
   
    print "s$i=$s\n"; 
    
    return ($i,$s);
}







