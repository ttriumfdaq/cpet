#!/usr/bin/perl -w 
# above is magic first line to invoke perl
# or for debug
use warnings;
use strict;
###  !/usr/bin/perl -d  cbyte.pl
#
#   Note: for testing, use cbyte.dat with does not require input params 1,2,3
#
# Translates bytecode.dat (for old PPG) to instructions for new PPG
# Input parameters
#       param 1 include path  ....online/ppg/perl
#       param 2 path and name of input file (bytecode.dat)
#       param 3 path and name of output file (ppgload.dat -> $ppgload)
#
#       Only the above three parameters are required. The following params are used for special cases or testing.
#       param 4 write address of scan loop counter to settings/output area of odb  (Y=1 N=0)
#       param 5 write address of end loop of outermost loop to settings/output area of odb ((Y=1 N=0)
#       param 6 nominal PPG frequency - (e.g. if nominal clock is 10MHz and actual clock of NEW PPG is 100MHz,
#           output times will be multiplied by 10 to give the correct result i.e. as if NEW PPG runs at 10MHz).
#       param 7 shift_outputs  - shift left all the outputs of newppg (for testing)


my ($inc_dir, $filename, $ppgload, $write_scanloop_pc, $write_endloop_pc, $nominal_freq, $shift_outputs) = @ARGV;
my $num = $#ARGV;
my $clock_freq_MHz = 100; 
my $multiply;
# these are input parameters
#my $filename="/home/$exp/online/ppg/ppgload/bytecode.dat"; # old PPG instruction file
#my $ppgload="/home/$exp/online/ppg/ppgload/ppgload.dat";
my $name = "convert_bytecode";
my $exp = $ENV{MIDAS_EXPT_NAME}; # experiment name 
my $minimal_delay=3; # 3 clock cycles (new ppg)
my $halt = 0; # halt instruction (new ppg)
my $vme_addr = 0x100000;
my $status_reg = $vme_addr;
my $pc_reg = $vme_addr | 0x8;
my $lo_reg = $vme_addr | 0xC;
my $med_reg = $vme_addr | 0x10;
my $hi_reg = $vme_addr | 0x14;
my $top_reg = $vme_addr | 0x18;
my ($cmd,$comment);
my ($ins,$lc,$delay,$bitpat, $clrpat);
my $tmp;
my $numlines;
my @fields;
# instruction list for new and old ppg
my @new_instructions= ("Halt","Continue","Begin Loop","End Loop","Call Subr","Return","Branch");
my @old_instructions=("Continue","Stop","Begin Loop","End Loop","JSR","RSR","Branch","Long Delay");
my @convert_instructions=qw(1 0 2 3 4 5 6); # no "Long Delay"
my $old_ins;
my $len=$#new_instructions;
my $lenc=$#convert_instructions;
my $pc =0; # program counter
my $time_ms;
my $debug=0;
my $count=0;
my $freq_MHz = $clock_freq_MHz;
my $numloops = 0;
my $endloop_pc=0;
my( $odb_scan_pc, $eqppath, $eqpname, $odb_endloop_pc, $status);
print "\nconvert_bytecode.pl starting\n";

if ($exp eq ""){ die "MIDAS_EXPT_NAME environment variable not defined";}


our $path;  # defined in params.pm as /home/$expt/online/ppg/
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/params.pm"; # params.pm  (for $path)
require "$path/perl/perlmidas.pl";

MIDAS_env(); 

$num++;
print "Number of input parameters supplied: $num\n";

if ($num < 3)
{
     MIDAS_sendmsg("convert_bytecode","Not enough parameters supplied");
    die "Must supply at least 3 parameters: Include dir, Input file, Output file, [ write scan loopcount addr, nominal freq; shift inputs ]\n";
}
unless ($nominal_freq) { $nominal_freq = 99 }; # 100MHz frequency; do not adjust times for freq difference
unless ($shift_outputs) {$shift_outputs = 0 }; # do not shift outputs
unless ($write_scanloop_pc ) {$write_scanloop_pc = 0 }; # do not write addr to odb
unless ($write_endloop_pc ) {$write_endloop_pc = 0 }; # do not write addr to odb
#($inc_dir, $filename, $ppgload, $write_scanloop_pc, $write_endloop_pc, $nominal_freq, $shift_outputs) = @ARGV;
my $string="";
my $default="(Default)";

print "Input Parameters:  \n";
print "      1. include path      $inc_dir\n";
print "      2. input file        $filename\n";
print "      3. output file       $ppgload\n";
if ($num < 4) { $string=$default;}
print "      4. write_scanloop_pc $write_scanloop_pc $string\n";
if ($num < 5) { $string=$default;}
print "      5. write_endloop_pc  $write_endloop_pc $string\n";
if ($num < 6) { $string=$default;}
print "      6. nominal_freq      $nominal_freq $string\n";
if ($num < 7) { $string=$default;}
print "      7. shift_outputs     $shift_outputs $string\n";


if($write_scanloop_pc || $write_endloop_pc)
{ # assign required ODB paths    
    if($exp eq "pol")
    { 
       $eqpname = $exp."_acq";
       $eqppath    = "Equipment/".$eqpname."/settings/output";
    }
    else
    {
	$eqpname = "titan_acq";
        $eqppath    = "Equipment/".$eqpname."/settings/ppg/output";
    }
    $odb_scan_pc = $eqppath."/scanloop pc";
    $odb_endloop_pc = $eqppath."/loops/end_outer_loop_pc";
    print "paths are \"$odb_scan_pc\" and \"$odb_endloop_pc\" \n";
}


if($write_scanloop_pc ) { MIDAS_varset($odb_scan_pc, 0);} # clear value to start
if($write_endloop_pc ) { MIDAS_varset($odb_endloop_pc, 0);} # clear value to start

if($shift_outputs > 0)
{ 
    my ($i,$j);
    $i=$shift_outputs+1;
    $j=$shift_outputs+5;
    print "PPG Outputs will all be shifted left by $shift_outputs compared with the input file\n"; 
    print "e.g. PPG outputs 1-5 will be shifted to outputs $i - $j \n";
}

if ($nominal_freq != 100)
{ 
    $multiply = $clock_freq_MHz/ $nominal_freq;
    print "PPG delay counts will be multiplied by $multiply compared with the input file\n";
    print "PPG will appear to be clocked at $nominal_freq MHz (actual clock frequence is $clock_freq_MHz)  \n";
}
else
{$multiply = 1;}

if($debug){ print "@new_instructions; len=$len\n";}


open (IN,"$filename") or ($tmp= $!);
if ($tmp)
{
    #send_message($name,$MINFO,"FAILURE cannot open file \"$file\"; $tmp" ) ;
    MIDAS_sendmsg("convert_bytecode","Cannot open input file \"$filename\";$tmp");
    die "FAILURE cannot open input file \"$filename\";$tmp\n";
}
open (LDF,">$ppgload") or ($tmp= $!);
if ($tmp)
{
    #send_message($name,$MINFO,"FAILURE cannot open file \"$file\"; $tmp" ) ;
    MIDAS_sendmsg("convert_bytecode","Cannot open input file \"$ppgload\";$tmp");
    die "FAILURE cannot open input file \"$ppgload\";$tmp\n";
}
printf LDF "#Ins 0=halt 1=cont 2=loop 3=endloop   \n"; 
printf LDF "#Note: PC is decimal; bitpats,delay,ins/data are hex  \n";
printf LDF "#PC set bitpat clr bitpat       delay  ins/data\n";

my $linenum=0;
while (<IN>)
{
    $linenum++;
    if (/Instruction Lines/)
    {
	s/Instruction Lines//;
	$numlines = $_ + 0;
        print "number of program lines in bytecode.dat : $numlines\n"; # same number of lines in output file
        $numlines +=2; # add two for two the halt instructions   
        printf LDF ("Num Instruction Lines = %4.0d         #  IDDDDD\n",$numlines);
	}
    if ($linenum >= 8)
    {
	$count++;
	print "\nWorking on line $linenum (instructions line $count): $_ ";
	@fields=split;
	$old_ins = hex($fields[0]);  # old PPG instruction
	$lc  = hex($fields[1]);
	$delay  = hex($fields[2]);
        $bitpat  = hex($fields[3]);
	
	if($shift_outputs > 0) 
	{
	    printf "Bitpat 0x%x ", $bitpat;
	    $bitpat = $bitpat << $shift_outputs;
	    printf "shifted by %d ->  0x%x \n",$shift_outputs,$bitpat;
	}
	
	if ($multiply > 1)
	{
	    printf "Delay 0x%x (%d) ", $delay,$delay;
	    $delay*=$multiply;
	    printf "multiplied by %d ->  0x%x (%d) \n",$multiply,$delay,$delay;
	}
	
        if ($old_ins > $lenc) { die "Instruction (\"$ins\")at line $linenum in $filename is not supported by new PPG \n"; } 
        $ins = $convert_instructions[$old_ins]; # convert old instruction to new.
	if($debug)
	{
	    if($ins != $old_ins)
	    { print "Converted old instruction $old_ins to $ins\n"; }
	}
	
	
	if ($ins == 2 ) # begin loop
	{
	    $numloops++;
	    # Check loop count (maximum 20 bits)
	    $lc++; # convert from old ppg loop count (where 0 meant 1 loop)
	    if ($lc >= 0x100000)
	    {
	       	print "WARNING : data field for loop count is 20 bits maximum. Loop count will be set to $lc\n";
		MIDAS_sendmsg("convert_bytecode","loop count ($lc) for loop $numloops is too large. Loop count will be set to max");
		print "WARNING : data field for loop count is 20 bits maximum. Loop count will be set to $lc\n";
		$lc = 0xFFFFF;
	    }
	}
	elsif ($ins == 4 || $ins == 6)
	{ # check address (max 20 bits)
	    if ($lc >= 0x100000)
	    { 
		MIDAS_sendmsg("convert_bytecode","Address ($lc) at line $linenum for JSR/Branch  too large. Must be less than 0x100000");
		die "Illegal address $lc at line $linenum. Address must be 20bits or less";
	    }
	}
	
	print "fields: @fields\n"; # fields[0..3] code; loop count; clock count; bitpat 	
	printf ("ins=%d lc=0x%x delay=0x%x bitpat=0x%x \n",$ins,$lc,$delay,$bitpat);
	print "Assembling instruction at pc = $pc\n";
	
	
	$time_ms =  ($delay + 3)/($freq_MHz * 1000); # add the 3 clock cycles that each instruction takes
	$tmp = $ins << 20 ;
	$tmp = $tmp | $lc;
	
	if  ($ins == 2)  # begin loop
	{
	    if( $numloops == 1 &&  $write_scanloop_pc)
	    {
		# record the pc of the scan loop instruction
		printf("($numloops, $write_scanloop_pc)PC for begin_scan instruction is $pc\n");
              
		MIDAS_varset($odb_scan_pc, $pc);
	    }
	} # end of begin_loop

	if  ($ins == 3)  # end loop
	{
	    if( $write_endloop_pc)
	    {
		# record the pc of the end-loop instruction
                $endloop_pc = $pc;
	    }
	} # end of end loop


        printf "pc=%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$delay,$tmp;
	printf LDF "%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$delay,$tmp;
	$pc++;  # next instruction
    }
}
# end of input file

if($write_endloop_pc ) 
{
    print " PC for last endloop instruction is $pc\n";
    MIDAS_varset($odb_endloop_pc, $endloop_pc); 
}
# Add HALT at end of program; keep output pattern
$tmp=$ins=0;
$count++; # add one for HALT
printf ("\nAdding HALT instruction, keeping output pattern\n");
printf ("ins=%d lc=0x%x delay=0x%x bitpat=0x%x \n",$ins,$lc,$delay,$bitpat);
printf "pc=%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$minimal_delay,$tmp;
printf LDF "%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$minimal_delay,$tmp;
$pc++;

# Add a halt at pc=500 to set outputs to required state in case pgm is stopped mid-cycle.
# Rather than remembering last instruction of this program, repeat the HALT at PC=500
# This will set output bits to last output pattern (if stopped mid-cycle)
if ($pc > 499)
{
   MIDAS_sendmsg("convert_bytecode","Cannot add extra HALT at pc=500 since program is too long. Fix convert_bytecode.pl");
   print "Cannot add extra HALT at pc=500  since program is too long. Fix convert_bytecode.pl\n"; 
   close LDF;
   close IN;
   system ("rm $ppgload");
   die "\nERROR :  Output file is deleted\n";
}

$pc=500; 
print " PC for repeated Halt instruction is $pc\n";
printf ("ins=%d lc=0x%x delay=0x%x bitpat=0x%x \n",$ins,$lc,$delay,$bitpat);
printf "pc=%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$minimal_delay,$tmp;
printf LDF "%3.3d 0x%8.8x 0x%8.8x  0x%8.8x 0x%6.6x \n",$pc, $bitpat, (~$bitpat & 0xFFFFFFFF),$minimal_delay,$tmp;
$count++;
if  ($numlines != $count) 
{ print "Error - number of instructions lines in input file $filename (i.e. $numlines) does not agree with count ($count)\n";}
close LDF;
close IN;
print "\nDone. Output file is $ppgload\n";
