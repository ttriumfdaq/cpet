sub readloopinfo($$)
{
    my $inc_dir = shift;
    my $loopfile = shift;
    our %loopinfo; # store the loop information in a hash of hashes
    my ($i,$j,$nloops,$my_name,$w);
    my (@temp,@items);

    unless (-e $loopfile)
    {
	print " readloopinfo: no such file as $loopfile\n";
        return -1; # error
    }
    open (IN,$loopfile) or die $!;
# get the information out of loopfile
    
    print "readloopinfo: opened loop information file $loopfile:\n";    
    $i=0;$nloops=0;
    while (<IN>)
    {
	chomp $_;
	unless(/\d/)
	{
	    print "blank line\n";
	    next;
	}
	
	if(/^\#/)
	{
	    print "working on line: $_\n";
	    if ( /(\d+)/ ) 
	    { 
	#	print "match is '$1'\n" if defined $1;   
		$nloops = $1  if defined $1 ; 
	    }
	    next;
	}
	else
	{
	    print "working on line: $_\n";
	    
	    @temp = split;
	    $my_name = $temp[0];
	    $j=1;
	    while ($j <= $#temp)
	    {
		#print "j=$j   $temp[$j]\n";
		if ($temp[$j] =~ /(\d+)/) 
		{
		 #   print "match is '$1'\n" if defined $1;
		    $items[$j-1]=$1;
		}
		$j++;
	    }
	    
	    $loopinfo { $temp[0] } = { 
		start => $items[0],
		end =>  $items[1],
		count =>  $items[2],
		index =>  $items[3]
		};
	    $i++;
	}
    }
    if ($nloops != $i )
    { 
	print "readloopinfo:  inconsistent number of loops found ($i) with nloops value ($nloops) in file $loopfile";
	return -1;
    }
    return $nloops;
}
# IMPORTANT
# this 1 is needed at the EOF so require returns a true value
1;
