#!/usr/bin/perl -w
use warnings;
use strict;
use POSIX; # needed for isalpha

my $info_dir ="/titan/data4/";
my $expt = $ENV{"MIDAS_EXPT_NAME"};

my ($run_number) = @ARGV;
my $len =$#ARGV;
if ($len < 0)
{ die "Run this routine with arguments: run_number \n";}

print " ...  parameters: @ARGV  \n";
my $rn;
$rn = sprintf("%5.5d",$run_number);
my $filename = $info_dir.$expt."/run".$rn.".odb";
print "filename = $filename\n";

my $tmp;
my @fields;

open (IN,$filename) or ($tmp= $!);
if($tmp) {die "Cannot open file \"$filename\"; $tmp\n"; }
print "Opened file  \"$filename\" \n";
my $blocks="not found";
my $mode;

while (<IN>)
{
      #[/Equipment/Titan_acq]
      #ppg cycle = LINK : [39] /Equipment/titan_acq/ppg_cycle_mode_1f


    chomp $_;
    if( /ppg cycle = LINK :/i)
    {
	
	(@fields)=split /\//; # split by slashes
        shift @fields; # lose the first element
        $blocks = join "/", @fields;
        print "ppg_blocks path: $blocks\n";
        $_ = pop @fields; # gives e.g. ppg_cycle_mode_1f
        @fields = split /_/; # split by underscores
        $mode = pop @fields; # gives e.g. 1f
        print "ppg mode : $mode\n";
	last;
    }
}
close IN;
if ( $blocks =~ /not found/) { die "Could not find LINK for ppg cycle in file $filename\n"; }

# reopen file
open (IN,$filename) or ($tmp= $!);
if($tmp) {die "Cannot reopen file \"$filename\"; $tmp\n"; }

my $outfile = "run".$run_number."_".$mode.".odb";
print "Output file name is $outfile\n";
open (OUT, ">>$outfile") or ($tmp= $!);
if($tmp) {die "Cannot open output file \"$outfile\"; $tmp\n"; }

my $num = 0;
my $flag = 0;
while (<IN>)
{
    #looking for e.g. [/Equipment/Titan_acq/ppg_cycle_mode_1f/.....]
    chomp $_;
    if ($flag) 
    {
	if ( /image/i )
	{
	    $flag=0; # end of lines to be written to outfile
	    last;
	}
	   
	# write line into outfile
	print OUT $_ . "\n";
	$num++;
    }
    else
    {
	if( /\[\/$blocks/i) 
	{ # set a flag to write lines into outfile
	    $flag = 1;
	    print OUT $_ . "\n";
	    $num++;
	}
    }
}
close IN;
close OUT;
print "$num lines written to  $outfile\n";
exit;
	    
