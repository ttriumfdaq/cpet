#!/usr/bin/perl 
# above is magic first line to invoke perl
# or for debug
###  !/usr/bin/perl -d
use warnings;
use strict;
#
#   Normally invoked from user button
# 
# invoke this script with cmd e.g.
#                  include_path               experiment   mode      tune name   
# tune.pl  /home/ebit/online/ppg/perl          ebit         1e       defaults
#
#   tune file  is loaded when button pressed
#       (providing it exists)
#
#   the path for tune files is /home1/<expt>/online/tunes/<mode>/
#             the tune file name is  <defaults>.odb
# based on change_mode.pl Rev 1.31 BNMR
# $Log: change_mode.pl,v $
#
######### G L O B A L S ##################
our  @ARRAY;
our $FALSE=0;
our $FAILURE=0;
our $TRUE=1;
our $SUCCESS=1;
our $ODB_SUCCESS=0;   # status = 0 is success for odb
our $DEBUG=$FALSE;    # set to 1 for debug, 0 for no debug
our $EXPERIMENT=" ";
our $ANSWER=" ";      # reply from odb_cmd
our $COMMAND=" ";     # copy of command sent be odb_cmd (for error handling)
our $STATE_STOPPED=1; # Run state is stopped
our $STATE_RUNNING=3; # Run state is running
# for odb  msg cmd:
our $MERROR=1; # error
our $MINFO=2;  # info
our $MTALK=32; # talk
# constants for print_3
our $DIE = $TRUE;  # die after print_3
our $CONT = $FALSE; # do not die after print_3 (continue)
#e.g.    print_3($name,  "ERROR: no path supplied",$MERROR,$DIE);
#    or   print_3($name,  "INFO: run number has not changed",$MINFO,$CONT);
#######################################################################
#  parameters needed by init2_check.pl (required code common to perlscripts)
#
# input parameters :
our ($inc_dir, $expt, $mode, $tune_name ) = @ARGV;
our $len = $#ARGV; # array length
our $name = "tune" ; # same as filename
our $other_name = "get_next_run_number";
our $outfile = "tune.txt"; # path will be added by file open
our $parameter_msg = "include path , experiment , expt mode  tune file";
our $nparam = 4;  # no. of input parameters
our $beamline = $expt; # beamline is not supplied. Same as $expt for bnm/qr, pol mpet
############################################################################
# local variables:
my $tunes_path = "/home/$expt/online/tunes/";
my $my_tune_filename;
my ($path, $key, $status);
my $input_path = "/Equipment/Titan_acq/";
my $old_ppg;
my ($ppg_path,$string);
my ($process,$run_state,$transition);
my ($msg,$loop,$flag);
my $changing_mode=$FALSE;
my $experiment_name;
my $fixed_link = "/alias/ppg_blocks&"; # name of fixed BLOCKS link
my $ppg_cycle_link = "$input_path/ppg cycle";
my $ppg_cycle_mode = "$input_path/ppg_cycle_mode_";
#--------------------------------------------------------
$|=1; # flush output buffers



# Inc_dir needed because when script is invoked by browser it can't find the
# code for require
unless ($inc_dir) { die "$name: No include directory path has been supplied\n";}
$inc_dir =~ s/\/$//;  # remove any trailing slash
require "$inc_dir/odb_access.pl"; 
require "$inc_dir/do_link.pl";

#bypass check on get_run_number.pl for titan expt  - doesn't use MUSR run numbering
if ($expt =~ /ebit/i  ){ $other_name=""; }
if ($expt =~ /cpet/i  ){ $other_name=""; }
# Init2_check.pl checks:
#   one copy of this script running
#   no copies of $other_name running (may interfere with this script)
#   no. of input parameters is correct
#   opens output file:
#
require "$inc_dir/init2_check.pl";

# Output will be sent to file $outfile 
# because this is for use with the browser and STDOUT and STDERR get set to null


print FOUT  "$name starting with parameters:  \n";
print FOUT  "Experiment = $expt, mode = $mode  tune name $tune_name \n";

print FOUT ("fixed_link = $fixed_link\n");
#print FOUT ("fixed_link1 = $fixed_link1\n");

unless ($mode)
{
    print FOUT "FAILURE: present mode  not supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE:  present mode not supplied " ) ;
	unless ($status) { print FOUT "$name: Failure return after msg \n"; } 
        die  "FAILURE:  present mode  not supplied \n";
}

unless ($expt =~ /ebit/i || $expt =~ /cpet/i  )
{
    print FOUT "FAILURE: experiment $expt not supported\n";
    odb_cmd ( "msg","$MERROR","","$name", "FAILURE:  experiment $expt not supported " ) ;
    unless ($status) { print FOUT "$name: Failure return after msg \n"; } 
    die  "FAILURE:  experiment $expt not supported\n";
}

unless ($tune_name)
{
    print FOUT "FAILURE: tune name  not supplied\n";
        odb_cmd ( "msg","$MERROR","","$name", "FAILURE: tune name not supplied " ) ;
	unless ($status) { print FOUT "$name: Failure return after msg \n"; } 
        die  "FAILURE: tune name  not supplied \n";
}

$mode =~ tr/A-Z/a-z/; # lower case



# make a few sanity checks:
# titan experiments only support Mode 1
if ($mode =~ /^1/) # Type 1 experiment 
{
    if ( $expt =~  /ebit/i   ) 
    {
	unless ($mode=~/[abcde]$/  )  # modes 1a 1b 1c 1d 1e
	{
	    print_3 ($name, " Unsupported  mode  for $expt ($mode)",$MERROR, 1);
	}
    }
    elsif ( $expt =~  /cpet/i   ) 
    {
	unless ($mode=~/[abcdef]$/  )  # modes 1a 1b 1c 1d 1e 1f
	{
	    print_3 ($name, " Unsupported  mode  for $expt ($mode)",$MERROR, 1);
	}
    }
}
else
{
 # no other types supported
 print_3 ($name, " Unsupported  mode  for $expt ($mode)",$MERROR, 1);
}

#
# check whether run is in progress
($run_state,$transition) = get_run_state();
if($DEBUG) { print FOUT "After get_run_state, run_state=$run_state, trans=$transition\n"; } 

if($transition ) 
{ 
    print FOUT "Run is in transition. Try again later \n";
    print FOUT "Or set /Runinfo/Transition in progress to 0\n";
    ($status)=odb_cmd ( "msg","$MINFO","","$name", "Run is in transition. Try again later or set /Runinfo/Transition in progress to 0 " ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "Run is in transition. Try again later or set /Runinfo/Transition in progress to 0 " ;
}

if ($run_state != $STATE_STOPPED) 
{   # Run is not stopped; 
    print FOUT "Run is in progress. Cannot load a tune \n";
    ($status)=odb_cmd ( "msg","$MERROR","","$name", "FAILURE: Run in progress. Cannot load a tune" ) ;
    unless ($status) { print FOUT "$name: Failure status after odb_cmd (msg)\n"; } 
    die "Run is in  progress. Cannot load a tune " ;
}

# build path for tunes file
$my_tune_filename = $tunes_path.$mode."/".$tune_name.".odb";


# load the file of default values for this mode
print FOUT "\n---- loading default values for mode $mode from file \"$my_tune_filename\"-------\n";
print "\n----loading default values for mode $mode from file \"$my_tune_filename\"-------\n";
$status =  load_mode_defaults ($my_tune_filename);
unless($status) 
{  
    print_2($name, "Error return from load_mode_defaults",$CONT); 
    print_3($name, "WARNING: could not load mode defaults from file $my_tune_filename ",
	    $MINFO,$CONT); 
}
else
{  
    print_3($name, "INFO - mode default values from file $my_tune_filename have been loaded ",$MINFO,$CONT); 
}

write_time($name, 3);
exit;




























