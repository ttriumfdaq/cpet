// CPET with newppg only

tri_config -s -p
    -p flag produces gnuplot files
    ppg_structure.c  
              rm.sh 
              ppg_plot.pl inc_path  rn SCAN 1 loop lc ... 
gnuplot button
    links to /customscript/gnuplot
              ppg_plot.pl inc_path  rn SCAN 1 loop lc ...


ppg_plot.pl             (params.pm include file - defines loopnames, log file name etc.)

 -> sub my_do_plots inc_path expt mode run logfiles @loopinfo
    -> convert_to_plot inc_path 1  => converts ppgload.dat to  ppgplot.dat (clock cycles)
                                   => writes ppgloops.dat (loops start/end/lc)
                                   => write ppgplotparams.dat (input params for...
    -> loops.pl inc_path ppgplot.dat loopname lc  (if loop(s) to be expanded) converts ppgplot.dat ppgplotloop.dat
                                                   if multiple loops,   plotparams.dat.multiple_plots