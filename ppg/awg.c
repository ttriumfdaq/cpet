/* awg.c  talk to AWG driver 
 
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include "midas.h"
#include "msystem.h"
#include "tri_config.h"
#include "tri_config_prototypes.h"

#include "AWG.h"

#ifdef ALPHI_DA816
ALPHIDA816  *da816;
#endif

#ifdef ALPHI_SOFTDAC
ALPHISOFTDAC  *softdac;
#endif

void close_AWG(INT awg_unit)
{
  printf("close_AWG: starting with awg_unit=%d\n",awg_unit);
  if(strcmp(awg_params[awg_unit].type, psoftdac)==0)
    {
#ifdef  ALPHI_SOFTDAC
#ifndef DUMMY
      if (softdac) softdac_Close(softdac);
#endif // DUMMY
#else
      cm_msg(MERROR,"close_awg","SOFTDAC driver has not been built in this version");
      return;
#endif
    }
  else if(strcmp(awg_params[awg_unit].type, pda816)==0)
    {
#ifdef ALPHI_DA816
#ifndef DUMMY      
      if (da816) da816_Close(da816);
#endif // DUMMY 
#else
      printf("close_awg: DA816 driver has not been built in this version; AWG Unit %d not supported\n",
	     awg_unit);
      cm_msg(MERROR,"close_awg",
	     "DA816 driver has not been built in this version; AWG Unit %d not supported",awg_unit);
      return;
#endif
    }
  else
    {
      cm_msg(MERROR,"close_awg","Unknown AWG device (%s)!", 
	     awg_params[awg_unit].type);
      return;
    }
}


INT config_AWG(INT awg_unit)
{
  
  // Device specifics
  INT status;
  
  printf("config_AWG: starting with awg_unit=%d\n",awg_unit);


  if(strcmp(awg_params[awg_unit].type, psoftdac)==0)
    {
#ifdef  ALPHI_SOFTDAC

#ifndef DUMMY
      if (softdac_Open(&softdac) < 0)
	{
	  /*      fprintf(stderr,"Cannot open PMC-SOFTDAC device!\n"); */
	  cm_msg(MERROR,"config_awg","Cannot open PMC-SOFTDAC device!");
	  return FE_ERR_HW;
	}
      
      softdac_Reset(softdac);
      softdac_Status(softdac, 1);
      status = softdac_Setup(softdac, 0 , 3);
      softdac_ScaleSet(softdac, SOFTDAC_RANGE_PM10V, 0., 0.);

#else // DUMMY
  printf("config_AWG: DUMMY version... does not actually open softdac (AWG unit %d)\n",awg_unit);
#endif // DUMMY

#else
      cm_msg(MERROR,"config_awg","SOFTDAC driver has not been built in this version");
      return FE_ERR_HW;
#endif
    }
  else if(strcmp(awg_params[awg_unit].type, pda816)==0)
    {
#ifdef ALPHI_DA816
#ifndef DUMMY      
      if (da816_Open(&da816) < 0)
	{
	  /*      fprintf(stderr,"Cannot open PMC-DA816 device!\n"); */
	  cm_msg(MERROR,"config_awg","Cannot open PMC-DA816 device!");
	  return FE_ERR_HW;
	}
      
      da816_Reset(da816);
      da816_Status(da816,1);

      //      printf("setting internal clock with setup param 1\n");

      //da816_Setup(da816, 0, 1);  // change 3 to 1 for testing (external -> internal clock)
      da816_Setup(da816, 0 , 3); 
      //da816_ScaleSet(da816, DA816_RANGE_PM10V, 0., 0.);

    da816_Status(da816,1);
#else
  printf("config_AWG: DUMMY version... does not actually open da816 (AWG unit %d)\n",awg_unit);
#endif // DUMMY


#else
      printf("config_awg: DA816 driver has not been built in this version; AWG Unit %d not supported\n",
	     awg_unit);
      cm_msg(MERROR,"config_awg",
	     "DA816 driver has not been built in this version; AWG Unit %d not supported",awg_unit);
      return FE_ERR_HW;
#endif
    
    }
  else
    {
       cm_msg(MERROR,"config_awg","Unknown AWG device (%s)!", 
	      awg_params[awg_unit].type);
       return FE_ERR_HW;
    }
  return SUCCESS;
}

/*
INT AwgConfLoad( INT Nsample, DWORD last_address,
			 INT clock_mode ,
			 INT configuration,
			 INT buffer_num,
			 INT update_mode)
{
  printf("AwgConfLoad called with Nsample=%d last_addr=%d, clock_mode=%d config=%d\n",
	 Nsample,last_address,clock_mode,configuration);
  printf("       buf_num=%d update_mode=%d \n",buffer_num,update_mode);
  return SUCCESS;
  }
*/

INT AwgMemLoad(double Vstart, double Vend, INT step_start, INT nsteps, INT AWG_buffer_num, 
		 INT AWG_channel, INT awg_unit)
{
  printf("AwgMemLoad called with Vstart=%f Vend=%f step_start=%d nsteps=%d AWG_channel=%d AWG unit=%d\n",
	 Vstart,  Vend,  step_start,  nsteps, AWG_channel, awg_unit);
 
  if(strcmp(awg_params[awg_unit].type, psoftdac)==0) 
    {
#ifndef DUMMY

#ifdef  ALPHI_SOFTDAC
      softdac_LinLoad(softdac, Vstart, Vend, nsteps, &step_start, AWG_buffer_num, AWG_channel);
#else
      cm_msg(MERROR,"AwgMemLoad","SOFTDAC driver has not been built in this version");
      return FE_ERR_HW;
#endif

#else // DUMMY
      printf("AwgMemLoad:   DUMMY version... softdac no action (awg unit %d)\n",awg_unit);
#endif // DUMMY
    }


  else if (strcmp(awg_params[awg_unit].type, pda816)==0)
    {
#ifndef DUMMY

#ifdef ALPHI_DA816
      printf("Do DA816 LinLoad...\n");
      da816_LinLoad(da816, Vstart, Vend, nsteps, &step_start, AWG_buffer_num, AWG_channel);
#else
      cm_msg(MERROR,"AwgMemLoad","DA816 driver has not been built in this version");
      return FE_ERR_HW;
#endif 
#else // DUMMY
      printf("AwgMemLoad:   DUMMY version... softdac no action (awg unit %d)\n",awg_unit);
#endif // DUMMY
    }

  else
    {
      cm_msg(MERROR,"AwgMemLoad","Unknown AWG device (%s)!", 
	     awg_params[awg_unit].type);
      return FE_ERR_HW;
    }
  return SUCCESS;
}


INT AwgSetSample( INT awg_unit, int nsamples, int offset, int ibuf, int maxchan, int flag, int bank)
{
  INT ichan;
#ifndef DUMMY

  printf("AwgSetSample: awg_unit %d, nsamples %d, bank %d\n", awg_unit, nsamples, bank);
  
  if(strcmp(awg_params[awg_unit].type, psoftdac)==0) 
    {
#ifdef  ALPHI_SOFTDAC
      printf("AwgSetSample: calling softdac_DacVoltRead\n");
	for (ichan=0; ichan < maxchan ;ichan++)  
	    softdac_DacVoltRead(softdac, nsamples, offset, ibuf, ichan, flag); 
	softdac_SampleSet(softdac, bank, nsamples);
	//	softdac_SMEnable(softdac); // don't enable here
	softdac_Status(softdac, 1);
#else
      cm_msg(MERROR,"AwgSetSample","SOFTDAC driver has not been built in this version");
      return FE_ERR_HW;
#endif
    }
  else if (strcmp(awg_params[awg_unit].type, pda816)==0)
    {
#ifdef ALPHI_DA816
      printf("AwgSetSample: calling da816_DacVoltRead\n");
      //   for (ichan=0; ichan < maxchan ;ichan++)  
      //da816_DacVoltRead(da816, nsamples, offset, ibuf, ichan, flag); 
      da816_SampleSet(da816, bank, nsamples);
      // da816_BankSwitch(da816, 1);
      //	da816_SMEnable(softdac); // don't enable here
       da816_Status(da816, 1);
       //da816_Status(da816);
#else
      cm_msg(MERROR,"AwgSetSample","DA816 driver has not been built in this version");
      return FE_ERR_HW;
#endif 
    }
  else
    {
      cm_msg(MERROR,"AwgSetSample","Unknown AWG device (%s)!", 
	     awg_params[awg_unit].type);
      return FE_ERR_HW;      
    }
#else // DUMMY

#ifdef  ALPHI_SOFTDAC
  printf("AwgSetSample:   DUMMY version... no action  Softdac (awg unit %d)\n",awg_unit);
#else 
#ifdef  ALPHI_DA816
  printf("AwgSetSample:   DUMMY version... no action  da816 (awg unit %d)\n",awg_unit);
#endif // ALPHI_DA816
#endif // AL_HI_SOFTDAC
#endif // DUMMY
  return SUCCESS;

}  



