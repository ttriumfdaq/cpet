#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(void)
{
  long unsigned int max_count=0xFFFFFFFF;
  double freq=100000000; // 100MHz
  printf("max count is 0x%x (%u)\n",max_count,max_count);

  double  max_time;
  double time_slice = 1/freq;
  max_time = time_slice * (float) max_count;
  printf("Time slice = %g sec and max time =%g sec\n",time_slice,max_time);
}
